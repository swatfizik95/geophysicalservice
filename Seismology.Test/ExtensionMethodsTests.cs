﻿using NUnit.Framework;
using Seismology.Core;
using System;

namespace Seismology.Test
{
    [TestFixture]
    public class ExtensionMethodsTests
    {
        [Test]
        public void ChangeYear()
        {
            // arrange
            var actual = DateTime.Today;
            // act
            var expected = new DateTime(1995, actual.Month, actual.Day);
            actual = actual.ChangeYears(1995);
            // assert
            Assert.AreEqual(actual, expected);
        }

        [Test]
        public void ChangeMonth()
        {
            // arrange
            var actual = DateTime.Today;
            // act
            var expected = new DateTime(actual.Year, 7, actual.Day);
            actual = actual.ChangeMonths(7);
            // assert
            Assert.AreEqual(actual, expected);
        }

        [Test]
        public void ChangeDay()
        {
            // arrange
            var actual = DateTime.Today;
            // act
            var expected = new DateTime(actual.Year, actual.Month, 3);
            actual = actual.ChangeDays(3);
            // assert
            Assert.AreEqual(actual, expected);
        }
    }
}
