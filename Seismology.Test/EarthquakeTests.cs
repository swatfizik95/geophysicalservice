﻿using NUnit.Framework;
using Seismology.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Seismology.Test
{
    [TestFixture]
    public class EarthquakeTests
    {
        readonly FakeEarthquake _fake = new FakeEarthquake();

        [SetUp]
        public void Init()
        {
            string fileName = "stations.json";
            string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\{fileName}";

            Configuration.UseDefaultParams(filePath);
        }

        [Test]
        public void Earthquake_Sort()
        {
            var actual = new List<Earthquake>()
            {
                new Earthquake
                {
                    Date = new DateTime(2017, 7, 12),
                    T0 = new DateTime(2017, 7, 12, 23, 42, 12)
                },
                new Earthquake
                {
                    Date = new DateTime(2016, 7, 12),
                    T0 = new DateTime(2016, 7, 12, 23, 42, 11)
                },
                new Earthquake
                {
                    Date = new DateTime(2016, 7, 12),
                    T0 = new DateTime(2016, 7, 12, 23, 42, 12)
                },
            };

            actual.Sort();

            var expected = new List<Earthquake>
            {
                new Earthquake
                {
                    Date = new DateTime(2016, 7, 12),
                    T0 = new DateTime(2016, 7, 12, 23, 42, 11)
                },
                new Earthquake
                {
                    Date = new DateTime(2016, 7, 12),
                    T0 = new DateTime(2016, 7, 12, 23, 42, 12)
                },
                new Earthquake
                {
                    Date = new DateTime(2017, 7, 12),
                    T0 = new DateTime(2017, 7, 12, 23, 42, 12)
                },
            };

            CollectionAssert.AreEqual(actual, expected);
        }

        [Test]
        public void Earthquake_Equality()
        {
            var earthquake1 = _fake.CreateFakeEarthquake(true);
            var earthquake2 = earthquake1;

            var earthquake3 = new Earthquake()
            {
                Date = earthquake1.Date,
                Lat = earthquake1.Lat,
                Lng = earthquake1.Lng,
                Dt = earthquake1.Dt,
                H = earthquake1.H,
                K = earthquake1.K,
                Number = earthquake1.Number,
                T0 = earthquake1.T0,
                VpVs = earthquake1.VpVs,
                Stations = earthquake1.Stations.ToList()
            };

            Assert.AreEqual(earthquake1, earthquake2);
            Assert.AreSame(earthquake1, earthquake2);
            Assert.AreEqual(earthquake1, earthquake3);
            Assert.AreNotSame(earthquake1, earthquake3);
        }
    }
}
