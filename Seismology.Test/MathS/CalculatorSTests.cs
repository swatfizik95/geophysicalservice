﻿using NUnit.Framework;
using Seismology.Core;
using Seismology.Core.MathS;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Test.MathS
{
    [TestFixture]
    public class CalculatorSTests
    {
        //[Test]
        public void FindVpVs_T0()
        {
            // arrange
            var calc = new CalculatorS();
            var earthquake = new Earthquake();
            var date = DateTime.Today.AddHours(5).AddMinutes(53);
            earthquake.Stations = new List<EarthquakeStation>()
            {
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(15.4),
                    Ts = date.AddSeconds(19.4)
                },
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(14.5),
                    Ts = date.AddSeconds(18.5)
                },
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(16),
                    Ts = date.AddSeconds(20.5)
                },
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(18.7),
                    Ts = date.AddSeconds(24.7)
                },
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(19),
                    Ts = date.AddSeconds(25.2)
                },
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(19.8),
                    Ts = date.AddSeconds(26)
                },
                new EarthquakeStation
                {
                    Tp = date.AddSeconds(21.2),
                    Ts = date.AddSeconds(28)
                },
            };
            // act
            calc.FindVpVsAndT0(ref earthquake);
            var t0Excpected = date.AddSeconds(6.3);
            // assert
            Assert.AreEqual(t0Excpected, earthquake.T0);
        }

        [Test]
        public void SpeedTest()
        {
            // arrange
            int count = 500000;
            var numbers = new double[count];
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                numbers[i] = -2 + rnd.NextDouble() * 34;
            }
            // act
            var excepted1 = numbers.Select(CalculatorS.CalculateClass).ToArray();
            var excepted2 = numbers.Select(CalculatorS.CalculateClassForTest).ToArray();
            //assert
            CollectionAssert.AreEqual(excepted1, excepted2);
        }
    }
}
