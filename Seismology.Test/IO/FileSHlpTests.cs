﻿using NUnit.Framework;
using Seismology.Core.IO;
using System;

namespace Seismology.Test.IO
{
    [TestFixture]
    public class FileSHlpTests
    {
        [Test]
        public void GetPeriod_SameDate()
        {
            // arrange
            var firstDate = DateTime.Now;
            var endDate = firstDate;
            string expected = $"{firstDate:dd MMM yyyy}";

            // act
            var actual = FileSHlp.GetPeriod(firstDate, endDate);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPeriod_SameDay()
        {
            // arrange
            var now = DateTime.Now;
            var firstDate = now;
            var endDate = now;
            if (now.Month == 1) endDate = endDate.AddMonths(1);
            else firstDate = firstDate.AddMonths(-1);
            firstDate = firstDate.AddYears(-1);

            string expected = $"{firstDate:dd MMM yyyy} - {endDate:dd MMM yyyy}";

            // act
            var actual = FileSHlp.GetPeriod(firstDate, endDate);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPeriod_SameMonth()
        {
            // arrange
            var now = DateTime.Now;
            var firstDate = now;
            var endDate = now;
            if (now.Day == 1) endDate = endDate.AddDays(1);
            else firstDate = firstDate.AddDays(-1);
            firstDate = firstDate.AddYears(-1);

            string expected = $"{firstDate:dd MMM yyyy} - {endDate:dd MMM yyyy}";

            // act
            var actual = FileSHlp.GetPeriod(firstDate, endDate);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPeriod_DiffDay()
        {
            // arrange
            var now = DateTime.Now;
            var firstDate = now;
            var endDate = now;
            if (now.Day == 1) endDate = endDate.AddDays(1);
            else firstDate = firstDate.AddDays(-1);
            string expected = $"{firstDate:dd} - {endDate:dd MMM yyyy}";

            // act
            var actual = FileSHlp.GetPeriod(firstDate, endDate);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPeriod_DiffDayAndMonth()
        {
            // arrange
            var now = DateTime.Now;
            var firstDate = now;
            var endDate = now;
            if (now.Month == 1) endDate = endDate.AddMonths(1);
            else firstDate = firstDate.AddMonths(-1);
            if (now.Day == 1) endDate = endDate.AddDays(1);
            else firstDate = firstDate.AddDays(-1);

            string expected = $"{firstDate:dd MMM} - {endDate:dd MMM yyyy}";

            // act
            var actual = FileSHlp.GetPeriod(firstDate, endDate);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPeriod_DiffDate()
        {
            // arrange
            var rnd = new Random();
            var firstDate = DateTime.Now;
            var endDate = firstDate.AddDays(-rnd.Next(370, 2000));
            string expected = $"{firstDate:dd MMM yyyy} - {endDate:dd MMM yyyy}";

            // act
            var actual = FileSHlp.GetPeriod(firstDate, endDate);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}