﻿using Moq;
using NUnit.Framework;
using Seismology.Core;
using Seismology.Core.IO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Test.IO
{
    [TestFixture]
    class EarthquakeFileTests_WithStubData
    {
        FileS _ef;
        List<Earthquake> _earthquakes;
        List<Earthquake> _expected;

        [SetUp]
        public void Init()
        {
            _earthquakes = new List<Earthquake>
            {
                new Earthquake(0, new DateTime(2017, 7, 1), new DateTime(2017, 7, 1, 2, 35, 10, 390),
                6.871, 16.5, 41.498, 47.507, -0.024, 1.729),
                new Earthquake(1, new DateTime(2017, 1, 1), new DateTime(2017, 1, 1, 14, 25, 29, 640),
                7.544, 17.500, 41.403, 45.697, -0.171, 1.844),
                new Earthquake(2, new DateTime(2017, 1, 3), new DateTime(2017, 1, 3, 7, 1, 12, 110),
                6.668, 14, 42.357, 45.731, 0.079, 1.75),
                new Earthquake(3, new DateTime(2017, 1, 6), new DateTime(2017, 1, 6, 5, 1, 0, 200),
                7.451, 28.5, 43.204, 45.064, -0.321, 1.918),
                new Earthquake(3, new DateTime(2017, 1, 6), new DateTime(2017, 1, 6, 5, 1, 0, 200),
                    7.451, 28.5, 43.204, 45.064, -10.321, 11.918)
            };
            var now = DateTime.Now;
            _expected = new List<Earthquake>
            {
                new Earthquake(0, new DateTime(2017, 7, 1), new DateTime(2017, 7, 1, 2, 35, 10, 390),
                6.871, 16.5, 41.498, 47.507, -0.024, 1.729),
                new Earthquake(1, new DateTime(2017, 1, 1), new DateTime(2017, 1, 1, 14, 25, 29, 640),
                7.544, 17.500, 41.403, 45.697, -0.171, 1.844),
                new Earthquake(2, new DateTime(2017, 1, 3), new DateTime(2017, 1, 3, 7, 1, 12, 110),
                6.668, 14, 42.357, 45.731, 0.079, 1.75),
                new Earthquake(3, new DateTime(2017, 1, 6), new DateTime(2017, 1, 6, 5, 1, 0, 200),
                7.451, 28.5, 43.204, 45.064, -0.321, 1.918),
                new Earthquake(3, new DateTime(2017, 1, 6), new DateTime(2017, 1, 6, 5, 1, 0, 200),
                    7.451, 28.5, 43.204, 45.064, -10.321, 11.918)
            };
        }

        // Замена файла на stub объект
        [Test]
        public void ReadCatalogTxtTestWithMock()
        {
            List<string> storage = new List<string>
            {
                @"                              01.07.2017",
                @"0:  2-35-10.39; k= 6.871; h=16.500; f=41.498; l=47.507; dt= -0.024; vp/vs= 1.729",
                @"                              01.01.2017",
                @"0: 14-25-29.64; k= 7.544; h=17.500; f=41.403; l=45.697; dt= -0.171; vp/vs= 1.844",
                @"                              03.01.2017",
                @"0:  7- 1-12.11; k= 6.668; h=14.000; f=42.357; l=45.731; dt=  0.079; vp/vs= 1.750",
                @"                              06.01.2017",
                @"0:  5- 1- 0.20; k= 7.451; h=28.500; f=43.204; l=45.064; dt= -0.321; vp/vs= 1.918",
                @"                              06.01.2017",
                @"0:  5- 1- 0.20; k= 7.451; h=28.500; f=43.204; l=45.064; dt=-10.321; vp/vs=11.918"
            };

            var mockReader = new Mock<IStorageReader>();
            //mockReader.Setup(r => r.Open("someText"));
            mockReader.Setup(r => r.ReadLine())
                      .Returns(() =>
                      {
                          if (storage.Count == 0) return null;
                          string result = storage[0];
                          storage.RemoveAt(0);
                          return result;
                      });
            //mockReader.Setup(r => r.Close());

            _ef = new FileS(mockReader.Object, null);

            List<Earthquake> actual = _ef.ReadCatalogTxt("someTxt").ToList();

            CollectionAssert.AreEqual(_expected, actual);
        }

        // Замена файла на stub объект
        [Test]
        public void WriteCatalogTxtTestWithMock()
        {
            // arrange
            List<string> expectedStorage = new List<string>()
            {
                @"                              01.07.2017",
                @"0:  2-35-10.39; k= 6.871; h=16.500; f=41.498; l=47.507; dt= -0.024; vp/vs= 1.729",
                @"                              01.01.2017",
                @"0: 14-25-29.64; k= 7.544; h=17.500; f=41.403; l=45.697; dt= -0.171; vp/vs= 1.844",
                @"                              03.01.2017",
                @"0:  7- 1-12.11; k= 6.668; h=14.000; f=42.357; l=45.731; dt=  0.079; vp/vs= 1.750",
                @"                              06.01.2017",
                @"0:  5- 1- 0.20; k= 7.451; h=28.500; f=43.204; l=45.064; dt= -0.321; vp/vs= 1.918",
                @"                              06.01.2017",
                @"0:  5- 1- 0.20; k= 7.451; h=28.500; f=43.204; l=45.064; dt=-10.321; vp/vs=11.918"
            };
            List<string> actualStorage = new List<string>();
            // act
            var mockWriter = new Mock<IStorageWriter>();
            mockWriter.Setup(w => w.Write("someText", It.IsAny<string>()))
                      .Callback<string, string>((s, str) =>
                      {
                          actualStorage.AddRange(str.Split(new[] { Environment.NewLine }, StringSplitOptions.None));
                          actualStorage.RemoveAt(actualStorage.Count - 1);
                      });

            _ef = new FileS(null, mockWriter.Object);
            _ef.WriteCatalogTxt("someText", _earthquakes);
            // assert
            CollectionAssert.AreEqual(expectedStorage, actualStorage);
        }
    }
}
