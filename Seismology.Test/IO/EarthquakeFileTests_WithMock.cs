﻿using Moq;
using NUnit.Framework;
using Seismology.Core;
using Seismology.Core.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Seismology.Test.IO
{
    [TestFixture]
    public class EarthquakeFileTests_WithMock
    {
        FileS _ef;
        Mock<IStorageWriter> _mockWriter;
        Mock<IStorageReader> _mockReader;
        readonly FakeEarthquake _fake = new FakeEarthquake();
        List<string> _storage;

        [SetUp]
        public void Init()
         {
            string fileName = "stations.json";
            string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\{fileName}";

            Configuration.UseDefaultParams(filePath);
            
            _mockWriter = new Mock<IStorageWriter>();
            _mockWriter.Setup(w => w.Write(It.IsAny<string>(), It.IsAny<string>()))
                      .Callback<string, string>((s, str) =>
                      {
                          _storage.AddRange(str.Split(new[] { Environment.NewLine }, StringSplitOptions.None));
                          //storage.RemoveAt(storage.Count - 1);
                      });

            _mockReader = new Mock<IStorageReader>();
            _mockReader.Setup(r => r.Open(It.IsAny<string>()))
                      .Callback<string>(s => { });
            _mockReader.Setup(r => r.ReadLine())
                      .Returns(() =>
                      {
                          if (_storage.Count == 0) return null;
                          string result = _storage[0];
                          _storage.RemoveAt(0);
                          return result;
                      });
            _mockReader.Setup(r => r.Close());
            _ef = new FileS(_mockReader.Object, _mockWriter.Object);
        }
        
        // Полностью на moq
        [Test]
        public void Read_And_Write_CatalogTxt()
        {
            Earthquake[] fakeEarthquakes = _fake.CreateFakeEarthquakes(6).ToArray();

            _storage = new List<string>();

            _ef.WriteCatalogTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = _ef.ReadCatalogTxt("somePath").ToArray();

            CollectionAssert.AreEqual(fakeEarthquakes, actualEarthquakes);
        }

        [Test]
        public void Read_And_Write_BulletinTxt_1000()
        {
            Earthquake[] fakeEarthquakes = _fake.CreateFakeEarthquakes(1000, true).ToArray();

            _storage = new List<string>();
            Stopwatch sp = new Stopwatch();
            sp.Start();
            _ef.WriteBulletinTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = _ef.ReadBulletinTxt("somePath").ToArray();
            var count = actualEarthquakes.SelectMany(s => s.Stations).Count();
            sp.Stop();
            Assert.Less(sp.ElapsedMilliseconds, 3000);
        }

        [Test]
        public void Read_And_Write_BulletinTxt_With_Special_Times()
        {
            Earthquake[] fakeEarthquakes =
            {
                new Earthquake(0, new DateTime(2017, 7, 13), new DateTime(2017, 7, 13, 23, 59, 54, 360),
                    3.837, 10.5, 43.198, 46.744, 0.282, 1.597,
                    new List<EarthquakeStation>
                    {
                        new EarthquakeStation("DBC", new DateTime(2017, 7, 13, 23, 59, 56, 340),
                            new DateTime(2017, 7, 13, 23, 59, 59, 710),
                            new DateTime(2017, 7, 14, 0, 0, 09, 800), 21.262, 0.44, 3.92, 1.594,
                            0.007, 0.349, 0.002, 0.6)
                    })
            };


            _storage = new List<string>();
            _ef.WriteBulletinTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = _ef.ReadBulletinTxt("somePath").ToArray();
            CollectionAssert.AreEqual(fakeEarthquakes, actualEarthquakes);
        }

        [Test]
        public void Read_And_Write_BulletinTxt()
        {
            Earthquake[] fakeEarthquakes = _fake.CreateFakeEarthquakes(1, true).ToArray();

            _storage = new List<string>();

            _ef.WriteBulletinTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = _ef.ReadBulletinTxt("somePath").ToArray();
            CollectionAssert.AreEqual(fakeEarthquakes, actualEarthquakes);
        }

        //[Test]
        //public void ReadCsv_With_IncorrectTitle()
        //{
        //    storage = new List<string>() {
        //        "МИсяц;День;Коорд;Коорд",
        //        "1;10;42,59;45,27"
        //    };
        //    //Earthquake[] actualEarthquakes = ef.ReadCsv("s").ToArray();
        //    Assert.Throws(Is.TypeOf<FormatException>(),
        //                () => ef.ReadCsv("s").ToArray());
        //}

        //[Test]
        //public void Read_And_Write_Csv()
        //{
        //    Earthquake[] fakeEarthquakes = fake.CreateFakeEarthquakes(6).ToArray();

        //    storage = new List<string>();

        //    ef.WriteCsv("somePath", fakeEarthquakes);
        //    Earthquake[] actualEarthquakes = ef.ReadCsv("somePath").ToArray();

        //    Assert.AreEqual(fakeEarthquakes.Length, actualEarthquakes.Length);
        //    for (int i = 0; i < fakeEarthquakes.Length; i++)
        //    {
        //        Assert.AreEqual(fakeEarthquakes[i].Date,
        //                      actualEarthquakes[i].Date);
        //        Assert.AreEqual(fakeEarthquakes[i].K,
        //                      actualEarthquakes[i].K);
        //        Assert.AreEqual(fakeEarthquakes[i].H,
        //                      actualEarthquakes[i].H);
        //        Assert.AreEqual(fakeEarthquakes[i].Lat,
        //                      actualEarthquakes[i].Lat);
        //        Assert.AreEqual(fakeEarthquakes[i].Lng,
        //                      actualEarthquakes[i].Lng);

        //    }
        //    ef.ToString();
        //}
    }
}
