﻿using NUnit.Framework;
using Seismology.Core;
using Seismology.Core.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Seismology.Test.IO
{
    [TestFixture]
    class EarthquakeFileTests_WithRealData
    {
        string _testPath;
        FileS _ef;
        List<Earthquake> _earthquakes;
        List<Earthquake> _earthquakesWithStation;

        [SetUp]
        public void Init()
        {
            _testPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                ?.Replace(@"\bin\Debug", string.Empty)
                .Replace(@"\bin\Release", string.Empty);
            DateTime now = DateTime.Now;
            _earthquakes = new List<Earthquake>()
            {
                new Earthquake(0, new DateTime(2017, 7, 1), new DateTime(2017, 7, 1, 2, 35, 10, 390),
                6.871, 16.5, 41.498, 47.507, -0.024, 1.729),
                new Earthquake(1, new DateTime(2017, 1, 1), new DateTime(2017, 1, 1, 14, 25, 29, 640),
                7.544, 17.500, 41.403, 45.697, -0.171, 1.844),
                new Earthquake(2, new DateTime(2017, 1, 3), new DateTime(2017, 1, 3, 7, 1, 12, 110),
                6.668, 14, 42.357, 45.731, 0.079, 1.75),
                new Earthquake(3, new DateTime(2017, 1, 6), new DateTime(2017, 1, 6, 5, 1, 0, 200),
                7.451, 28.5, 43.204, 45.064, -0.321, 1.918)
            };
            _earthquakesWithStation = new List<Earthquake>()
            {
                new Earthquake(0, new DateTime(2017, 7, 4), new DateTime(2017, 7, 4, 11, 16, 33, 800),
                7.143, 16, 42.135, 46.294, 0.2, 1.805,
                new List<EarthquakeStation>()
                {
                    new EarthquakeStation("BTLR", new DateTime(2017, 7, 4, 11, 16, 43, 270),
                    new DateTime(2017, 7, 4, 11, 16, 49, 940),
                    new DateTime(2017, 7, 4, 11, 16, 34, 100), 58.896, 0.306, 6.679, 1.704,
                    0.119, 0.02, 0.439, 0.029),

                    new EarthquakeStation("XNZR", new DateTime(2017, 7, 4, 11, 16, 43, 130),
                    new DateTime(2017, 7, 4, 11, 16, 50, 500),
                    new DateTime(2017, 7, 4, 11, 16, 33, 810), 56.098, 0.014, 6.220, 1.789,
                    0.2, 0.014, 0.05, 0.004),

                    new EarthquakeStation("GNBR", new DateTime(2017, 7, 4, 11, 16, 43, 560),
                    new DateTime(2017, 7, 4, 11, 16, 51, 770),
                    new DateTime(2017, 7, 4, 11, 16, 34, 150), 61.026, 0.356, 7.930, 1.841,
                    0.1, 0.02, 0.2, 0.197)
                }),

                new Earthquake(0, new DateTime(2017, 7, 13), new DateTime(2017, 7, 13, 7, 20, 39, 360),
                3.837, 10.5, 43.198, 46.744, 0.282, 1.597,
                new List<EarthquakeStation>(){
                    new EarthquakeStation("DBC", new DateTime(2017, 7, 13, 7, 20, 43, 340),
                    new DateTime(2017, 7, 13, 7, 20, 45, 710),
                    new DateTime(2017, 7, 13, 7, 20, 39, 800), 21.262, 0.44, 3.92, 1.594,
                    0.349, 0.007, 0.6, 0.002),

                    new EarthquakeStation("KRNR", new DateTime(2017, 7, 13, 7, 20, 47, 410),
                    new DateTime(2017, 7, 13, 7, 20, 52, 250),
                    new DateTime(2017, 7, 13, 7, 20, 39, 590), 43.18, 0.237, 5.145, 1.602,
                    0.6, 0.002, 0.28, 0.01)
                })
            };
        }

        // Мой стандартный тест с файлами
        [Test]
        public void ReadCatalogTxtTest()
        {
            // arrange
            string txtCatalogPath = _testPath + @"\Documents\Каталог (Малое количество данных).txt";
            _ef = new FileS();

            List<Earthquake> actual;
            // act
            actual = _ef.ReadCatalogTxt(txtCatalogPath).ToList();
            // assert
            CollectionAssert.AreEqual(_earthquakes, actual);
        }

        // Мой стандартный тест с файлами
        [Test]
        public void WriteCatalogTxtTest()
        {
            // arrange
            string txtCatalogPath = _testPath + @"\Documents\Каталог (Малое количество данных).txt";
            string txtWriteCatalogPath = _testPath + @"\Documents\ЗаписанныйКаталог (Малое количество данных).txt";

            List<string> actual = new List<string>();
            List<string> expected = new List<string>();

            // act
            _ef = new FileS();
            _ef.WriteCatalogTxt(txtWriteCatalogPath, _earthquakes);
            string line;
            using (StreamReader sr = new StreamReader(txtWriteCatalogPath))
            {
                while ((line = sr.ReadLine()) != null)
                    actual.Add(line);
            }
            using (StreamReader sr = new StreamReader(txtCatalogPath))
            {
                while ((line = sr.ReadLine()) != null)
                    expected.Add(line);
            }

            // assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void ReadBulletinTxtTest()
        {
            // arrange
            string txtBulletinPath = _testPath + @"\Documents\Бюллетень (Малое количество данных).txt";
            _ef = new FileS();

            List<Earthquake> actual;
            // act
            actual = _ef.ReadBulletinTxt(txtBulletinPath).ToList();
            // assert
            CollectionAssert.AreEqual(_earthquakesWithStation, actual);
        }

        [Test]
        public void WriteBulletinTxtTest()
        {
            // arrange
            string txtBulletinPath = _testPath + @"\Documents\Бюллетень (Малое количество данных).txt";
            string txtWriteBulletinPath = _testPath + @"\Documents\ЗаписанныйБюллетень (Малое количество данных).txt";

            List<string> actual = new List<string>();
            List<string> expected = new List<string>();

            // act
            _ef = new FileS();
            _ef.WriteBulletinTxt(txtWriteBulletinPath, _earthquakesWithStation);
            string line;
            using (StreamReader sr = new StreamReader(txtWriteBulletinPath))
            {
                while ((line = sr.ReadLine()) != null)
                    actual.Add(line);
            }
            using (StreamReader sr = new StreamReader(txtBulletinPath))
            {
                while ((line = sr.ReadLine()) != null)
                    expected.Add(line);
            }

            // assert
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
