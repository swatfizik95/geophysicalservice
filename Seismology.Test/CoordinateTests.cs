﻿using NUnit.Framework;
using Seismology.Core.Map;
using System;

namespace Seismology.Test
{
    [TestFixture]
    public class CoordinateTests
    {
        private Random _rnd;
        [SetUp]
        public void Init()
        {
            _rnd = new Random();
        }

        [Test]
        public void Coordinate_Equality()
        {
            var coord1 = new Coordinate(_rnd.NextDouble(), _rnd.NextDouble());
            var coord2 = coord1;
            var coord3 = new Coordinate(coord1.Lat, coord1.Lng);

            Assert.AreEqual(coord1, coord2);
            Assert.AreSame(coord1, coord2);
            Assert.AreEqual(coord1, coord3);
            Assert.AreNotSame(coord1, coord3);
        }

        //[Test]
        //public void Coordiante_distanceTo()
        //{
        //    var coord1 = new Coordinate(64.28, 100.22);
        //    var coord2 = new Coordinate(40.71, 74.01);
        //    double dist = coord1.DistanceTo(coord2);

        //    Assert.Fail("s");
        //}
    }
}
