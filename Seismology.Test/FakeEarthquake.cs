﻿using Seismology.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Test
{
    /// <summary>
    /// Класс <see cref="FakeEarthquake"/> повзоляет получить
    /// рандомные (не настоящие) данные об землетрясениях
    /// </summary>
    class FakeEarthquake
    {
        // Если поместить его внутри метода, то числа
        // будут повторяться, так как каждый раз создается
        // экземпляр
        readonly Random _rnd = new Random();
        internal IEnumerable<Earthquake> CreateFakeEarthquakes(int count = 1, bool withStations = false)
        {
            return Enumerable.Range(0, count).Select(e =>
            {
                var date = GiveRandomDate();
                var time = GiveRandomTime(date);
                var eqs = new List<EarthquakeStation>();
                if (withStations)
                {
                    int eqsCount = _rnd.Next(1, 6);
                    var t0 = GiveRandomTime(time);
                    var tp = GiveRandomTime(t0);
                    var ts = GiveRandomTime(tp);
                    for (int i = 0; i < eqsCount; i++)
                    {
                        eqs.Add(new EarthquakeStation
                        {
                            Name = GiveRandomText(_rnd.Next(3, 4)),
                            Ep = "ep",
                            Tp = tp,
                            PeriodZ = GiveRandomNumber(0, 0),
                            AmplitudeZ = GiveRandomNumber(0, 0),

                            Es = "es",
                            Ts = ts,
                            T0 = t0,
                            EpicDistance = GiveRandomNumber(5, 150),
                            Dt = GiveRandomNumber(-1, 1),

                            Period = GiveRandomNumber(0, 0),
                            Amplitude = GiveRandomNumber(0, 0),
                            K = GiveRandomNumber(2, 12),
                            VpVs = GiveRandomNumber(1, 1)
                        });
                    }
                }


                return new Earthquake
                {
                    Date = date,
                    T0 = time,
                    K = GiveRandomNumber(2, 12),
                    H = GiveRandomNumber(5, 30),
                    Lat = GiveRandomNumber(41, 45),
                    Lng = GiveRandomNumber(45, 49),
                    Dt = GiveRandomNumber(-1, 1),
                    VpVs = GiveRandomNumber(1, 1),
                    Stations = eqs
                };
            });
        }

        internal Earthquake CreateFakeEarthquake(bool withStations = false)
        {
            var date = GiveRandomDate();
            var time = GiveRandomTime(date);
            var eqs = new List<EarthquakeStation>();
            if (withStations)
            {
                int eqsCount = _rnd.Next(1, 6);
                var t0 = GiveRandomTime(time);
                var tp = GiveRandomTime(t0);
                var ts = GiveRandomTime(tp);
                for (int i = 0; i < eqsCount; i++)
                {
                    eqs.Add(new EarthquakeStation
                    {
                        Name = GiveRandomText(_rnd.Next(3, 4)),
                        Ep = "ep",
                        Tp = tp,
                        PeriodZ = GiveRandomNumber(0, 0),
                        AmplitudeZ = GiveRandomNumber(0, 0),

                        Es = "es",
                        Ts = ts,
                        T0 = t0,
                        EpicDistance = GiveRandomNumber(5, 150),
                        Dt = GiveRandomNumber(-1, 1),

                        Period = GiveRandomNumber(0, 0),
                        Amplitude = GiveRandomNumber(0, 0),
                        K = GiveRandomNumber(2, 12),
                        VpVs = GiveRandomNumber(1, 1)
                    });
                }
            }


            return new Earthquake
            {
                Date = date,
                T0 = time,
                K = GiveRandomNumber(2, 12),
                H = GiveRandomNumber(5, 30),
                Lat = GiveRandomNumber(41, 45),
                Lng = GiveRandomNumber(45, 49),
                Dt = GiveRandomNumber(-1, 1),
                VpVs = GiveRandomNumber(1, 1),
                Stations = eqs
            };
        }

        // Иногда math.Round() вместо округления, дает наоборот число
        // что-то типа 0.21349999999999999999999999999999999999 и тест, проваливается
        // Такая проблема иногда возникла и при делении (я помню читал из-за
        // чего это происходит, но так и не смог понять как решить это)
        // upd: Решил просто сделать через формат строки
        double GiveRandomNumber(int start, int end, int length = 3)
        {
            if (length == 3)
                return double.Parse($"{_rnd.Next(start, end) + _rnd.NextDouble():F3}");
            if (length == 2)
                return double.Parse($"{_rnd.Next(start, end) + _rnd.NextDouble():F2}");
            return 0;
        }
        string GiveRandomText(int len)
        {
            return new string(Enumerable
                .Range(0, len)
                .Select(i => (char)_rnd.Next(65, 90))
                .ToArray());
        }
        DateTime GiveRandomTime()
        {
            return new DateTime(
                DateTime.Now.Year,
                DateTime.Now.Month,
                DateTime.Now.Day,
                _rnd.Next(0, 24),
                _rnd.Next(0, 60),
                _rnd.Next(0, 60),
                _rnd.Next(0, 99) * 10
               );
        }
        DateTime GiveRandomTime(DateTime date)
        {
            return date
                .AddSeconds(Math.Round(_rnd.NextDouble() / 3.0 * (date.AddDays(1) - date).TotalSeconds, 2));
        }
        DateTime GiveRandomDate()
        {
            return new DateTime(
                _rnd.Next(2000, 2018),
                _rnd.Next(1, 12),
                _rnd.Next(1, 28)
                );
        }
    }
}
