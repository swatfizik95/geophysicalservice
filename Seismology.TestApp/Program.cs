﻿using System;
using Seismology.Core.IO;
using Seismology.Core.Map;
using Seismology.TestApp.Helpers;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Seismology.Core;
using Seismology.Core.Dto;
using Seismology.Core.Mapping;

namespace Seismology.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = StationHlp.StationsDagestanRusHydro;

            string path = @"S:\MyProjects\stationsDagestanRusHydro.json";
            //string path = @"S:\MyProjects\stationsRusHydro.json";

            var stations = data.Select(s => s.ToStationDto()).ToList();

            //var rnd = new Random();
            //foreach (var station in stations)
            //{
            //    station.Color = Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
            //}

            var settings = new JsonSettings<List<StationDto>>(path);
            settings.Save(stations);
        }
    }
}
