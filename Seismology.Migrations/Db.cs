﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Seismology.Migrations
{
    public class Db
    {
        private const string InsertMigrationQuery = "INSERT INTO Migrations ([Name]) VALUES (?)";

        private readonly OleDbConnectionStringBuilder _sb;

        public Db(string dataSource)
        {
            _sb = new OleDbConnectionStringBuilder
            {
                DataSource = dataSource,
                Provider = "Microsoft.Jet.OLEDB.4.0"
            };
        }

        public bool ExecuteMigration(Migration migration)
        {
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    if (!CreateMigration(connection, transaction, migration)) return false;

                    var cmd = connection.CreateCommand();
                    cmd.Transaction = transaction;
                    cmd.CommandText = migration.Query;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    return true;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private bool CreateMigration(OleDbConnection connection, OleDbTransaction transaction, Migration migration)
        {
            if (IsMatch(connection, transaction, migration.Name)) return false;

            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText = InsertMigrationQuery;
            cmd.Parameters.AddWithValue("@Name", migration.Name);
            cmd.ExecuteNonQuery();
            return true;
        }

        private bool IsMatch(OleDbConnection connection, OleDbTransaction transaction, string name)
        {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText = $"SELECT COUNT(*) FROM Migrations WHERE name = '{name}'";
            var isParsed = int.TryParse(cmd.ExecuteScalar().ToString(), out var count);
            return isParsed && count > 0;
        }
    }
}