﻿using System;

namespace Seismology.Migrations
{
    public class Migration
    {
        private const string AlterEarthquake = "ALTER TABLE Earthquake ADD COLUMN";

        public Migration(string name, string query)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new NullReferenceException(nameof(name));
            if (string.IsNullOrWhiteSpace(query))
                throw new NullReferenceException(nameof(query));
            Name = name;
            Query = query;
        }

        public string Name { get; }
        public string Query { get; }

        public static Migration[] GetMigrations { get; } =
        {
            new Migration(
                "21122019_Alter_Earthquake_DtRms", 
                $"{AlterEarthquake} DtRms DOUBLE DEFAULT 0")
        };
    }
}