﻿using System;

namespace Seismology.Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            string dataSource =
                @"C:\MyProjects\GitLab\geophysicalservice\Seismology.Core\Resources\MainSeismologyDatabase.mdb";

            var db = new Db(dataSource);

            int count = 0;
            foreach (var migration in Migration.GetMigrations)
            {
                if (db.ExecuteMigration(migration)) count++;
            }

            Console.WriteLine(count > 0 ? $"Успешно было выполнено миграций: {count}" : "Нечего обновлять!");
            Console.WriteLine("Нажмите любую кнопку для Выхода.");

            //using (var connection = new OleDbConnection(sb.ConnectionString))
            //{
            //    connection.Open();
            //    var transaction = connection.BeginTransaction();
            //    try
            //    {
            //        var cmd = connection.CreateCommand();
            //        cmd.Transaction = transaction;
            //        foreach (var migration in Migration.GetMigrations)
            //        {
            //            cmd.CommandText = migration.Query;
            //            cmd.ExecuteNonQuery();
            //        }

            //        transaction.Commit();
            //        Console.WriteLine("Миграции были успешно применены!");
            //        Console.WriteLine("Нажмите любую кнопку, чтобы выйти.");
            //    }
            //    catch (Exception ex)
            //    {
            //        transaction.Rollback();
            //        Console.WriteLine("Произошла ошибка.");
            //        Console.WriteLine($"Детали: {ex.Message}");
            //        Console.WriteLine($"{ex.InnerException?.Message}");
            //    }
            //    finally
            //    {
            //        connection.Close();
            //    }
            //}

            Console.ReadKey();
        }
    }
}
