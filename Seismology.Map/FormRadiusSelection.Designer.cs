﻿namespace Seismology.Map
{
    partial class FormRadiusSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.numericUpDownStartClass = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownEndClass = new System.Windows.Forms.NumericUpDown();
            this.labelStartClass = new System.Windows.Forms.Label();
            this.labelEndClass = new System.Windows.Forms.Label();
            this.numericUpDownRadiusStart = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRadiusMult = new System.Windows.Forms.NumericUpDown();
            this.labelStartRadius = new System.Windows.Forms.Label();
            this.labelRadiusMult = new System.Windows.Forms.Label();
            this.buttonApply = new System.Windows.Forms.Button();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelClassRadiusColor = new System.Windows.Forms.TableLayoutPanel();
            this.labelClass = new System.Windows.Forms.Label();
            this.labelRadius = new System.Windows.Forms.Label();
            this.labelColor = new System.Windows.Forms.Label();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.tableLayoutPanelInterval = new System.Windows.Forms.TableLayoutPanel();
            this.labeIntervals = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEndClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadiusStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadiusMult)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelClassRadiusColor.SuspendLayout();
            this.tableLayoutPanelInterval.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(541, 623);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 25);
            this.buttonCancel.TabIndex = 101;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(453, 623);
            this.buttonOk.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(80, 25);
            this.buttonOk.TabIndex = 100;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // numericUpDownStartClass
            // 
            this.numericUpDownStartClass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownStartClass.Location = new System.Drawing.Point(154, 3);
            this.numericUpDownStartClass.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownStartClass.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStartClass.Name = "numericUpDownStartClass";
            this.numericUpDownStartClass.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownStartClass.TabIndex = 102;
            this.numericUpDownStartClass.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStartClass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownNew_KeyDown);
            // 
            // numericUpDownEndClass
            // 
            this.numericUpDownEndClass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownEndClass.Location = new System.Drawing.Point(154, 32);
            this.numericUpDownEndClass.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownEndClass.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownEndClass.Name = "numericUpDownEndClass";
            this.numericUpDownEndClass.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownEndClass.TabIndex = 103;
            this.numericUpDownEndClass.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownEndClass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownNew_KeyDown);
            // 
            // labelStartClass
            // 
            this.labelStartClass.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelStartClass.AutoSize = true;
            this.labelStartClass.Location = new System.Drawing.Point(3, 6);
            this.labelStartClass.Name = "labelStartClass";
            this.labelStartClass.Size = new System.Drawing.Size(114, 16);
            this.labelStartClass.TabIndex = 104;
            this.labelStartClass.Text = "Начать с класса";
            // 
            // labelEndClass
            // 
            this.labelEndClass.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelEndClass.AutoSize = true;
            this.labelEndClass.Location = new System.Drawing.Point(3, 35);
            this.labelEndClass.Name = "labelEndClass";
            this.labelEndClass.Size = new System.Drawing.Size(145, 16);
            this.labelEndClass.TabIndex = 105;
            this.labelEndClass.Text = "Закончить на классе";
            // 
            // numericUpDownRadiusStart
            // 
            this.numericUpDownRadiusStart.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownRadiusStart.Location = new System.Drawing.Point(154, 61);
            this.numericUpDownRadiusStart.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownRadiusStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRadiusStart.Name = "numericUpDownRadiusStart";
            this.numericUpDownRadiusStart.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownRadiusStart.TabIndex = 106;
            this.numericUpDownRadiusStart.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRadiusStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownNew_KeyDown);
            // 
            // numericUpDownRadiusMult
            // 
            this.numericUpDownRadiusMult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownRadiusMult.Location = new System.Drawing.Point(154, 90);
            this.numericUpDownRadiusMult.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownRadiusMult.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRadiusMult.Name = "numericUpDownRadiusMult";
            this.numericUpDownRadiusMult.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownRadiusMult.TabIndex = 107;
            this.numericUpDownRadiusMult.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRadiusMult.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownNew_KeyDown);
            // 
            // labelStartRadius
            // 
            this.labelStartRadius.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelStartRadius.AutoSize = true;
            this.labelStartRadius.Location = new System.Drawing.Point(3, 64);
            this.labelStartRadius.Name = "labelStartRadius";
            this.labelStartRadius.Size = new System.Drawing.Size(124, 16);
            this.labelStartRadius.TabIndex = 108;
            this.labelStartRadius.Text = "Начать с радиуса";
            // 
            // labelRadiusMult
            // 
            this.labelRadiusMult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRadiusMult.AutoSize = true;
            this.labelRadiusMult.Location = new System.Drawing.Point(3, 93);
            this.labelRadiusMult.Name = "labelRadiusMult";
            this.labelRadiusMult.Size = new System.Drawing.Size(141, 16);
            this.labelRadiusMult.TabIndex = 109;
            this.labelRadiusMult.Text = "Множитель Радиуса";
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonApply.Location = new System.Drawing.Point(3, 119);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(90, 23);
            this.buttonApply.TabIndex = 110;
            this.buttonApply.Text = "Применить";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.ButtonApply_Click);
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.AutoSize = true;
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownStartClass, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelRadiusMult, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.labelStartRadius, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownEndClass, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.labelEndClass, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownRadiusStart, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.labelStartClass, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.numericUpDownRadiusMult, 1, 3);
            this.tableLayoutPanelMain.Controls.Add(this.buttonApply, 0, 4);
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(10, 40);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 5;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(231, 145);
            this.tableLayoutPanelMain.TabIndex = 111;
            // 
            // tableLayoutPanelClassRadiusColor
            // 
            this.tableLayoutPanelClassRadiusColor.AutoSize = true;
            this.tableLayoutPanelClassRadiusColor.ColumnCount = 3;
            this.tableLayoutPanelClassRadiusColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanelClassRadiusColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelClassRadiusColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelClassRadiusColor.Controls.Add(this.labelColor, 2, 0);
            this.tableLayoutPanelClassRadiusColor.Controls.Add(this.labelRadius, 1, 0);
            this.tableLayoutPanelClassRadiusColor.Controls.Add(this.labelClass, 0, 0);
            this.tableLayoutPanelClassRadiusColor.Location = new System.Drawing.Point(300, 10);
            this.tableLayoutPanelClassRadiusColor.Name = "tableLayoutPanelClassRadiusColor";
            this.tableLayoutPanelClassRadiusColor.RowCount = 1;
            this.tableLayoutPanelClassRadiusColor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelClassRadiusColor.Size = new System.Drawing.Size(220, 30);
            this.tableLayoutPanelClassRadiusColor.TabIndex = 112;
            // 
            // labelClass
            // 
            this.labelClass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelClass.AutoSize = true;
            this.labelClass.Location = new System.Drawing.Point(7, 0);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(46, 16);
            this.labelClass.TabIndex = 105;
            this.labelClass.Text = "Класс";
            // 
            // labelRadius
            // 
            this.labelRadius.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelRadius.AutoSize = true;
            this.labelRadius.Location = new System.Drawing.Point(72, 0);
            this.labelRadius.Name = "labelRadius";
            this.labelRadius.Size = new System.Drawing.Size(56, 16);
            this.labelRadius.TabIndex = 106;
            this.labelRadius.Text = "Радиус";
            // 
            // labelColor
            // 
            this.labelColor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelColor.AutoSize = true;
            this.labelColor.Location = new System.Drawing.Point(160, 0);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(40, 16);
            this.labelColor.TabIndex = 107;
            this.labelColor.Text = "Цвет";
            // 
            // colorDialog
            // 
            this.colorDialog.AnyColor = true;
            // 
            // tableLayoutPanelInterval
            // 
            this.tableLayoutPanelInterval.AutoSize = true;
            this.tableLayoutPanelInterval.ColumnCount = 1;
            this.tableLayoutPanelInterval.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelInterval.Controls.Add(this.labeIntervals, 0, 0);
            this.tableLayoutPanelInterval.Location = new System.Drawing.Point(526, 10);
            this.tableLayoutPanelInterval.Name = "tableLayoutPanelInterval";
            this.tableLayoutPanelInterval.RowCount = 1;
            this.tableLayoutPanelInterval.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanelInterval.Size = new System.Drawing.Size(79, 45);
            this.tableLayoutPanelInterval.TabIndex = 113;
            // 
            // labeIntervals
            // 
            this.labeIntervals.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labeIntervals.AutoSize = true;
            this.labeIntervals.Location = new System.Drawing.Point(3, 0);
            this.labeIntervals.Name = "labeIntervals";
            this.labeIntervals.Size = new System.Drawing.Size(73, 16);
            this.labeIntervals.TabIndex = 105;
            this.labeIntervals.Text = "Интервал";
            // 
            // FormRadiusSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 661);
            this.Controls.Add(this.tableLayoutPanelInterval);
            this.Controls.Add(this.tableLayoutPanelClassRadiusColor);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormRadiusSelection";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выбор радиусов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRadiusSelection_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEndClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadiusStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadiusMult)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelClassRadiusColor.ResumeLayout(false);
            this.tableLayoutPanelClassRadiusColor.PerformLayout();
            this.tableLayoutPanelInterval.ResumeLayout(false);
            this.tableLayoutPanelInterval.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.NumericUpDown numericUpDownStartClass;
        private System.Windows.Forms.NumericUpDown numericUpDownEndClass;
        private System.Windows.Forms.Label labelStartClass;
        private System.Windows.Forms.Label labelEndClass;
        private System.Windows.Forms.NumericUpDown numericUpDownRadiusStart;
        private System.Windows.Forms.NumericUpDown numericUpDownRadiusMult;
        private System.Windows.Forms.Label labelStartRadius;
        private System.Windows.Forms.Label labelRadiusMult;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelClassRadiusColor;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.Label labelRadius;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelInterval;
        private System.Windows.Forms.Label labeIntervals;
    }
}