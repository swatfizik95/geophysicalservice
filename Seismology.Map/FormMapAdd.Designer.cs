﻿namespace Seismology.Map
{
    partial class FormMapAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAdd = new System.Windows.Forms.Button();
            this.groupBoxLat = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelLat = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownLatEndDeg = new System.Windows.Forms.NumericUpDown();
            this.labelLatEndDeg = new System.Windows.Forms.Label();
            this.numericUpDownLatStartDeg = new System.Windows.Forms.NumericUpDown();
            this.labelLatStartDeg = new System.Windows.Forms.Label();
            this.numericUpDownLatEndPx = new System.Windows.Forms.NumericUpDown();
            this.labelLatEndPx = new System.Windows.Forms.Label();
            this.labelLatStartPx = new System.Windows.Forms.Label();
            this.numericUpDownLatStartPx = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelRation = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownRatio = new System.Windows.Forms.NumericUpDown();
            this.labelRatio = new System.Windows.Forms.Label();
            this.groupBoxLng = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelLng = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownLngEndDeg = new System.Windows.Forms.NumericUpDown();
            this.labelLngEndDeg = new System.Windows.Forms.Label();
            this.numericUpDownLngStartDeg = new System.Windows.Forms.NumericUpDown();
            this.labelLngStartDeg = new System.Windows.Forms.Label();
            this.numericUpDownLngEndPx = new System.Windows.Forms.NumericUpDown();
            this.labelLngEndPx = new System.Windows.Forms.Label();
            this.labelLngStartPx = new System.Windows.Forms.Label();
            this.numericUpDownLngStartPx = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanelFileName = new System.Windows.Forms.TableLayoutPanel();
            this.labelFileName = new System.Windows.Forms.Label();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelName = new System.Windows.Forms.TableLayoutPanel();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.groupBoxLat.SuspendLayout();
            this.tableLayoutPanelLat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatEndDeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatStartDeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatEndPx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatStartPx)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelRation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRatio)).BeginInit();
            this.groupBoxLng.SuspendLayout();
            this.tableLayoutPanelLng.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngEndDeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngStartDeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngEndPx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngStartPx)).BeginInit();
            this.tableLayoutPanelFileName.SuspendLayout();
            this.tableLayoutPanelName.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonAdd.Location = new System.Drawing.Point(653, 359);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(80, 25);
            this.buttonAdd.TabIndex = 0;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            // 
            // groupBoxLat
            // 
            this.groupBoxLat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLat.AutoSize = true;
            this.groupBoxLat.Controls.Add(this.tableLayoutPanelLat);
            this.groupBoxLat.Location = new System.Drawing.Point(13, 67);
            this.groupBoxLat.Margin = new System.Windows.Forms.Padding(13);
            this.groupBoxLat.Name = "groupBoxLat";
            this.groupBoxLat.Size = new System.Drawing.Size(334, 154);
            this.groupBoxLat.TabIndex = 1;
            this.groupBoxLat.TabStop = false;
            this.groupBoxLat.Text = "Широта";
            // 
            // tableLayoutPanelLat
            // 
            this.tableLayoutPanelLat.AutoSize = true;
            this.tableLayoutPanelLat.ColumnCount = 2;
            this.tableLayoutPanelLat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelLat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelLat.Controls.Add(this.numericUpDownLatEndDeg, 1, 3);
            this.tableLayoutPanelLat.Controls.Add(this.labelLatEndDeg, 0, 3);
            this.tableLayoutPanelLat.Controls.Add(this.numericUpDownLatStartDeg, 1, 2);
            this.tableLayoutPanelLat.Controls.Add(this.labelLatStartDeg, 0, 2);
            this.tableLayoutPanelLat.Controls.Add(this.numericUpDownLatEndPx, 1, 1);
            this.tableLayoutPanelLat.Controls.Add(this.labelLatEndPx, 0, 1);
            this.tableLayoutPanelLat.Controls.Add(this.labelLatStartPx, 0, 0);
            this.tableLayoutPanelLat.Controls.Add(this.numericUpDownLatStartPx, 1, 0);
            this.tableLayoutPanelLat.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanelLat.Name = "tableLayoutPanelLat";
            this.tableLayoutPanelLat.RowCount = 4;
            this.tableLayoutPanelLat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLat.Size = new System.Drawing.Size(291, 112);
            this.tableLayoutPanelLat.TabIndex = 0;
            // 
            // numericUpDownLatEndDeg
            // 
            this.numericUpDownLatEndDeg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLatEndDeg.Location = new System.Drawing.Point(163, 87);
            this.numericUpDownLatEndDeg.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownLatEndDeg.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownLatEndDeg.Name = "numericUpDownLatEndDeg";
            this.numericUpDownLatEndDeg.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLatEndDeg.TabIndex = 7;
            this.numericUpDownLatEndDeg.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            // 
            // labelLatEndDeg
            // 
            this.labelLatEndDeg.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLatEndDeg.AutoSize = true;
            this.labelLatEndDeg.Location = new System.Drawing.Point(3, 90);
            this.labelLatEndDeg.Name = "labelLatEndDeg";
            this.labelLatEndDeg.Size = new System.Drawing.Size(113, 16);
            this.labelLatEndDeg.TabIndex = 6;
            this.labelLatEndDeg.Text = "Конец (градусы)";
            // 
            // numericUpDownLatStartDeg
            // 
            this.numericUpDownLatStartDeg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLatStartDeg.Location = new System.Drawing.Point(163, 59);
            this.numericUpDownLatStartDeg.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownLatStartDeg.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownLatStartDeg.Name = "numericUpDownLatStartDeg";
            this.numericUpDownLatStartDeg.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLatStartDeg.TabIndex = 5;
            this.numericUpDownLatStartDeg.Value = new decimal(new int[] {
            41,
            0,
            0,
            0});
            // 
            // labelLatStartDeg
            // 
            this.labelLatStartDeg.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLatStartDeg.AutoSize = true;
            this.labelLatStartDeg.Location = new System.Drawing.Point(3, 62);
            this.labelLatStartDeg.Name = "labelLatStartDeg";
            this.labelLatStartDeg.Size = new System.Drawing.Size(123, 16);
            this.labelLatStartDeg.TabIndex = 4;
            this.labelLatStartDeg.Text = "Начало (градусы)";
            // 
            // numericUpDownLatEndPx
            // 
            this.numericUpDownLatEndPx.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLatEndPx.Location = new System.Drawing.Point(163, 31);
            this.numericUpDownLatEndPx.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownLatEndPx.Name = "numericUpDownLatEndPx";
            this.numericUpDownLatEndPx.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLatEndPx.TabIndex = 3;
            // 
            // labelLatEndPx
            // 
            this.labelLatEndPx.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLatEndPx.AutoSize = true;
            this.labelLatEndPx.Location = new System.Drawing.Point(3, 34);
            this.labelLatEndPx.Name = "labelLatEndPx";
            this.labelLatEndPx.Size = new System.Drawing.Size(113, 16);
            this.labelLatEndPx.TabIndex = 2;
            this.labelLatEndPx.Text = "Конец (пиксели)";
            // 
            // labelLatStartPx
            // 
            this.labelLatStartPx.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLatStartPx.AutoSize = true;
            this.labelLatStartPx.Location = new System.Drawing.Point(3, 6);
            this.labelLatStartPx.Name = "labelLatStartPx";
            this.labelLatStartPx.Size = new System.Drawing.Size(123, 16);
            this.labelLatStartPx.TabIndex = 0;
            this.labelLatStartPx.Text = "Начало (пиксели)";
            // 
            // numericUpDownLatStartPx
            // 
            this.numericUpDownLatStartPx.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLatStartPx.Location = new System.Drawing.Point(163, 3);
            this.numericUpDownLatStartPx.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownLatStartPx.Name = "numericUpDownLatStartPx";
            this.numericUpDownLatStartPx.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLatStartPx.TabIndex = 1;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelMain.AutoSize = true;
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelRation, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.groupBoxLng, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelFileName, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.groupBoxLat, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelName, 0, 0);
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 3;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(721, 288);
            this.tableLayoutPanelMain.TabIndex = 3;
            // 
            // tableLayoutPanelRation
            // 
            this.tableLayoutPanelRation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelRation.AutoSize = true;
            this.tableLayoutPanelRation.ColumnCount = 2;
            this.tableLayoutPanelRation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelRation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelRation.Controls.Add(this.numericUpDownRatio, 0, 0);
            this.tableLayoutPanelRation.Controls.Add(this.labelRatio, 0, 0);
            this.tableLayoutPanelRation.Location = new System.Drawing.Point(13, 247);
            this.tableLayoutPanelRation.Margin = new System.Windows.Forms.Padding(13);
            this.tableLayoutPanelRation.Name = "tableLayoutPanelRation";
            this.tableLayoutPanelRation.RowCount = 1;
            this.tableLayoutPanelRation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRation.Size = new System.Drawing.Size(334, 28);
            this.tableLayoutPanelRation.TabIndex = 5;
            // 
            // numericUpDownRatio
            // 
            this.numericUpDownRatio.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownRatio.Enabled = false;
            this.numericUpDownRatio.Location = new System.Drawing.Point(239, 3);
            this.numericUpDownRatio.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownRatio.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRatio.Name = "numericUpDownRatio";
            this.numericUpDownRatio.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownRatio.TabIndex = 2;
            this.numericUpDownRatio.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // labelRatio
            // 
            this.labelRatio.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRatio.AutoSize = true;
            this.labelRatio.Location = new System.Drawing.Point(3, 6);
            this.labelRatio.Name = "labelRatio";
            this.labelRatio.Size = new System.Drawing.Size(230, 16);
            this.labelRatio.TabIndex = 1;
            this.labelRatio.Text = "Отношение к реальному размеру:";
            // 
            // groupBoxLng
            // 
            this.groupBoxLng.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLng.AutoSize = true;
            this.groupBoxLng.Controls.Add(this.tableLayoutPanelLng);
            this.groupBoxLng.Location = new System.Drawing.Point(373, 67);
            this.groupBoxLng.Margin = new System.Windows.Forms.Padding(13);
            this.groupBoxLng.Name = "groupBoxLng";
            this.groupBoxLng.Size = new System.Drawing.Size(335, 154);
            this.groupBoxLng.TabIndex = 4;
            this.groupBoxLng.TabStop = false;
            this.groupBoxLng.Text = "Долгота";
            // 
            // tableLayoutPanelLng
            // 
            this.tableLayoutPanelLng.AutoSize = true;
            this.tableLayoutPanelLng.ColumnCount = 2;
            this.tableLayoutPanelLng.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelLng.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelLng.Controls.Add(this.numericUpDownLngEndDeg, 1, 3);
            this.tableLayoutPanelLng.Controls.Add(this.labelLngEndDeg, 0, 3);
            this.tableLayoutPanelLng.Controls.Add(this.numericUpDownLngStartDeg, 1, 2);
            this.tableLayoutPanelLng.Controls.Add(this.labelLngStartDeg, 0, 2);
            this.tableLayoutPanelLng.Controls.Add(this.numericUpDownLngEndPx, 1, 1);
            this.tableLayoutPanelLng.Controls.Add(this.labelLngEndPx, 0, 1);
            this.tableLayoutPanelLng.Controls.Add(this.labelLngStartPx, 0, 0);
            this.tableLayoutPanelLng.Controls.Add(this.numericUpDownLngStartPx, 1, 0);
            this.tableLayoutPanelLng.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanelLng.Name = "tableLayoutPanelLng";
            this.tableLayoutPanelLng.RowCount = 4;
            this.tableLayoutPanelLng.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLng.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLng.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLng.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelLng.Size = new System.Drawing.Size(291, 112);
            this.tableLayoutPanelLng.TabIndex = 0;
            // 
            // numericUpDownLngEndDeg
            // 
            this.numericUpDownLngEndDeg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLngEndDeg.Location = new System.Drawing.Point(163, 87);
            this.numericUpDownLngEndDeg.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownLngEndDeg.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownLngEndDeg.Name = "numericUpDownLngEndDeg";
            this.numericUpDownLngEndDeg.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLngEndDeg.TabIndex = 7;
            this.numericUpDownLngEndDeg.Value = new decimal(new int[] {
            49,
            0,
            0,
            0});
            // 
            // labelLngEndDeg
            // 
            this.labelLngEndDeg.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLngEndDeg.AutoSize = true;
            this.labelLngEndDeg.Location = new System.Drawing.Point(3, 90);
            this.labelLngEndDeg.Name = "labelLngEndDeg";
            this.labelLngEndDeg.Size = new System.Drawing.Size(113, 16);
            this.labelLngEndDeg.TabIndex = 6;
            this.labelLngEndDeg.Text = "Конец (градусы)";
            // 
            // numericUpDownLngStartDeg
            // 
            this.numericUpDownLngStartDeg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLngStartDeg.Location = new System.Drawing.Point(163, 59);
            this.numericUpDownLngStartDeg.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownLngStartDeg.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownLngStartDeg.Name = "numericUpDownLngStartDeg";
            this.numericUpDownLngStartDeg.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLngStartDeg.TabIndex = 5;
            this.numericUpDownLngStartDeg.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            // 
            // labelLngStartDeg
            // 
            this.labelLngStartDeg.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLngStartDeg.AutoSize = true;
            this.labelLngStartDeg.Location = new System.Drawing.Point(3, 62);
            this.labelLngStartDeg.Name = "labelLngStartDeg";
            this.labelLngStartDeg.Size = new System.Drawing.Size(123, 16);
            this.labelLngStartDeg.TabIndex = 4;
            this.labelLngStartDeg.Text = "Начало (градусы)";
            // 
            // numericUpDownLngEndPx
            // 
            this.numericUpDownLngEndPx.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLngEndPx.Location = new System.Drawing.Point(163, 31);
            this.numericUpDownLngEndPx.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownLngEndPx.Name = "numericUpDownLngEndPx";
            this.numericUpDownLngEndPx.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLngEndPx.TabIndex = 3;
            // 
            // labelLngEndPx
            // 
            this.labelLngEndPx.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLngEndPx.AutoSize = true;
            this.labelLngEndPx.Location = new System.Drawing.Point(3, 34);
            this.labelLngEndPx.Name = "labelLngEndPx";
            this.labelLngEndPx.Size = new System.Drawing.Size(113, 16);
            this.labelLngEndPx.TabIndex = 2;
            this.labelLngEndPx.Text = "Конец (пиксели)";
            // 
            // labelLngStartPx
            // 
            this.labelLngStartPx.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLngStartPx.AutoSize = true;
            this.labelLngStartPx.Location = new System.Drawing.Point(3, 6);
            this.labelLngStartPx.Name = "labelLngStartPx";
            this.labelLngStartPx.Size = new System.Drawing.Size(123, 16);
            this.labelLngStartPx.TabIndex = 0;
            this.labelLngStartPx.Text = "Начало (пиксели)";
            // 
            // numericUpDownLngStartPx
            // 
            this.numericUpDownLngStartPx.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownLngStartPx.Location = new System.Drawing.Point(163, 3);
            this.numericUpDownLngStartPx.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownLngStartPx.Name = "numericUpDownLngStartPx";
            this.numericUpDownLngStartPx.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownLngStartPx.TabIndex = 1;
            // 
            // tableLayoutPanelFileName
            // 
            this.tableLayoutPanelFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelFileName.AutoSize = true;
            this.tableLayoutPanelFileName.ColumnCount = 2;
            this.tableLayoutPanelFileName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelFileName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelFileName.Controls.Add(this.labelFileName, 0, 0);
            this.tableLayoutPanelFileName.Controls.Add(this.textBoxFileName, 1, 0);
            this.tableLayoutPanelFileName.Location = new System.Drawing.Point(373, 13);
            this.tableLayoutPanelFileName.Margin = new System.Windows.Forms.Padding(13);
            this.tableLayoutPanelFileName.Name = "tableLayoutPanelFileName";
            this.tableLayoutPanelFileName.RowCount = 1;
            this.tableLayoutPanelFileName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFileName.Size = new System.Drawing.Size(335, 28);
            this.tableLayoutPanelFileName.TabIndex = 3;
            // 
            // labelFileName
            // 
            this.labelFileName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(3, 6);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(83, 16);
            this.labelFileName.TabIndex = 1;
            this.labelFileName.Text = "Имя файла:";
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFileName.Location = new System.Drawing.Point(92, 3);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(240, 22);
            this.textBoxFileName.TabIndex = 2;
            // 
            // tableLayoutPanelName
            // 
            this.tableLayoutPanelName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelName.AutoSize = true;
            this.tableLayoutPanelName.ColumnCount = 2;
            this.tableLayoutPanelName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelName.Controls.Add(this.labelName, 0, 0);
            this.tableLayoutPanelName.Controls.Add(this.textBoxName, 1, 0);
            this.tableLayoutPanelName.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanelName.Margin = new System.Windows.Forms.Padding(13);
            this.tableLayoutPanelName.Name = "tableLayoutPanelName";
            this.tableLayoutPanelName.RowCount = 1;
            this.tableLayoutPanelName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelName.Size = new System.Drawing.Size(334, 28);
            this.tableLayoutPanelName.TabIndex = 2;
            // 
            // labelName
            // 
            this.labelName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(3, 6);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(37, 16);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Имя:";
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxName.Location = new System.Drawing.Point(46, 3);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(285, 22);
            this.textBoxName.TabIndex = 2;
            // 
            // FormMapAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 396);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.buttonAdd);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMapAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление новой карты";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMapAdd_FormClosing);
            this.Load += new System.EventHandler(this.FormMapAdd_Load);
            this.groupBoxLat.ResumeLayout(false);
            this.groupBoxLat.PerformLayout();
            this.tableLayoutPanelLat.ResumeLayout(false);
            this.tableLayoutPanelLat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatEndDeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatStartDeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatEndPx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatStartPx)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelRation.ResumeLayout(false);
            this.tableLayoutPanelRation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRatio)).EndInit();
            this.groupBoxLng.ResumeLayout(false);
            this.groupBoxLng.PerformLayout();
            this.tableLayoutPanelLng.ResumeLayout(false);
            this.tableLayoutPanelLng.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngEndDeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngStartDeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngEndPx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngStartPx)).EndInit();
            this.tableLayoutPanelFileName.ResumeLayout(false);
            this.tableLayoutPanelFileName.PerformLayout();
            this.tableLayoutPanelName.ResumeLayout(false);
            this.tableLayoutPanelName.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.GroupBox groupBoxLat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelLat;
        private System.Windows.Forms.NumericUpDown numericUpDownLatEndDeg;
        private System.Windows.Forms.Label labelLatEndDeg;
        private System.Windows.Forms.NumericUpDown numericUpDownLatStartDeg;
        private System.Windows.Forms.Label labelLatStartDeg;
        private System.Windows.Forms.NumericUpDown numericUpDownLatEndPx;
        private System.Windows.Forms.Label labelLatEndPx;
        private System.Windows.Forms.Label labelLatStartPx;
        private System.Windows.Forms.NumericUpDown numericUpDownLatStartPx;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFileName;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.GroupBox groupBoxLng;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelLng;
        private System.Windows.Forms.NumericUpDown numericUpDownLngEndDeg;
        private System.Windows.Forms.Label labelLngEndDeg;
        private System.Windows.Forms.NumericUpDown numericUpDownLngStartDeg;
        private System.Windows.Forms.Label labelLngStartDeg;
        private System.Windows.Forms.NumericUpDown numericUpDownLngEndPx;
        private System.Windows.Forms.Label labelLngEndPx;
        private System.Windows.Forms.Label labelLngStartPx;
        private System.Windows.Forms.NumericUpDown numericUpDownLngStartPx;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRation;
        private System.Windows.Forms.NumericUpDown numericUpDownRatio;
        private System.Windows.Forms.Label labelRatio;
    }
}