﻿using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace Seismology.Map
{
    public class Settings
    {
        public static readonly string Path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public static readonly string MapPath = $"{Path}\\Карты";

        private static readonly string SettingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\settigs.json";

        public static FormSettings DefauldSettings => new FormSettings();

        public static void Save(FormSettings settings)
        {
            File.WriteAllText(SettingPath, JsonConvert.SerializeObject(settings));
        }

        public static FormSettings Load()
        {
            if (File.Exists(SettingPath))
            {
                return JsonConvert.DeserializeObject<FormSettings>(File.ReadAllText(SettingPath));
            }

            Save(DefauldSettings);
            return DefauldSettings;
        }

        public static bool IsChanged(FormSettings settings)
        {
            return settings != Load();
        }
    }
}