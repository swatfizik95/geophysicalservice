﻿using System;

namespace Seismology.Map
{
    public static class DateTimeExtension
    {
        public static bool Less(this DateTime source, DateTime value, TimeSpan period)
        {
            return source > value ? source - value <= period : value - source <= period;
        }
    }
}