﻿using System.Drawing;

namespace Seismology.Map
{
    public class Map
    {
        #region Properties

        #region Images

        /// <summary>
        /// Рисунок карты
        /// </summary>
        public Image MapImage { get; set; }

        /// <summary>
        /// Рисунок легенды
        /// </summary>
        public Image LegendImage { get; set; }

        /// <summary>
        /// Рисунок наружной легенды
        /// </summary>
        public Image LegendOutImage { get; set; }

        #endregion

        #region Lat

        /// <summary>
        /// Старт широты в пикселях (вертикаль)
        /// </summary>
        public int LatStartPx { get; }

        /// <summary>
        /// Конец широты в пикселях (вертикаль)
        /// </summary>
        public int LatEndPx { get; }

        /// <summary>
        /// Длина широты - от начала до конца в пикселях (вертикаль)
        /// </summary>
        public int LatLengthPx { get; }

        /// <summary>
        /// Старт широты в градусах (вертикаль)
        /// </summary>
        public int LatStartDeg { get; }

        /// <summary>
        /// Конец широты в градусах (вертикаль)
        /// </summary>
        public int LatEndDeg { get; }

        /// <summary>
        /// Длина широты в градусах
        /// </summary>
        public int LatLengthDeg { get; set; }

        /// <summary>
        /// Градус широты в пикселях (вертикаль)
        /// </summary>
        public int LatPx { get; }

        /// <summary>
        /// Пиксель широты в градусах
        /// </summary>
        public double LatDeg { get; set; }

        #endregion

        #region Lng

        /// <summary>
        /// Старт долготы (горизонталь)
        /// </summary>
        public int LngStartPx { get; }

        /// <summary>
        /// Конец долготы (горизонталь)
        /// </summary>
        public int LngEndPx { get; }

        /// <summary>
        /// Длина долготы (горизонталь)
        /// </summary>
        public int LngLengthPx { get; }

        /// <summary>
        /// Старт долготы в градусах (горизонталь)
        /// </summary>
        public int LngStartDeg { get; }

        /// <summary>
        /// Конец долготы в градусах (горизонталь)
        /// </summary>
        public int LngEndDeg { get; }

        /// <summary>
        /// Длина долготы в градусах
        /// </summary>
        public int LngLengthDeg { get; set; }

        /// <summary>
        /// Градус долготы в пикселях (горизонталь)
        /// </summary>
        public int LngPx { get; }

        /// <summary>
        /// Пиксель широты в градусах
        /// </summary>
        public double LngDeg { get; set; }

        #endregion

        /// <summary>
        /// Отношение рисунка карты к размеру pictureBox, к которой он прикреплен
        /// </summary>
        public float RatioF { get; }

        public int Ratio { get; }

        #endregion

        #region Constructors

        public Map(Image mapImage, int latStartPx, int latEndPx, int latStartDeg, int latEndDeg, int lngStartPx, int lngEndPx, int lngStartDeg, int lngEndDeg, float ratioF, int ratio)
        {
            // img
            MapImage = (Image)mapImage.Clone();
            // lat px
            LatStartPx = latStartPx;
            LatEndPx = latEndPx;
            LatLengthPx = latEndPx - latStartPx;
            LatPx = LatLengthPx / (latEndDeg - latStartDeg);
            // lat deg
            LatStartDeg = latStartDeg;
            LatEndDeg = latEndDeg;
            LatLengthDeg = latEndDeg - latStartDeg;
            LatDeg = (double)LatLengthDeg / (latEndPx - latStartPx);
            // lng px
            LngStartPx = lngStartPx;
            LngEndPx = lngEndPx;
            LngLengthPx = lngEndPx - lngStartPx;
            LngPx = LngLengthPx / (lngEndDeg - lngStartDeg);
            // lng deg
            LngStartDeg = lngStartDeg;
            LngEndDeg = lngEndDeg;
            LngLengthDeg = lngEndDeg - lngStartDeg;
            LngDeg = (double)LngLengthDeg / (lngEndPx - lngStartPx);
            // rat
            RatioF = ratioF;
            Ratio = ratio;
        }

        #endregion

        #region Methods

        public Image GetMapCopy()
        {
            return (Image)MapImage.Clone();
        }

        public Image GetLegendCopy()
        {
            return (Image)LegendImage.Clone();
        }

        public Image GetLegendOutCopy()
        {
            return (Image)LegendOutImage.Clone();
        }

        public double PxToLat(double px)
        {
            var res1 = LatStartDeg + px * LatDeg;
            var res2 = LatStartDeg + px / LatPx;
            return res2;
        }

        public double LatToPx(double lat)
        {
            return LatEndPx - (lat - LatStartDeg) * LatPx;
        }

        public double LngToPx(double lng)
        {
            return LngStartPx + (lng - LngStartDeg) * LngPx;
        }

        #endregion
    }
}