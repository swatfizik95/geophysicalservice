﻿using Seismology.Core;
using Seismology.Core.Map;
using Seismology.Map.ColorHelper;
using System;
using System.Drawing;
using System.Linq;

namespace Seismology.Map
{
    public class Marker
    {
        const double ClassBorder = 0.5;

        private static MarkerColorType _colorType = MarkerColorType.VpVs;

        #region Properties

        public static Map Map { get; set; }

        #region Main

        public int Number { get; }

        public string SelectedNumberText { get; set; } = string.Empty;

        public bool HasCouple { get; set; }

        public int[] CoupleGroups { get; set; }

        public DateTime Date { get; }

        public DateTime T0 { get; }

        public DateTime DateTime => Date.Date.Add(T0.TimeOfDay);

        public double K { get; }

        public double Depth { get; }

        public double Lat { get; }

        public double Lng { get; }

        public double VpVs { get; }

        public float Width { get; }

        public DateTime DateT0 { get; set; }

        public float Height { get; }

        public Coordinate Coordinate { get; set; }

        #endregion

        #region Colors & Radius

        #region Color

        // Индекс по цвету
        public int ColorIndex => _colorIndex;
        // Цвет маркера
        public Color Color => _colors[ColorIndex];
        // Индекс по цвету
        private int _colorIndex;
        // Массив цветов
        private static Color[] _colors = _colorsVpVs;
        // Числовая линия для цветов
        private static double[] _colorIntervals = _colorIntervalsVpVs;

        public int ColorIndexVpVs => _colorIndexVpVs;
        public Color ColorVpVs => _colorsVpVs[_colorIndexVpVs];
        private int _colorIndexVpVs;
        private static Color[] _colorsVpVs;
        private static double[] _colorIntervalsVpVs;

        public int ColorIndexClass => _colorIndexClass;
        public Color ColorClass => _colorsClass[_colorIndexClass];
        private int _colorIndexClass;
        private static Color[] _colorsClass;
        private static double[] _colorIntervalsClass;

        #endregion

        #region Radius

        // Класс
        public int Class { get; set; }
        // Индекс Радиуса
        public int RadiusIndex { get; }
        // Радиус
        public int Radius => _radiuses[RadiusIndex];
        // Радиусы
        private static int[] _initRadiuses;
        private static int[] _radiuses;
        // Интервалы классов
        private static int[] _classes;

        // Множитель радиуса
        public static double RadiusMult { get; private set; }

        #endregion

        #endregion

        public static MarkerColorType ColorType => _colorType;

        #endregion

        #region Constructors

        public Marker(Earthquake eq)
        {
            Number = eq.Number;
            Date = eq.Date;
            T0 = eq.T0;
            DateT0 = eq.Date.Date.Add(eq.T0.TimeOfDay);
            K = eq.K;
            Depth = eq.H;
            Lat = eq.Lat;
            Lng = eq.Lng;
            Coordinate = new Coordinate(eq.Lat, eq.Lng);
            VpVs = eq.VpVs;
            Width = GetWidth(Lng);
            Height = GetHeight(Lat);

            Class = GetClass(K);
            RadiusIndex = GetRadiusIndex(Class);
            _colorIndexVpVs = GetColorIndexVpVs(VpVs);
            _colorIndexClass = GetColorIndexClass(K);
            switch (_colorType)
            {
                case MarkerColorType.VpVs:
                    _colorIndex = _colorIndexVpVs;
                    break;
                case MarkerColorType.Class:
                    _colorIndex = _colorIndexClass;
                    break;
            }
        }

        #endregion

        #region Methods

        public static int GetRadius(int @class)
        {
            for (int index = 0; index < _classes.Length; index++)
            {
                if (@class == _classes[index])
                {
                    return _radiuses[index];
                }
            }

            throw new Exception("Error");
        }

        public static Color GetColor(int @class)
        {
            for (int index = 0; index < _classes.Length; index++)
            {
                if (@class == _classes[index])
                {
                    return _colorsClass[index];
                }
            }

            throw new Exception("Error");
        }

        #region GetHelpers

        private float GetWidth(double lng)
        {
            return Map.LngStartPx + ((float)lng - Map.LngStartDeg) * Map.LngPx;
        }

        private float GetHeight(double lat)
        {
            return Map.LatEndPx - ((float)lat - Map.LatStartDeg)
                   * Map.LatPx;
        }

        private int GetColorIndexVpVs(double vpVs)
        {
            for (int index = 0; index < _colorIntervalsVpVs.Length; index++)
            {
                if (vpVs < _colorIntervalsVpVs[index])
                {
                    return index;
                }
            }

            return _colorsVpVs.Length - 1;
        }

        private int GetColorIndexClass(double @class)
        {
            for (int index = 0; index < _colorIntervalsClass.Length; index++)
            {
                if (@class < _colorIntervalsClass[index])
                {
                    return index;
                }
            }

            return _colorsClass.Length - 1;
        }

        private int GetClass(double k)
        {
            for (int index = 0; index < _colorIntervalsClass.Length; index++)
            {
                if (k < _colorIntervalsClass[index])
                {
                    return _classes[index];
                }
            }

            return _classes[_classes.Length - 1];
        }

        private int GetRadiusIndex(int klass)
        {
            for (int index = 0; index < _classes.Length; index++)
            {
                if (klass == _classes[index])
                {
                    return index;
                }
            }

            throw new Exception("Error");
        }

        #endregion

        #region Set

        public static void SetVpVsValues(Color[] colors, double[] intervals)
        {
            if (colors.Length != intervals.Length + 1)
            {
                throw new Exception("Внимание, количество цветов должно совпадать" +
                                    " с количеством промежутков числовой линии (количество значений числовой линии + 1)");
            }

            _colorsVpVs = colors.ToArray();
            _colorIntervalsVpVs = intervals.ToArray();

            if (ColorType == MarkerColorType.VpVs)
            {
                _colors = colors.ToArray();
                _colorIntervals = intervals.ToArray();
            }
        }

        public static void SetClassValue(int[] radiuses, int[] classes, Color[] colors, double[] intervals)
        {
            if (radiuses.Length != classes.Length || classes.Length != colors.Length)
            {
                throw new Exception("Количество радиусов и классов, а также их цветов должны совпадать");
            }
            if (colors.Length != intervals.Length + 1)
            {
                throw new Exception("Внимание, количество цветов должно совпадать" +
                                    " с количеством промежутков числовой линии (количество значений числовой линии + 1)");
            }

            _initRadiuses = radiuses.ToArray();
            _radiuses = radiuses.ToArray();
            _classes = classes.ToArray();
            _colorsClass = colors.ToArray();
            _colorIntervalsClass = intervals.ToArray();

            if (ColorType == MarkerColorType.Class)
            {
                _colors = colors.ToArray();
                _colorIntervals = intervals.ToArray();
            }
        }

        public static void SetRadiusMult(double radiusMult)
        {
            RadiusMult = radiusMult;
            for (int index = 0; index < _initRadiuses.Length; index++)
            {
                _radiuses[index] = (int)(_initRadiuses[index] * radiusMult);
            }
        }

        #endregion

        public static void SetColorType(MarkerColorType colorType)
        {
            _colorType = colorType;
            switch (colorType)
            {
                case MarkerColorType.VpVs:
                    _colors = _colorsVpVs;
                    _colorIntervals = _colorIntervalsVpVs;
                    break;
                case MarkerColorType.Class:
                    _colors = _colorsClass;
                    _colorIntervals = _colorIntervalsClass;
                    break;
            }
        }

        #endregion
    }
}