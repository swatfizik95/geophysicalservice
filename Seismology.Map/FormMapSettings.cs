﻿namespace Seismology.Map
{
    public class FormMapSettings
    {
        #region Properties
        public string FileName { get; set; }
        public int LatStartPx { get; set; }
        public int LatEndPx { get; set; }
        public int LatStartDeg { get; set; }
        public int LatEndDeg { get; set; }
        public int LngStartPx { get; set; }
        public int LngEndPx { get; set; }
        public int LngStartDeg { get; set; }
        public int LngEndDeg { get; set; }
        public float RatioF { get; set; }
        public int Ratio { get; set; }

        #endregion

        public FormMapSettings(string fileName, int latStartPx, int latEndPx, int latStartDeg, int latEndDeg, int lngStartPx, int lngEndPx, int lngStartDeg, int lngEndDeg, float ratioF, int ratio)
        {
            FileName = fileName;
            LatStartPx = latStartPx;
            LatEndPx = latEndPx;
            LatStartDeg = latStartDeg;
            LatEndDeg = latEndDeg;
            LngStartPx = lngStartPx;
            LngEndPx = lngEndPx;
            LngStartDeg = lngStartDeg;
            LngEndDeg = lngEndDeg;
            RatioF = ratioF;
            Ratio = ratio;
        }

        #region Equals

        protected bool Equals(FormMapSettings other)
        {
            return string.Equals(FileName,
                       other.FileName) &&
                   LatStartPx == other.LatStartPx &&
                   LatEndPx == other.LatEndPx &&
                   LatStartDeg == other.LatStartDeg &&
                   LatEndDeg == other.LatEndDeg &&
                   LngStartPx == other.LngStartPx &&
                   LngEndPx == other.LngEndPx &&
                   LngStartDeg == other.LngStartDeg &&
                   LngEndDeg == other.LngEndDeg &&
                   RatioF.Equals(other.RatioF) &&
                   Ratio == other.Ratio;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FormMapSettings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FileName != null ? FileName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LatStartPx;
                hashCode = (hashCode * 397) ^ LatEndPx;
                hashCode = (hashCode * 397) ^ LatStartDeg;
                hashCode = (hashCode * 397) ^ LatEndDeg;
                hashCode = (hashCode * 397) ^ LngStartPx;
                hashCode = (hashCode * 397) ^ LngEndPx;
                hashCode = (hashCode * 397) ^ LngStartDeg;
                hashCode = (hashCode * 397) ^ LngEndDeg;
                hashCode = (hashCode * 397) ^ RatioF.GetHashCode();
                hashCode = (hashCode * 397) ^ Ratio;
                return hashCode;
            }
        }

        public static bool operator ==(FormMapSettings left, FormMapSettings right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FormMapSettings left, FormMapSettings right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}