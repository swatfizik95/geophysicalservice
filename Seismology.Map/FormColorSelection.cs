﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.Map
{
    public partial class FormColorSelection : Form
    {
        int _count;

        private const int Max = 20;
        private const int IntervalsMax = Max - 1;
        private const int Min = 3;

        private PictureBox[] _picBoxes;
        private NumericUpDown[] _numericBoxes;
        //private MarkerColorType _colorType;

        private Color[] _colors;
        private double[] _intervals;

        public FormColorSelection(Color[] colors, double[] intervals/*, MarkerColorType colorType*/)
        {
            InitializeComponent();

            //_colorType = colorType;
            _count = intervals.Length;
            InitFormControls(colors, intervals);
        }

        #region Events

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (_count == IntervalsMax)
            {
                MessageBox.Show($"Количество интервалов не должно быть больше: {IntervalsMax}");
            }
            else
            {
                _picBoxes[_count + 1].BackColor = Color.Black;
                _picBoxes[_count + 1].Visible = true;

                //_txtBoxes[_count].Text = string.Empty;
                _numericBoxes[_count].Value = _numericBoxes[_count].Minimum;
                _numericBoxes[_count].Visible = true;

                _count++;
            }
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (_count == Min)
            {
                MessageBox.Show($"Количество интервалов не должно быть меньше: {Min}");
            }
            else
            {
                _picBoxes[_count].Visible = false;
                _numericBoxes[_count - 1].Visible = false;
                _count--;
            }
        }

        private void ColorPicker_Click(object sender, EventArgs e)
        {
            colorDialog.Color = ((PictureBox)sender).BackColor;
            if (colorDialog.ShowDialog() == DialogResult.OK)
                ((PictureBox)sender).BackColor = colorDialog.Color;
        }

        private void FormColorSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;
            _colors = _picBoxes.Where(b => b.Visible).Select(b => b.BackColor).ToArray();
            _intervals = _numericBoxes.Where(n => n.Visible).Select(n => (double)n.Value).ToArray();

            // Проверка на одинаковые значения
            if (_intervals.Length != _intervals.Distinct().Count())
            {
                e.Cancel = true;
                MessageBox.Show("Некоторые значения в промежутке совпадают!", "Внимание!");
                return;
            }

            // Проверка на одинаковые цвета
            if (_colors.Length != _colors.Distinct().Count())
            {
                var dialogResult =
                    MessageBox.Show(
                        "Некоторые из цветов совпадают\r\n" +
                        "Хотите изменить цвет?",
                        "Внимание!",
                        MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                    e.Cancel = true;
            }

            // Проверка на возрастание интервалов
            if (!IsSorted(_intervals))
            {
                e.Cancel = true;
                var dialogResult =
                    MessageBox.Show(
                        "Значения интервалов не являются последовательно возрастающими!\r\n" +
                        "Для корректной работы программы, значения интервалов должны " +
                        "быть отсортированы в порядке возрастания.\r\n" +
                        "Провести автоматическую сортировку?",
                        "Внимание!", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    var sortedIntervals = _intervals.OrderBy(i => i).ToArray();
                    for (var i = 0; i < sortedIntervals.Length; i++)
                        //_txtBoxes[i].Text = $"{sortedIntervals[i]}";
                        _numericBoxes[i].Value = (decimal)sortedIntervals[i];
                }
            }
        }

        #endregion

        #region Logic

        private void InitFormControls(Color[] colors, double[] intervals)
        {
            _picBoxes = Enumerable.Range(0, Max).Select(i =>
            {
                var pictBox = new PictureBox
                {
                    Parent = this,
                    Size = new Size(75, 25),
                    Location = new Point(110, 15 + 30 * i),
                    Visible = false,
                    Cursor = Cursors.Hand,
                    BackColor = Color.Black
                };
                if (i < colors.Length)
                {
                    pictBox.BackColor = colors[i];
                    pictBox.Visible = true;
                }
                pictBox.Click += ColorPicker_Click;

                return pictBox;
            }).ToArray();

            _numericBoxes = Enumerable.Range(0, IntervalsMax).Select(i =>
            {
                var numericUpDown = new NumericUpDown()
                {
                    Parent = this,
                    Size = new Size(75, 25),
                    Location = new Point(200, 28 + 30 * i),
                    Visible = false,
                    TabIndex = i + 2,
                    Minimum = 0.1M,
                    Maximum = 5,
                    DecimalPlaces = 2,
                    Increment = 0.1M,
                };

                //switch (_colorType)
                //{
                //    case MarkerColorType.VpVs:
                //        numericUpDown.Minimum = 0.1M;
                //        numericUpDown.Maximum = 5;
                //        numericUpDown.DecimalPlaces = 2;
                //        numericUpDown.Increment = 0.1M;
                //        break;
                //    case MarkerColorType.Class:
                //        numericUpDown.Minimum       = 0.1M;
                //        numericUpDown.Maximum       = 20;
                //        numericUpDown.DecimalPlaces = 1;
                //        numericUpDown.Increment     = 1;
                //        break;
                //}

                if (i < intervals.Length)
                {
                    numericUpDown.Value = (decimal)intervals[i];
                    numericUpDown.Visible = true;
                }

                return numericUpDown;
            }).ToArray();
        }

        private bool IsSorted(double[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] > array[i])
                    return false;
            }

            return true;
        }

        public Color[] ReturnColors()
        {
            return _colors;
        }

        public double[] ReturnInterval()
        {
            return _intervals;
        }

        #endregion
    }
}
