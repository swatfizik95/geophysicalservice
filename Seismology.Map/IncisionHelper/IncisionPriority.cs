﻿namespace Seismology.Map.IncisionHelper
{
    public enum IncisionPriority
    {
        Distance,
        Radius
    }
}