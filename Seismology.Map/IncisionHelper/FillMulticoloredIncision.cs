﻿using System.Drawing;

namespace Seismology.Map.IncisionHelper
{
    public class FillMulticoloredIncision : IIncisionMarker
    {
        private readonly SolidBrush _brush;
        private readonly int _alpha;

        public FillMulticoloredIncision(int alpha)
        {
            _brush = new SolidBrush(Color.FromArgb(alpha, Color.Black));
            _alpha = alpha;
        }

        public void Draw(Graphics graphics, Marker marker, int width, int height, int radius)
        {
            _brush.Color = Color.FromArgb(_alpha, marker.Color);
            graphics.FillEllipse(_brush, width, height, radius, radius);
        }
    }
}