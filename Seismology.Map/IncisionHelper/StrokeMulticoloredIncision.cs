﻿using System.Drawing;

namespace Seismology.Map.IncisionHelper
{
    public class StrokeMulticoloredIncision : IIncisionMarker
    {
        private readonly Pen _pen;
        private readonly int _alpha;

        public StrokeMulticoloredIncision(int alpha, float width)
        {
            _pen = new Pen(Color.FromArgb(alpha, Color.Black), width);
            _alpha = alpha;
        }

        public void Draw(Graphics graphics, Marker marker, int width, int height, int radius)
        {
            _pen.Color = Color.FromArgb(_alpha, marker.Color);
            graphics.DrawEllipse(_pen, width, height, radius, radius);
        }
    }
}