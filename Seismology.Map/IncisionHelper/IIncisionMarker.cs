﻿using System.Drawing;

namespace Seismology.Map.IncisionHelper
{
    public interface IIncisionMarker
    {
        void Draw(Graphics graphics, Marker marker, int width, int height, int radius);
    }
}