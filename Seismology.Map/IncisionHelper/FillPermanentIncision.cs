﻿using System.Drawing;

namespace Seismology.Map.IncisionHelper
{
    public class FillPermanentIncision : IIncisionMarker
    {
        private readonly SolidBrush _brush;

        public FillPermanentIncision(Color color)
        {
            _brush = new SolidBrush(color);
        }

        public void Draw(Graphics graphics, Marker color, int width, int height, int radius)
        {
            graphics.FillEllipse(_brush, width, height, radius, radius);
        }
    }
}