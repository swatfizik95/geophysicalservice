﻿namespace Seismology.Map.IncisionHelper
{
    public struct IncisionValue
    {
        public double StartLat { get; set; }
        public double StartLng { get; set; }
        public double EndLat { get; set; }
        public double EndLng { get; set; }
        public double Length { get; set; }
    }
}