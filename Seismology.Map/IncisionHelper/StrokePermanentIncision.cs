﻿using System.Drawing;

namespace Seismology.Map.IncisionHelper
{
    public class StrokePermanentIncision : IIncisionMarker
    {
        private readonly Pen _pen;

        public StrokePermanentIncision(Color color, float width)
        {
            _pen = new Pen(color, width);
        }

        public void Draw(Graphics graphics, Marker color, int width, int height, int radius)
        {
            graphics.DrawEllipse(_pen, width, height, radius, radius);
        }
    }
}