﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;
// ReSharper disable All

namespace Seismology.Map
{
    public partial class FormRadiusSelection : Form
    {
        private const int Max = 20;
        private const int IntervalsMax = Max - 1;
        private const int Min = 3;

        private NumericUpDown[] _numericRadius;
        private Label[] _labelClass;
        private PictureBox[] _colorPickers;
        private NumericUpDown[] _numericIntervals;

        private int[] _radiuses;
        private int[] _classIntervals;
        private Color[] _colors;
        private double[] _intervals;

        public FormRadiusSelection(int[] radiuses, int[] classes, Color[] colors, double[] intevals)
        {
            InitializeComponent();

            InitFormControls(radiuses, classes, colors, intevals);
        }

        #region Events

        private void NumericUpDownNew_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Apply();
            }
        }

        private void ButtonApply_Click(object sender, System.EventArgs e)
        {
            Apply();
        }

        private void FormRadiusSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;

            _radiuses = _numericRadius.Where(n => n.Visible).Select(n => (int)n.Value).ToArray();
            _classIntervals = _labelClass.Where(l => l.Visible).Select(l => int.Parse(l.Text)).ToArray();
            _colors = _colorPickers.Where(cp => cp.Visible).Select(cp => cp.BackColor).ToArray();
            _intervals = _numericIntervals.Where(n => n.Visible).Select(n => (double)n.Value).ToArray();
        }

        private void ColorPicker_Click(object sender, System.EventArgs e)
        {
            var colorPicker = (PictureBox)sender;

            colorDialog.Color = colorPicker.BackColor;
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            colorPicker.BackColor = colorDialog.Color;
        }

        #endregion

        #region Logic

        private void Apply()
        {
            int classStart = (int)numericUpDownStartClass.Value;
            int classEnd = (int)numericUpDownEndClass.Value;
            if (classEnd - classStart < Min || classEnd - classStart > Max)
            {
                MessageBox.Show(
                    $"Разница между конечным и начальным классом должна быт больше {Min} и меньше {Max}!");
            }
            else
            {
                int radiusMult = (int)numericUpDownRadiusMult.Value;
                int radiusStart = (int)numericUpDownRadiusStart.Value;
                int length = classEnd - classStart + 1;
                SetLabelClasses(Enumerable.Range(0, length).Select(i => i + classStart).ToArray());
                SetNumericRadiuses(Enumerable.Range(0, length).Select(i => radiusStart + i * radiusMult).ToArray());
                SetColorPickerColors(length);
                SetNumericIntervals(Enumerable.Range(0, length - 1).Select(i => i + classStart + 0.5).ToArray());
            }
        }

        private void InitFormControls(int[] radiuses, int[] classes, Color[] colors, double[] intervals)
        {
            _labelClass = new Label[Max];
            _numericRadius = new NumericUpDown[Max];
            _colorPickers = new PictureBox[Max];
            _numericIntervals = new NumericUpDown[IntervalsMax];

            for (int index = 0, rowIndex = 1; index < Max; index++, rowIndex++)
            {
                tableLayoutPanelClassRadiusColor.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

                #region label

                _labelClass[index] = new Label()
                {
                    Parent = tableLayoutPanelClassRadiusColor,
                    Anchor = AnchorStyles.Left,
                    TextAlign = ContentAlignment.MiddleLeft,
                    AutoSize = true,
                    Visible = false,
                };

                if (index < classes.Length)
                {
                    _labelClass[index].Text = $"{classes[index]}";
                    _labelClass[index].Visible = true;
                }

                //tableLayoutPanelClassRadiusColor.SetCellPosition(_labelClass[index], new TableLayoutPanelCellPosition(0, rowIndex));

                #endregion

                #region numeric

                _numericRadius[index] = new NumericUpDown()
                {
                    Parent = tableLayoutPanelClassRadiusColor,
                    Width = 74,
                    Anchor = AnchorStyles.None,
                    Minimum = 1,
                    Maximum = 100,
                    Visible = false
                };

                if (index < radiuses.Length)
                {
                    _numericRadius[index].Value = radiuses[index];
                    _numericRadius[index].Visible = true;
                }

                //tableLayoutPanelClassRadiusColor.SetCellPosition(_numericRadius[index], new TableLayoutPanelCellPosition(1, rowIndex));

                #endregion

                #region colorPicker

                _colorPickers[index] = new PictureBox()
                {
                    Parent = tableLayoutPanelClassRadiusColor,
                    Width = 72,
                    Height = 20,
                    Anchor = AnchorStyles.None,
                    BackColor = Color.Black,
                    Cursor = Cursors.Hand,
                    Visible = false,
                };
                _colorPickers[index].Click += ColorPicker_Click;

                if (index < colors.Length)
                {
                    _colorPickers[index].Visible = true;
                    _colorPickers[index].BackColor = colors[index];
                }

                #endregion
            }

            for (int index = 0, rowIndex = 1; index < IntervalsMax; index++, rowIndex++)
            {
                tableLayoutPanelInterval.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

                _numericIntervals[index] = new NumericUpDown()
                {
                    Parent = tableLayoutPanelInterval,
                    Width = 74,
                    Anchor = AnchorStyles.None,
                    Minimum = 1,
                    Maximum = 100,
                    Visible = false,
                    DecimalPlaces = 1,
                    Increment = 1,
                };

                if (index < intervals.Length)
                {
                    _numericIntervals[index].Value = (decimal)intervals[index];
                    _numericIntervals[index].Visible = true;
                }
            }

            numericUpDownRadiusStart.Value = radiuses[0];
            numericUpDownRadiusMult.Value = 5;
            numericUpDownStartClass.Value = classes[0];
            numericUpDownEndClass.Value = classes.Last();
        }

        private void SetNumericRadiuses(int[] radiuses)
        {
            for (int index = 0; index < Max; index++)
            {
                if (index < radiuses.Length)
                {
                    _numericRadius[index].Visible = true;
                    _numericRadius[index].Value = radiuses[index];
                }
                else
                {
                    _numericRadius[index].Visible = false;
                    _numericRadius[index].Value = 1;
                }
            }
        }

        private void SetLabelClasses(int[] classes)
        {
            for (int index = 0; index < Max; index++)
            {
                if (index < classes.Length)
                {
                    _labelClass[index].Visible = true;
                    _labelClass[index].Text = $"{classes[index]}";
                }
                else
                {
                    _labelClass[index].Visible = false;
                    _labelClass[index].Text = "0";
                }
            }
        }

        private void SetColorPickerColors(int length)
        {
            for (int index = 0; index < Max; index++)
            {
                _colorPickers[index].Visible = index < length;
            }
        }

        private void SetNumericIntervals(double[] intervals)
        {
            for (int index = 0; index < IntervalsMax; index++)
            {
                if (index < intervals.Length)
                {
                    _numericIntervals[index].Visible = true;
                    _numericIntervals[index].Value = (decimal)intervals[index];
                }
                else
                {
                    _numericIntervals[index].Visible = false;
                    _numericIntervals[index].Value = 1;
                }
            }
        }

        public int[] ReturnRadiuses()
        {
            return _radiuses;
        }

        public int[] ReturnClasses()
        {
            return _classIntervals;
        }

        public Color[] ReturnColors()
        {
            return _colors;
        }

        public double[] ReturnIntervals()
        {
            return _intervals;
        }

        #endregion
    }
}
