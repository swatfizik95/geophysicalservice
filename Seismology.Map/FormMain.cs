﻿using Seismology.Core;
using Seismology.Core.IO;
using Seismology.Core.Map;
using Seismology.Core.Map.Interfaces;
using Seismology.Core.MathS;
using Seismology.Map.ColorHelper;
using Seismology.Map.IncisionHelper;
using Seismology.Map.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Seismology.Map
{
    public partial class FormMain : Form
    {
        private FormSettings _settings;
        private Map _map;
        private List<Earthquake> _earthquakes = new List<Earthquake>();
        private List<Marker> _markers = new List<Marker>();
        private List<Marker> _markersSelected = new List<Marker>();
        private List<Marker> _markersZoomed = new List<Marker>();


        private Size _currentSize;
        private Point _pointOld, _pointNew;

        public FormMain()
        {
            InitializeComponent();

            _settings = Settings.Load();
#if DEBUG
            _settings = Settings.DefauldSettings;
#endif
        }

#if DEBUG
        sealed class ResourceReader : IStorageReader
        {
            private readonly string[] _data;
            private int _index;

            public ResourceReader()
            {
                _data = Resources.Каталог16_17.Split(new[] {"\r\n"}, StringSplitOptions.None);
                _index = 0;
            }

            public void Open(string filePath) => _index = 0;

            public string ReadLine()
            {
                string result = null;
                if (_index < _data.Length) result = _data[_index];
                _index++;
                return result;
            }

            public void Close() => _index = 0;
        }
#endif

        #region Events

        #region Form Events

        private void FormMain_Load(object sender, EventArgs e)
        {
            InitMenu();

            if (_settings.MapSettings.ContainsKey("Стандарт"))
            {
                SetMap(_settings.MapSettings["Стандарт"]);
            }
            else
            {
                SetMap(Resources.MapDagestan);
            }

            pictureBoxMap.MouseWheel += (s, eventArgs) =>
            {
                if (eventArgs.Delta > 0 && trackBarZoom.Value < trackBarZoom.Maximum)
                {
                    trackBarZoom.Value++;
                    ZoomMap();
                    labelZoom.Text = $"x{trackBarZoom.Value}";
                }
                else if (eventArgs.Delta < 0 && trackBarZoom.Value > trackBarZoom.Minimum)
                {
                    trackBarZoom.Value--;
                    ZoomMap();
                    labelZoom.Text = $"x{trackBarZoom.Value}";
                }
            };

            InitTab1();
            InitTab4();
            InitTabCharts();
            InitSettingsValues();
#if DEBUG
            var file = new FileS(new ResourceReader(), null);
            _earthquakes = file.ReadCatalogTxt(null)
                .Where(eq => eq.Date.Year >= 1960)
                //.Where(eq => eq.Class != 9)
                .ToList();
            _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();

            InitDatePicker();
            InitDepthNumeration();
            DrawOnMapAndLegendSelected();
            tabMainControl.SelectTab(0);
            tabControlView.SelectTab(1);
            //radioButtonActivity.Checked = true;
            checkBoxCoord.Checked = true;
            //tabControlCoord.SelectTab(1);
#endif
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //_settings = new FormSettings();
            _settings.ColorType = GetColorType();
            _settings.RadiusMult = trackBarMarkerRadius.Value / 10.0;
            _settings.DontShowIfZero = checkBoxDontShowIfZero.Checked;
            _settings.GridLinesColor = colorPickerGridLines.BackColor;
            _settings.GridLinesWidth = (int)numericUpDownGridLinesWidth.Value;
            _settings.GridLinesLatCount = (int)numericUpDownGridLinesLat.Value;
            _settings.GridLinesLngCount = (int)numericUpDownGridLinesLng.Value;
            _settings.GridDegreesColor = colorPickerGridDegrees.BackColor;
            _settings.GridDegreesWidth = (int)numericUpDownGridDegreesWidth.Value;
            _settings.NumerationColor = colorPickerNumeration.BackColor;
            _settings.NumerationWidth = (int)numericUpDownNumerationWidth.Value;
            _settings.SurferLatCount = (int)numericUpDownLatCount.Value;
            _settings.SurferLngCount = (int)numericUpDownLngCount.Value;
            _settings.PolygonCoordinates = GetPolygonCoordinates(false).ToArray();
            _settings.IncisionsValues = GetIncisionsValues(false).ToArray();
            _settings.IncisionStroke = checkBoxIncisionStroke.Checked;
            _settings.IncisionStrokeUserColor = checkBoxIncisionStrokeUserColor.Checked;
            _settings.IncisionStrokeColor = colorPickerIncisionStrokeColor.BackColor;
            _settings.IncisionStrokeWidth = (double)numericUpDownIncisionStrokeWidth.Value;
            _settings.IncisionStrokeAlpha = (double)numericUpDownIncisionStrokeAlpha.Value;
            _settings.IncisionFill = checkBoxIncisionFill.Checked;
            _settings.IncisionFillUserColor = checkBoxIncisionFillUserColor.Checked;
            _settings.IncisionFillColor = colorPickerIncisionFillColor.BackColor;
            _settings.IncisionFillAlpha = (double)numericUpDownIncisionFillAlpha.Value;
            _settings.IncisionDistanceLine = checkBoxIncisionDistanceLine.Checked;
            _settings.IncisionDistanceLineColor = colorPickerIncisionDistanceLineColor.BackColor;
            _settings.IncisionDistanceLineWidth = (double)numericUpDownIncisionDistanceLineWidth.Value;
            _settings.IncisionDistanceLineMult = (int)numericUpDownIncisionDistanceLineMult.Value;
            _settings.IncisionDistanceNumber = checkBoxIncisionDistanceNumber.Checked;
            _settings.IncisionDistanceNumberColor = colorPickerIncisionDistanceNumberColor.BackColor;
            _settings.IncisionDistanceNumberFontSize = (double)numericUpDownIncisionDistanceNumberFontSize.Value;
            _settings.IncisionDistanceNumberMult = (int)numericUpDownIncisionDistanceNumberMult.Value;
            _settings.IncisionDepthLine = checkBoxIncisionDepthLine.Checked;
            _settings.IncisionDepthLineColor = colorPickerIncisionDepthLineColor.BackColor;
            _settings.IncisionDepthLineWidth = (double)numericUpDownIncisionDepthLineWidth.Value;
            _settings.IncisionDepthLineMult = (int)numericUpDownIncisionDepthLineMult.Value;
            _settings.IncisionDepthNumber = checkBoxIncisionDepthNumber.Checked;
            _settings.IncisionDepthNumberColor = colorPickerIncisionDepthNumberColor.BackColor;
            _settings.IncisionDepthNumberFontSize = (double)numericUpDownIncisionDepthNumberFontSize.Value;
            _settings.IncisionDepthNumberMult = (int)numericUpDownIncisionDepthNumberMult.Value;
            _settings.IncisionRadiusMult = (int)numericUpDownIncisionRadiusMult.Value;
            _settings.IncisionRadiusFontSize = (double)numericUpDownIncisionRadiusFontSize.Value;
            _settings.IncisionPriority = GetIncisionPriority();


            if (Settings.IsChanged(_settings))
            {
                if (MessageBox.Show(
                        "Настройки программы были изменены.\r\n" +
                        "Сохранить новые настройки?",
                        "Внимание",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    try
                    {
                        Settings.Save(_settings);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Не удалось сохранить настройки!");
                    }
                }
            }
        }

        #endregion

        #region Menu Events

        #region File

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new OpenFileDialog())
            {
                fileDialog.FileName = "Документ";
                fileDialog.Filter = "Документ txt (*.txt)|*.txt| Документ xlsx (*.xlsx)|*.xlsx";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var file = new FileS();
                        switch (fileDialog.FilterIndex)
                        {
                            case 1:
                                _earthquakes = file.ReadBulletinTxt(filePath).ToList();
                                _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();
                                break;
                            case 2:
                                using (var form = new FormExcelReadFormatSelection(filePath, false, ExcelType.Xlsx))
                                {
                                    if (form.ShowDialog() == DialogResult.OK)
                                    {
                                        _earthquakes = file.ReadExcel(filePath, form.Formats, form.IsFirstHeader).ToList();
                                        _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();
                                    }
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалось прочитать документ.\r\nПричина: {ex.Message}", "Ошибка");
                    }

                    DrawOnMapAndLegendSelected();

                    InitDatePicker();
                    InitTimePicker();
                    InitDepthNumeration();
                }
            }
        }

        private void SaveImageAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new SaveFileDialog())
            {
                fileDialog.FileName = "Карта_Дагестан";
                fileDialog.Filter = "PNG (*.png) | *.png|JPEG (*.jpg) | *.jpg|BMP (*.bmp) | *.bmp|GIF (*.gif) | *.gif";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    var imageFormat = GetImageFormat(fileDialog.FilterIndex);
                    CreateAndSaveImage(filePath, imageFormat);
                }
            }
        }

        private void SaveImageAsIncisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!checkBoxCoord.Checked || tabControlCoord.SelectedIndex != 3)
            {
                MessageBox.Show("Вы не выбрали разрез", "Внимание");
                return;
            }

            using (var fileDialog = new SaveFileDialog())
            {
                fileDialog.FileName = "Карта_Дагестан_Разрез {номер_разреза}";
                fileDialog.Filter = "PNG (*.png) | *.png|JPEG (*.jpg) | *.jpg|BMP (*.bmp) | *.bmp|GIF (*.gif) | *.gif";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    var imageFormat = GetImageFormat(fileDialog.FilterIndex);
                    CreateAndSaveIncisionImage(filePath, imageFormat);
                }
            }
        }

        private void SaveImageAsRepeatabilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!checkBoxCoord.Checked)
            {
                MessageBox.Show("Вы не выбрали координаты", "Внимание");
                return;
            }

            //if (tabControlCoord.SelectedIndex == 3)
            //{
            //    MessageBox.Show("Для вывода подходит только круг или прямоугоьник", "Внимание");
            //    return;
            //}

            using (var fileDialog = new SaveFileDialog())
            {
                fileDialog.FileName = "Карта_Дагестан_Повторяемость";
                fileDialog.Filter = "PNG (*.png) | *.png|JPEG (*.jpg) | *.jpg|BMP (*.bmp) | *.bmp|GIF (*.gif) | *.gif";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    var imageFormat = GetImageFormat(fileDialog.FilterIndex);

                    var figure = GetFigure();
                    if (figure == null)
                    {
                        return;
                    }

                    var form = new FormChartRepeatability(SelectEarthquakes()/*.ToList()*/, figure);
                    form.ShowDialog();
                    form.SaveImage(filePath, imageFormat);
                }
            }
        }

        private void CatalogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new SaveFileDialog())
            {
                var earthquakes = SelectEarthquakes();//.ToList();
                fileDialog.FileName = FileSHlp.GetCatalogFullName(earthquakes);
                fileDialog.Filter = FileSHlp.TxtFilter;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var fileS = new FileS();
                        fileS.WriteCatalogTxt(filePath, earthquakes);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалос сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                    }
                }
            }
        }

        private void BulletinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new SaveFileDialog())
            {
                var earthquakes = SelectEarthquakes();//.ToList();
                fileDialog.FileName = FileSHlp.GetBulletinFullName(earthquakes);
                fileDialog.Filter = FileSHlp.TxtFilter;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var fileS = new FileS();
                        fileS.WriteBulletinTxt(filePath, earthquakes);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                    }
                }
            }
        }

        private void CatalogExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var formatForm = new FormExcelWriteFormatSelection(false, ExcelType.Xlsx))
            {
                formatForm.ShowDialog();
                if (formatForm.DialogResult == DialogResult.OK)
                {
                    using (var fileDialog = new SaveFileDialog())
                    {
                        var earthquakes = SelectEarthquakes();//.ToList();
                        fileDialog.FileName = FileSHlp.GetCatalogFullName(earthquakes);
                        fileDialog.Filter = FileSHlp.ExcelFilter;
                        if (fileDialog.ShowDialog() == DialogResult.OK)
                        {
                            string filePath = fileDialog.FileName;
                            try
                            {
                                var fileS = new FileS();
                                fileS.WriteCatalogExcel(filePath,
                                    earthquakes,
                                    formatForm.GetFormats(),
                                    formatForm.GetFormatsName(),
                                    formatForm.GetWorksheetName());
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                            }
                        }
                    }
                }
            }
        }

        private void BulletinExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var formatForm = new FormExcelWriteFormatSelection(true, ExcelType.Xlsx))
            {
                formatForm.ShowDialog();
                if (formatForm.DialogResult == DialogResult.OK)
                {
                    using (var fileDialog = new SaveFileDialog())
                    {
                        var earthquakes = SelectEarthquakes();//.ToList();
                        fileDialog.FileName = FileSHlp.GetBulletinFullName(earthquakes);
                        fileDialog.Filter = FileSHlp.ExcelFilter;
                        if (fileDialog.ShowDialog() == DialogResult.OK)
                        {
                            string filePath = fileDialog.FileName;
                            try
                            {
                                var fileS = new FileS();
                                fileS.WriteCatalogExcel(filePath,
                                    earthquakes,
                                    formatForm.GetFormats(),
                                    formatForm.GetFormatsName(),
                                    formatForm.GetWorksheetName());
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                            }
                        }
                    }
                }
            }
        }

        private void SurferDocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new SaveFileDialog())
            {
                var scalarType = GetScalarType();
                var earthquakes = SelectEarthquakeForSurfer(scalarType);
                fileDialog.FileName = FileSHlp.GetSurferClassFullName(earthquakes, scalarType);
                fileDialog.Filter = FileSHlp.TxtFilter;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var calc = new CalculatorS();
                        var fileS = new FileS();
                        fileS.WriteScalarData(
                            filePath,
                            calc.FindScalars(earthquakes,
                                _map.LatStartDeg, _map.LatEndDeg, (int)numericUpDownLatCount.Value,
                                _map.LngStartDeg, _map.LngEndDeg, (int)numericUpDownLngCount.Value, scalarType, GetParamValues()));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                    }
                }
            }
        }

        private void DistributionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new SaveFileDialog())
            {
                var earthquakes = SelectEarthquakes();//.ToList();
                fileDialog.FileName = FileSHlp.GetFullName("Распределение", earthquakes);
                fileDialog.Filter = FileSHlp.TxtFilter;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var calc = new CalculatorS();
                        var fileS = new FileS();
                        fileS.WriteDistribution(filePath, calc.FindEnergyClasses(earthquakes, 5, 17, 0, 50, 5));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                    }
                }
            }
        }

        private void DistributionEnergyClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new SaveFileDialog())
            {
                var earthquakes = SelectEarthquakes();//.ToList();
                fileDialog.FileName = FileSHlp.GetEnergyClassFullName(earthquakes);
                fileDialog.Filter = FileSHlp.TxtFilter;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var calc = new CalculatorS();
                        var fileS = new FileS();
                        fileS.WriteEnergyClass(filePath, calc.FindEnergyClass(earthquakes));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                    }
                }
            }
        }

        private void RepeatabilityPeriodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_repeatabilityPeriod == null || _repeatabilityPeriod.Repeatabilities.Count == 0)
            {
                MessageBox.Show("Период повторяемости не был рассчитан", "Внимание");
                return;
            }
            using (var fileDialog = new SaveFileDialog())
            {
                fileDialog.FileName = FileSHlp.GetRepeatabilityPeriodFullName(_repeatabilityPeriod);
                fileDialog.Filter = FileSHlp.ExcelFilter;

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = fileDialog.FileName;
                    try
                    {
                        var fileS = new FileS();
                        fileS.WriteRepeatabilityPeriod(filePath, _repeatabilityPeriod);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Не удалось сохранить.\r\nПричина: {ex.Message}", "Внимание!");
                    }
                }
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region Map

        private void AddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new OpenFileDialog())
            {
                fileDialog.Filter = "Изображения| *.png;*.jpg;*.bmp;*.gif";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string path = fileDialog.FileName;
                    using (var formMapAdd = new FormMapAdd(path, _settings.MapSettings))
                    {
                        if (formMapAdd.ShowDialog() == DialogResult.OK)
                        {
                            string name = formMapAdd.ReturnName();
                            _settings.MapSettings[name] = formMapAdd.ReturnMapSettings();
                            AddMapMenu(name, _settings.MapSettings[name]);
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Tab1 Events

        private void CheckBoxSelection_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxCoord.Enabled = checkBoxCoord.Checked;

            groupBoxNumeration.Enabled = checkBoxNumeration.Checked;

            groupBoxDate.Enabled = checkBoxDate.Checked;

            groupBoxTime.Enabled = checkBoxTime.Checked;

            groupBoxDepth.Enabled = checkBoxDepth.Checked;


            DrawOnMapAndLegendSelected();
        }

        private void TrackBarK_Scroll(object sender, EventArgs e)
        {
            if (trackBarKmax.Value < trackBarKmin.Value)
            {
                labelKMinMax.Text = "K = error";
            }
            else
            {
                labelKMinMax.Text = trackBarKmax.Value == trackBarKmin.Value
                    ? $"K = {_settings.Classes[trackBarKmax.Value]}"
                    : $"K = {_settings.Classes[trackBarKmin.Value]} - {_settings.Classes[trackBarKmax.Value]}";
            }


            DrawOnMapAndLegendSelected();
        }

        private void TrackBarV_Scroll(object sender, EventArgs e)
        {
            int minIndex = trackBarVmin.Value == trackBarVmin.Minimum ? trackBarVmin.Minimum : trackBarVmin.Value - 1;
            int maxIndex = trackBarVmax.Value == trackBarVmax.Maximum ? trackBarVmax.Maximum - 1 : trackBarVmax.Value;

            if (trackBarVmax.Value < trackBarVmin.Value)
            {
                labelVMinMax.Text = "V = Error";
            }
            else
            {
                string value = minIndex == maxIndex ? $"{_settings.ColorIntervalsVpVs[minIndex]}" : $"{_settings.ColorIntervalsVpVs[minIndex]} - {_settings.ColorIntervalsVpVs[maxIndex]}";

                if (trackBarVmin.Value == trackBarVmin.Minimum)
                {
                    value = $"< {value}";
                }

                if (trackBarVmax.Value == trackBarVmax.Maximum)
                {
                    value = $"{value} <";
                }

                labelVMinMax.Text = $"V = {value}";
            }

            DrawOnMapAndLegendSelected();
        }

        private void TabControlCoord_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawOnMapAndLegendSelected();
        }

        private void ButtonShowCoord_Click(object sender, EventArgs e)
        {
            DrawOnMapAndLegendSelected();
            using (var g = Graphics.FromImage(pictureBoxMap.Image))
            {
                var pen = new Pen(Color.Red, 5);
                switch (tabControlCoord.SelectedIndex)
                {
                    case 0:
                        g.DrawRectangle(pen,
                            (float)_map.LngToPx((double)numericUpDownRecLngStart.Value),
                            (float)_map.LatToPx((double)numericUpDownRecLatEnd.Value),
                            (float)((numericUpDownRecLngEnd.Value - numericUpDownRecLngStart.Value) * _map.LngPx),
                            (float)((numericUpDownRecLatEnd.Value - numericUpDownRecLatStart.Value) * _map.LatPx));
                        break;
                    case 1:
                        var circle = GetCircle();
                        g.DrawEllipse(pen,
                            (float)_map.LngToPx(circle.Lng - circle.LngRadius),
                            (float)_map.LatToPx(circle.Lat + circle.LatRadius),
                            (float)(circle.LngRadius * _map.LngPx * 2),
                            (float)(circle.LatRadius * _map.LatPx * 2));
                        break;
                    case 2:
                        var coordinates = GetPolygonCoordinates();
                        if (coordinates.Count < 3)
                        {
                            break;
                        }
                        var poly = new PolygonS(coordinates);
                        g.DrawPolygon(pen, coordinates
                            .Select(coordinate => new PointF(
                                (float)_map.LngToPx(coordinate.Lng),
                                (float)_map.LatToPx(coordinate.Lat)))
                            .ToArray());
                        break;
                    case 3:
                        var incisions = GetIncisions();
                        foreach (var incision in incisions)
                        {
                            g.DrawPolygon(pen, incision
                                .GetPoints()
                                .Select(coordinate => new PointF(
                                    (float)_map.LngToPx(coordinate.Lng),
                                    (float)_map.LatToPx(coordinate.Lat)))
                                .ToArray());
                        }
                        break;
                }
                pictureBoxMap.Refresh();
            }
        }

        private void ButtonApplyCoord_Click(object sender, EventArgs e)
        {
            DrawOnMapAndLegendSelected();
        }

        private void Selection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DrawOnMapAndLegendSelected();
        }

        private void DataGridViewPolygon_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dataGridViewPolygon.Rows[e.RowIndex].Cells[0].Value = e.RowIndex + 1;
        }

        private void DataGridViewIncision_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dataGridViewIncision.Rows[e.RowIndex].Cells[0].Value = e.RowIndex + 1;
        }

        #endregion

        #region Tab2 Events

        private void CheckBoxNumeration_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxNumeration.Checked)
            {
                ResetCoupleMarkers();
                DrawOnMapAndLegendSelected();
            }

            groupBoxNumeration.Enabled = checkBoxNumeration.Checked;
        }

        private void ColorPickerNumeration_Click(object sender, EventArgs e)
        {
            colorDialog.Color = colorPickerNumeration.BackColor;
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            colorPickerNumeration.BackColor = colorDialog.Color;
        }

        private void ButtonNumerationApply_Click(object sender, EventArgs e)
        {
            DrawOnMap(_markersSelected);
        }

        #region RadioButton

        private void RadioButtonNumerationCommon_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxNumerationCommon.Enabled = radioButtonNumerationCommon.Checked;
        }

        private void RadioButtonCouple_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxCouple.Enabled = radioButtonCouple.Checked;

            if (!radioButtonCouple.Checked)
            {
                ResetCoupleMarkers();
                DrawOnMapAndLegendSelected();
            }
        }
        private void RadioButtonNumerationDepth_CheckedChanged(object sender, EventArgs e)
        {
            //var invariantCulture = CultureInfo.InvariantCulture;

            _markers.ForEach(m =>
            {
                m.SelectedNumberText = m.Depth.ToString();
            });

            DrawOnMap(_markersSelected);
        }

        #endregion

        private void ButtonNumerationCommonApply_Click(object sender, EventArgs e)
        {
            _markers.ForEach(m =>
            {
                m.SelectedNumberText = string.Empty;
            });

            int number = 1;

            foreach (var marker in _markersZoomed)
            {
                number++;
                marker.SelectedNumberText = number.ToString();
            }

            DrawOnMap(_markersSelected);
        }

        private void NumericUpDownCoupleKBegin_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownCoupleKEnd.Value = numericUpDownCoupleKBegin.Value + 1;
        }

        private void ButtonCoupleApply_Click(object sender, EventArgs e)
        {
            ResetCoupleMarkers();

            SelectAndZoomMarkers(true);

            double kBegin = (double)numericUpDownCoupleKBegin.Value;
            double kEnd = (double)numericUpDownCoupleKEnd.Value;
            int days = (int)numericUpDownCoupleDays.Value;
            var time = dateTimePickerCoupleTime.Value;
            var totalSeconds = new TimeSpan(days, time.Hour, time.Minute, time.Second).TotalSeconds;
            double distance = (int)numericUpDownCoupleDistance.Value;

            var markersCoupled = _markersZoomed.Where(m => m.K >= kBegin && m.K <= kEnd).ToList();

            #region Method 1

            int count = markersCoupled.Count;
            var options = new List<int>[count];
            var indexes = new List<List<int>>();
            var numbers = Enumerable.Range(0, count).Select(i => new List<int>()).ToList();

            for (int index = 0; index < count - 1; index++)
            {
                options[index] = new List<int>();

                for (int nextIndex = index + 1; nextIndex < count; nextIndex++)
                {
                    if (Math.Abs((markersCoupled[index].DateT0 - markersCoupled[nextIndex].DateT0).TotalSeconds) <=
                        totalSeconds &&
                        markersCoupled[index].Coordinate.DistanceTo(markersCoupled[nextIndex].Coordinate) <= distance)
                    {
                        markersCoupled[index].HasCouple = true;
                        markersCoupled[nextIndex].HasCouple = true;
                        options[index].Add(nextIndex);
                    }
                }

            }

            options[count - 1] = new List<int>();

            int indexesIndex = -1;
            int numberCoupled = 0;
            for (int index = count - 1; index >= 0; index--)
            {
                if (options[index].Count > 0)
                {
                    numberCoupled++;

                    while (options[index].Count > 0)
                    {
                        int relIndex = options[index][0];
                        options[index].RemoveAt(0);
                        indexesIndex++;
                        indexes.Add(new List<int>());
                        indexes[indexesIndex].Add(relIndex);
                        indexes[indexesIndex].Add(index);
                        numbers[index].Add(numberCoupled);
                        numbers[relIndex].Add(numberCoupled);

                        for (int prevIndex = index - 1; prevIndex >= 0; prevIndex--)
                        {
                            if (options[prevIndex].Count > 0 &&
                                options[prevIndex].Contains(indexes[indexesIndex]))
                            {
                                options[prevIndex] = options[prevIndex].Except(indexes[indexesIndex]).ToList();
                                indexes[indexesIndex].Add(prevIndex);
                                numbers[prevIndex].Add(numberCoupled);
                            }
                        }
                    }
                }
            }

            for (int index = 0; index < count; index++)
            {
                if (numbers[index].Count > 0)
                {
                    markersCoupled[index].CoupleGroups = numbers[index].ToArray();
                    markersCoupled[index].SelectedNumberText = string.Join("-", numbers[index]);
                }
            }

            #endregion

            DrawOnMapAndLegendSelected();
        }

        #endregion

        #region Tab3 Events

        private void CheckBoxLegend_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxLegend.Visible = checkBoxLegend.Checked;
            pictureBoxLegendOut.Visible = checkBoxLegendOut.Checked;
            DrawOnLegendsZoomed();
        }

        private void ButtonVpVs_Click(object sender, EventArgs e)
        {
            var colorSelection = new FormColorSelection(_settings.ColorsVpVs, _settings.ColorIntervalsVpVs/*, _settings.ColorType*/);

            if (colorSelection.ShowDialog() == DialogResult.OK)
            {
                _settings.ColorsVpVs = colorSelection.ReturnColors().ToArray();
                _settings.ColorIntervalsVpVs = colorSelection.ReturnInterval().ToArray();
                Marker.SetVpVsValues(_settings.ColorsVpVs, _settings.ColorIntervalsVpVs);
                _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();
                InitSpeedTrackBar();
                InitLegend();
                DrawOnMapAndLegendSelected();
                pictureBoxMap.Refresh();
            }
        }

        private void ButtonClass_Click(object sender, EventArgs e)
        {
            var radiusSelection = new FormRadiusSelection(
                _settings.Radiuses, _settings.Classes, _settings.ColorsClass,
                _settings.ColorIntervalsClass);
            if (radiusSelection.ShowDialog() == DialogResult.OK)
            {
                _settings.Radiuses = radiusSelection.ReturnRadiuses();
                _settings.Classes = radiusSelection.ReturnClasses();
                _settings.ColorsClass = radiusSelection.ReturnColors();
                _settings.ColorIntervalsClass = radiusSelection.ReturnIntervals();

                Marker.SetClassValue(_settings.Radiuses, _settings.Classes, _settings.ColorsClass,
                    _settings.ColorIntervalsClass);
                _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();
                InitEnergyTrackBar();
                InitLegend();
                InitLegendOut();
                DrawOnMapAndLegendSelected();
                pictureBoxMap.Refresh();
            }
        }

        private void TrackBarRadius_Scroll(object sender, EventArgs e)
        {
            double multRadius = trackBarMarkerRadius.Value / 10.0;
            Marker.SetRadiusMult(multRadius);
            labelMarkerRadius.Text = $"x {multRadius}";
            DrawOnMapAndLegendSelected();
            pictureBoxMap.Refresh();
        }

        private void RadioButtonMarkerColor_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonMarkerColorVpVs.Checked)
            {
                _settings.ColorType = MarkerColorType.VpVs;
                if (!checkBoxLegend.Enabled)
                {
                    checkBoxLegend.Checked = true;
                    checkBoxLegend.Enabled = true;
                }
            }
            else if (radioButtonMarkerColorClass.Checked)
            {
                _settings.ColorType = MarkerColorType.Class;
                checkBoxLegend.Checked = false;
                checkBoxLegend.Enabled = false;
            }

            Marker.SetColorType(_settings.ColorType);
            _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();
            InitLegend();
            DrawOnMapAndLegendSelected();
            pictureBoxMap.Refresh();
        }

        private void CheckBoxDontShowIfZero_CheckedChanged(object sender, EventArgs e)
        {
            DrawOnMapAndLegendSelected();
        }

        #endregion

        #region Tab4 Evets

        private void CheckBoxGrid_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxGridLines.Enabled = checkBoxGridLines.Checked;
            groupBoxGridDegrees.Enabled = checkBoxGridDegrees.Checked;

            pictureBoxGrid.Refresh();
        }

        private void NumericUpDownGridLines_ValueChanged(object sender, EventArgs e)
        {
            pictureBoxMap.Refresh();
            pictureBoxGrid.Refresh();
        }

        private void ColorPickerGridLines_Click(object sender, EventArgs e)
        {
            colorDialog.Color = colorPickerGridLines.BackColor;
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            colorPickerGridLines.BackColor = colorDialog.Color;
            pictureBoxMap.Refresh();
        }

        private void NumericUpDownGridDeg_ValueChanged(object sender, EventArgs e)
        {
            pictureBoxGrid.Refresh();
        }

        private void ColorPickerGridDeg_Click(object sender, EventArgs e)
        {
            colorDialog.Color = colorPickerGridDegrees.BackColor;
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            colorPickerGridDegrees.BackColor = colorDialog.Color;
            pictureBoxGrid.Refresh();
        }

        #endregion

        #region Tab5 Events

        private void NumericUpDownLatLngCount_ValueChanged(object sender, EventArgs e)
        {
            labelLatKm.Text = $"{Coordinate.LatToKm(_map.LatLengthDeg / (double)numericUpDownLatCount.Value):0.##} км.";
            labelLngKm.Text =
            $"{Coordinate.LngToKm((_map.LatStartDeg + _map.LatEndDeg) / 2.0, _map.LngLengthDeg / (double)numericUpDownLngCount.Value):0.00} км. на широте {(_map.LatStartDeg + _map.LatEndDeg) / 2.0}";
        }

        private (Label label, NumericUpDown numeric)[] _paramTuples;

        private void RadioButtonSurferDefault_CheckedChanged(object sender, EventArgs e)
        {
            SetFilterParamVisible();
            SetParamVisible();
        }

        private void RadioButtonActivity_CheckedChanged(object sender, EventArgs e)
        {
            SetFilterParamVisible();
            SetParamVisible(2, true);
            _paramTuples[0].label.Text = "Число землетрясений";
            _paramTuples[0].numeric.Value = 5;
            _paramTuples[1].label.Text = "Класс активности";
            _paramTuples[1].numeric.Minimum = 7;
            _paramTuples[1].numeric.Value = 8;
        }

        private void RadioButtonScoreField_CheckedChanged(object sender, EventArgs e)
        {
            var date = _markersSelected.FirstOrDefault()?.DateTime;
            dateTimePickerFilterParam1.Value = date ?? DateTime.Now;
            SetFilterParamVisible(true);
            SetParamVisible();
        }

        #endregion

        #region Tab6 Events

        #region Incision

        #region Stroke

        private void CheckBoxIncisionStroke_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxIncisionStroke.Enabled = checkBoxIncisionStroke.Checked;
        }

        private void CheckBoxIncisionUserColor_CheckedChanged(object sender, EventArgs e)
        {
            labelIncisionStrokeColor.Enabled = checkBoxIncisionStrokeUserColor.Checked;
            colorPickerIncisionStrokeColor.Enabled = checkBoxIncisionStrokeUserColor.Checked;
        }

        #endregion

        #region Fill

        private void CheckBoxIncisionFill_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxIncisionFill.Enabled = checkBoxIncisionFill.Checked;
        }

        private void CheckBoxIncisionFillColor_CheckedChanged(object sender, EventArgs e)
        {
            labelIncisionFillColor.Enabled = checkBoxIncisionFillUserColor.Checked;
            colorPickerIncisionFillColor.Enabled = checkBoxIncisionFillUserColor.Checked;
        }

        #endregion

        #region Distance

        private void CheckBoxIncisionDistanceLine_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxIncisionDistanceLine.Enabled = checkBoxIncisionDistanceLine.Checked;
        }

        private void CheckBoxIncisionDistanceNumber_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxIncisionDistanceNumber.Enabled = checkBoxIncisionDistanceNumber.Checked;
        }

        #endregion

        #region Depth

        private void CheckBoxIncisionDepthLine_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxIncisionDepthLine.Enabled = checkBoxIncisionDepthLine.Checked;
        }

        private void CheckBoxIncisionDepthNumber_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxIncisionDepthNumber.Enabled = checkBoxIncisionDepthNumber.Checked;
        }

        #endregion

        private void ColorPicker_Click(object sender, EventArgs e)
        {
            var colorPicker = (PictureBox)sender;

            colorDialog.Color = colorPicker.BackColor;
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;

            colorPicker.BackColor = colorDialog.Color;
        }

        #endregion

        #endregion

        #region PB Events

        private void PictureBoxMap_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxMap.Cursor = Cursors.SizeAll;

            _pointOld.X = e.X;
            _pointOld.Y = e.Y;
        }

        private void PictureBoxMap_MouseMove(object sender, MouseEventArgs e)
        {
            if (!pictureBoxMap.Capture)
                return;

            int dx = e.X - _pointOld.X,
                dy = e.Y - _pointOld.Y;
            _pointNew.X += dx;
            _pointNew.Y += dy;
            CheckOutOfBounds();
            _pointOld.X = e.X;
            _pointOld.Y = e.Y;

            pictureBoxMap.Refresh();
            pictureBoxGrid.Refresh();

            DrawOnLegendsZoomed();
        }

        private void PictureBoxMap_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxMap.Cursor = Cursors.Default;
        }

        private void PictureBoxMap_Paint(object sender, PaintEventArgs e)
        {
            // Перерисовываем карту с маркерами
            e.Graphics.DrawImage(pictureBoxMap.Image,
                new Rectangle(_pointNew, _currentSize),
                new Rectangle(Point.Empty, _map.MapImage.Size),
                GraphicsUnit.Pixel);

            // Рисуем сетку (линии)
            if (!checkBoxGridLines.Checked)
                return;

            var pen = new Pen(
                colorPickerGridLines.BackColor,
                (int)numericUpDownGridLinesWidth.Value)
            {
                DashStyle = DashStyle.Dash
            };

            float step = (_map.LngEndPx - _map.LngStartPx) / (float)numericUpDownGridLinesLng.Value;
            for (int i = 1; i < numericUpDownGridLinesLng.Value; i++)
            {
                float pos = (_map.LngStartPx + step * i) / _map.RatioF;
                e.Graphics.DrawLine(pen, pos, 0, pos, pictureBoxMap.Height);
            }

            step = (_map.LatEndPx - _map.LatStartPx) / (float)numericUpDownGridLinesLat.Value;
            for (int i = 1; i < numericUpDownGridLinesLat.Value; i++)
            {
                float pos = (_map.LatStartPx + step * i) / _map.RatioF;
                e.Graphics.DrawLine(pen, 0, pos, pictureBoxMap.Width, pos);
            }
        }



        private void PictureBoxGrid_Paint(object sender, PaintEventArgs e)
        {
            if (!checkBoxGridDegrees.Checked) return;
            var font = new Font("Arial", (int)numericUpDownGridDegreesWidth.Value);
            var brush = new SolidBrush(colorPickerGridDegrees.BackColor);
            var culture = CultureInfo.InvariantCulture;

            float diff = (_map.LngEndDeg - _map.LngStartDeg) /
                         (float)(trackBarZoom.Value * numericUpDownGridLinesLng.Value);
            float start = _map.LngStartDeg - _pointNew.X /
                         (_map.LngPx * trackBarZoom.Value / _map.RatioF);
            float step = (_map.LngEndPx - _map.LngStartPx) / (float)numericUpDownGridLinesLng.Value;
            for (int i = 0; i <= numericUpDownGridLinesLng.Value; i++)
            {
                float val = start + i * diff;
                float pos = (_map.LngStartPx + step * i) / _map.RatioF;
                e.Graphics.DrawString(string.Format(culture, "{0:f2}°", val), font, brush, pos + 20, 3);
                e.Graphics.DrawString(string.Format(culture, "{0:f2}°", val), font, brush, pos + 20,
                    pictureBoxGrid.Height - 20);
            }

            diff = (_map.LatEndDeg - _map.LatStartDeg) /
                   (float)(trackBarZoom.Value * numericUpDownGridLinesLat.Value);
            start = _map.LatEndDeg + _pointNew.Y /
                   (_map.LatPx * trackBarZoom.Value / _map.RatioF);
            step = (_map.LatEndPx - _map.LatStartPx) / (float)numericUpDownGridLinesLat.Value;
            for (int i = 0; i <= numericUpDownGridLinesLat.Value; i++)
            {
                float val = start - i * diff;
                float pos = (_map.LatStartPx + step * i) / _map.RatioF;
                DrawRotatedTextAt(e.Graphics, -45, string.Format(culture, "{0:f2}°", val), -5, pos + 45, font, brush);
                DrawRotatedTextAt(e.Graphics, -45, string.Format(culture, "{0:f2}°", val), pictureBoxGrid.Width - 40,
                    pos + 45, font, brush);
            }
        }

        #endregion

        private void TrackBarZoom_Scroll(object sender, EventArgs e)
        {
            ZoomMap();

            labelZoom.Text = $"x{trackBarZoom.Value}";
        }

        #endregion

        #region Logic

        #region Draw

        // Рисуем строку по определенному углу
        private void DrawRotatedTextAt(Graphics graph, float angle, string text, float x, float y, Font font, Brush brush)
        {
            var state = graph.Save();
            graph.ResetTransform();
            graph.RotateTransform(angle);
            graph.TranslateTransform(x, y, MatrixOrder.Append);
            graph.DrawString(text, font, brush, 0, 0);
            graph.Restore(state);
        }

        private void DrawOnMap(List<Marker> markers)
        {
            pictureBoxMap.Image = _map.GetMapCopy();
            using (var graph = Graphics.FromImage(pictureBoxMap.Image))
            {
                var pen = new Pen(Color.Black, 1);

                var font = new Font("Arial", (float)numericUpDownNumerationWidth.Value / trackBarZoom.Value);
                Func<Marker, float, Font> getFont;
                if (checkBoxNumerationAutoSize.Checked)
                {
                    getFont = (marker, rectangleWidth) =>
                    {
                        var size = graph.MeasureString(marker.SelectedNumberText, font);
                        if (size.Width > rectangleWidth)
                        {
                            return new Font("Arial", rectangleWidth / marker.SelectedNumberText.Length);
                        }

                        return font;
                    };
                }
                else
                {
                    getFont = (marker, rectangleWidth) => font;
                }

                var brush = new SolidBrush(colorPickerNumeration.BackColor);
                var format = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center,
                    //Trimming = StringTrimming.Word
                };

                foreach (var marker in markers)
                {
                    int radius = marker.Radius / trackBarZoom.Value;
                    float width = marker.Width - radius / _map.RatioF,
                        height = marker.Height - radius / _map.RatioF;

                    graph.FillEllipse(new SolidBrush(marker.Color),
                        width, height, radius, radius);
                    graph.DrawEllipse(pen, width, height, radius, radius);

                    var rectangle = new RectangleF(width, height, radius, radius);

                    graph.DrawString(marker.SelectedNumberText, getFont(marker, rectangle.Width), brush, rectangle, format);
                }
            }
        }

        private void DrawOnLegends(List<Marker> markers)
        {
            var font = new Font("Arial", 18);
            var format = new StringFormat
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };

            #region Legend

            if (checkBoxLegend.Checked)
            {
                pictureBoxLegend.Image = _map.GetLegendCopy();
                using (var graph = Graphics.FromImage(pictureBoxLegend.Image))
                {
                    var colors = _settings.ColorsVpVs;

                    int colorLen = colors.Length,
                        start = 50,
                        spaceValue = 30;
                    int space = pictureBoxLegend.Image.Width / colorLen;
                    for (int i = 0; i < colorLen; i++)
                    {
                        //TODO: изменить логику
                        int count = markers.Count(m => m.ColorIndexVpVs == i);
                        if (count == 0 && checkBoxDontShowIfZero.Checked)
                        {
                            graph.FillRectangle(Brushes.White, i * space, 0, space, pictureBoxLegend.Image.Height);
                            continue;
                        }
                        var rectangle = new Rectangle(i * space, start, space, spaceValue);
                        graph.DrawString($"({count})", font, Brushes.Black, rectangle, format);
                    }
                }
            }

            #endregion

            #region LegendOut

            if (checkBoxLegendOut.Checked)
            {
                var pen = new Pen(Color.Black);
                pictureBoxLegendOut.Image = _map.GetLegendOutCopy();
                using (var graph = Graphics.FromImage(pictureBoxLegendOut.Image))
                {
                    var classes = markers.Select(m => m.Class).Distinct().ToArray();
                    Array.Sort(classes);
                    int len = classes.Length;
                    if (len != 0)
                    {
                        int start = 40,
                            //max = classes[len - 1],
                            maxSpace = Math.Min(_map.LegendOutImage.Width / len, _map.LegendOutImage.Height - start),
                            //mult = Math.Min(maxSpace, markers.FirstOrDefault(m => m.Class == max).Radius) / max,
                            middle = _map.LegendOutImage.Height / 2;
                        graph.Clear(Color.White);
                        for (int i = 0; i < len; i++)
                        {
                            //int radius = classes[i] * mult,
                            //int radius = markers.FirstOrDefault(m => m.Class == classes[i]).Radius,
                            int radius = Marker.GetRadius(classes[i]),
                                space = middle - start / 2 - radius / 2;
                            int count = markers.Count(m => m.Class == classes[i]);
                            var rectangle = new Rectangle(i * maxSpace + (maxSpace - radius) / 2, space, radius, radius);
                            var markerBrush = _settings.ColorType == MarkerColorType.Class ? new SolidBrush(Marker.GetColor(classes[i])) : Brushes.Bisque;
                            graph.FillEllipse(markerBrush, rectangle);
                            graph.DrawEllipse(pen, rectangle);
                            graph.DrawString($"{classes[i]}", font, Brushes.Black, rectangle, format);
                            rectangle = new Rectangle(i * maxSpace, pictureBoxLegendOut.Image.Height - start, maxSpace, start);
                            graph.DrawString($"({count})", font, Brushes.Black, rectangle, format);
                        }
                    }
                }
            }

            #endregion
        }

        private void DrawOnMapAndLegendSelected()
        {
            SelectAndZoomMarkers();
            DrawOnMap(_markersSelected);

            DrawOnLegendsZoomed();
        }

        private void DrawOnLegendsZoomed()
        {
            if (!checkBoxLegend.Checked && !checkBoxLegendOut.Checked)
                return;

            ZoomMarkers();
            DrawOnLegends(_markersZoomed);
        }

        #endregion

        #region Select

        private void SelectAndZoomMarkers(bool coupleIgnore = false)
        {
            SelectMarkers(coupleIgnore);
            ZoomMarkers();
        }

        private void SelectMarkers(bool coupleIgnore = false)
        {
            IEnumerable<Marker> markers = _markers;

            #region Couple

            if (checkBoxNumeration.Checked && radioButtonCouple.Checked && !coupleIgnore)
            {
                markers = markers.Where(m => m.HasCouple);
            }

            #endregion

            #region Energy

            if (trackBarKmax.Value == trackBarKmin.Value)
            {
                markers = markers.Where(m => m.RadiusIndex == trackBarKmax.Value);
            }
            else
            {
                if (trackBarKmax.Value != trackBarKmax.Maximum)
                    markers = markers.Where(m => m.RadiusIndex <= trackBarKmax.Value);

                if (trackBarKmin.Value != trackBarKmin.Minimum)
                    markers = markers.Where(m => m.RadiusIndex >= trackBarKmin.Value);
            }

            #endregion

            #region Speed

            if (trackBarVmin.Value == trackBarVmax.Value)
            {
                markers = markers.Where(m => m.ColorIndexVpVs == trackBarVmax.Value);
            }
            else
            {
                if (trackBarVmax.Value != trackBarVmax.Maximum)
                    markers = markers.Where(m => m.ColorIndexVpVs <= trackBarVmax.Value);

                if (trackBarVmin.Value != trackBarVmin.Minimum)
                    markers = markers.Where(m => m.ColorIndexVpVs >= trackBarVmin.Value);
            }

            #endregion

            #region Coord

            if (checkBoxCoord.Checked)
            {
                switch (tabControlCoord.SelectedIndex)
                {
                    case 0:
                        var rectangle = GetRectangle();
                        markers = markers.Where(m => rectangle.IsInside(m.Lat, m.Lng));
                        break;
                    case 1:
                        var circle = GetCircle();
                        markers = markers.Where(m => circle.IsInside(m.Lat, m.Lng));
                        break;
                    case 2:
                        try
                        {
                            var coordinates = GetPolygonCoordinates();
                            if (coordinates.Count < 3)
                            {
                                break;
                            }
                            var polygon = new PolygonS(coordinates);
                            markers = markers.Where(m => polygon.IsInside(m.Lat, m.Lng));
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Данные в таблице полигонов записаны некорректно!");
                        }
                        break;
                    case 3:
                        try
                        {
                            var incisions = GetIncisions();
                            markers = markers.Where(m => incisions.Any(i => i.IsInside(m.Lat, m.Lng)));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"Данные в таблице полигонов записаны некорректно!\r\nПричина: {ex.Message}", "Ошибка");
                        }
                        break;
                }
            }

            #endregion

            #region Date

            if (checkBoxDate.Checked)
            {
                markers = markers.Where(m =>
                    m.Date >= dateTimePickerStart.Value.Date &&
                    m.Date <= dateTimePickerEnd.Value.Date);
            }

            #endregion

            #region Time

            if (checkBoxTime.Checked)
            {
                var startTime = dateTimePickerStart.Value.TimeOfDay;
                var endTime = dateTimePickerEnd.Value.TimeOfDay;
                markers = markers.Where(m =>
                    m.T0.TimeOfDay >= startTime &&
                    m.T0.TimeOfDay <= endTime);
            }

            #endregion

            #region Depth

            if (checkBoxDepth.Checked)
            {
                markers = markers.Where(m =>
                    m.Depth >= (double)numericUpDownStartDepth.Value &&
                    m.Depth <= (double)numericUpDownEndDepth.Value);
            }

            #endregion

            _markersSelected = markers.ToList();
        }

        private void ZoomMarkers()
        {
            if (trackBarZoom.Value > 1)
            {
                var rectangleS = GetZoomRectangle();
                _markersZoomed = _markersSelected.Where(m => rectangleS.IsInside(m.Coordinate)).ToList();
            }
            else
            {
                _markersZoomed = _markersSelected;
            }
        }

        private List<Earthquake> SelectEarthquakes()
        {
            SelectAndZoomMarkers();

            return _earthquakes
                .Where(e => _markersZoomed.Any(m => e.Number == m.Number)).ToList();
        }

        private List<Earthquake> SelectEarthquakeForSurfer(EarthquakeScalarType scalarType)
        {
            if (scalarType != EarthquakeScalarType.ScoreField)
            {
                return SelectEarthquakes()
                    .Where(eq => eq.Lat >= 41 &&
                                 eq.Lat <= 45 &&
                                 eq.Lng >= 45 &&
                                 eq.Lng <= 49)
                    .ToList();
            }

            var first = SelectEarthquakes().FirstOrDefault(eq => eq.DateTime == dateTimePickerFilterParam1.Value);
            return first is null ? Enumerable.Empty<Earthquake>().ToList() : new List<Earthquake> { first };
        }

        #endregion

        #region Init

        private void InitMenu()
        {
            foreach (var pair in _settings.MapSettings)
            {
                AddMapMenu(pair.Key, pair.Value);
            }
        }

        /// <summary>
        /// Инициализирует карты
        /// </summary>
        private void InitMap()
        {
            // pb
            int gridAddWidth = 70, gridAddHeight = 50;
            // Карта
            pictureBoxMap.Parent = pictureBoxGrid;
            pictureBoxMap.Image = (Image)_map.MapImage.Clone();
            pictureBoxMap.Location = new Point(gridAddWidth / _map.Ratio, gridAddHeight / _map.Ratio);
            pictureBoxMap.Size =
                new Size(_map.MapImage.Width / _map.Ratio, _map.MapImage.Height / _map.Ratio);
            pictureBoxMap.SizeMode = PictureBoxSizeMode.Zoom;
            _currentSize = pictureBoxMap.Size;
            // Сетка
            pictureBoxGrid.Location = new Point(0, 0);
            pictureBoxGrid.Size =
                new Size(pictureBoxMap.Width + gridAddWidth, pictureBoxMap.Height + gridAddHeight);
        }

        /// <summary>
        /// Инициализирует легенду
        /// </summary>
        private void InitLegend()
        {
            // image
            int legendHeight = 40;
            var size = new Size(_map.MapImage.Width, legendHeight * _map.Ratio);
            using (Image imgLegend = new Bitmap(size.Width, size.Height))
            {
                using (var graph = Graphics.FromImage(imgLegend))
                {
                    graph.Clear(Color.White);

                    var font = new Font("Arial", 18);
                    var pen = new Pen(Color.Black, 1);
                    var format = new StringFormat
                    {
                        LineAlignment = StringAlignment.Center,
                        Alignment = StringAlignment.Center
                    };
                    var culture = CultureInfo.InvariantCulture;

                    var colors = _settings.ColorsVpVs;
                    var intervals = _settings.ColorIntervalsVpVs;

                    int colorLen = colors.Length,
                        spaceValue = 30,
                        spaceColor = 20;
                    int space = size.Width / colorLen;
                    // Цвета
                    for (int i = 0; i < colorLen; i++)
                    {
                        var rectangle = new Rectangle(i * space, 0, space, spaceColor);
                        graph.FillRectangle(new SolidBrush(colors[i]), rectangle);
                        rectangle = new Rectangle(i * space, spaceColor, space, spaceValue);

                        if (i == 0)
                            graph.DrawString(string.Format(culture, "< {0}", intervals[0]), font, Brushes.Black, rectangle, format);
                        else if (i == colorLen - 1)
                            graph.DrawString(string.Format(culture, " {0} ≤", intervals[colorLen - 2]), font, Brushes.Black, rectangle, format);
                        else
                            graph.DrawString(string.Format(culture, "{0:0.##} - {1:0.##}",
                                intervals[i - 1], intervals[i]), font, Brushes.Black, rectangle, format);
                        rectangle.Height += spaceValue * 2;
                    }
                    // Рамка
                    graph.DrawRectangle(pen, 0, 0, imgLegend.Width - 1, imgLegend.Height - 1);
                }
                _map.LegendImage = (Image)imgLegend.Clone();
            }

            // pb
            pictureBoxLegend.Image = (Image)_map.LegendImage.Clone();
            pictureBoxLegend.Parent = pictureBoxMap;
            pictureBoxLegend.Size = new Size(size.Width / _map.Ratio, size.Height / _map.Ratio);
            pictureBoxLegend.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxLegend.Location =
                new Point(0, 0);
        }

        /// <summary>
        /// Инициализирует 2ю легенду
        /// </summary>
        private void InitLegendOut()
        {
            // image
            int legendOutHeight = 100;
            using (Image imgLegend = new Bitmap(pictureBoxGrid.Width * _map.Ratio, legendOutHeight * _map.Ratio))
            {
                using (var graph = Graphics.FromImage(imgLegend))
                {
                    graph.Clear(Color.AliceBlue);
                }
                _map.LegendOutImage = (Image)imgLegend.Clone();
            }
            // pb
            pictureBoxLegendOut.Image = (Image)_map.LegendOutImage.Clone();
            pictureBoxLegendOut.Location = new Point(0, pictureBoxGrid.Height);
            pictureBoxLegendOut.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxLegendOut.Size =
                new Size(pictureBoxGrid.Width, legendOutHeight);
        }

        private void InitPanelImage()
        {
            //panelImages.MinimumSize = new Size(pictureBoxGrid.Width, pictureBoxGrid.Height + pictureBoxLegendOut.Height);
        }

        /// <summary>
        /// Инициализирует маркеры
        /// </summary>
        private void InitMarkers()
        {
            Marker.Map = _map;
            Marker.SetVpVsValues(_settings.ColorsVpVs, _settings.ColorIntervalsVpVs);
            Marker.SetClassValue(_settings.Radiuses, _settings.Classes, _settings.ColorsClass, _settings.ColorIntervalsClass);
            Marker.SetRadiusMult(_settings.RadiusMult);
            Marker.SetColorType(_settings.ColorType);
        }

        private void RefreshMarkers()
        {
            _markers = _earthquakes.Select(eq => new Marker(eq)).ToList();
            DrawOnMapAndLegendSelected();
        }

        private void InitTab1()
        {
            InitEnergyTrackBar();
            InitSpeedTrackBar();
            InitCoordNumeric();
            InitDatePicker();
            InitTimePicker();
            InitDepthNumeration();
        }

        private void InitTab4()
        {
            _paramTuples = new[]
            {
                (labelParam1, numericUpDownParam1),
                (labelParam2, numericUpDownParam2)
            };
            SetFilterParamVisible();
            SetParamVisible();
        }

        private void InitSettingsValues()
        {
            #region Init Tab1

            numericUpDownNumerationWidth.Value = _settings.NumerationWidth;
            colorPickerNumeration.BackColor = _settings.NumerationColor;

            #endregion

            #region Init Tab2

            trackBarMarkerRadius.Value = (int)(_settings.RadiusMult * 10);
            labelMarkerRadius.Text = $"x {_settings.RadiusMult}";
            checkBoxDontShowIfZero.Checked = _settings.DontShowIfZero;
            SetColorType(_settings.ColorType);

            foreach (var coordinate in _settings.PolygonCoordinates)
            {
                dataGridViewPolygon.Rows.Add(null, coordinate.Lat, coordinate.Lng);
            }

            foreach (var value in _settings.IncisionsValues)
            {
                dataGridViewIncision.Rows.Add(null,
                    value.StartLat, value.StartLng,
                    value.EndLat, value.EndLng,
                    value.Length);
            }
            //    

            #endregion

            #region Init Tab3

            numericUpDownGridDegreesWidth.Value = _settings.GridDegreesWidth;
            colorPickerGridDegrees.BackColor = _settings.GridDegreesColor;

            numericUpDownGridLinesLat.Value = _settings.GridLinesLatCount;
            numericUpDownGridLinesLng.Value = _settings.GridLinesLngCount;
            numericUpDownGridLinesWidth.Value = _settings.GridLinesWidth;
            colorPickerGridLines.BackColor = _settings.GridLinesColor;

            #endregion

            #region Init Tab4

            numericUpDownLatCount.Value = _settings.SurferLatCount;
            numericUpDownLngCount.Value = _settings.SurferLngCount;

            #endregion

            #region Init Tab5

            checkBoxIncisionStroke.Checked = _settings.IncisionStroke;
            checkBoxIncisionStrokeUserColor.Checked = _settings.IncisionStrokeUserColor;
            colorPickerIncisionStrokeColor.BackColor = _settings.IncisionStrokeColor;
            numericUpDownIncisionStrokeWidth.Value = (decimal)_settings.IncisionStrokeWidth;
            numericUpDownIncisionStrokeAlpha.Value = (decimal)_settings.IncisionStrokeAlpha;
            checkBoxIncisionFill.Checked = _settings.IncisionFill;
            checkBoxIncisionFillUserColor.Checked = _settings.IncisionFillUserColor;
            colorPickerIncisionFillColor.BackColor = _settings.IncisionFillColor;
            numericUpDownIncisionFillAlpha.Value = (decimal)_settings.IncisionFillAlpha;
            checkBoxIncisionDistanceLine.Checked = _settings.IncisionDistanceLine;
            colorPickerIncisionDistanceLineColor.BackColor = _settings.IncisionDistanceLineColor;
            numericUpDownIncisionDistanceLineWidth.Value = (decimal)_settings.IncisionDistanceLineWidth;
            numericUpDownIncisionDistanceLineMult.Value = _settings.IncisionDistanceLineMult;
            checkBoxIncisionDistanceNumber.Checked = _settings.IncisionDistanceNumber;
            colorPickerIncisionDistanceNumberColor.BackColor = _settings.IncisionDistanceNumberColor;
            numericUpDownIncisionDistanceNumberFontSize.Value = (decimal)_settings.IncisionDistanceNumberFontSize;
            numericUpDownIncisionDistanceNumberMult.Value = _settings.IncisionDistanceNumberMult;
            checkBoxIncisionDepthLine.Checked = _settings.IncisionDepthLine;
            colorPickerIncisionDepthLineColor.BackColor = _settings.IncisionDepthLineColor;
            numericUpDownIncisionDepthLineWidth.Value = (decimal)_settings.IncisionDepthLineWidth;
            numericUpDownIncisionDepthLineMult.Value = _settings.IncisionDepthLineMult;
            checkBoxIncisionDepthNumber.Checked = _settings.IncisionDepthNumber;
            colorPickerIncisionDepthNumberColor.BackColor = _settings.IncisionDepthNumberColor;
            numericUpDownIncisionDepthNumberFontSize.Value = (decimal)_settings.IncisionDepthNumberFontSize;
            numericUpDownIncisionDepthNumberMult.Value = _settings.IncisionDepthNumberMult;
            numericUpDownIncisionRadiusMult.Value = _settings.IncisionRadiusMult;
            numericUpDownIncisionRadiusFontSize.Value = (decimal)_settings.IncisionRadiusFontSize;

            SetIncisionPriority(_settings.IncisionPriority);

            #endregion
        }

        #region Init Tab1

        /// <summary>
        /// Инициализирует trackbar энергии
        /// </summary>
        private void InitEnergyTrackBar()
        {
            // Энергия
            trackBarKmax.Maximum = _settings.Classes.Length - 1;
            trackBarKmax.Minimum = 0;
            trackBarKmax.Value = trackBarKmax.Maximum;

            trackBarKmin.Maximum = _settings.Classes.Length - 1;
            trackBarKmin.Minimum = 0;
            trackBarKmin.Value = trackBarKmin.Minimum;

            labelKMinMax.Text = $@"K = {_settings.Classes[trackBarKmin.Value]} - {_settings.Classes[trackBarKmax.Value]}";
        }

        /// <summary>
        /// Инициализирует trackbar отношения скорости
        /// </summary>
        private void InitSpeedTrackBar()
        {
            // Скорость
            trackBarVmax.Maximum = _settings.ColorsVpVs.Length - 1;
            trackBarVmax.Minimum = 0;
            trackBarVmax.Value = trackBarVmax.Maximum;

            trackBarVmin.Maximum = _settings.ColorsVpVs.Length - 1;
            trackBarVmin.Minimum = 0;
            trackBarVmin.Value = trackBarVmax.Minimum;

            labelVMinMax.Text = $"V = < {_settings.ColorIntervalsVpVs[trackBarVmin.Value]} - {_settings.ColorIntervalsVpVs[trackBarVmax.Value - 1]} ≤";
        }

        private void InitCoordNumeric()
        {
            numericUpDownRecLatStart.Value = _map.LatStartDeg;
            numericUpDownRecLatEnd.Value = _map.LatEndDeg;
            numericUpDownRecLngStart.Value = _map.LngStartDeg;
            numericUpDownRecLngEnd.Value = _map.LngEndDeg;

            numericUpDownCenterLng.Value = (decimal)((_map.LngStartDeg + _map.LngEndDeg) / 2.0);
            numericUpDownCenterLat.Value = (decimal)((_map.LatStartDeg + _map.LatEndDeg) / 2.0);
            var dagestanCoord = new Coordinate((_map.LatStartDeg + _map.LatEndDeg) / 2.0, (_map.LngStartDeg + _map.LngEndDeg) / 2.0);
            numericUpDownRadius.Value = (decimal)(
                Math.Sqrt(
                    Math.Pow(((double)numericUpDownCenterLng.Value - _map.LngStartDeg) * dagestanCoord.LatKm, 2)
                    + Math.Pow((double)(numericUpDownCenterLat.Value - _map.LatStartDeg) * dagestanCoord.LngKm, 2))
                / 2.0);
        }

        private void InitDatePicker()
        {
            var min = DateTime.Today;
            var max = DateTime.Today;
            if (_markers.Count > 0)
            {
                min = _markers.Min(m => m.Date);
                max = _markers.Max(m => m.Date);
            }

            string errorMessage = string.Empty;
            if (min < dateTimePickerStart.MinDate)
            {
                errorMessage += $"Минимальная дата в файле ({min:d}) оказалась ниже возможной: {dateTimePickerStart.MinDate:d}";
                dateTimePickerStart.Value = dateTimePickerStart.MinDate;
            }
            else
            {
                dateTimePickerStart.Value = min;
            }

            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                MessageBox.Show(errorMessage, "Внимание");
            }



            dateTimePickerEnd.Value = max.AddDays(1).AddSeconds(-1);
        }

        private void InitTimePicker()
        {
            timeTimePickerStart.Value = DateTime.Today;
            timeTimePickerEnd.Value = DateTime.Today.AddDays(1).AddSeconds(-1);
        }

        private void InitDepthNumeration()
        {
            double min = 3;
            double max = 80;

            if (_markers.Count > 0)
            {
                min = _markers.Min(m => m.Depth);
                max = _markers.Max(m => m.Depth);
            }

            numericUpDownStartDepth.Minimum = (decimal)min;
            numericUpDownStartDepth.Maximum = (decimal)max;
            numericUpDownStartDepth.Value = (decimal)min;

            numericUpDownEndDepth.Minimum = (decimal)min;
            numericUpDownEndDepth.Maximum = (decimal)max;
            numericUpDownEndDepth.Value = (decimal)max;
        }

        #endregion

        #endregion

        #region Zoom

        private void ZoomMap()
        {
            _currentSize.Width = pictureBoxMap.Width * trackBarZoom.Value;
            _currentSize.Height = pictureBoxMap.Height * trackBarZoom.Value;

            CheckOutOfBounds();

            pictureBoxMap.Refresh();
            pictureBoxGrid.Refresh();
            // TODO: Удалить
            //pictureBoxLegendOut.Refresh();

            DrawOnMapAndLegendSelected();
        }

        private RectangleS GetZoomRectangle()
        {
            return new RectangleS(
                _map.LatEndDeg + (_pointNew.Y - pictureBoxMap.Height) / (_map.LatPx * trackBarZoom.Value / _map.RatioF),
                _map.LngStartDeg - _pointNew.X / (_map.LngPx * trackBarZoom.Value / _map.RatioF),
                _map.LatEndDeg + _pointNew.Y / (_map.LatPx * trackBarZoom.Value / _map.RatioF),
                _map.LngStartDeg - (_pointNew.X - pictureBoxMap.Width) / (_map.LngPx * trackBarZoom.Value / _map.RatioF)
            );
        }

        /// <summary>
        /// Проверка выхода за границы pictureBoxMap нашей карты
        /// </summary>
        private void CheckOutOfBounds()
        {
            if (Math.Abs(_pointNew.X) + pictureBoxMap.Width > _currentSize.Width)
                _pointNew.X = pictureBoxMap.Width - _currentSize.Width;
            else if (_pointNew.X > 0)
                _pointNew.X = 0;

            if (Math.Abs(_pointNew.Y) + pictureBoxMap.Height > _currentSize.Height)
                _pointNew.Y = pictureBoxMap.Height - _currentSize.Height;
            else if (_pointNew.Y > 0)
                _pointNew.Y = 0;
        }

        #endregion

        #region Coord Helpers

        private RectangleS GetRectangle()
        {
            return new RectangleS(
                (double)numericUpDownRecLatStart.Value,
                (double)numericUpDownRecLngStart.Value,
                (double)numericUpDownRecLatEnd.Value,
                (double)numericUpDownRecLngEnd.Value);
        }

        private CircleS GetCircle()
        {
            return new CircleS(
                (double)numericUpDownCenterLat.Value,
                (double)numericUpDownCenterLng.Value,
                (double)numericUpDownRadius.Value);
        }

        private List<Coordinate> GetPolygonCoordinates(bool showWarningMessage = true)
        {
            var coordinates = new List<Coordinate>();

            bool isOne = false;

            foreach (DataGridViewRow row in dataGridViewPolygon.Rows)
            {
                if (row.Cells[1].Value == null && row.Cells[2].Value == null)
                {
                    continue;
                }

                if (row.Cells[1].Value == null || row.Cells[2].Value == null)
                {
                    isOne = true;
                    continue;
                }

                coordinates.Add(new Coordinate(ParseDouble(row.Cells[1].Value), ParseDouble(row.Cells[2].Value)));
            }

            if (showWarningMessage && isOne)
            {
                MessageBox.Show("Один параметр широты или долготы не был заполнен!", "Внимание");
            }

            return coordinates;
        }

        private List<IncisionS> GetIncisions()
        {
            return GetIncisionsValues().Select(value =>
                new IncisionS(value.StartLat, value.StartLng, value.EndLat, value.EndLng, value.Length)).ToList();
        }

        private List<IncisionValue> GetIncisionsValues(bool showWarningMessage = true)
        {
            var incisionsValues = new List<IncisionValue>();

            bool isOne = false;

            foreach (DataGridViewRow row in dataGridViewIncision.Rows)
            {
                if (row.Cells[1].Value == null &&
                    row.Cells[2].Value == null &&
                    row.Cells[3].Value == null &&
                    row.Cells[4].Value == null &&
                    row.Cells[5].Value == null)
                {
                    continue;
                }

                if (row.Cells[1].Value == null ||
                    row.Cells[2].Value == null ||
                    row.Cells[3].Value == null ||
                    row.Cells[4].Value == null ||
                    row.Cells[5].Value == null)
                {
                    isOne = true;
                    continue;
                }

                incisionsValues.Add(new IncisionValue
                {
                    StartLat = ParseDouble(row.Cells[1].Value),
                    StartLng = ParseDouble(row.Cells[2].Value),
                    EndLat = ParseDouble(row.Cells[3].Value),
                    EndLng = ParseDouble(row.Cells[4].Value),
                    Length = ParseDouble(row.Cells[5].Value)
                });
            }

            if (showWarningMessage && isOne)
            {
                MessageBox.Show("Один параметр широты или долготы не был заполнен!");
            }

            return incisionsValues;
        }

        #endregion

        private double ParseDouble(object value)
        {
            if (double.TryParse($"{value}".Replace(",", "."), NumberStyles.Float, CultureInfo.InvariantCulture, out var result))
            {
                return result;
            }

            throw new Exception();
        }

        private void ResetCoupleMarkers()
        {
            _markers.ForEach(m =>
            {
                m.SelectedNumberText = string.Empty;
                m.CoupleGroups = new int[0];
                m.HasCouple = false;
            });
        }

        private IAreaFigureS GetFigure()
        {
            IAreaFigureS figure;

            switch (tabControlCoord.SelectedIndex)
            {
                case 0:
                    figure = GetRectangle();
                    break;
                case 1:
                    figure = GetCircle();
                    break;
                case 2:
                    figure = new PolygonS(GetPolygonCoordinates());
                    break;
                case 3:
                    var figures = GetIncisions();
                    if (figures.Count > 1)
                    {
                        MessageBox.Show("Для рассчета будет выбран только 1й разрез!", "Внимание");
                    }
                    figure = figures[0];
                    break;
                default:
                    return null;
            }

            return figure;
        }

        #region Image

        private IncisionPriority GetIncisionPriority()
        {
            if (radioButtonIncisionPriorityDistance.Checked)
            {
                return IncisionPriority.Distance;
            }

            if (radioButtonIncisionPriorityRadius.Checked)
            {
                return IncisionPriority.Radius;
            }

            throw new Exception("Не выбран параметр");
        }

        private void SetIncisionPriority(IncisionPriority priority)
        {
            switch (priority)
            {
                case IncisionPriority.Distance:
                    radioButtonIncisionPriorityDistance.Checked = true;
                    break;
                case IncisionPriority.Radius:
                    radioButtonIncisionPriorityRadius.Checked = true;
                    break;
            }
        }

        private MarkerColorType GetColorType()
        {
            if (radioButtonMarkerColorVpVs.Checked)
            {
                return MarkerColorType.VpVs;
            }

            if (radioButtonMarkerColorClass.Checked)
            {
                return MarkerColorType.Class;
            }

            throw new Exception("Не выбран параметр");
        }

        private void SetColorType(MarkerColorType colorType)
        {
            switch (colorType)
            {
                case MarkerColorType.VpVs:
                    radioButtonMarkerColorVpVs.Checked = true;
                    break;
                case MarkerColorType.Class:
                    radioButtonMarkerColorClass.Checked = true;
                    break;
            }
        }

        #endregion

        #region Surfer

        private EarthquakeScalarType GetScalarType()
        {
            if (radioButtonDensity.Checked)
            {
                return EarthquakeScalarType.Density;
            }
            if (radioButtonV.Checked)
            {
                return EarthquakeScalarType.VpVs;
            }
            if (radioButtonStationV.Checked)
            {
                return EarthquakeScalarType.StationVpVs;
            }
            if (radioButtonActivity.Checked)
            {
                return EarthquakeScalarType.Activity;
            }
            if (radioButtonScoreField.Checked)
            {
                return EarthquakeScalarType.ScoreField;
            }

            throw new Exception("Не выбран параметр");
        }

        private void SetParamVisible(bool value = false)
        {
            for (int i = 0; i < _paramTuples.Length; i++)
            {
                _paramTuples[i].label.Visible = value;
                _paramTuples[i].numeric.Visible = value;
            }
        }

        private void SetParamVisible(int count, bool value = false)
        {
            if (count <= _paramTuples.Length)
            {
                for (int i = 0; i < _paramTuples.Length; i++)
                {
                    _paramTuples[i].label.Visible = value;
                    _paramTuples[i].numeric.Visible = value;
                }

                for (int i = count; i < _paramTuples.Length; i++)
                {
                    _paramTuples[i].label.Visible = !value;
                    _paramTuples[i].numeric.Visible = !value;
                }
            }
        }

        private void SetFilterParamVisible(bool value = false) => groupBoxSurferFilter.Visible = value;

        private decimal[] GetParamValues() => _paramTuples.Select(t => t.numeric.Value).ToArray();

        #endregion

        #region Images

        private void CreateAndSaveImage(string filePath, ImageFormat imageFormat)
        {
            var size = new Size(pictureBoxGrid.Width * _map.Ratio, pictureBoxGrid.Height * _map.Ratio);

            var gridSize = new Size(pictureBoxGrid.Width - pictureBoxMap.Width,
                pictureBoxGrid.Height - pictureBoxMap.Height);

            int legendHeight = checkBoxLegendOut.Checked ? pictureBoxLegendOut.Height * _map.Ratio : 0;

            SelectAndZoomMarkers();//.ToList();

            using (Image imgForSave = new Bitmap(size.Width, size.Height + legendHeight))
            {
                using (var graph = Graphics.FromImage(imgForSave))
                {
                    // Рисуем чистую карту
                    graph.DrawImage(_map.MapImage,
                        _pointNew.X * _map.Ratio + gridSize.Width,
                        _pointNew.Y * _map.Ratio + gridSize.Height,
                        _currentSize.Width * _map.Ratio,
                        _currentSize.Height * _map.Ratio);

                    #region Numeration

                    var pen = new Pen(Color.Black, 1);
                    var font = new Font("Arial", (float)numericUpDownNumerationWidth.Value);
                    var brush = new SolidBrush(colorPickerNumeration.BackColor);
                    var format = new StringFormat
                    {
                        Alignment = StringAlignment.Center,
                        LineAlignment = StringAlignment.Center
                    };
                    foreach (var marker in _markersSelected)
                    {
                        int
                            radius = marker.Radius,
                            zoom = trackBarZoom.Value;
                        float
                            width = marker.Width * zoom - radius / _map.RatioF + gridSize.Width + _pointNew.X * _map.Ratio,
                            height = marker.Height * zoom - radius / _map.RatioF + gridSize.Height + _pointNew.Y * _map.Ratio;

                        graph.FillEllipse(new SolidBrush(marker.Color),
                            width, height, radius, radius);
                        graph.DrawEllipse(pen, width, height, radius, radius);

                        var rectangle = new RectangleF(width, height, radius, radius);
                        graph.DrawString($"{marker.SelectedNumberText}", font, brush, rectangle, format);
                    }

                    #endregion

                    #region Grid Lines

                    if (checkBoxGridLines.Checked)
                    {
                        pen = new Pen(colorPickerGridLines.BackColor, (int)numericUpDownGridLinesWidth.Value)
                        {
                            DashStyle = DashStyle.Dash
                        };

                        float step = (_map.LngEndPx - _map.LngStartPx) / (float)numericUpDownGridLinesLng.Value;
                        float start = _map.LngStartPx + gridSize.Width;
                        for (int i = 0; i <= numericUpDownGridLinesLng.Value; i++)
                        {
                            float pos = start + step * i;
                            graph.DrawLine(pen, pos, gridSize.Height, pos, gridSize.Height + _map.MapImage.Height);
                        }

                        step = (_map.LatEndPx - _map.LatStartPx) / (float)numericUpDownGridLinesLat.Value;
                        start = _map.LatStartPx + gridSize.Height;
                        for (int i = 0; i <= numericUpDownGridLinesLat.Value; i++)
                        {
                            float pos = start + step * i;
                            graph.DrawLine(pen, gridSize.Width, pos, gridSize.Width + _map.MapImage.Width, pos);
                        }
                    }

                    #endregion

                    #region Grid Deg

                    if (checkBoxGridDegrees.Checked)
                    {
                        // Закрашиваем белым цветом
                        graph.FillRectangles(Brushes.White,
                            new[] {
                                new Rectangle(0, 0, size.Width, gridSize.Height),
                                new Rectangle(0, 0, gridSize.Width, size.Height),
                                new Rectangle(0, size.Height - gridSize.Height,
                                    size.Width, size.Height),
                                new Rectangle(size.Width - gridSize.Width, 0,
                                    size.Width, size.Height)
                            });
                        // Окантовываем черным прямоуголником края
                        graph.DrawRectangle(new Pen(Color.Black, 1),
                            gridSize.Width,
                            gridSize.Height,
                            size.Width - gridSize.Width * _map.Ratio - 1,
                            size.Height - gridSize.Height * _map.Ratio - 1);

                        font = new Font("Arial", (int)numericUpDownGridDegreesWidth.Value * _map.Ratio);
                        brush = new SolidBrush(colorPickerGridDegrees.BackColor);
                        var culture = CultureInfo.InvariantCulture;

                        float diff = (_map.LngEndDeg - _map.LngStartDeg)
                                     / (float)(trackBarZoom.Value * numericUpDownGridLinesLng.Value);
                        float start = _map.LngStartDeg - _pointNew.X
                                      / (_map.LngPx * trackBarZoom.Value / _map.RatioF);

                        float step = (_map.LngEndPx - _map.LngStartPx)
                                     / (float)numericUpDownGridLinesLng.Value;
                        for (int i = 0; i <= numericUpDownGridLinesLng.Value; i++)
                        {
                            float val = start + i * diff;
                            float pos = (_map.LngStartPx + step * i);
                            graph.DrawString(string.Format(culture, "{0:f2}", val), font, brush, pos + 40, 3);
                            graph.DrawString(string.Format(culture, "{0:f2}", val), font, brush, pos + 40, size.Height - 40);
                        }

                        diff = (_map.LatEndDeg - _map.LatStartDeg)
                               / (float)(trackBarZoom.Value * numericUpDownGridLinesLat.Value);
                        start = _map.LatEndDeg + _pointNew.Y
                                / (_map.LatPx * trackBarZoom.Value / _map.RatioF);
                        step = (_map.LatEndPx - _map.LatStartPx)
                               / (float)numericUpDownGridLinesLat.Value;
                        for (int i = 0; i <= numericUpDownGridLinesLat.Value; i++)
                        {
                            float val = start - i * diff;
                            float pos = (_map.LatStartPx + step * i) + gridSize.Height;
                            DrawRotatedTextAt(graph, -45, string.Format(culture, "{0:f2}", val), -5, pos + 45, font, brush);
                            DrawRotatedTextAt(graph, -45, string.Format(culture, "{0:f2}", val), size.Width - 80, pos + 45, font, brush);
                        }
                    }

                    #endregion

                    #region Legend

                    if (checkBoxLegend.Checked)
                    {
                        graph.DrawImage(
                            pictureBoxLegend.Image,
                            gridSize.Width, gridSize.Height,
                            pictureBoxLegend.Image.Width, pictureBoxLegend.Image.Height);
                    }

                    #endregion

                    #region Legend Out

                    if (checkBoxLegendOut.Checked)
                    {
                        graph.DrawImage(
                            pictureBoxLegendOut.Image,
                            0,
                            size.Height,
                            pictureBoxLegendOut.Image.Width,
                            pictureBoxLegendOut.Image.Height);
                    }

                    #endregion
                }

                imgForSave.Save(filePath, imageFormat);
            }
        }

        private void CreateAndSaveIncisionImage(string filePath, ImageFormat imageFormat)
        {
            SelectAndZoomMarkers();//.ToList();

            var startSize = new Size(50, 50);
            var legendSize = new Size();
            var mapSize = new Size();

            var incisions = GetIncisions();
            int incisionNumber = 0;
            foreach (var incision in incisions)
            {
                incisionNumber++;
                // Наши маркеры в разрезе
                var markers = _markersSelected.Where(m => incision.IsInside(m.Lat, m.Lng)).ToList();
                if (markers.Count == 0)
                {
                    MessageBox.Show($"В разрезе под номер {incisionNumber} нет землетрясений, поэтому изображение будет без них!", "Внимание!");
                }
                // Все классы в разрезе
                var classes = markers.Select(m => m.Class).Distinct().OrderBy(c => c).ToArray();
                // Количество классов
                int classesCount = classes.Length;
                // Множитель радиуса
                double radiusMult = (double)numericUpDownIncisionRadiusMult.Value;
                int[] radiuses = classes.Select(Marker.GetRadius).ToArray();
                int radiusesMax = markers.Count == 0 ? 50 : (radiuses.Max());
                int radiusesAvg = radiusesMax;

                int distanceMult = (int)numericUpDownIncisionDistanceNumberMult.Value;
                int depthMult = (int)numericUpDownIncisionDepthNumberMult.Value;
                int forRadiusMult = 10;

                int distanceRadiusCount =
                    (int)Math.Ceiling(
                        (int)Math.Ceiling(
                            (incision.Start.DistanceTo(incision.End) + forRadiusMult * 0.5 * radiusMult) /
                            distanceMult) *
                        (double)distanceMult / forRadiusMult);
                int depthRadiusCount = markers.Count == 0
                    ? 2
                    : (int)Math.Ceiling(
                        (int)Math.Ceiling((markers.Max(m => m.Depth) + forRadiusMult * 0.5 * radiusMult) / depthMult) *
                        (double)depthMult / forRadiusMult);


                var multRadiusSize = new Size
                {
                    Width = radiusesAvg,
                    Height = radiusesAvg
                };

                if (radioButtonIncisionPriorityRadius.Checked)
                {
                    distanceRadiusCount = Math.Max(distanceRadiusCount, (int)(classesCount * radiusMult));
                }

                mapSize.Width = distanceRadiusCount * multRadiusSize.Width;
                mapSize.Height = depthRadiusCount * multRadiusSize.Height;
                var fontL = new Font("Arial", (float)(numericUpDownIncisionRadiusFontSize.Value));
                legendSize.Width = mapSize.Width;
                legendSize.Height = fontL.Height + (int)(radiusesMax * radiusMult) + 10;

                using (Image imgForSave = new Bitmap(
                    startSize.Width + mapSize.Width,
                    startSize.Height + mapSize.Height + legendSize.Height))
                {
                    using (var graph = Graphics.FromImage(imgForSave))
                    {
                        graph.Clear(Color.White);

                        #region Markers

                        #region Stroke Draw

                        IIncisionMarker stroke;
                        if (checkBoxIncisionStroke.Checked)
                        {
                            int alpha = (int)(numericUpDownIncisionStrokeAlpha.Value * 255);
                            float width = (float)numericUpDownIncisionStrokeWidth.Value;
                            if (checkBoxIncisionStrokeUserColor.Checked)
                                stroke = new StrokePermanentIncision(Color.FromArgb(alpha, colorPickerIncisionStrokeColor.BackColor), width);
                            else
                                stroke = new StrokeMulticoloredIncision(alpha, width);
                        }
                        else
                        {
                            stroke = new StrokeDummyIncision();
                        }

                        #endregion

                        #region Fill Draw

                        IIncisionMarker fill;
                        if (checkBoxIncisionFill.Checked)
                        {
                            int alpha = (int)(numericUpDownIncisionFillAlpha.Value * 255);
                            if (checkBoxIncisionFillUserColor.Checked)
                                fill = new FillPermanentIncision(Color.FromArgb(alpha, colorPickerIncisionFillColor.BackColor));
                            else
                                fill = new FillMulticoloredIncision(alpha);
                        }
                        else
                        {
                            fill = new FillDummyIncision();
                        }

                        #endregion

                        var line = new LineS(incision.Start, incision.End);

                        var fontSelectedNumber = new Font("Arial",
                            (float)((double)numericUpDownNumerationWidth.Value * radiusMult) / trackBarZoom.Value);
                        var brushSelectedNumber = new SolidBrush(colorPickerNumeration.BackColor);
                        var formatSelectedNumber = new StringFormat
                        {
                            Alignment = StringAlignment.Center,
                            LineAlignment = StringAlignment.Center,
                            //Trimming = StringTrimming.Word
                        };
                        foreach (var marker in markers)
                        {
                            var intersection = line.FindIntersection(marker.Lat, marker.Lng);
                            double distance = incision.Start.DistanceTo(intersection);
                            double depth = marker.Depth;
                            int radius = (int)(marker.Radius * radiusMult);
                            int width = startSize.Width + (int)(distance / forRadiusMult * multRadiusSize.Width) - radius / 2;
                            int height = startSize.Height + (int)(depth / forRadiusMult * multRadiusSize.Height) - radius / 2;
                            fill.Draw(graph, marker, width, height, radius);
                            stroke.Draw(graph, marker, width, height, radius);

                            //
                            var rectangle = new RectangleF(width, height, radius, radius);
                            //
                            graph.DrawString(marker.SelectedNumberText, fontSelectedNumber, brushSelectedNumber, rectangle, formatSelectedNumber);
                        }

                        #endregion

                        #region Fill

                        var fillBrush = new SolidBrush(Color.AliceBlue);
                        graph.FillRectangle(fillBrush, 0, 0, startSize.Width, imgForSave.Height);
                        graph.FillRectangle(fillBrush, 0, 0, imgForSave.Width, startSize.Height);
                        graph.FillRectangle(fillBrush,
                            startSize.Width,
                            startSize.Height + mapSize.Height,
                            imgForSave.Width,
                            legendSize.Height);

                        #endregion

                        #region Border

                        graph.DrawRectangle(new Pen(Color.Black, 1),
                            startSize.Width,
                            startSize.Height,
                            mapSize.Width - 1,
                            mapSize.Height);

                        #endregion

                        #region Grid

                        if (checkBoxIncisionDistanceLine.Checked)
                        {
                            int distanceLineMult = (int)numericUpDownIncisionDistanceLineMult.Value;
                            int distanceLineWidth = multRadiusSize.Width * distanceLineMult / forRadiusMult;
                            int distanceLineCount = distanceRadiusCount * forRadiusMult / distanceLineMult;

                            var penDistanceLine = new Pen(colorPickerIncisionDistanceLineColor.BackColor, (float)numericUpDownIncisionDistanceLineWidth.Value)
                            {
                                DashStyle = DashStyle.Dash
                            };
                            for (int index = 0; index < distanceLineCount; index++)
                            {
                                graph.DrawLine(penDistanceLine,
                                        startSize.Width + index * distanceLineWidth,
                                        startSize.Height,
                                        startSize.Width + index * distanceLineWidth,
                                        startSize.Height + mapSize.Height);
                            }
                        }

                        if (checkBoxIncisionDistanceNumber.Checked)
                        {
                            int distanceWidth = multRadiusSize.Width * distanceMult / forRadiusMult;
                            int distanceCount =
                                1 + (int)Math.Ceiling(distanceRadiusCount * forRadiusMult / (decimal)distanceMult);
                            int[] distances = Enumerable
                                .Range(0, distanceCount)
                                .Select(i => i * distanceMult)
                                .ToArray();
                            var formatDistance = new StringFormat
                            {
                                Alignment = StringAlignment.Center,
                                LineAlignment = StringAlignment.Far
                            };
                            var font = new Font("Arial",
                                (int)numericUpDownIncisionDistanceNumberFontSize.Value);
                            var fontBrush = new SolidBrush(colorPickerIncisionDistanceNumberColor.BackColor);
                            for (int index = 0; index < distances.Length; index++)
                            {
                                graph.DrawString($"{distances[index]}", font, fontBrush,
                                    new Rectangle(
                                        startSize.Width + (index - 1) * distanceWidth,
                                        0,
                                        distanceWidth * 2,
                                        startSize.Height),
                                    formatDistance);
                            }
                        }

                        if (checkBoxIncisionDepthLine.Checked)
                        {
                            int depthLineMult = (int)numericUpDownIncisionDepthLineMult.Value;
                            int depthLineHeight = multRadiusSize.Height * depthLineMult / forRadiusMult;
                            int depthLineCount = depthRadiusCount * forRadiusMult / depthLineMult;

                            var penDepthLine = new Pen(colorPickerIncisionDepthLineColor.BackColor, (float)numericUpDownIncisionDepthLineWidth.Value)
                            {
                                DashStyle = DashStyle.Dash
                            };

                            for (int index = 0; index < depthLineCount; index++)
                            {
                                graph.DrawLine(penDepthLine,
                                    startSize.Width,
                                    startSize.Height + index * depthLineHeight,
                                    startSize.Width + mapSize.Width,
                                    startSize.Height + index * depthLineHeight);
                            }
                        }

                        if (checkBoxIncisionDepthNumber.Checked)
                        {
                            int depthHeigth = multRadiusSize.Height * depthMult / forRadiusMult;
                            int depthCount =
                                1 + (int)Math.Ceiling(depthRadiusCount * forRadiusMult / (decimal)depthMult);
                            int[] depths = Enumerable
                                .Range(0, depthCount)
                                .Select(i => i * depthMult)
                                .ToArray();
                            var formatDepth = new StringFormat
                            {
                                Alignment = StringAlignment.Far,
                                LineAlignment = StringAlignment.Center
                            };
                            var font = new Font("Arial",
                                (int)numericUpDownIncisionDepthNumberFontSize.Value);
                            var fontBrush = new SolidBrush(colorPickerIncisionDepthNumberColor.BackColor);
                            for (int index = 0; index < depths.Length; index++)
                            {
                                graph.DrawString($"{depths[index]}", font, fontBrush,
                                    new Rectangle(
                                        0,
                                        startSize.Height + (index - 1) * depthHeigth,
                                        startSize.Width,
                                        depthHeigth * 2),
                                    formatDepth);
                            }
                        }

                        #endregion

                        #region Legend

                        var penL = new Pen(Color.Black, 1) { DashStyle = DashStyle.Solid };
                        var formatL = new StringFormat
                        {
                            Alignment = StringAlignment.Center,
                            LineAlignment = StringAlignment.Center
                        };

                        if (classesCount != 0)
                        {
                            int start = fontL.Height,
                                //max = classes[classesLength - 1],
                                maxSpace = Math.Min(legendSize.Width / classesCount, legendSize.Height - start),
                                middle = legendSize.Height / 2;
                            for (int index = 0; index < classesCount; index++)
                            {
                                int radius = (int)(Marker.GetRadius(classes[index]) * radiusMult),
                                    space = middle - start / 2 - radius / 2;
                                int count = markers.Count(m => m.Class == classes[index]);
                                var rectangle = new Rectangle(startSize.Width + index * maxSpace + (maxSpace - radius) / 2, space + startSize.Height + mapSize.Height, radius, radius);
                                graph.FillEllipse(Brushes.BlanchedAlmond, rectangle);
                                graph.DrawEllipse(penL, rectangle);
                                graph.DrawString($"{classes[index]}", fontL, Brushes.Black, rectangle, formatL);
                                rectangle = new Rectangle(startSize.Width + index * maxSpace, imgForSave.Height - start, maxSpace, start);
                                graph.DrawString($"({count})", fontL, Brushes.Black, rectangle, formatL);
                            }
                        }

                        #endregion
                    }

                    imgForSave.Save(filePath.Replace("{номер_разреза}", incisionNumber.ToString()), imageFormat);
                }


            }

        }



        private ImageFormat GetImageFormat(int index)
        {
            switch (index)
            {
                case 1:
                    return ImageFormat.Png;
                case 2:
                    return ImageFormat.Jpeg;
                case 3:
                    return ImageFormat.Bmp;
                case 4:
                    return ImageFormat.Gif;
                default:
                    return ImageFormat.Png;
            }
        }

        #endregion

        #region Map

        private void AddMapMenu(string name, FormMapSettings mapSettings)
        {
            var stripItem = (ToolStripMenuItem)mapToolStripMenuItem.DropDownItems.Add(name);
            stripItem.Click += (sender, args) =>
            {
                SetMap(mapSettings);
            };
            //var stripItemSelect = stripItem.DropDownItems.Add("Выбрать");
            //stripItemSelect.Click += (sender, args) =>
            //{
            //    SetMap(mapSettings);
            //};
            var stripItemRemove = stripItem.DropDownItems.Add("Удалить");
            stripItemRemove.Click += (sender, args) =>
            {
                var dialogResult = MessageBox.Show("Вы точно хотите удалить?", "Внимание", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }
                mapToolStripMenuItem.DropDownItems.Remove(stripItem);
                var mapSetting = _settings.MapSettings[stripItem.Text];
                _settings.MapSettings.Remove(stripItem.Text);
                //File.Delete($"{Settings.MapPath}\\{mapSetting.FileName}");
            };
        }

        private void SetMap(FormMapSettings mapSettings)
        {
            string path = $"{Settings.Path}\\Карты\\{mapSettings.FileName}";

            try
            {
                Image img = Image.FromFile(path);
                _map = new Map(img,
                    mapSettings.LatStartPx,
                    mapSettings.LatEndPx,
                    mapSettings.LatStartDeg,
                    mapSettings.LatEndDeg,
                    mapSettings.LngStartPx,
                    mapSettings.LngEndPx,
                    mapSettings.LngStartDeg,
                    mapSettings.LngEndDeg,
                    mapSettings.RatioF,
                    mapSettings.Ratio
                );
            }
            catch (Exception)
            {
#if !DEBUG
                MessageBox.Show("Не удалось выбрать эту карту. Будет выбрана карта по-умолчанию", "Внимание");
#endif
                Image imgDag = Resources.MapDagestan;
                _map = new Map(imgDag, 0, imgDag.Height, 41, 45, 0, imgDag.Width, 45, 49, 2.0f, 2);
            }

            InitMap();
            InitLegend();
            InitLegendOut();
            InitMarkers();
            InitPanelImage();
            RefreshMarkers();

            pictureBoxMap.Refresh();
            pictureBoxGrid.Refresh();
            pictureBoxLegend.Refresh();
            pictureBoxLegendOut.Refresh();
        }



        private void SetMap(Image img)
        {
            _map = new Map(img, 0, img.Height, 41, 45, 0, img.Width, 45, 49, 2.0f, 2);

            InitMap();
            InitLegend();
            InitLegendOut();
            InitMarkers();
            InitPanelImage();
        }

        #endregion

        #region Chart

        private EarthquakeRepeatabilityPeriod _repeatabilityPeriod;
        private int _repeatabilityPeriodIndex = 0;

        #region Chart Events

        private void ButtonRepeatabilityCalculate_Click(object sender, EventArgs e)
        {
            var earthquakes = SelectEarthquakes();
            var figure = GetFigure();

            if (figure == null || !checkBoxCoord.Checked)
            {
                MessageBox.Show("Вы не выбрали разрез", "Внимание");
                return;
            }

            var periodType = GetDateTimeIntervalType();
            int periodValue = (int)numericUpDownRepeatabilityPeriod.Value;
            int a = (int)numericUpDownRepeatabilityA.Value;
            int kBegin = (int)numericUpDownRepeatabilityKBegin.Value;
            var calc = new CalculatorS();
            _repeatabilityPeriod = calc.FindChartRepeatabilityPeriod(earthquakes, figure, periodType, periodValue, a, kBegin);
            _repeatabilityPeriodIndex = 0;
            comboBoxRepeatabilityPeriodChart.Items.Clear();
            for (int index = 0; index < _repeatabilityPeriod.Period.Length - 1; index++)
            {
                comboBoxRepeatabilityPeriodChart.Items.Add(
                    $"{_repeatabilityPeriod.Period[index].ToString(_repeatabilityPeriod.Period.PeriodTypeString)} - {_repeatabilityPeriod.Period[index + 1].ToString(_repeatabilityPeriod.Period.PeriodTypeString)}");
            }

            chartRepeatabilityPeriod.ChartAreas[0].AxisX.LabelStyle.Format = _repeatabilityPeriod.Period.PeriodTypeString;
            chartRepeatabilityPeriod.ChartAreas[0].AxisX.Interval = periodValue;

            chartRepeatabilityPeriod.ChartAreas[0].AxisX.IntervalType = periodType;
            DrawRepeatabilityPeriod();
            DrawRepeatability(_repeatabilityPeriod.Repeatabilities[0]);
            CheckRepeatabilityPeriodNavButtons();
        }

        private void CheckBoxKBeginAuto_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownRepeatabilityKBegin.Enabled = !checkBoxRepeatabilityKBeginAuto.Checked;
        }

        private void ComboBoxRepeatabilityTypeY_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawRepeatabilityPeriod();
        }

        private void ButtonRepeatabilityPeriodNext_Click(object sender, EventArgs e)
        {
            if (_repeatabilityPeriodIndex < _repeatabilityPeriod.Repeatabilities.Count - 1)
            {
                _repeatabilityPeriodIndex++;
                DrawRepeatability(_repeatabilityPeriod.Repeatabilities[_repeatabilityPeriodIndex]);
            }

            CheckRepeatabilityPeriodNavButtons();
        }

        private void ButtonRepeatabilityPeriodPrev_Click(object sender, EventArgs e)
        {
            if (_repeatabilityPeriodIndex > 0)
            {
                _repeatabilityPeriodIndex--;
                DrawRepeatability(_repeatabilityPeriod.Repeatabilities[_repeatabilityPeriodIndex]);
            }

            CheckRepeatabilityPeriodNavButtons();
        }

        private void ComboBoxRepeatabilityPeriodChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            _repeatabilityPeriodIndex = comboBoxRepeatabilityPeriodChart.SelectedIndex;
            DrawRepeatability(_repeatabilityPeriod.Repeatabilities[_repeatabilityPeriodIndex]);
            CheckRepeatabilityPeriodNavButtons();
        }

        #endregion

        #region Chart Logic

        private void InitTabCharts()
        {
            comboBoxRepeatabilityTypeY.SelectedIndex = 0;
            comboBoxRepeatabilityPeriod.SelectedIndex = 0;

            chartRepeatabilityPeriod.Series[0].XValueType = ChartValueType.DateTime;
            chartRepeatabilityPeriod.ChartAreas[0].AxisX.IntervalOffset = 0;

            chartRepeatability.ChartAreas[0].AxisX.Interval = 1;
            chartRepeatability.ChartAreas[0].AxisX.IntervalOffset = 1;
        }

        private DateTimeIntervalType GetDateTimeIntervalType()
        {
            switch (comboBoxRepeatabilityPeriod.SelectedIndex)
            {
                case 0:
                    return DateTimeIntervalType.Years;
                case 1:
                    return DateTimeIntervalType.Months;
                case 2:
                    return DateTimeIntervalType.Days;
                case 3:
                    return DateTimeIntervalType.Hours;
                case 4:
                    return DateTimeIntervalType.Minutes;
                case 5:
                    return DateTimeIntervalType.Seconds;
                case 6:
                    return DateTimeIntervalType.Milliseconds;
                default:
                    return DateTimeIntervalType.NotSet;
            }
        }

        private void DrawRepeatabilityPeriod()
        {
            if (_repeatabilityPeriod != null)
            {
                chartRepeatabilityPeriod.Series[0].Points.Clear();

                foreach (var repeatability in _repeatabilityPeriod.Repeatabilities)
                {
                    double value = comboBoxRepeatabilityTypeY.SelectedIndex == 0 ? repeatability.A : repeatability.K;
                    chartRepeatabilityPeriod.Series[0].Points.AddXY(repeatability.Date, value);
                }
            }
        }

        private void DrawRepeatability(EarthquakeRepeatability repeatability)
        {
            if (repeatability == null)
            {
                return;
            }

            foreach (var series in chartRepeatability.Series)
            {
                series.Points.Clear();
            }

            for (int index = 0; index < repeatability.ClassRepeatability.Classes.Length; index++)
            {
                chartRepeatability.Series[0].Points.AddXY(
                    repeatability.ClassRepeatability.Classes[index],
                    repeatability.ClassRepeatability.Repeatabilities[index]);
            }

            for (int index = 0; index < repeatability.ClassRepeatabilityCalculate.Classes.Length; index++)
            {
                chartRepeatability.Series[1].Points.AddXY(
                    repeatability.ClassRepeatabilityCalculate.Classes[index],
                    repeatability.Calculate(repeatability.ClassRepeatabilityCalculate.Classes[index]));
            }

            chartRepeatability.Titles[0].Text = $"{repeatability.K:F3} x + {repeatability.B:F3}";
            comboBoxRepeatabilityPeriodChart.SelectedIndex = _repeatabilityPeriodIndex;

            chartRepeatability.Refresh();
        }



        private void CheckRepeatabilityPeriodNavButtons()
        {
            buttonRepeatabilityPeriodPrev.Enabled = _repeatabilityPeriodIndex > 0;
            buttonRepeatabilityPeriodNext.Enabled = _repeatabilityPeriodIndex < _repeatabilityPeriod.Repeatabilities.Count - 1;
        }

        #endregion

        #endregion

        #endregion
    }
}