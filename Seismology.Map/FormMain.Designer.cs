﻿namespace Seismology.Map
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.panelImages = new System.Windows.Forms.Panel();
            this.pictureBoxLegendOut = new System.Windows.Forms.PictureBox();
            this.pictureBoxGrid = new System.Windows.Forms.PictureBox();
            this.pictureBoxLegend = new System.Windows.Forms.PictureBox();
            this.pictureBoxMap = new System.Windows.Forms.PictureBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageAsIncisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageAsRepeatabilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulletinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulletinExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.surferDocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distributionEnergyClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repeatabilityPeriodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabMainControl = new System.Windows.Forms.TabControl();
            this.tabPageSelection = new System.Windows.Forms.TabPage();
            this.checkBoxTime = new System.Windows.Forms.CheckBox();
            this.groupBoxTime = new System.Windows.Forms.GroupBox();
            this.timeTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.timeTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.checkBoxDepth = new System.Windows.Forms.CheckBox();
            this.groupBoxDepth = new System.Windows.Forms.GroupBox();
            this.numericUpDownEndDepth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStartDepth = new System.Windows.Forms.NumericUpDown();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.groupBoxDate = new System.Windows.Forms.GroupBox();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.checkBoxCoord = new System.Windows.Forms.CheckBox();
            this.groupBoxCoord = new System.Windows.Forms.GroupBox();
            this.buttonShowCoord = new System.Windows.Forms.Button();
            this.tabControlCoord = new System.Windows.Forms.TabControl();
            this.tabPageRectangle = new System.Windows.Forms.TabPage();
            this.labelRecLng = new System.Windows.Forms.Label();
            this.labelRecLat = new System.Windows.Forms.Label();
            this.numericUpDownRecLatEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRecLatStart = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRecLngEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRecLngStart = new System.Windows.Forms.NumericUpDown();
            this.tabPageCircle = new System.Windows.Forms.TabPage();
            this.labelCircleLng = new System.Windows.Forms.Label();
            this.labelCircleRadius = new System.Windows.Forms.Label();
            this.labelCircleLat = new System.Windows.Forms.Label();
            this.numericUpDownRadius = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCenterLat = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCenterLng = new System.Windows.Forms.NumericUpDown();
            this.tabPagePolygon = new System.Windows.Forms.TabPage();
            this.buttonPolygon = new System.Windows.Forms.Button();
            this.dataGridViewPolygon = new System.Windows.Forms.DataGridView();
            this.ColumnPolygonId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLng = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageIncision = new System.Windows.Forms.TabPage();
            this.buttonIncision = new System.Windows.Forms.Button();
            this.dataGridViewIncision = new System.Windows.Forms.DataGridView();
            this.ColumnIncisionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLatStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLngStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLatEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLngEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxSpeed = new System.Windows.Forms.GroupBox();
            this.trackBarVmin = new System.Windows.Forms.TrackBar();
            this.labelVMinMax = new System.Windows.Forms.Label();
            this.trackBarVmax = new System.Windows.Forms.TrackBar();
            this.groupBoxEnergy = new System.Windows.Forms.GroupBox();
            this.trackBarKmin = new System.Windows.Forms.TrackBar();
            this.labelKMinMax = new System.Windows.Forms.Label();
            this.trackBarKmax = new System.Windows.Forms.TrackBar();
            this.tabPageSelectionAdditional = new System.Windows.Forms.TabPage();
            this.checkBoxNumeration = new System.Windows.Forms.CheckBox();
            this.groupBoxNumeration = new System.Windows.Forms.GroupBox();
            this.radioButtonNumerationDepth = new System.Windows.Forms.RadioButton();
            this.radioButtonCouple = new System.Windows.Forms.RadioButton();
            this.radioButtonNumerationCommon = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanelNumeration = new System.Windows.Forms.TableLayoutPanel();
            this.labelNumerationColor = new System.Windows.Forms.Label();
            this.labelNumerationWidth = new System.Windows.Forms.Label();
            this.numericUpDownNumerationWidth = new System.Windows.Forms.NumericUpDown();
            this.colorPickerNumeration = new System.Windows.Forms.PictureBox();
            this.buttonNumerationApply = new System.Windows.Forms.Button();
            this.checkBoxNumerationAutoSize = new System.Windows.Forms.CheckBox();
            this.groupBoxNumerationCommon = new System.Windows.Forms.GroupBox();
            this.buttonNumerationCommonApply = new System.Windows.Forms.Button();
            this.groupBoxCouple = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelCouple = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownCoupleKEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCoupleKBegin = new System.Windows.Forms.NumericUpDown();
            this.labelCoupleK = new System.Windows.Forms.Label();
            this.labelCoupleDistance = new System.Windows.Forms.Label();
            this.numericUpDownCoupleDistance = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCoupleDays = new System.Windows.Forms.NumericUpDown();
            this.dateTimePickerCoupleTime = new System.Windows.Forms.DateTimePicker();
            this.labelCoupleDays = new System.Windows.Forms.Label();
            this.labelCoupleTime = new System.Windows.Forms.Label();
            this.buttonCoupleApply = new System.Windows.Forms.Button();
            this.tabPageLegendAndMarker = new System.Windows.Forms.TabPage();
            this.checkBoxDontShowIfZero = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanelMarkerColor = new System.Windows.Forms.TableLayoutPanel();
            this.radioButtonMarkerColorVpVs = new System.Windows.Forms.RadioButton();
            this.radioButtonMarkerColorClass = new System.Windows.Forms.RadioButton();
            this.buttonMarkerClass = new System.Windows.Forms.Button();
            this.labelMarkerRadius = new System.Windows.Forms.Label();
            this.trackBarMarkerRadius = new System.Windows.Forms.TrackBar();
            this.buttonMarkerVpVs = new System.Windows.Forms.Button();
            this.checkBoxLegendOut = new System.Windows.Forms.CheckBox();
            this.checkBoxLegend = new System.Windows.Forms.CheckBox();
            this.tabPageGrid = new System.Windows.Forms.TabPage();
            this.checkBoxGridDegrees = new System.Windows.Forms.CheckBox();
            this.checkBoxGridLines = new System.Windows.Forms.CheckBox();
            this.groupBoxGridDegrees = new System.Windows.Forms.GroupBox();
            this.numericUpDownGridDegreesWidth = new System.Windows.Forms.NumericUpDown();
            this.colorPickerGridDegrees = new System.Windows.Forms.PictureBox();
            this.groupBoxGridLines = new System.Windows.Forms.GroupBox();
            this.numericUpDownGridLinesWidth = new System.Windows.Forms.NumericUpDown();
            this.colorPickerGridLines = new System.Windows.Forms.PictureBox();
            this.numericUpDownGridLinesLng = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownGridLinesLat = new System.Windows.Forms.NumericUpDown();
            this.tabPageSurfer = new System.Windows.Forms.TabPage();
            this.groupBoxSurferFilter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelSurferFilterParam = new System.Windows.Forms.TableLayoutPanel();
            this.dateTimePickerFilterParam1 = new System.Windows.Forms.DateTimePicker();
            this.radioButtonFilterParam1 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanelSurferParam = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownParam2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownParam1 = new System.Windows.Forms.NumericUpDown();
            this.labelParam1 = new System.Windows.Forms.Label();
            this.labelParam2 = new System.Windows.Forms.Label();
            this.labelLngKm = new System.Windows.Forms.Label();
            this.labelLatKm = new System.Windows.Forms.Label();
            this.groupBoxSurferType = new System.Windows.Forms.GroupBox();
            this.radioButtonScoreField = new System.Windows.Forms.RadioButton();
            this.radioButtonActivity = new System.Windows.Forms.RadioButton();
            this.radioButtonStationV = new System.Windows.Forms.RadioButton();
            this.radioButtonV = new System.Windows.Forms.RadioButton();
            this.radioButtonDensity = new System.Windows.Forms.RadioButton();
            this.numericUpDownLngCount = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLatCount = new System.Windows.Forms.NumericUpDown();
            this.labelLngCount = new System.Windows.Forms.Label();
            this.labelLatCount = new System.Windows.Forms.Label();
            this.tabPageImage = new System.Windows.Forms.TabPage();
            this.tabControlImage = new System.Windows.Forms.TabControl();
            this.tabPageIncisionImage = new System.Windows.Forms.TabPage();
            this.groupBoxIncisionPriority = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionPriority = new System.Windows.Forms.TableLayoutPanel();
            this.radioButtonIncisionPriorityRadius = new System.Windows.Forms.RadioButton();
            this.radioButtonIncisionPriorityDistance = new System.Windows.Forms.RadioButton();
            this.groupBoxIncisionRadius = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionRadius = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownIncisionRadiusMult = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionRadiusFontWidth = new System.Windows.Forms.Label();
            this.labelIncisionRadiusMult = new System.Windows.Forms.Label();
            this.numericUpDownIncisionRadiusFontSize = new System.Windows.Forms.NumericUpDown();
            this.groupBoxIncisionDepth = new System.Windows.Forms.GroupBox();
            this.checkBoxIncisionDepthNumber = new System.Windows.Forms.CheckBox();
            this.groupBoxIncisionDepthNumber = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionDepthNumber = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownIncisionDepthNumberMult = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionDepthNumberMult = new System.Windows.Forms.Label();
            this.labelIncisionDepthNumberWidth = new System.Windows.Forms.Label();
            this.labelIncisionDepthNumberColor = new System.Windows.Forms.Label();
            this.colorPickerIncisionDepthNumberColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownIncisionDepthNumberFontSize = new System.Windows.Forms.NumericUpDown();
            this.checkBoxIncisionDepthLine = new System.Windows.Forms.CheckBox();
            this.groupBoxIncisionDepthLine = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionDepthLine = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownIncisionDepthLineMult = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionDepthLineMult = new System.Windows.Forms.Label();
            this.labelIncisionDepthLineWidth = new System.Windows.Forms.Label();
            this.labelIncisionDepthLineColor = new System.Windows.Forms.Label();
            this.colorPickerIncisionDepthLineColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownIncisionDepthLineWidth = new System.Windows.Forms.NumericUpDown();
            this.groupBoxIncisionDistance = new System.Windows.Forms.GroupBox();
            this.checkBoxIncisionDistanceNumber = new System.Windows.Forms.CheckBox();
            this.groupBoxIncisionDistanceNumber = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionDistanceNumber = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownIncisionDistanceNumberMult = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionDistanceNumberMult = new System.Windows.Forms.Label();
            this.labelIncisionDistanceNumberWidth = new System.Windows.Forms.Label();
            this.labelIncisionDistanceNumberColor = new System.Windows.Forms.Label();
            this.colorPickerIncisionDistanceNumberColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownIncisionDistanceNumberFontSize = new System.Windows.Forms.NumericUpDown();
            this.checkBoxIncisionDistanceLine = new System.Windows.Forms.CheckBox();
            this.groupBoxIncisionDistanceLine = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelDistanceLine = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownIncisionDistanceLineMult = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionDistanceLineMult = new System.Windows.Forms.Label();
            this.labelIncisionDistanceLineWidth = new System.Windows.Forms.Label();
            this.labelIncisionDistanceLineColor = new System.Windows.Forms.Label();
            this.colorPickerIncisionDistanceLineColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownIncisionDistanceLineWidth = new System.Windows.Forms.NumericUpDown();
            this.checkBoxIncisionFill = new System.Windows.Forms.CheckBox();
            this.groupBoxIncisionFill = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionFill = new System.Windows.Forms.TableLayoutPanel();
            this.labelIncisionFillAlpha = new System.Windows.Forms.Label();
            this.numericUpDownIncisionFillAlpha = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionFillColor = new System.Windows.Forms.Label();
            this.colorPickerIncisionFillColor = new System.Windows.Forms.PictureBox();
            this.checkBoxIncisionFillUserColor = new System.Windows.Forms.CheckBox();
            this.checkBoxIncisionStroke = new System.Windows.Forms.CheckBox();
            this.groupBoxIncisionStroke = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIncisionStroke = new System.Windows.Forms.TableLayoutPanel();
            this.labelIncisionStrokeAlpha = new System.Windows.Forms.Label();
            this.labelIncisionStrokeWdith = new System.Windows.Forms.Label();
            this.numericUpDownIncisionStrokeAlpha = new System.Windows.Forms.NumericUpDown();
            this.labelIncisionStrokeColor = new System.Windows.Forms.Label();
            this.numericUpDownIncisionStrokeWidth = new System.Windows.Forms.NumericUpDown();
            this.colorPickerIncisionStrokeColor = new System.Windows.Forms.PictureBox();
            this.checkBoxIncisionStrokeUserColor = new System.Windows.Forms.CheckBox();
            this.labelZoom = new System.Windows.Forms.Label();
            this.trackBarZoom = new System.Windows.Forms.TrackBar();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.tabControlView = new System.Windows.Forms.TabControl();
            this.tabPageMap = new System.Windows.Forms.TabPage();
            this.tabPageChart = new System.Windows.Forms.TabPage();
            this.comboBoxRepeatabilityPeriodChart = new System.Windows.Forms.ComboBox();
            this.buttonRepeatabilityPeriodPrev = new System.Windows.Forms.Button();
            this.buttonRepeatabilityPeriodNext = new System.Windows.Forms.Button();
            this.chartRepeatabilityPeriod = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanelRepeatability = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxRepeatabilityPeriod = new System.Windows.Forms.ComboBox();
            this.labelRepeatabilityPeriod = new System.Windows.Forms.Label();
            this.labelRepeatabilityKBegin = new System.Windows.Forms.Label();
            this.labelRepeatabilityA = new System.Windows.Forms.Label();
            this.labelRepeatabilityTypeY = new System.Windows.Forms.Label();
            this.comboBoxRepeatabilityTypeY = new System.Windows.Forms.ComboBox();
            this.numericUpDownRepeatabilityA = new System.Windows.Forms.NumericUpDown();
            this.checkBoxRepeatabilityKBeginAuto = new System.Windows.Forms.CheckBox();
            this.numericUpDownRepeatabilityKBegin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRepeatabilityPeriod = new System.Windows.Forms.NumericUpDown();
            this.buttonRepeatabilityCalculate = new System.Windows.Forms.Button();
            this.chartRepeatability = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegendOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.tabMainControl.SuspendLayout();
            this.tabPageSelection.SuspendLayout();
            this.groupBoxTime.SuspendLayout();
            this.groupBoxDepth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEndDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartDepth)).BeginInit();
            this.groupBoxDate.SuspendLayout();
            this.groupBoxCoord.SuspendLayout();
            this.tabControlCoord.SuspendLayout();
            this.tabPageRectangle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLatEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLatStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLngEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLngStart)).BeginInit();
            this.tabPageCircle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCenterLat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCenterLng)).BeginInit();
            this.tabPagePolygon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPolygon)).BeginInit();
            this.tabPageIncision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIncision)).BeginInit();
            this.groupBoxSpeed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmax)).BeginInit();
            this.groupBoxEnergy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarKmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarKmax)).BeginInit();
            this.tabPageSelectionAdditional.SuspendLayout();
            this.groupBoxNumeration.SuspendLayout();
            this.tableLayoutPanelNumeration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumerationWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerNumeration)).BeginInit();
            this.groupBoxNumerationCommon.SuspendLayout();
            this.groupBoxCouple.SuspendLayout();
            this.tableLayoutPanelCouple.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleKEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleKBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleDays)).BeginInit();
            this.tabPageLegendAndMarker.SuspendLayout();
            this.tableLayoutPanelMarkerColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMarkerRadius)).BeginInit();
            this.tabPageGrid.SuspendLayout();
            this.groupBoxGridDegrees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridDegreesWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerGridDegrees)).BeginInit();
            this.groupBoxGridLines.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridLinesWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerGridLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridLinesLng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridLinesLat)).BeginInit();
            this.tabPageSurfer.SuspendLayout();
            this.groupBoxSurferFilter.SuspendLayout();
            this.tableLayoutPanelSurferFilterParam.SuspendLayout();
            this.tableLayoutPanelSurferParam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownParam2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownParam1)).BeginInit();
            this.groupBoxSurferType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatCount)).BeginInit();
            this.tabPageImage.SuspendLayout();
            this.tabControlImage.SuspendLayout();
            this.tabPageIncisionImage.SuspendLayout();
            this.groupBoxIncisionPriority.SuspendLayout();
            this.tableLayoutPanelIncisionPriority.SuspendLayout();
            this.groupBoxIncisionRadius.SuspendLayout();
            this.tableLayoutPanelIncisionRadius.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionRadiusMult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionRadiusFontSize)).BeginInit();
            this.groupBoxIncisionDepth.SuspendLayout();
            this.groupBoxIncisionDepthNumber.SuspendLayout();
            this.tableLayoutPanelIncisionDepthNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthNumberMult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDepthNumberColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthNumberFontSize)).BeginInit();
            this.groupBoxIncisionDepthLine.SuspendLayout();
            this.tableLayoutPanelIncisionDepthLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthLineMult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDepthLineColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthLineWidth)).BeginInit();
            this.groupBoxIncisionDistance.SuspendLayout();
            this.groupBoxIncisionDistanceNumber.SuspendLayout();
            this.tableLayoutPanelIncisionDistanceNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceNumberMult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDistanceNumberColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceNumberFontSize)).BeginInit();
            this.groupBoxIncisionDistanceLine.SuspendLayout();
            this.tableLayoutPanelDistanceLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceLineMult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDistanceLineColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceLineWidth)).BeginInit();
            this.groupBoxIncisionFill.SuspendLayout();
            this.tableLayoutPanelIncisionFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionFillAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionFillColor)).BeginInit();
            this.groupBoxIncisionStroke.SuspendLayout();
            this.tableLayoutPanelIncisionStroke.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionStrokeAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionStrokeWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionStrokeColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarZoom)).BeginInit();
            this.tabControlView.SuspendLayout();
            this.tabPageMap.SuspendLayout();
            this.tabPageChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRepeatabilityPeriod)).BeginInit();
            this.tableLayoutPanelRepeatability.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatabilityA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatabilityKBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatabilityPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartRepeatability)).BeginInit();
            this.SuspendLayout();
            // 
            // panelImages
            // 
            this.panelImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelImages.Controls.Add(this.pictureBoxLegendOut);
            this.panelImages.Controls.Add(this.pictureBoxGrid);
            this.panelImages.Controls.Add(this.pictureBoxLegend);
            this.panelImages.Controls.Add(this.pictureBoxMap);
            this.panelImages.Location = new System.Drawing.Point(5, 5);
            this.panelImages.Margin = new System.Windows.Forms.Padding(4);
            this.panelImages.Name = "panelImages";
            this.panelImages.Size = new System.Drawing.Size(593, 882);
            this.panelImages.TabIndex = 0;
            // 
            // pictureBoxLegendOut
            // 
            this.pictureBoxLegendOut.BackColor = System.Drawing.Color.White;
            this.pictureBoxLegendOut.Location = new System.Drawing.Point(175, 179);
            this.pictureBoxLegendOut.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxLegendOut.Name = "pictureBoxLegendOut";
            this.pictureBoxLegendOut.Size = new System.Drawing.Size(150, 150);
            this.pictureBoxLegendOut.TabIndex = 8;
            this.pictureBoxLegendOut.TabStop = false;
            // 
            // pictureBoxGrid
            // 
            this.pictureBoxGrid.BackColor = System.Drawing.Color.White;
            this.pictureBoxGrid.Location = new System.Drawing.Point(333, 21);
            this.pictureBoxGrid.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxGrid.Name = "pictureBoxGrid";
            this.pictureBoxGrid.Size = new System.Drawing.Size(150, 150);
            this.pictureBoxGrid.TabIndex = 7;
            this.pictureBoxGrid.TabStop = false;
            this.pictureBoxGrid.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBoxGrid_Paint);
            // 
            // pictureBoxLegend
            // 
            this.pictureBoxLegend.BackColor = System.Drawing.Color.White;
            this.pictureBoxLegend.Location = new System.Drawing.Point(175, 21);
            this.pictureBoxLegend.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxLegend.Name = "pictureBoxLegend";
            this.pictureBoxLegend.Size = new System.Drawing.Size(150, 150);
            this.pictureBoxLegend.TabIndex = 6;
            this.pictureBoxLegend.TabStop = false;
            // 
            // pictureBoxMap
            // 
            this.pictureBoxMap.BackColor = System.Drawing.Color.White;
            this.pictureBoxMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxMap.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxMap.Location = new System.Drawing.Point(17, 21);
            this.pictureBoxMap.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxMap.Name = "pictureBoxMap";
            this.pictureBoxMap.Size = new System.Drawing.Size(150, 150);
            this.pictureBoxMap.TabIndex = 5;
            this.pictureBoxMap.TabStop = false;
            this.pictureBoxMap.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBoxMap_Paint);
            this.pictureBoxMap.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBoxMap_MouseDown);
            this.pictureBoxMap.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxMap_MouseMove);
            this.pictureBoxMap.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBoxMap_MouseUp);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.mapToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(1204, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveImageAsToolStripMenuItem,
            this.saveImageAsIncisionToolStripMenuItem,
            this.saveImageAsRepeatabilityToolStripMenuItem,
            this.saveAsDocumentToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.openToolStripMenuItem.Text = "Открыть";
            this.openToolStripMenuItem.ToolTipText = "Открывает документ (бюллетень или каталог)";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveImageAsToolStripMenuItem
            // 
            this.saveImageAsToolStripMenuItem.Name = "saveImageAsToolStripMenuItem";
            this.saveImageAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveImageAsToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.saveImageAsToolStripMenuItem.Text = "Сохранить как изображение";
            this.saveImageAsToolStripMenuItem.Click += new System.EventHandler(this.SaveImageAsToolStripMenuItem_Click);
            // 
            // saveImageAsIncisionToolStripMenuItem
            // 
            this.saveImageAsIncisionToolStripMenuItem.Name = "saveImageAsIncisionToolStripMenuItem";
            this.saveImageAsIncisionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.saveImageAsIncisionToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.saveImageAsIncisionToolStripMenuItem.Text = "Сохранить как разрез";
            this.saveImageAsIncisionToolStripMenuItem.Click += new System.EventHandler(this.SaveImageAsIncisionToolStripMenuItem_Click);
            // 
            // saveImageAsRepeatabilityToolStripMenuItem
            // 
            this.saveImageAsRepeatabilityToolStripMenuItem.Name = "saveImageAsRepeatabilityToolStripMenuItem";
            this.saveImageAsRepeatabilityToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.saveImageAsRepeatabilityToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.saveImageAsRepeatabilityToolStripMenuItem.Text = "Сохранить как повторяемость";
            this.saveImageAsRepeatabilityToolStripMenuItem.Click += new System.EventHandler(this.SaveImageAsRepeatabilityToolStripMenuItem_Click);
            // 
            // saveAsDocumentToolStripMenuItem
            // 
            this.saveAsDocumentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogToolStripMenuItem,
            this.bulletinToolStripMenuItem,
            this.catalogExcelToolStripMenuItem,
            this.bulletinExcelToolStripMenuItem,
            this.surferDocToolStripMenuItem,
            this.distributionToolStripMenuItem,
            this.distributionEnergyClassToolStripMenuItem,
            this.repeatabilityPeriodToolStripMenuItem});
            this.saveAsDocumentToolStripMenuItem.Name = "saveAsDocumentToolStripMenuItem";
            this.saveAsDocumentToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.saveAsDocumentToolStripMenuItem.Text = "Сохранить как документ";
            this.saveAsDocumentToolStripMenuItem.ToolTipText = "Сохраняет данные виде документа (бюллетень или каталог) всех маркеров, находящихс" +
    "я на карте";
            // 
            // catalogToolStripMenuItem
            // 
            this.catalogToolStripMenuItem.Name = "catalogToolStripMenuItem";
            this.catalogToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.catalogToolStripMenuItem.Text = "Каталог";
            this.catalogToolStripMenuItem.Click += new System.EventHandler(this.CatalogToolStripMenuItem_Click);
            // 
            // bulletinToolStripMenuItem
            // 
            this.bulletinToolStripMenuItem.Name = "bulletinToolStripMenuItem";
            this.bulletinToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.bulletinToolStripMenuItem.Text = "Бюллетень";
            this.bulletinToolStripMenuItem.Click += new System.EventHandler(this.BulletinToolStripMenuItem_Click);
            // 
            // catalogExcelToolStripMenuItem
            // 
            this.catalogExcelToolStripMenuItem.Name = "catalogExcelToolStripMenuItem";
            this.catalogExcelToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.catalogExcelToolStripMenuItem.Text = "Каталог Excel (Свой формат)";
            this.catalogExcelToolStripMenuItem.Click += new System.EventHandler(this.CatalogExcelToolStripMenuItem_Click);
            // 
            // bulletinExcelToolStripMenuItem
            // 
            this.bulletinExcelToolStripMenuItem.Name = "bulletinExcelToolStripMenuItem";
            this.bulletinExcelToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.bulletinExcelToolStripMenuItem.Text = "Бюллетень Excel (Свой формат)";
            this.bulletinExcelToolStripMenuItem.Click += new System.EventHandler(this.BulletinExcelToolStripMenuItem_Click);
            // 
            // surferDocToolStripMenuItem
            // 
            this.surferDocToolStripMenuItem.Name = "surferDocToolStripMenuItem";
            this.surferDocToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.surferDocToolStripMenuItem.Text = "Surfer";
            this.surferDocToolStripMenuItem.Click += new System.EventHandler(this.SurferDocToolStripMenuItem_Click);
            // 
            // distributionToolStripMenuItem
            // 
            this.distributionToolStripMenuItem.Name = "distributionToolStripMenuItem";
            this.distributionToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.distributionToolStripMenuItem.Text = "Распределение";
            this.distributionToolStripMenuItem.Click += new System.EventHandler(this.DistributionToolStripMenuItem_Click);
            // 
            // distributionEnergyClassToolStripMenuItem
            // 
            this.distributionEnergyClassToolStripMenuItem.Name = "distributionEnergyClassToolStripMenuItem";
            this.distributionEnergyClassToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.distributionEnergyClassToolStripMenuItem.Text = "Распределение (энергетический класс)";
            this.distributionEnergyClassToolStripMenuItem.Click += new System.EventHandler(this.DistributionEnergyClassToolStripMenuItem_Click);
            // 
            // repeatabilityPeriodToolStripMenuItem
            // 
            this.repeatabilityPeriodToolStripMenuItem.Name = "repeatabilityPeriodToolStripMenuItem";
            this.repeatabilityPeriodToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.repeatabilityPeriodToolStripMenuItem.Text = "Период повторяемости";
            this.repeatabilityPeriodToolStripMenuItem.Click += new System.EventHandler(this.RepeatabilityPeriodToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(278, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.ToolTipText = "Выход из программы";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem});
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.mapToolStripMenuItem.Text = "Карты";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.addToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.addToolStripMenuItem.Text = "Добавить";
            this.addToolStripMenuItem.ToolTipText = "Добавить новую карту";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.AddToolStripMenuItem_Click);
            // 
            // tabMainControl
            // 
            this.tabMainControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabMainControl.Controls.Add(this.tabPageSelection);
            this.tabMainControl.Controls.Add(this.tabPageSelectionAdditional);
            this.tabMainControl.Controls.Add(this.tabPageLegendAndMarker);
            this.tabMainControl.Controls.Add(this.tabPageGrid);
            this.tabMainControl.Controls.Add(this.tabPageSurfer);
            this.tabMainControl.Controls.Add(this.tabPageImage);
            this.tabMainControl.Location = new System.Drawing.Point(13, 28);
            this.tabMainControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabMainControl.Name = "tabMainControl";
            this.tabMainControl.SelectedIndex = 0;
            this.tabMainControl.Size = new System.Drawing.Size(550, 860);
            this.tabMainControl.TabIndex = 6;
            // 
            // tabPageSelection
            // 
            this.tabPageSelection.BackColor = System.Drawing.Color.Transparent;
            this.tabPageSelection.Controls.Add(this.checkBoxTime);
            this.tabPageSelection.Controls.Add(this.groupBoxTime);
            this.tabPageSelection.Controls.Add(this.checkBoxDepth);
            this.tabPageSelection.Controls.Add(this.groupBoxDepth);
            this.tabPageSelection.Controls.Add(this.checkBoxDate);
            this.tabPageSelection.Controls.Add(this.groupBoxDate);
            this.tabPageSelection.Controls.Add(this.checkBoxCoord);
            this.tabPageSelection.Controls.Add(this.groupBoxCoord);
            this.tabPageSelection.Controls.Add(this.groupBoxSpeed);
            this.tabPageSelection.Controls.Add(this.groupBoxEnergy);
            this.tabPageSelection.Location = new System.Drawing.Point(4, 25);
            this.tabPageSelection.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageSelection.Name = "tabPageSelection";
            this.tabPageSelection.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageSelection.Size = new System.Drawing.Size(542, 831);
            this.tabPageSelection.TabIndex = 0;
            this.tabPageSelection.Text = "Выборка";
            // 
            // checkBoxTime
            // 
            this.checkBoxTime.AutoSize = true;
            this.checkBoxTime.Location = new System.Drawing.Point(25, 560);
            this.checkBoxTime.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxTime.Name = "checkBoxTime";
            this.checkBoxTime.Size = new System.Drawing.Size(68, 20);
            this.checkBoxTime.TabIndex = 16;
            this.checkBoxTime.Text = "Время";
            this.checkBoxTime.UseVisualStyleBackColor = true;
            this.checkBoxTime.CheckedChanged += new System.EventHandler(this.CheckBoxSelection_CheckedChanged);
            // 
            // groupBoxTime
            // 
            this.groupBoxTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxTime.Controls.Add(this.timeTimePickerEnd);
            this.groupBoxTime.Controls.Add(this.timeTimePickerStart);
            this.groupBoxTime.Enabled = false;
            this.groupBoxTime.Location = new System.Drawing.Point(10, 560);
            this.groupBoxTime.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTime.Name = "groupBoxTime";
            this.groupBoxTime.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTime.Size = new System.Drawing.Size(520, 60);
            this.groupBoxTime.TabIndex = 15;
            this.groupBoxTime.TabStop = false;
            // 
            // timeTimePickerEnd
            // 
            this.timeTimePickerEnd.CustomFormat = "";
            this.timeTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeTimePickerEnd.Location = new System.Drawing.Point(280, 25);
            this.timeTimePickerEnd.Margin = new System.Windows.Forms.Padding(4);
            this.timeTimePickerEnd.Name = "timeTimePickerEnd";
            this.timeTimePickerEnd.ShowUpDown = true;
            this.timeTimePickerEnd.Size = new System.Drawing.Size(199, 22);
            this.timeTimePickerEnd.TabIndex = 1;
            this.timeTimePickerEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // timeTimePickerStart
            // 
            this.timeTimePickerStart.CustomFormat = "";
            this.timeTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeTimePickerStart.Location = new System.Drawing.Point(15, 25);
            this.timeTimePickerStart.Margin = new System.Windows.Forms.Padding(4);
            this.timeTimePickerStart.Name = "timeTimePickerStart";
            this.timeTimePickerStart.ShowUpDown = true;
            this.timeTimePickerStart.Size = new System.Drawing.Size(199, 22);
            this.timeTimePickerStart.TabIndex = 0;
            this.timeTimePickerStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxDepth
            // 
            this.checkBoxDepth.AutoSize = true;
            this.checkBoxDepth.Location = new System.Drawing.Point(25, 630);
            this.checkBoxDepth.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDepth.Name = "checkBoxDepth";
            this.checkBoxDepth.Size = new System.Drawing.Size(82, 20);
            this.checkBoxDepth.TabIndex = 14;
            this.checkBoxDepth.Text = "Глубина";
            this.checkBoxDepth.UseVisualStyleBackColor = true;
            this.checkBoxDepth.CheckedChanged += new System.EventHandler(this.CheckBoxSelection_CheckedChanged);
            // 
            // groupBoxDepth
            // 
            this.groupBoxDepth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxDepth.Controls.Add(this.numericUpDownEndDepth);
            this.groupBoxDepth.Controls.Add(this.numericUpDownStartDepth);
            this.groupBoxDepth.Enabled = false;
            this.groupBoxDepth.Location = new System.Drawing.Point(10, 630);
            this.groupBoxDepth.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxDepth.Name = "groupBoxDepth";
            this.groupBoxDepth.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxDepth.Size = new System.Drawing.Size(520, 60);
            this.groupBoxDepth.TabIndex = 13;
            this.groupBoxDepth.TabStop = false;
            // 
            // numericUpDownEndDepth
            // 
            this.numericUpDownEndDepth.Location = new System.Drawing.Point(100, 25);
            this.numericUpDownEndDepth.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownEndDepth.Name = "numericUpDownEndDepth";
            this.numericUpDownEndDepth.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownEndDepth.TabIndex = 1;
            this.numericUpDownEndDepth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownStartDepth
            // 
            this.numericUpDownStartDepth.Location = new System.Drawing.Point(15, 25);
            this.numericUpDownStartDepth.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownStartDepth.Name = "numericUpDownStartDepth";
            this.numericUpDownStartDepth.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownStartDepth.TabIndex = 0;
            this.numericUpDownStartDepth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxDate
            // 
            this.checkBoxDate.AutoSize = true;
            this.checkBoxDate.Location = new System.Drawing.Point(25, 490);
            this.checkBoxDate.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(59, 20);
            this.checkBoxDate.TabIndex = 12;
            this.checkBoxDate.Text = "Дата";
            this.checkBoxDate.UseVisualStyleBackColor = true;
            this.checkBoxDate.CheckedChanged += new System.EventHandler(this.CheckBoxSelection_CheckedChanged);
            // 
            // groupBoxDate
            // 
            this.groupBoxDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxDate.Controls.Add(this.dateTimePickerEnd);
            this.groupBoxDate.Controls.Add(this.dateTimePickerStart);
            this.groupBoxDate.Enabled = false;
            this.groupBoxDate.Location = new System.Drawing.Point(10, 490);
            this.groupBoxDate.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxDate.Name = "groupBoxDate";
            this.groupBoxDate.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxDate.Size = new System.Drawing.Size(520, 60);
            this.groupBoxDate.TabIndex = 11;
            this.groupBoxDate.TabStop = false;
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.CustomFormat = "";
            this.dateTimePickerEnd.Location = new System.Drawing.Point(280, 25);
            this.dateTimePickerEnd.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(199, 22);
            this.dateTimePickerEnd.TabIndex = 1;
            this.dateTimePickerEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.CustomFormat = "";
            this.dateTimePickerStart.Location = new System.Drawing.Point(15, 25);
            this.dateTimePickerStart.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(199, 22);
            this.dateTimePickerStart.TabIndex = 0;
            this.dateTimePickerStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxCoord
            // 
            this.checkBoxCoord.AutoSize = true;
            this.checkBoxCoord.Location = new System.Drawing.Point(25, 210);
            this.checkBoxCoord.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxCoord.Name = "checkBoxCoord";
            this.checkBoxCoord.Size = new System.Drawing.Size(107, 20);
            this.checkBoxCoord.TabIndex = 10;
            this.checkBoxCoord.Text = "Координаты";
            this.checkBoxCoord.UseVisualStyleBackColor = true;
            this.checkBoxCoord.CheckedChanged += new System.EventHandler(this.CheckBoxSelection_CheckedChanged);
            // 
            // groupBoxCoord
            // 
            this.groupBoxCoord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxCoord.Controls.Add(this.buttonShowCoord);
            this.groupBoxCoord.Controls.Add(this.tabControlCoord);
            this.groupBoxCoord.Enabled = false;
            this.groupBoxCoord.Location = new System.Drawing.Point(10, 210);
            this.groupBoxCoord.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxCoord.Name = "groupBoxCoord";
            this.groupBoxCoord.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxCoord.Size = new System.Drawing.Size(520, 270);
            this.groupBoxCoord.TabIndex = 6;
            this.groupBoxCoord.TabStop = false;
            // 
            // buttonShowCoord
            // 
            this.buttonShowCoord.Location = new System.Drawing.Point(405, 190);
            this.buttonShowCoord.Name = "buttonShowCoord";
            this.buttonShowCoord.Size = new System.Drawing.Size(100, 25);
            this.buttonShowCoord.TabIndex = 17;
            this.buttonShowCoord.Text = "Показать";
            this.buttonShowCoord.UseVisualStyleBackColor = true;
            this.buttonShowCoord.Click += new System.EventHandler(this.ButtonShowCoord_Click);
            // 
            // tabControlCoord
            // 
            this.tabControlCoord.Controls.Add(this.tabPageRectangle);
            this.tabControlCoord.Controls.Add(this.tabPageCircle);
            this.tabControlCoord.Controls.Add(this.tabPagePolygon);
            this.tabControlCoord.Controls.Add(this.tabPageIncision);
            this.tabControlCoord.Location = new System.Drawing.Point(15, 25);
            this.tabControlCoord.Name = "tabControlCoord";
            this.tabControlCoord.SelectedIndex = 0;
            this.tabControlCoord.Size = new System.Drawing.Size(500, 236);
            this.tabControlCoord.TabIndex = 4;
            this.tabControlCoord.SelectedIndexChanged += new System.EventHandler(this.TabControlCoord_SelectedIndexChanged);
            // 
            // tabPageRectangle
            // 
            this.tabPageRectangle.BackColor = System.Drawing.Color.Transparent;
            this.tabPageRectangle.Controls.Add(this.labelRecLng);
            this.tabPageRectangle.Controls.Add(this.labelRecLat);
            this.tabPageRectangle.Controls.Add(this.numericUpDownRecLatEnd);
            this.tabPageRectangle.Controls.Add(this.numericUpDownRecLatStart);
            this.tabPageRectangle.Controls.Add(this.numericUpDownRecLngEnd);
            this.tabPageRectangle.Controls.Add(this.numericUpDownRecLngStart);
            this.tabPageRectangle.Location = new System.Drawing.Point(4, 25);
            this.tabPageRectangle.Name = "tabPageRectangle";
            this.tabPageRectangle.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRectangle.Size = new System.Drawing.Size(492, 207);
            this.tabPageRectangle.TabIndex = 0;
            this.tabPageRectangle.Text = "Прямоугольник";
            // 
            // labelRecLng
            // 
            this.labelRecLng.AutoSize = true;
            this.labelRecLng.Location = new System.Drawing.Point(10, 47);
            this.labelRecLng.Name = "labelRecLng";
            this.labelRecLng.Size = new System.Drawing.Size(62, 16);
            this.labelRecLng.TabIndex = 15;
            this.labelRecLng.Text = "Долгота";
            // 
            // labelRecLat
            // 
            this.labelRecLat.AutoSize = true;
            this.labelRecLat.Location = new System.Drawing.Point(10, 17);
            this.labelRecLat.Name = "labelRecLat";
            this.labelRecLat.Size = new System.Drawing.Size(58, 16);
            this.labelRecLat.TabIndex = 14;
            this.labelRecLat.Text = "Широта";
            // 
            // numericUpDownRecLatEnd
            // 
            this.numericUpDownRecLatEnd.DecimalPlaces = 3;
            this.numericUpDownRecLatEnd.Location = new System.Drawing.Point(170, 15);
            this.numericUpDownRecLatEnd.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRecLatEnd.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownRecLatEnd.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownRecLatEnd.Name = "numericUpDownRecLatEnd";
            this.numericUpDownRecLatEnd.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownRecLatEnd.TabIndex = 13;
            this.numericUpDownRecLatEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownRecLatStart
            // 
            this.numericUpDownRecLatStart.DecimalPlaces = 3;
            this.numericUpDownRecLatStart.Location = new System.Drawing.Point(80, 15);
            this.numericUpDownRecLatStart.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRecLatStart.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownRecLatStart.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownRecLatStart.Name = "numericUpDownRecLatStart";
            this.numericUpDownRecLatStart.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownRecLatStart.TabIndex = 12;
            this.numericUpDownRecLatStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownRecLngEnd
            // 
            this.numericUpDownRecLngEnd.DecimalPlaces = 3;
            this.numericUpDownRecLngEnd.Location = new System.Drawing.Point(170, 45);
            this.numericUpDownRecLngEnd.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRecLngEnd.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownRecLngEnd.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownRecLngEnd.Name = "numericUpDownRecLngEnd";
            this.numericUpDownRecLngEnd.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownRecLngEnd.TabIndex = 11;
            this.numericUpDownRecLngEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownRecLngStart
            // 
            this.numericUpDownRecLngStart.DecimalPlaces = 3;
            this.numericUpDownRecLngStart.Location = new System.Drawing.Point(80, 45);
            this.numericUpDownRecLngStart.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRecLngStart.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownRecLngStart.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownRecLngStart.Name = "numericUpDownRecLngStart";
            this.numericUpDownRecLngStart.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownRecLngStart.TabIndex = 10;
            this.numericUpDownRecLngStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // tabPageCircle
            // 
            this.tabPageCircle.BackColor = System.Drawing.Color.Transparent;
            this.tabPageCircle.Controls.Add(this.labelCircleLng);
            this.tabPageCircle.Controls.Add(this.labelCircleRadius);
            this.tabPageCircle.Controls.Add(this.labelCircleLat);
            this.tabPageCircle.Controls.Add(this.numericUpDownRadius);
            this.tabPageCircle.Controls.Add(this.numericUpDownCenterLat);
            this.tabPageCircle.Controls.Add(this.numericUpDownCenterLng);
            this.tabPageCircle.Location = new System.Drawing.Point(4, 25);
            this.tabPageCircle.Name = "tabPageCircle";
            this.tabPageCircle.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCircle.Size = new System.Drawing.Size(492, 207);
            this.tabPageCircle.TabIndex = 1;
            this.tabPageCircle.Text = "Круг";
            // 
            // labelCircleLng
            // 
            this.labelCircleLng.AutoSize = true;
            this.labelCircleLng.Location = new System.Drawing.Point(10, 47);
            this.labelCircleLng.Name = "labelCircleLng";
            this.labelCircleLng.Size = new System.Drawing.Size(58, 16);
            this.labelCircleLng.TabIndex = 16;
            this.labelCircleLng.Text = "Широта";
            // 
            // labelCircleRadius
            // 
            this.labelCircleRadius.AutoSize = true;
            this.labelCircleRadius.Location = new System.Drawing.Point(10, 77);
            this.labelCircleRadius.Name = "labelCircleRadius";
            this.labelCircleRadius.Size = new System.Drawing.Size(56, 16);
            this.labelCircleRadius.TabIndex = 15;
            this.labelCircleRadius.Text = "Радиус";
            // 
            // labelCircleLat
            // 
            this.labelCircleLat.AutoSize = true;
            this.labelCircleLat.Location = new System.Drawing.Point(10, 17);
            this.labelCircleLat.Name = "labelCircleLat";
            this.labelCircleLat.Size = new System.Drawing.Size(58, 16);
            this.labelCircleLat.TabIndex = 14;
            this.labelCircleLat.Text = "Широта";
            // 
            // numericUpDownRadius
            // 
            this.numericUpDownRadius.DecimalPlaces = 3;
            this.numericUpDownRadius.Location = new System.Drawing.Point(80, 75);
            this.numericUpDownRadius.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownRadius.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownRadius.Name = "numericUpDownRadius";
            this.numericUpDownRadius.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownRadius.TabIndex = 13;
            this.numericUpDownRadius.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownCenterLat
            // 
            this.numericUpDownCenterLat.DecimalPlaces = 3;
            this.numericUpDownCenterLat.Location = new System.Drawing.Point(80, 45);
            this.numericUpDownCenterLat.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownCenterLat.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownCenterLat.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownCenterLat.Name = "numericUpDownCenterLat";
            this.numericUpDownCenterLat.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownCenterLat.TabIndex = 12;
            this.numericUpDownCenterLat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownCenterLng
            // 
            this.numericUpDownCenterLng.DecimalPlaces = 3;
            this.numericUpDownCenterLng.Location = new System.Drawing.Point(80, 15);
            this.numericUpDownCenterLng.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownCenterLng.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownCenterLng.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownCenterLng.Name = "numericUpDownCenterLng";
            this.numericUpDownCenterLng.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownCenterLng.TabIndex = 11;
            this.numericUpDownCenterLng.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // tabPagePolygon
            // 
            this.tabPagePolygon.BackColor = System.Drawing.Color.Transparent;
            this.tabPagePolygon.Controls.Add(this.buttonPolygon);
            this.tabPagePolygon.Controls.Add(this.dataGridViewPolygon);
            this.tabPagePolygon.Location = new System.Drawing.Point(4, 25);
            this.tabPagePolygon.Name = "tabPagePolygon";
            this.tabPagePolygon.Size = new System.Drawing.Size(492, 207);
            this.tabPagePolygon.TabIndex = 2;
            this.tabPagePolygon.Text = "Многоугольник";
            // 
            // buttonPolygon
            // 
            this.buttonPolygon.Location = new System.Drawing.Point(385, 170);
            this.buttonPolygon.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPolygon.Name = "buttonPolygon";
            this.buttonPolygon.Size = new System.Drawing.Size(100, 25);
            this.buttonPolygon.TabIndex = 3;
            this.buttonPolygon.Text = "Применить";
            this.buttonPolygon.UseVisualStyleBackColor = true;
            this.buttonPolygon.Click += new System.EventHandler(this.ButtonApplyCoord_Click);
            // 
            // dataGridViewPolygon
            // 
            this.dataGridViewPolygon.AllowUserToResizeColumns = false;
            this.dataGridViewPolygon.AllowUserToResizeRows = false;
            this.dataGridViewPolygon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPolygon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPolygon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPolygon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPolygonId,
            this.ColumnLat,
            this.ColumnLng});
            this.dataGridViewPolygon.Location = new System.Drawing.Point(15, 15);
            this.dataGridViewPolygon.Name = "dataGridViewPolygon";
            this.dataGridViewPolygon.RowHeadersWidth = 25;
            this.dataGridViewPolygon.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewPolygon.Size = new System.Drawing.Size(203, 303);
            this.dataGridViewPolygon.TabIndex = 0;
            this.dataGridViewPolygon.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DataGridViewPolygon_RowPostPaint);
            // 
            // ColumnPolygonId
            // 
            this.ColumnPolygonId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnPolygonId.Frozen = true;
            this.ColumnPolygonId.HeaderText = "№";
            this.ColumnPolygonId.Name = "ColumnPolygonId";
            this.ColumnPolygonId.ReadOnly = true;
            this.ColumnPolygonId.Width = 30;
            // 
            // ColumnLat
            // 
            this.ColumnLat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnLat.HeaderText = "Широта";
            this.ColumnLat.Name = "ColumnLat";
            // 
            // ColumnLng
            // 
            this.ColumnLng.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnLng.HeaderText = "Долгота";
            this.ColumnLng.Name = "ColumnLng";
            // 
            // tabPageIncision
            // 
            this.tabPageIncision.BackColor = System.Drawing.Color.Transparent;
            this.tabPageIncision.Controls.Add(this.buttonIncision);
            this.tabPageIncision.Controls.Add(this.dataGridViewIncision);
            this.tabPageIncision.Location = new System.Drawing.Point(4, 25);
            this.tabPageIncision.Name = "tabPageIncision";
            this.tabPageIncision.Size = new System.Drawing.Size(492, 207);
            this.tabPageIncision.TabIndex = 3;
            this.tabPageIncision.Text = "Разрез";
            // 
            // buttonIncision
            // 
            this.buttonIncision.Location = new System.Drawing.Point(385, 170);
            this.buttonIncision.Margin = new System.Windows.Forms.Padding(4);
            this.buttonIncision.Name = "buttonIncision";
            this.buttonIncision.Size = new System.Drawing.Size(100, 25);
            this.buttonIncision.TabIndex = 4;
            this.buttonIncision.Text = "Применить";
            this.buttonIncision.UseVisualStyleBackColor = true;
            this.buttonIncision.Click += new System.EventHandler(this.ButtonApplyCoord_Click);
            // 
            // dataGridViewIncision
            // 
            this.dataGridViewIncision.AllowUserToResizeColumns = false;
            this.dataGridViewIncision.AllowUserToResizeRows = false;
            this.dataGridViewIncision.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewIncision.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewIncision.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIncision.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIncisionId,
            this.ColumnLatStart,
            this.ColumnLngStart,
            this.ColumnLatEnd,
            this.ColumnLngEnd,
            this.ColumnLength});
            this.dataGridViewIncision.Location = new System.Drawing.Point(15, 15);
            this.dataGridViewIncision.Name = "dataGridViewIncision";
            this.dataGridViewIncision.RowHeadersWidth = 25;
            this.dataGridViewIncision.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewIncision.Size = new System.Drawing.Size(365, 303);
            this.dataGridViewIncision.TabIndex = 1;
            this.dataGridViewIncision.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DataGridViewIncision_RowPostPaint);
            // 
            // ColumnIncisionId
            // 
            this.ColumnIncisionId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnIncisionId.Frozen = true;
            this.ColumnIncisionId.HeaderText = "№";
            this.ColumnIncisionId.Name = "ColumnIncisionId";
            this.ColumnIncisionId.ReadOnly = true;
            this.ColumnIncisionId.Width = 30;
            // 
            // ColumnLatStart
            // 
            this.ColumnLatStart.HeaderText = "Широта";
            this.ColumnLatStart.Name = "ColumnLatStart";
            // 
            // ColumnLngStart
            // 
            this.ColumnLngStart.HeaderText = "Долгота";
            this.ColumnLngStart.Name = "ColumnLngStart";
            // 
            // ColumnLatEnd
            // 
            this.ColumnLatEnd.HeaderText = "Широта";
            this.ColumnLatEnd.Name = "ColumnLatEnd";
            // 
            // ColumnLngEnd
            // 
            this.ColumnLngEnd.HeaderText = "Долгота";
            this.ColumnLngEnd.Name = "ColumnLngEnd";
            // 
            // ColumnLength
            // 
            this.ColumnLength.FillWeight = 80F;
            this.ColumnLength.HeaderText = "Длина";
            this.ColumnLength.Name = "ColumnLength";
            // 
            // groupBoxSpeed
            // 
            this.groupBoxSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSpeed.Controls.Add(this.trackBarVmin);
            this.groupBoxSpeed.Controls.Add(this.labelVMinMax);
            this.groupBoxSpeed.Controls.Add(this.trackBarVmax);
            this.groupBoxSpeed.Location = new System.Drawing.Point(10, 110);
            this.groupBoxSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxSpeed.Name = "groupBoxSpeed";
            this.groupBoxSpeed.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxSpeed.Size = new System.Drawing.Size(520, 90);
            this.groupBoxSpeed.TabIndex = 5;
            this.groupBoxSpeed.TabStop = false;
            this.groupBoxSpeed.Text = "Скорость";
            // 
            // trackBarVmin
            // 
            this.trackBarVmin.AutoSize = false;
            this.trackBarVmin.Location = new System.Drawing.Point(5, 60);
            this.trackBarVmin.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarVmin.Name = "trackBarVmin";
            this.trackBarVmin.Size = new System.Drawing.Size(300, 25);
            this.trackBarVmin.TabIndex = 2;
            this.trackBarVmin.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBarVmin.Scroll += new System.EventHandler(this.TrackBarV_Scroll);
            // 
            // labelVMinMax
            // 
            this.labelVMinMax.AutoSize = true;
            this.labelVMinMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVMinMax.Location = new System.Drawing.Point(325, 50);
            this.labelVMinMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelVMinMax.Name = "labelVMinMax";
            this.labelVMinMax.Size = new System.Drawing.Size(17, 16);
            this.labelVMinMax.TabIndex = 3;
            this.labelVMinMax.Text = "V";
            this.labelVMinMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarVmax
            // 
            this.trackBarVmax.AutoSize = false;
            this.trackBarVmax.Location = new System.Drawing.Point(5, 20);
            this.trackBarVmax.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarVmax.Name = "trackBarVmax";
            this.trackBarVmax.Size = new System.Drawing.Size(300, 25);
            this.trackBarVmax.TabIndex = 1;
            this.trackBarVmax.Scroll += new System.EventHandler(this.TrackBarV_Scroll);
            // 
            // groupBoxEnergy
            // 
            this.groupBoxEnergy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxEnergy.Controls.Add(this.trackBarKmin);
            this.groupBoxEnergy.Controls.Add(this.labelKMinMax);
            this.groupBoxEnergy.Controls.Add(this.trackBarKmax);
            this.groupBoxEnergy.Location = new System.Drawing.Point(10, 10);
            this.groupBoxEnergy.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxEnergy.Name = "groupBoxEnergy";
            this.groupBoxEnergy.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxEnergy.Size = new System.Drawing.Size(520, 90);
            this.groupBoxEnergy.TabIndex = 4;
            this.groupBoxEnergy.TabStop = false;
            this.groupBoxEnergy.Text = "Класс";
            // 
            // trackBarKmin
            // 
            this.trackBarKmin.AutoSize = false;
            this.trackBarKmin.Location = new System.Drawing.Point(5, 60);
            this.trackBarKmin.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarKmin.Name = "trackBarKmin";
            this.trackBarKmin.Size = new System.Drawing.Size(300, 25);
            this.trackBarKmin.TabIndex = 2;
            this.trackBarKmin.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBarKmin.Scroll += new System.EventHandler(this.TrackBarK_Scroll);
            // 
            // labelKMinMax
            // 
            this.labelKMinMax.AutoSize = true;
            this.labelKMinMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelKMinMax.Location = new System.Drawing.Point(325, 50);
            this.labelKMinMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelKMinMax.Name = "labelKMinMax";
            this.labelKMinMax.Size = new System.Drawing.Size(16, 16);
            this.labelKMinMax.TabIndex = 3;
            this.labelKMinMax.Text = "K";
            this.labelKMinMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarKmax
            // 
            this.trackBarKmax.AutoSize = false;
            this.trackBarKmax.Location = new System.Drawing.Point(5, 20);
            this.trackBarKmax.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarKmax.Name = "trackBarKmax";
            this.trackBarKmax.Size = new System.Drawing.Size(300, 25);
            this.trackBarKmax.TabIndex = 1;
            this.trackBarKmax.Scroll += new System.EventHandler(this.TrackBarK_Scroll);
            // 
            // tabPageSelectionAdditional
            // 
            this.tabPageSelectionAdditional.BackColor = System.Drawing.Color.Transparent;
            this.tabPageSelectionAdditional.Controls.Add(this.checkBoxNumeration);
            this.tabPageSelectionAdditional.Controls.Add(this.groupBoxNumeration);
            this.tabPageSelectionAdditional.Location = new System.Drawing.Point(4, 25);
            this.tabPageSelectionAdditional.Name = "tabPageSelectionAdditional";
            this.tabPageSelectionAdditional.Size = new System.Drawing.Size(542, 831);
            this.tabPageSelectionAdditional.TabIndex = 5;
            this.tabPageSelectionAdditional.Text = "Выборка (дополнительно)";
            // 
            // checkBoxNumeration
            // 
            this.checkBoxNumeration.AutoSize = true;
            this.checkBoxNumeration.Location = new System.Drawing.Point(25, 10);
            this.checkBoxNumeration.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxNumeration.Name = "checkBoxNumeration";
            this.checkBoxNumeration.Size = new System.Drawing.Size(101, 20);
            this.checkBoxNumeration.TabIndex = 9;
            this.checkBoxNumeration.Text = "Нумерация";
            this.checkBoxNumeration.UseVisualStyleBackColor = true;
            this.checkBoxNumeration.CheckedChanged += new System.EventHandler(this.CheckBoxNumeration_CheckedChanged);
            // 
            // groupBoxNumeration
            // 
            this.groupBoxNumeration.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxNumeration.Controls.Add(this.radioButtonNumerationDepth);
            this.groupBoxNumeration.Controls.Add(this.radioButtonCouple);
            this.groupBoxNumeration.Controls.Add(this.radioButtonNumerationCommon);
            this.groupBoxNumeration.Controls.Add(this.tableLayoutPanelNumeration);
            this.groupBoxNumeration.Controls.Add(this.groupBoxNumerationCommon);
            this.groupBoxNumeration.Controls.Add(this.groupBoxCouple);
            this.groupBoxNumeration.Enabled = false;
            this.groupBoxNumeration.Location = new System.Drawing.Point(10, 10);
            this.groupBoxNumeration.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxNumeration.Name = "groupBoxNumeration";
            this.groupBoxNumeration.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxNumeration.Size = new System.Drawing.Size(520, 810);
            this.groupBoxNumeration.TabIndex = 10;
            this.groupBoxNumeration.TabStop = false;
            // 
            // radioButtonNumerationDepth
            // 
            this.radioButtonNumerationDepth.AutoSize = true;
            this.radioButtonNumerationDepth.Location = new System.Drawing.Point(25, 415);
            this.radioButtonNumerationDepth.Name = "radioButtonNumerationDepth";
            this.radioButtonNumerationDepth.Size = new System.Drawing.Size(81, 20);
            this.radioButtonNumerationDepth.TabIndex = 25;
            this.radioButtonNumerationDepth.Text = "Глубина";
            this.radioButtonNumerationDepth.UseVisualStyleBackColor = true;
            this.radioButtonNumerationDepth.CheckedChanged += new System.EventHandler(this.RadioButtonNumerationDepth_CheckedChanged);
            // 
            // radioButtonCouple
            // 
            this.radioButtonCouple.AutoSize = true;
            this.radioButtonCouple.Location = new System.Drawing.Point(25, 215);
            this.radioButtonCouple.Name = "radioButtonCouple";
            this.radioButtonCouple.Size = new System.Drawing.Size(60, 20);
            this.radioButtonCouple.TabIndex = 22;
            this.radioButtonCouple.Text = "Пара";
            this.radioButtonCouple.UseVisualStyleBackColor = true;
            this.radioButtonCouple.CheckedChanged += new System.EventHandler(this.RadioButtonCouple_CheckedChanged);
            // 
            // radioButtonNumerationCommon
            // 
            this.radioButtonNumerationCommon.AutoSize = true;
            this.radioButtonNumerationCommon.Checked = true;
            this.radioButtonNumerationCommon.Location = new System.Drawing.Point(25, 150);
            this.radioButtonNumerationCommon.Name = "radioButtonNumerationCommon";
            this.radioButtonNumerationCommon.Size = new System.Drawing.Size(84, 20);
            this.radioButtonNumerationCommon.TabIndex = 11;
            this.radioButtonNumerationCommon.TabStop = true;
            this.radioButtonNumerationCommon.Text = "Обычная";
            this.radioButtonNumerationCommon.UseVisualStyleBackColor = true;
            this.radioButtonNumerationCommon.CheckedChanged += new System.EventHandler(this.RadioButtonNumerationCommon_CheckedChanged);
            // 
            // tableLayoutPanelNumeration
            // 
            this.tableLayoutPanelNumeration.AutoSize = true;
            this.tableLayoutPanelNumeration.ColumnCount = 2;
            this.tableLayoutPanelNumeration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelNumeration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelNumeration.Controls.Add(this.labelNumerationColor, 0, 1);
            this.tableLayoutPanelNumeration.Controls.Add(this.labelNumerationWidth, 0, 0);
            this.tableLayoutPanelNumeration.Controls.Add(this.numericUpDownNumerationWidth, 1, 0);
            this.tableLayoutPanelNumeration.Controls.Add(this.colorPickerNumeration, 1, 1);
            this.tableLayoutPanelNumeration.Controls.Add(this.buttonNumerationApply, 1, 3);
            this.tableLayoutPanelNumeration.Controls.Add(this.checkBoxNumerationAutoSize, 1, 2);
            this.tableLayoutPanelNumeration.Location = new System.Drawing.Point(15, 25);
            this.tableLayoutPanelNumeration.Name = "tableLayoutPanelNumeration";
            this.tableLayoutPanelNumeration.RowCount = 4;
            this.tableLayoutPanelNumeration.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelNumeration.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelNumeration.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelNumeration.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelNumeration.Size = new System.Drawing.Size(196, 117);
            this.tableLayoutPanelNumeration.TabIndex = 20;
            // 
            // labelNumerationColor
            // 
            this.labelNumerationColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelNumerationColor.AutoSize = true;
            this.labelNumerationColor.Location = new System.Drawing.Point(3, 37);
            this.labelNumerationColor.Name = "labelNumerationColor";
            this.labelNumerationColor.Size = new System.Drawing.Size(40, 16);
            this.labelNumerationColor.TabIndex = 22;
            this.labelNumerationColor.Text = "Цвет";
            // 
            // labelNumerationWidth
            // 
            this.labelNumerationWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelNumerationWidth.AutoSize = true;
            this.labelNumerationWidth.Location = new System.Drawing.Point(3, 7);
            this.labelNumerationWidth.Name = "labelNumerationWidth";
            this.labelNumerationWidth.Size = new System.Drawing.Size(59, 16);
            this.labelNumerationWidth.TabIndex = 21;
            this.labelNumerationWidth.Text = "Ширина";
            // 
            // numericUpDownNumerationWidth
            // 
            this.numericUpDownNumerationWidth.Location = new System.Drawing.Point(69, 4);
            this.numericUpDownNumerationWidth.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownNumerationWidth.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numericUpDownNumerationWidth.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDownNumerationWidth.Name = "numericUpDownNumerationWidth";
            this.numericUpDownNumerationWidth.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownNumerationWidth.TabIndex = 3;
            this.numericUpDownNumerationWidth.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // colorPickerNumeration
            // 
            this.colorPickerNumeration.BackColor = System.Drawing.Color.Black;
            this.colorPickerNumeration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerNumeration.Location = new System.Drawing.Point(69, 34);
            this.colorPickerNumeration.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerNumeration.Name = "colorPickerNumeration";
            this.colorPickerNumeration.Size = new System.Drawing.Size(75, 22);
            this.colorPickerNumeration.TabIndex = 4;
            this.colorPickerNumeration.TabStop = false;
            this.colorPickerNumeration.Click += new System.EventHandler(this.ColorPickerNumeration_Click);
            // 
            // buttonNumerationApply
            // 
            this.buttonNumerationApply.Location = new System.Drawing.Point(68, 89);
            this.buttonNumerationApply.Name = "buttonNumerationApply";
            this.buttonNumerationApply.Size = new System.Drawing.Size(100, 25);
            this.buttonNumerationApply.TabIndex = 18;
            this.buttonNumerationApply.Text = "Применить";
            this.buttonNumerationApply.UseVisualStyleBackColor = true;
            this.buttonNumerationApply.Click += new System.EventHandler(this.ButtonNumerationApply_Click);
            // 
            // checkBoxNumerationAutoSize
            // 
            this.checkBoxNumerationAutoSize.AutoSize = true;
            this.checkBoxNumerationAutoSize.Location = new System.Drawing.Point(68, 63);
            this.checkBoxNumerationAutoSize.Name = "checkBoxNumerationAutoSize";
            this.checkBoxNumerationAutoSize.Size = new System.Drawing.Size(108, 20);
            this.checkBoxNumerationAutoSize.TabIndex = 26;
            this.checkBoxNumerationAutoSize.Text = "Авторазмер";
            this.checkBoxNumerationAutoSize.UseVisualStyleBackColor = true;
            // 
            // groupBoxNumerationCommon
            // 
            this.groupBoxNumerationCommon.Controls.Add(this.buttonNumerationCommonApply);
            this.groupBoxNumerationCommon.Location = new System.Drawing.Point(15, 150);
            this.groupBoxNumerationCommon.Name = "groupBoxNumerationCommon";
            this.groupBoxNumerationCommon.Size = new System.Drawing.Size(490, 60);
            this.groupBoxNumerationCommon.TabIndex = 23;
            this.groupBoxNumerationCommon.TabStop = false;
            // 
            // buttonNumerationCommonApply
            // 
            this.buttonNumerationCommonApply.Location = new System.Drawing.Point(15, 25);
            this.buttonNumerationCommonApply.Name = "buttonNumerationCommonApply";
            this.buttonNumerationCommonApply.Size = new System.Drawing.Size(100, 25);
            this.buttonNumerationCommonApply.TabIndex = 19;
            this.buttonNumerationCommonApply.Text = "Применить";
            this.buttonNumerationCommonApply.UseVisualStyleBackColor = true;
            this.buttonNumerationCommonApply.Click += new System.EventHandler(this.ButtonNumerationCommonApply_Click);
            // 
            // groupBoxCouple
            // 
            this.groupBoxCouple.Controls.Add(this.tableLayoutPanelCouple);
            this.groupBoxCouple.Enabled = false;
            this.groupBoxCouple.Location = new System.Drawing.Point(15, 215);
            this.groupBoxCouple.Name = "groupBoxCouple";
            this.groupBoxCouple.Size = new System.Drawing.Size(490, 190);
            this.groupBoxCouple.TabIndex = 4;
            this.groupBoxCouple.TabStop = false;
            // 
            // tableLayoutPanelCouple
            // 
            this.tableLayoutPanelCouple.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelCouple.AutoSize = true;
            this.tableLayoutPanelCouple.ColumnCount = 3;
            this.tableLayoutPanelCouple.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCouple.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCouple.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCouple.Controls.Add(this.numericUpDownCoupleKEnd, 2, 0);
            this.tableLayoutPanelCouple.Controls.Add(this.numericUpDownCoupleKBegin, 1, 0);
            this.tableLayoutPanelCouple.Controls.Add(this.labelCoupleK, 0, 0);
            this.tableLayoutPanelCouple.Controls.Add(this.labelCoupleDistance, 0, 3);
            this.tableLayoutPanelCouple.Controls.Add(this.numericUpDownCoupleDistance, 1, 3);
            this.tableLayoutPanelCouple.Controls.Add(this.numericUpDownCoupleDays, 1, 2);
            this.tableLayoutPanelCouple.Controls.Add(this.dateTimePickerCoupleTime, 1, 1);
            this.tableLayoutPanelCouple.Controls.Add(this.labelCoupleDays, 0, 2);
            this.tableLayoutPanelCouple.Controls.Add(this.labelCoupleTime, 0, 1);
            this.tableLayoutPanelCouple.Controls.Add(this.buttonCoupleApply, 0, 4);
            this.tableLayoutPanelCouple.Location = new System.Drawing.Point(10, 35);
            this.tableLayoutPanelCouple.Name = "tableLayoutPanelCouple";
            this.tableLayoutPanelCouple.RowCount = 5;
            this.tableLayoutPanelCouple.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCouple.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCouple.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCouple.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCouple.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCouple.Size = new System.Drawing.Size(327, 143);
            this.tableLayoutPanelCouple.TabIndex = 2;
            // 
            // numericUpDownCoupleKEnd
            // 
            this.numericUpDownCoupleKEnd.DecimalPlaces = 3;
            this.numericUpDownCoupleKEnd.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownCoupleKEnd.Location = new System.Drawing.Point(227, 3);
            this.numericUpDownCoupleKEnd.Maximum = new decimal(new int[] {
            22,
            0,
            0,
            0});
            this.numericUpDownCoupleKEnd.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownCoupleKEnd.Name = "numericUpDownCoupleKEnd";
            this.numericUpDownCoupleKEnd.Size = new System.Drawing.Size(97, 22);
            this.numericUpDownCoupleKEnd.TabIndex = 7;
            this.numericUpDownCoupleKEnd.Value = new decimal(new int[] {
            85,
            0,
            0,
            65536});
            // 
            // numericUpDownCoupleKBegin
            // 
            this.numericUpDownCoupleKBegin.DecimalPlaces = 3;
            this.numericUpDownCoupleKBegin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownCoupleKBegin.Location = new System.Drawing.Point(124, 3);
            this.numericUpDownCoupleKBegin.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownCoupleKBegin.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownCoupleKBegin.Name = "numericUpDownCoupleKBegin";
            this.numericUpDownCoupleKBegin.Size = new System.Drawing.Size(97, 22);
            this.numericUpDownCoupleKBegin.TabIndex = 6;
            this.numericUpDownCoupleKBegin.Value = new decimal(new int[] {
            75,
            0,
            0,
            65536});
            this.numericUpDownCoupleKBegin.ValueChanged += new System.EventHandler(this.NumericUpDownCoupleKBegin_ValueChanged);
            // 
            // labelCoupleK
            // 
            this.labelCoupleK.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCoupleK.AutoSize = true;
            this.labelCoupleK.Location = new System.Drawing.Point(3, 6);
            this.labelCoupleK.Name = "labelCoupleK";
            this.labelCoupleK.Size = new System.Drawing.Size(46, 16);
            this.labelCoupleK.TabIndex = 7;
            this.labelCoupleK.Text = "Класс";
            // 
            // labelCoupleDistance
            // 
            this.labelCoupleDistance.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCoupleDistance.AutoSize = true;
            this.labelCoupleDistance.Location = new System.Drawing.Point(3, 90);
            this.labelCoupleDistance.Name = "labelCoupleDistance";
            this.labelCoupleDistance.Size = new System.Drawing.Size(115, 16);
            this.labelCoupleDistance.TabIndex = 8;
            this.labelCoupleDistance.Text = "Расстояние (км.)";
            // 
            // numericUpDownCoupleDistance
            // 
            this.numericUpDownCoupleDistance.Location = new System.Drawing.Point(124, 87);
            this.numericUpDownCoupleDistance.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownCoupleDistance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCoupleDistance.Name = "numericUpDownCoupleDistance";
            this.numericUpDownCoupleDistance.Size = new System.Drawing.Size(97, 22);
            this.numericUpDownCoupleDistance.TabIndex = 3;
            this.numericUpDownCoupleDistance.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownCoupleDistance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownCoupleDays
            // 
            this.numericUpDownCoupleDays.Location = new System.Drawing.Point(124, 59);
            this.numericUpDownCoupleDays.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownCoupleDays.Name = "numericUpDownCoupleDays";
            this.numericUpDownCoupleDays.Size = new System.Drawing.Size(97, 22);
            this.numericUpDownCoupleDays.TabIndex = 1;
            this.numericUpDownCoupleDays.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // dateTimePickerCoupleTime
            // 
            this.dateTimePickerCoupleTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerCoupleTime.Location = new System.Drawing.Point(124, 31);
            this.dateTimePickerCoupleTime.Name = "dateTimePickerCoupleTime";
            this.dateTimePickerCoupleTime.ShowUpDown = true;
            this.dateTimePickerCoupleTime.Size = new System.Drawing.Size(97, 22);
            this.dateTimePickerCoupleTime.TabIndex = 0;
            this.dateTimePickerCoupleTime.Value = new System.DateTime(2019, 3, 25, 0, 10, 0, 0);
            this.dateTimePickerCoupleTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // labelCoupleDays
            // 
            this.labelCoupleDays.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCoupleDays.AutoSize = true;
            this.labelCoupleDays.Location = new System.Drawing.Point(3, 62);
            this.labelCoupleDays.Name = "labelCoupleDays";
            this.labelCoupleDays.Size = new System.Drawing.Size(33, 16);
            this.labelCoupleDays.TabIndex = 7;
            this.labelCoupleDays.Text = "Дни";
            // 
            // labelCoupleTime
            // 
            this.labelCoupleTime.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCoupleTime.AutoSize = true;
            this.labelCoupleTime.Location = new System.Drawing.Point(3, 34);
            this.labelCoupleTime.Name = "labelCoupleTime";
            this.labelCoupleTime.Size = new System.Drawing.Size(49, 16);
            this.labelCoupleTime.TabIndex = 6;
            this.labelCoupleTime.Text = "Время";
            // 
            // buttonCoupleApply
            // 
            this.buttonCoupleApply.Location = new System.Drawing.Point(3, 115);
            this.buttonCoupleApply.Name = "buttonCoupleApply";
            this.buttonCoupleApply.Size = new System.Drawing.Size(100, 25);
            this.buttonCoupleApply.TabIndex = 19;
            this.buttonCoupleApply.Text = "Применить";
            this.buttonCoupleApply.UseVisualStyleBackColor = true;
            this.buttonCoupleApply.Click += new System.EventHandler(this.ButtonCoupleApply_Click);
            // 
            // tabPageLegendAndMarker
            // 
            this.tabPageLegendAndMarker.BackColor = System.Drawing.Color.Transparent;
            this.tabPageLegendAndMarker.Controls.Add(this.checkBoxDontShowIfZero);
            this.tabPageLegendAndMarker.Controls.Add(this.tableLayoutPanelMarkerColor);
            this.tabPageLegendAndMarker.Controls.Add(this.buttonMarkerClass);
            this.tabPageLegendAndMarker.Controls.Add(this.labelMarkerRadius);
            this.tabPageLegendAndMarker.Controls.Add(this.trackBarMarkerRadius);
            this.tabPageLegendAndMarker.Controls.Add(this.buttonMarkerVpVs);
            this.tabPageLegendAndMarker.Controls.Add(this.checkBoxLegendOut);
            this.tabPageLegendAndMarker.Controls.Add(this.checkBoxLegend);
            this.tabPageLegendAndMarker.Location = new System.Drawing.Point(4, 25);
            this.tabPageLegendAndMarker.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageLegendAndMarker.Name = "tabPageLegendAndMarker";
            this.tabPageLegendAndMarker.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageLegendAndMarker.Size = new System.Drawing.Size(542, 831);
            this.tabPageLegendAndMarker.TabIndex = 1;
            this.tabPageLegendAndMarker.Text = "Легенда и маркер";
            // 
            // checkBoxDontShowIfZero
            // 
            this.checkBoxDontShowIfZero.AutoSize = true;
            this.checkBoxDontShowIfZero.Checked = true;
            this.checkBoxDontShowIfZero.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDontShowIfZero.Location = new System.Drawing.Point(15, 215);
            this.checkBoxDontShowIfZero.Name = "checkBoxDontShowIfZero";
            this.checkBoxDontShowIfZero.Size = new System.Drawing.Size(379, 20);
            this.checkBoxDontShowIfZero.TabIndex = 8;
            this.checkBoxDontShowIfZero.Text = "Не показывать цвет, если маркеров в этом периоде 0";
            this.checkBoxDontShowIfZero.UseVisualStyleBackColor = true;
            this.checkBoxDontShowIfZero.CheckedChanged += new System.EventHandler(this.CheckBoxDontShowIfZero_CheckedChanged);
            // 
            // tableLayoutPanelMarkerColor
            // 
            this.tableLayoutPanelMarkerColor.AutoSize = true;
            this.tableLayoutPanelMarkerColor.ColumnCount = 2;
            this.tableLayoutPanelMarkerColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMarkerColor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMarkerColor.Controls.Add(this.radioButtonMarkerColorVpVs, 0, 0);
            this.tableLayoutPanelMarkerColor.Controls.Add(this.radioButtonMarkerColorClass, 1, 0);
            this.tableLayoutPanelMarkerColor.Location = new System.Drawing.Point(15, 170);
            this.tableLayoutPanelMarkerColor.Name = "tableLayoutPanelMarkerColor";
            this.tableLayoutPanelMarkerColor.RowCount = 1;
            this.tableLayoutPanelMarkerColor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMarkerColor.Size = new System.Drawing.Size(158, 30);
            this.tableLayoutPanelMarkerColor.TabIndex = 7;
            // 
            // radioButtonMarkerColorVpVs
            // 
            this.radioButtonMarkerColorVpVs.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonMarkerColorVpVs.AutoSize = true;
            this.radioButtonMarkerColorVpVs.Checked = true;
            this.radioButtonMarkerColorVpVs.Location = new System.Drawing.Point(3, 5);
            this.radioButtonMarkerColorVpVs.Name = "radioButtonMarkerColorVpVs";
            this.radioButtonMarkerColorVpVs.Size = new System.Drawing.Size(59, 20);
            this.radioButtonMarkerColorVpVs.TabIndex = 0;
            this.radioButtonMarkerColorVpVs.TabStop = true;
            this.radioButtonMarkerColorVpVs.Text = "VpVs";
            this.radioButtonMarkerColorVpVs.UseVisualStyleBackColor = true;
            this.radioButtonMarkerColorVpVs.CheckedChanged += new System.EventHandler(this.RadioButtonMarkerColor_CheckedChanged);
            // 
            // radioButtonMarkerColorClass
            // 
            this.radioButtonMarkerColorClass.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonMarkerColorClass.AutoSize = true;
            this.radioButtonMarkerColorClass.Location = new System.Drawing.Point(82, 5);
            this.radioButtonMarkerColorClass.Name = "radioButtonMarkerColorClass";
            this.radioButtonMarkerColorClass.Size = new System.Drawing.Size(73, 20);
            this.radioButtonMarkerColorClass.TabIndex = 1;
            this.radioButtonMarkerColorClass.Text = "Классы";
            this.radioButtonMarkerColorClass.UseVisualStyleBackColor = true;
            this.radioButtonMarkerColorClass.CheckedChanged += new System.EventHandler(this.RadioButtonMarkerColor_CheckedChanged);
            // 
            // buttonMarkerClass
            // 
            this.buttonMarkerClass.Location = new System.Drawing.Point(15, 133);
            this.buttonMarkerClass.Margin = new System.Windows.Forms.Padding(4);
            this.buttonMarkerClass.Name = "buttonMarkerClass";
            this.buttonMarkerClass.Size = new System.Drawing.Size(75, 25);
            this.buttonMarkerClass.TabIndex = 6;
            this.buttonMarkerClass.Text = "Класс";
            this.buttonMarkerClass.UseVisualStyleBackColor = true;
            this.buttonMarkerClass.Click += new System.EventHandler(this.ButtonClass_Click);
            // 
            // labelMarkerRadius
            // 
            this.labelMarkerRadius.AutoSize = true;
            this.labelMarkerRadius.Location = new System.Drawing.Point(15, 775);
            this.labelMarkerRadius.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMarkerRadius.Name = "labelMarkerRadius";
            this.labelMarkerRadius.Size = new System.Drawing.Size(24, 16);
            this.labelMarkerRadius.TabIndex = 5;
            this.labelMarkerRadius.Text = "x 1";
            // 
            // trackBarMarkerRadius
            // 
            this.trackBarMarkerRadius.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarMarkerRadius.Location = new System.Drawing.Point(5, 720);
            this.trackBarMarkerRadius.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarMarkerRadius.Maximum = 40;
            this.trackBarMarkerRadius.Minimum = 1;
            this.trackBarMarkerRadius.Name = "trackBarMarkerRadius";
            this.trackBarMarkerRadius.Size = new System.Drawing.Size(530, 45);
            this.trackBarMarkerRadius.TabIndex = 4;
            this.trackBarMarkerRadius.Value = 1;
            this.trackBarMarkerRadius.Scroll += new System.EventHandler(this.TrackBarRadius_Scroll);
            // 
            // buttonMarkerVpVs
            // 
            this.buttonMarkerVpVs.Location = new System.Drawing.Point(15, 100);
            this.buttonMarkerVpVs.Margin = new System.Windows.Forms.Padding(4);
            this.buttonMarkerVpVs.Name = "buttonMarkerVpVs";
            this.buttonMarkerVpVs.Size = new System.Drawing.Size(75, 25);
            this.buttonMarkerVpVs.TabIndex = 2;
            this.buttonMarkerVpVs.Text = "VpVs";
            this.buttonMarkerVpVs.UseVisualStyleBackColor = true;
            this.buttonMarkerVpVs.Click += new System.EventHandler(this.ButtonVpVs_Click);
            // 
            // checkBoxLegendOut
            // 
            this.checkBoxLegendOut.AutoSize = true;
            this.checkBoxLegendOut.Checked = true;
            this.checkBoxLegendOut.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLegendOut.Location = new System.Drawing.Point(15, 40);
            this.checkBoxLegendOut.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxLegendOut.Name = "checkBoxLegendOut";
            this.checkBoxLegendOut.Size = new System.Drawing.Size(85, 20);
            this.checkBoxLegendOut.TabIndex = 1;
            this.checkBoxLegendOut.Text = "Снаружи";
            this.checkBoxLegendOut.UseVisualStyleBackColor = true;
            this.checkBoxLegendOut.CheckedChanged += new System.EventHandler(this.CheckBoxLegend_CheckedChanged);
            // 
            // checkBoxLegend
            // 
            this.checkBoxLegend.AutoSize = true;
            this.checkBoxLegend.Checked = true;
            this.checkBoxLegend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLegend.Location = new System.Drawing.Point(15, 15);
            this.checkBoxLegend.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxLegend.Name = "checkBoxLegend";
            this.checkBoxLegend.Size = new System.Drawing.Size(75, 20);
            this.checkBoxLegend.TabIndex = 0;
            this.checkBoxLegend.Text = "Внутри";
            this.checkBoxLegend.UseVisualStyleBackColor = true;
            this.checkBoxLegend.CheckedChanged += new System.EventHandler(this.CheckBoxLegend_CheckedChanged);
            // 
            // tabPageGrid
            // 
            this.tabPageGrid.BackColor = System.Drawing.Color.Transparent;
            this.tabPageGrid.Controls.Add(this.checkBoxGridDegrees);
            this.tabPageGrid.Controls.Add(this.checkBoxGridLines);
            this.tabPageGrid.Controls.Add(this.groupBoxGridDegrees);
            this.tabPageGrid.Controls.Add(this.groupBoxGridLines);
            this.tabPageGrid.Location = new System.Drawing.Point(4, 25);
            this.tabPageGrid.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageGrid.Name = "tabPageGrid";
            this.tabPageGrid.Size = new System.Drawing.Size(542, 831);
            this.tabPageGrid.TabIndex = 2;
            this.tabPageGrid.Text = "Сетка";
            // 
            // checkBoxGridDegrees
            // 
            this.checkBoxGridDegrees.AutoSize = true;
            this.checkBoxGridDegrees.Checked = true;
            this.checkBoxGridDegrees.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGridDegrees.Location = new System.Drawing.Point(25, 110);
            this.checkBoxGridDegrees.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxGridDegrees.Name = "checkBoxGridDegrees";
            this.checkBoxGridDegrees.Size = new System.Drawing.Size(72, 20);
            this.checkBoxGridDegrees.TabIndex = 1;
            this.checkBoxGridDegrees.Text = "Цифры";
            this.checkBoxGridDegrees.UseVisualStyleBackColor = true;
            this.checkBoxGridDegrees.CheckedChanged += new System.EventHandler(this.CheckBoxGrid_CheckedChanged);
            // 
            // checkBoxGridLines
            // 
            this.checkBoxGridLines.AutoSize = true;
            this.checkBoxGridLines.Checked = true;
            this.checkBoxGridLines.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGridLines.Location = new System.Drawing.Point(25, 10);
            this.checkBoxGridLines.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxGridLines.Name = "checkBoxGridLines";
            this.checkBoxGridLines.Size = new System.Drawing.Size(68, 20);
            this.checkBoxGridLines.TabIndex = 0;
            this.checkBoxGridLines.Text = "Линии";
            this.checkBoxGridLines.UseVisualStyleBackColor = true;
            this.checkBoxGridLines.CheckedChanged += new System.EventHandler(this.CheckBoxGrid_CheckedChanged);
            // 
            // groupBoxGridDegrees
            // 
            this.groupBoxGridDegrees.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxGridDegrees.Controls.Add(this.numericUpDownGridDegreesWidth);
            this.groupBoxGridDegrees.Controls.Add(this.colorPickerGridDegrees);
            this.groupBoxGridDegrees.Location = new System.Drawing.Point(10, 110);
            this.groupBoxGridDegrees.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxGridDegrees.Name = "groupBoxGridDegrees";
            this.groupBoxGridDegrees.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxGridDegrees.Size = new System.Drawing.Size(520, 60);
            this.groupBoxGridDegrees.TabIndex = 99;
            this.groupBoxGridDegrees.TabStop = false;
            // 
            // numericUpDownGridDegreesWidth
            // 
            this.numericUpDownGridDegreesWidth.Location = new System.Drawing.Point(15, 26);
            this.numericUpDownGridDegreesWidth.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownGridDegreesWidth.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numericUpDownGridDegreesWidth.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownGridDegreesWidth.Name = "numericUpDownGridDegreesWidth";
            this.numericUpDownGridDegreesWidth.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownGridDegreesWidth.TabIndex = 10;
            this.numericUpDownGridDegreesWidth.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownGridDegreesWidth.ValueChanged += new System.EventHandler(this.NumericUpDownGridDeg_ValueChanged);
            // 
            // colorPickerGridDegrees
            // 
            this.colorPickerGridDegrees.BackColor = System.Drawing.Color.Black;
            this.colorPickerGridDegrees.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerGridDegrees.Location = new System.Drawing.Point(100, 25);
            this.colorPickerGridDegrees.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerGridDegrees.Name = "colorPickerGridDegrees";
            this.colorPickerGridDegrees.Size = new System.Drawing.Size(75, 22);
            this.colorPickerGridDegrees.TabIndex = 5;
            this.colorPickerGridDegrees.TabStop = false;
            this.colorPickerGridDegrees.Click += new System.EventHandler(this.ColorPickerGridDeg_Click);
            // 
            // groupBoxGridLines
            // 
            this.groupBoxGridLines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxGridLines.Controls.Add(this.numericUpDownGridLinesWidth);
            this.groupBoxGridLines.Controls.Add(this.colorPickerGridLines);
            this.groupBoxGridLines.Controls.Add(this.numericUpDownGridLinesLng);
            this.groupBoxGridLines.Controls.Add(this.numericUpDownGridLinesLat);
            this.groupBoxGridLines.Location = new System.Drawing.Point(10, 10);
            this.groupBoxGridLines.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxGridLines.Name = "groupBoxGridLines";
            this.groupBoxGridLines.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxGridLines.Size = new System.Drawing.Size(520, 90);
            this.groupBoxGridLines.TabIndex = 99;
            this.groupBoxGridLines.TabStop = false;
            // 
            // numericUpDownGridLinesWidth
            // 
            this.numericUpDownGridLinesWidth.Location = new System.Drawing.Point(100, 25);
            this.numericUpDownGridLinesWidth.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownGridLinesWidth.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownGridLinesWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownGridLinesWidth.Name = "numericUpDownGridLinesWidth";
            this.numericUpDownGridLinesWidth.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownGridLinesWidth.TabIndex = 5;
            this.numericUpDownGridLinesWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownGridLinesWidth.ValueChanged += new System.EventHandler(this.NumericUpDownGridLines_ValueChanged);
            // 
            // colorPickerGridLines
            // 
            this.colorPickerGridLines.BackColor = System.Drawing.Color.Black;
            this.colorPickerGridLines.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerGridLines.Location = new System.Drawing.Point(100, 55);
            this.colorPickerGridLines.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerGridLines.Name = "colorPickerGridLines";
            this.colorPickerGridLines.Size = new System.Drawing.Size(75, 22);
            this.colorPickerGridLines.TabIndex = 4;
            this.colorPickerGridLines.TabStop = false;
            this.colorPickerGridLines.Click += new System.EventHandler(this.ColorPickerGridLines_Click);
            // 
            // numericUpDownGridLinesLng
            // 
            this.numericUpDownGridLinesLng.Location = new System.Drawing.Point(15, 25);
            this.numericUpDownGridLinesLng.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownGridLinesLng.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownGridLinesLng.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownGridLinesLng.Name = "numericUpDownGridLinesLng";
            this.numericUpDownGridLinesLng.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownGridLinesLng.TabIndex = 2;
            this.numericUpDownGridLinesLng.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownGridLinesLng.ValueChanged += new System.EventHandler(this.NumericUpDownGridLines_ValueChanged);
            // 
            // numericUpDownGridLinesLat
            // 
            this.numericUpDownGridLinesLat.Location = new System.Drawing.Point(15, 55);
            this.numericUpDownGridLinesLat.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownGridLinesLat.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownGridLinesLat.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownGridLinesLat.Name = "numericUpDownGridLinesLat";
            this.numericUpDownGridLinesLat.Size = new System.Drawing.Size(75, 22);
            this.numericUpDownGridLinesLat.TabIndex = 3;
            this.numericUpDownGridLinesLat.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownGridLinesLat.ValueChanged += new System.EventHandler(this.NumericUpDownGridLines_ValueChanged);
            // 
            // tabPageSurfer
            // 
            this.tabPageSurfer.BackColor = System.Drawing.Color.Transparent;
            this.tabPageSurfer.Controls.Add(this.groupBoxSurferFilter);
            this.tabPageSurfer.Controls.Add(this.tableLayoutPanelSurferParam);
            this.tabPageSurfer.Controls.Add(this.labelLngKm);
            this.tabPageSurfer.Controls.Add(this.labelLatKm);
            this.tabPageSurfer.Controls.Add(this.groupBoxSurferType);
            this.tabPageSurfer.Controls.Add(this.numericUpDownLngCount);
            this.tabPageSurfer.Controls.Add(this.numericUpDownLatCount);
            this.tabPageSurfer.Controls.Add(this.labelLngCount);
            this.tabPageSurfer.Controls.Add(this.labelLatCount);
            this.tabPageSurfer.Location = new System.Drawing.Point(4, 25);
            this.tabPageSurfer.Name = "tabPageSurfer";
            this.tabPageSurfer.Size = new System.Drawing.Size(542, 831);
            this.tabPageSurfer.TabIndex = 3;
            this.tabPageSurfer.Text = "Surfer";
            // 
            // groupBoxSurferFilter
            // 
            this.groupBoxSurferFilter.Controls.Add(this.tableLayoutPanelSurferFilterParam);
            this.groupBoxSurferFilter.Location = new System.Drawing.Point(185, 75);
            this.groupBoxSurferFilter.Name = "groupBoxSurferFilter";
            this.groupBoxSurferFilter.Size = new System.Drawing.Size(339, 61);
            this.groupBoxSurferFilter.TabIndex = 11;
            this.groupBoxSurferFilter.TabStop = false;
            this.groupBoxSurferFilter.Text = "Фильтр";
            // 
            // tableLayoutPanelSurferFilterParam
            // 
            this.tableLayoutPanelSurferFilterParam.AutoSize = true;
            this.tableLayoutPanelSurferFilterParam.ColumnCount = 2;
            this.tableLayoutPanelSurferFilterParam.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelSurferFilterParam.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelSurferFilterParam.Controls.Add(this.dateTimePickerFilterParam1, 1, 0);
            this.tableLayoutPanelSurferFilterParam.Controls.Add(this.radioButtonFilterParam1, 0, 0);
            this.tableLayoutPanelSurferFilterParam.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanelSurferFilterParam.Name = "tableLayoutPanelSurferFilterParam";
            this.tableLayoutPanelSurferFilterParam.RowCount = 1;
            this.tableLayoutPanelSurferFilterParam.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelSurferFilterParam.Size = new System.Drawing.Size(290, 33);
            this.tableLayoutPanelSurferFilterParam.TabIndex = 10;
            // 
            // dateTimePickerFilterParam1
            // 
            this.dateTimePickerFilterParam1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateTimePickerFilterParam1.CustomFormat = "HH:mm:ss dd.MM.yyyy";
            this.dateTimePickerFilterParam1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFilterParam1.Location = new System.Drawing.Point(87, 5);
            this.dateTimePickerFilterParam1.Name = "dateTimePickerFilterParam1";
            this.dateTimePickerFilterParam1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerFilterParam1.TabIndex = 9;
            // 
            // radioButtonFilterParam1
            // 
            this.radioButtonFilterParam1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonFilterParam1.AutoSize = true;
            this.radioButtonFilterParam1.Checked = true;
            this.radioButtonFilterParam1.Location = new System.Drawing.Point(3, 6);
            this.radioButtonFilterParam1.Name = "radioButtonFilterParam1";
            this.radioButtonFilterParam1.Size = new System.Drawing.Size(78, 20);
            this.radioButtonFilterParam1.TabIndex = 10;
            this.radioButtonFilterParam1.TabStop = true;
            this.radioButtonFilterParam1.Text = "По дате";
            this.radioButtonFilterParam1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelSurferParam
            // 
            this.tableLayoutPanelSurferParam.AutoSize = true;
            this.tableLayoutPanelSurferParam.ColumnCount = 2;
            this.tableLayoutPanelSurferParam.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelSurferParam.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelSurferParam.Controls.Add(this.numericUpDownParam2, 1, 1);
            this.tableLayoutPanelSurferParam.Controls.Add(this.numericUpDownParam1, 1, 0);
            this.tableLayoutPanelSurferParam.Controls.Add(this.labelParam1, 0, 0);
            this.tableLayoutPanelSurferParam.Controls.Add(this.labelParam2, 0, 1);
            this.tableLayoutPanelSurferParam.Location = new System.Drawing.Point(15, 240);
            this.tableLayoutPanelSurferParam.Name = "tableLayoutPanelSurferParam";
            this.tableLayoutPanelSurferParam.RowCount = 2;
            this.tableLayoutPanelSurferParam.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelSurferParam.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelSurferParam.Size = new System.Drawing.Size(252, 67);
            this.tableLayoutPanelSurferParam.TabIndex = 8;
            // 
            // numericUpDownParam2
            // 
            this.numericUpDownParam2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownParam2.Location = new System.Drawing.Point(93, 36);
            this.numericUpDownParam2.Name = "numericUpDownParam2";
            this.numericUpDownParam2.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownParam2.TabIndex = 10;
            // 
            // numericUpDownParam1
            // 
            this.numericUpDownParam1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownParam1.Location = new System.Drawing.Point(93, 3);
            this.numericUpDownParam1.Name = "numericUpDownParam1";
            this.numericUpDownParam1.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownParam1.TabIndex = 9;
            // 
            // labelParam1
            // 
            this.labelParam1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelParam1.AutoSize = true;
            this.labelParam1.Location = new System.Drawing.Point(3, 6);
            this.labelParam1.Name = "labelParam1";
            this.labelParam1.Size = new System.Drawing.Size(84, 16);
            this.labelParam1.TabIndex = 7;
            this.labelParam1.Text = "Параметр 1";
            // 
            // labelParam2
            // 
            this.labelParam2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelParam2.AutoSize = true;
            this.labelParam2.Location = new System.Drawing.Point(3, 39);
            this.labelParam2.Name = "labelParam2";
            this.labelParam2.Size = new System.Drawing.Size(84, 16);
            this.labelParam2.TabIndex = 8;
            this.labelParam2.Text = "Параметр 2";
            // 
            // labelLngKm
            // 
            this.labelLngKm.AutoSize = true;
            this.labelLngKm.Location = new System.Drawing.Point(270, 48);
            this.labelLngKm.Name = "labelLngKm";
            this.labelLngKm.Size = new System.Drawing.Size(27, 16);
            this.labelLngKm.TabIndex = 6;
            this.labelLngKm.Text = "км.";
            // 
            // labelLatKm
            // 
            this.labelLatKm.AutoSize = true;
            this.labelLatKm.Location = new System.Drawing.Point(270, 18);
            this.labelLatKm.Name = "labelLatKm";
            this.labelLatKm.Size = new System.Drawing.Size(27, 16);
            this.labelLatKm.TabIndex = 5;
            this.labelLatKm.Text = "км.";
            // 
            // groupBoxSurferType
            // 
            this.groupBoxSurferType.Controls.Add(this.radioButtonScoreField);
            this.groupBoxSurferType.Controls.Add(this.radioButtonActivity);
            this.groupBoxSurferType.Controls.Add(this.radioButtonStationV);
            this.groupBoxSurferType.Controls.Add(this.radioButtonV);
            this.groupBoxSurferType.Controls.Add(this.radioButtonDensity);
            this.groupBoxSurferType.Location = new System.Drawing.Point(15, 75);
            this.groupBoxSurferType.Name = "groupBoxSurferType";
            this.groupBoxSurferType.Size = new System.Drawing.Size(166, 158);
            this.groupBoxSurferType.TabIndex = 4;
            this.groupBoxSurferType.TabStop = false;
            this.groupBoxSurferType.Text = "Тип";
            // 
            // radioButtonScoreField
            // 
            this.radioButtonScoreField.AutoSize = true;
            this.radioButtonScoreField.Location = new System.Drawing.Point(10, 129);
            this.radioButtonScoreField.Name = "radioButtonScoreField";
            this.radioButtonScoreField.Size = new System.Drawing.Size(132, 20);
            this.radioButtonScoreField.TabIndex = 4;
            this.radioButtonScoreField.Tag = "";
            this.radioButtonScoreField.Text = "Поле бальности";
            this.radioButtonScoreField.UseVisualStyleBackColor = true;
            this.radioButtonScoreField.CheckedChanged += new System.EventHandler(this.RadioButtonScoreField_CheckedChanged);
            // 
            // radioButtonActivity
            // 
            this.radioButtonActivity.AutoSize = true;
            this.radioButtonActivity.Location = new System.Drawing.Point(10, 103);
            this.radioButtonActivity.Name = "radioButtonActivity";
            this.radioButtonActivity.Size = new System.Drawing.Size(102, 20);
            this.radioButtonActivity.TabIndex = 3;
            this.radioButtonActivity.Tag = "";
            this.radioButtonActivity.Text = "Активность";
            this.radioButtonActivity.UseVisualStyleBackColor = true;
            this.radioButtonActivity.CheckedChanged += new System.EventHandler(this.RadioButtonActivity_CheckedChanged);
            // 
            // radioButtonStationV
            // 
            this.radioButtonStationV.AutoSize = true;
            this.radioButtonStationV.Location = new System.Drawing.Point(10, 77);
            this.radioButtonStationV.Name = "radioButtonStationV";
            this.radioButtonStationV.Size = new System.Drawing.Size(128, 20);
            this.radioButtonStationV.TabIndex = 2;
            this.radioButtonStationV.Tag = "";
            this.radioButtonStationV.Text = "Vp/Vs (станций)";
            this.radioButtonStationV.UseVisualStyleBackColor = true;
            this.radioButtonStationV.CheckedChanged += new System.EventHandler(this.RadioButtonSurferDefault_CheckedChanged);
            // 
            // radioButtonV
            // 
            this.radioButtonV.AutoSize = true;
            this.radioButtonV.Location = new System.Drawing.Point(10, 51);
            this.radioButtonV.Name = "radioButtonV";
            this.radioButtonV.Size = new System.Drawing.Size(63, 20);
            this.radioButtonV.TabIndex = 1;
            this.radioButtonV.Tag = "";
            this.radioButtonV.Text = "Vp/Vs";
            this.radioButtonV.UseVisualStyleBackColor = true;
            this.radioButtonV.CheckedChanged += new System.EventHandler(this.RadioButtonSurferDefault_CheckedChanged);
            // 
            // radioButtonDensity
            // 
            this.radioButtonDensity.AutoSize = true;
            this.radioButtonDensity.Checked = true;
            this.radioButtonDensity.Location = new System.Drawing.Point(10, 25);
            this.radioButtonDensity.Name = "radioButtonDensity";
            this.radioButtonDensity.Size = new System.Drawing.Size(96, 20);
            this.radioButtonDensity.TabIndex = 0;
            this.radioButtonDensity.TabStop = true;
            this.radioButtonDensity.Tag = "";
            this.radioButtonDensity.Text = "Плотность";
            this.radioButtonDensity.UseVisualStyleBackColor = true;
            this.radioButtonDensity.CheckedChanged += new System.EventHandler(this.RadioButtonSurferDefault_CheckedChanged);
            // 
            // numericUpDownLngCount
            // 
            this.numericUpDownLngCount.Location = new System.Drawing.Point(170, 45);
            this.numericUpDownLngCount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownLngCount.Name = "numericUpDownLngCount";
            this.numericUpDownLngCount.Size = new System.Drawing.Size(70, 22);
            this.numericUpDownLngCount.TabIndex = 3;
            this.numericUpDownLngCount.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownLngCount.ValueChanged += new System.EventHandler(this.NumericUpDownLatLngCount_ValueChanged);
            // 
            // numericUpDownLatCount
            // 
            this.numericUpDownLatCount.Location = new System.Drawing.Point(170, 15);
            this.numericUpDownLatCount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownLatCount.Name = "numericUpDownLatCount";
            this.numericUpDownLatCount.Size = new System.Drawing.Size(70, 22);
            this.numericUpDownLatCount.TabIndex = 2;
            this.numericUpDownLatCount.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownLatCount.ValueChanged += new System.EventHandler(this.NumericUpDownLatLngCount_ValueChanged);
            // 
            // labelLngCount
            // 
            this.labelLngCount.AutoSize = true;
            this.labelLngCount.Location = new System.Drawing.Point(15, 48);
            this.labelLngCount.Name = "labelLngCount";
            this.labelLngCount.Size = new System.Drawing.Size(132, 16);
            this.labelLngCount.TabIndex = 1;
            this.labelLngCount.Text = "Делений в долготе";
            // 
            // labelLatCount
            // 
            this.labelLatCount.AutoSize = true;
            this.labelLatCount.Location = new System.Drawing.Point(15, 18);
            this.labelLatCount.Name = "labelLatCount";
            this.labelLatCount.Size = new System.Drawing.Size(127, 16);
            this.labelLatCount.TabIndex = 0;
            this.labelLatCount.Text = "Делений в широте";
            // 
            // tabPageImage
            // 
            this.tabPageImage.BackColor = System.Drawing.Color.Transparent;
            this.tabPageImage.Controls.Add(this.tabControlImage);
            this.tabPageImage.Location = new System.Drawing.Point(4, 25);
            this.tabPageImage.Name = "tabPageImage";
            this.tabPageImage.Size = new System.Drawing.Size(542, 831);
            this.tabPageImage.TabIndex = 4;
            this.tabPageImage.Text = "При сохранении изображения";
            // 
            // tabControlImage
            // 
            this.tabControlImage.Controls.Add(this.tabPageIncisionImage);
            this.tabControlImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlImage.Location = new System.Drawing.Point(0, 0);
            this.tabControlImage.Name = "tabControlImage";
            this.tabControlImage.SelectedIndex = 0;
            this.tabControlImage.Size = new System.Drawing.Size(542, 831);
            this.tabControlImage.TabIndex = 0;
            // 
            // tabPageIncisionImage
            // 
            this.tabPageIncisionImage.BackColor = System.Drawing.Color.Transparent;
            this.tabPageIncisionImage.Controls.Add(this.groupBoxIncisionPriority);
            this.tabPageIncisionImage.Controls.Add(this.groupBoxIncisionRadius);
            this.tabPageIncisionImage.Controls.Add(this.groupBoxIncisionDepth);
            this.tabPageIncisionImage.Controls.Add(this.groupBoxIncisionDistance);
            this.tabPageIncisionImage.Controls.Add(this.checkBoxIncisionFill);
            this.tabPageIncisionImage.Controls.Add(this.groupBoxIncisionFill);
            this.tabPageIncisionImage.Controls.Add(this.checkBoxIncisionStroke);
            this.tabPageIncisionImage.Controls.Add(this.groupBoxIncisionStroke);
            this.tabPageIncisionImage.Location = new System.Drawing.Point(4, 25);
            this.tabPageIncisionImage.Name = "tabPageIncisionImage";
            this.tabPageIncisionImage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIncisionImage.Size = new System.Drawing.Size(534, 802);
            this.tabPageIncisionImage.TabIndex = 0;
            this.tabPageIncisionImage.Text = "Разрез";
            // 
            // groupBoxIncisionPriority
            // 
            this.groupBoxIncisionPriority.Controls.Add(this.tableLayoutPanelIncisionPriority);
            this.groupBoxIncisionPriority.Location = new System.Drawing.Point(230, 460);
            this.groupBoxIncisionPriority.Name = "groupBoxIncisionPriority";
            this.groupBoxIncisionPriority.Size = new System.Drawing.Size(210, 90);
            this.groupBoxIncisionPriority.TabIndex = 5;
            this.groupBoxIncisionPriority.TabStop = false;
            this.groupBoxIncisionPriority.Text = "Приоритет";
            // 
            // tableLayoutPanelIncisionPriority
            // 
            this.tableLayoutPanelIncisionPriority.AutoSize = true;
            this.tableLayoutPanelIncisionPriority.ColumnCount = 1;
            this.tableLayoutPanelIncisionPriority.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelIncisionPriority.Controls.Add(this.radioButtonIncisionPriorityRadius, 0, 1);
            this.tableLayoutPanelIncisionPriority.Controls.Add(this.radioButtonIncisionPriorityDistance, 0, 0);
            this.tableLayoutPanelIncisionPriority.Location = new System.Drawing.Point(10, 25);
            this.tableLayoutPanelIncisionPriority.Name = "tableLayoutPanelIncisionPriority";
            this.tableLayoutPanelIncisionPriority.RowCount = 2;
            this.tableLayoutPanelIncisionPriority.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelIncisionPriority.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelIncisionPriority.Size = new System.Drawing.Size(165, 52);
            this.tableLayoutPanelIncisionPriority.TabIndex = 0;
            // 
            // radioButtonIncisionPriorityRadius
            // 
            this.radioButtonIncisionPriorityRadius.AutoSize = true;
            this.radioButtonIncisionPriorityRadius.Location = new System.Drawing.Point(3, 29);
            this.radioButtonIncisionPriorityRadius.Name = "radioButtonIncisionPriorityRadius";
            this.radioButtonIncisionPriorityRadius.Size = new System.Drawing.Size(159, 20);
            this.radioButtonIncisionPriorityRadius.TabIndex = 1;
            this.radioButtonIncisionPriorityRadius.Text = "По легенде (радиус)";
            this.radioButtonIncisionPriorityRadius.UseVisualStyleBackColor = true;
            // 
            // radioButtonIncisionPriorityDistance
            // 
            this.radioButtonIncisionPriorityDistance.AutoSize = true;
            this.radioButtonIncisionPriorityDistance.Checked = true;
            this.radioButtonIncisionPriorityDistance.Location = new System.Drawing.Point(3, 3);
            this.radioButtonIncisionPriorityDistance.Name = "radioButtonIncisionPriorityDistance";
            this.radioButtonIncisionPriorityDistance.Size = new System.Drawing.Size(117, 20);
            this.radioButtonIncisionPriorityDistance.TabIndex = 0;
            this.radioButtonIncisionPriorityDistance.TabStop = true;
            this.radioButtonIncisionPriorityDistance.Text = "По дистанции";
            this.radioButtonIncisionPriorityDistance.UseVisualStyleBackColor = true;
            // 
            // groupBoxIncisionRadius
            // 
            this.groupBoxIncisionRadius.Controls.Add(this.tableLayoutPanelIncisionRadius);
            this.groupBoxIncisionRadius.Location = new System.Drawing.Point(10, 460);
            this.groupBoxIncisionRadius.Name = "groupBoxIncisionRadius";
            this.groupBoxIncisionRadius.Size = new System.Drawing.Size(210, 90);
            this.groupBoxIncisionRadius.TabIndex = 4;
            this.groupBoxIncisionRadius.TabStop = false;
            this.groupBoxIncisionRadius.Text = "Радиус";
            // 
            // tableLayoutPanelIncisionRadius
            // 
            this.tableLayoutPanelIncisionRadius.AutoSize = true;
            this.tableLayoutPanelIncisionRadius.ColumnCount = 2;
            this.tableLayoutPanelIncisionRadius.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIncisionRadius.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelIncisionRadius.Controls.Add(this.numericUpDownIncisionRadiusMult, 1, 0);
            this.tableLayoutPanelIncisionRadius.Controls.Add(this.labelIncisionRadiusFontWidth, 0, 1);
            this.tableLayoutPanelIncisionRadius.Controls.Add(this.labelIncisionRadiusMult, 0, 0);
            this.tableLayoutPanelIncisionRadius.Controls.Add(this.numericUpDownIncisionRadiusFontSize, 1, 1);
            this.tableLayoutPanelIncisionRadius.Location = new System.Drawing.Point(10, 25);
            this.tableLayoutPanelIncisionRadius.Name = "tableLayoutPanelIncisionRadius";
            this.tableLayoutPanelIncisionRadius.RowCount = 2;
            this.tableLayoutPanelIncisionRadius.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionRadius.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionRadius.Size = new System.Drawing.Size(190, 56);
            this.tableLayoutPanelIncisionRadius.TabIndex = 4;
            // 
            // numericUpDownIncisionRadiusMult
            // 
            this.numericUpDownIncisionRadiusMult.DecimalPlaces = 1;
            this.numericUpDownIncisionRadiusMult.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownIncisionRadiusMult.Location = new System.Drawing.Point(113, 3);
            this.numericUpDownIncisionRadiusMult.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownIncisionRadiusMult.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownIncisionRadiusMult.Name = "numericUpDownIncisionRadiusMult";
            this.numericUpDownIncisionRadiusMult.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionRadiusMult.TabIndex = 13;
            this.numericUpDownIncisionRadiusMult.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // labelIncisionRadiusFontWidth
            // 
            this.labelIncisionRadiusFontWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionRadiusFontWidth.AutoSize = true;
            this.labelIncisionRadiusFontWidth.Location = new System.Drawing.Point(3, 34);
            this.labelIncisionRadiusFontWidth.Name = "labelIncisionRadiusFontWidth";
            this.labelIncisionRadiusFontWidth.Size = new System.Drawing.Size(104, 16);
            this.labelIncisionRadiusFontWidth.TabIndex = 6;
            this.labelIncisionRadiusFontWidth.Text = "Размер шрифт";
            this.labelIncisionRadiusFontWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionRadiusMult
            // 
            this.labelIncisionRadiusMult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionRadiusMult.AutoSize = true;
            this.labelIncisionRadiusMult.Location = new System.Drawing.Point(3, 6);
            this.labelIncisionRadiusMult.Name = "labelIncisionRadiusMult";
            this.labelIncisionRadiusMult.Size = new System.Drawing.Size(82, 16);
            this.labelIncisionRadiusMult.TabIndex = 0;
            this.labelIncisionRadiusMult.Text = "Множитель";
            this.labelIncisionRadiusMult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownIncisionRadiusFontSize
            // 
            this.numericUpDownIncisionRadiusFontSize.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionRadiusFontSize.DecimalPlaces = 2;
            this.numericUpDownIncisionRadiusFontSize.Location = new System.Drawing.Point(113, 31);
            this.numericUpDownIncisionRadiusFontSize.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDownIncisionRadiusFontSize.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownIncisionRadiusFontSize.Name = "numericUpDownIncisionRadiusFontSize";
            this.numericUpDownIncisionRadiusFontSize.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionRadiusFontSize.TabIndex = 9;
            this.numericUpDownIncisionRadiusFontSize.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // groupBoxIncisionDepth
            // 
            this.groupBoxIncisionDepth.Controls.Add(this.checkBoxIncisionDepthNumber);
            this.groupBoxIncisionDepth.Controls.Add(this.groupBoxIncisionDepthNumber);
            this.groupBoxIncisionDepth.Controls.Add(this.checkBoxIncisionDepthLine);
            this.groupBoxIncisionDepth.Controls.Add(this.groupBoxIncisionDepthLine);
            this.groupBoxIncisionDepth.Location = new System.Drawing.Point(230, 170);
            this.groupBoxIncisionDepth.Name = "groupBoxIncisionDepth";
            this.groupBoxIncisionDepth.Size = new System.Drawing.Size(210, 285);
            this.groupBoxIncisionDepth.TabIndex = 3;
            this.groupBoxIncisionDepth.TabStop = false;
            this.groupBoxIncisionDepth.Text = "Глубина";
            // 
            // checkBoxIncisionDepthNumber
            // 
            this.checkBoxIncisionDepthNumber.AutoSize = true;
            this.checkBoxIncisionDepthNumber.Checked = true;
            this.checkBoxIncisionDepthNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionDepthNumber.Location = new System.Drawing.Point(25, 150);
            this.checkBoxIncisionDepthNumber.Name = "checkBoxIncisionDepthNumber";
            this.checkBoxIncisionDepthNumber.Size = new System.Drawing.Size(72, 20);
            this.checkBoxIncisionDepthNumber.TabIndex = 11;
            this.checkBoxIncisionDepthNumber.Text = "Цифры";
            this.checkBoxIncisionDepthNumber.UseVisualStyleBackColor = true;
            this.checkBoxIncisionDepthNumber.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionDepthNumber_CheckedChanged);
            // 
            // groupBoxIncisionDepthNumber
            // 
            this.groupBoxIncisionDepthNumber.Controls.Add(this.tableLayoutPanelIncisionDepthNumber);
            this.groupBoxIncisionDepthNumber.Location = new System.Drawing.Point(10, 150);
            this.groupBoxIncisionDepthNumber.Name = "groupBoxIncisionDepthNumber";
            this.groupBoxIncisionDepthNumber.Size = new System.Drawing.Size(190, 125);
            this.groupBoxIncisionDepthNumber.TabIndex = 12;
            this.groupBoxIncisionDepthNumber.TabStop = false;
            // 
            // tableLayoutPanelIncisionDepthNumber
            // 
            this.tableLayoutPanelIncisionDepthNumber.AutoSize = true;
            this.tableLayoutPanelIncisionDepthNumber.ColumnCount = 2;
            this.tableLayoutPanelIncisionDepthNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIncisionDepthNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelIncisionDepthNumber.Controls.Add(this.numericUpDownIncisionDepthNumberMult, 1, 2);
            this.tableLayoutPanelIncisionDepthNumber.Controls.Add(this.labelIncisionDepthNumberMult, 0, 2);
            this.tableLayoutPanelIncisionDepthNumber.Controls.Add(this.labelIncisionDepthNumberWidth, 0, 1);
            this.tableLayoutPanelIncisionDepthNumber.Controls.Add(this.labelIncisionDepthNumberColor, 0, 0);
            this.tableLayoutPanelIncisionDepthNumber.Controls.Add(this.colorPickerIncisionDepthNumberColor, 1, 0);
            this.tableLayoutPanelIncisionDepthNumber.Controls.Add(this.numericUpDownIncisionDepthNumberFontSize, 1, 1);
            this.tableLayoutPanelIncisionDepthNumber.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanelIncisionDepthNumber.Name = "tableLayoutPanelIncisionDepthNumber";
            this.tableLayoutPanelIncisionDepthNumber.RowCount = 3;
            this.tableLayoutPanelIncisionDepthNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionDepthNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionDepthNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionDepthNumber.Size = new System.Drawing.Size(182, 90);
            this.tableLayoutPanelIncisionDepthNumber.TabIndex = 3;
            // 
            // numericUpDownIncisionDepthNumberMult
            // 
            this.numericUpDownIncisionDepthNumberMult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDepthNumberMult.Location = new System.Drawing.Point(105, 64);
            this.numericUpDownIncisionDepthNumberMult.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthNumberMult.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthNumberMult.Name = "numericUpDownIncisionDepthNumberMult";
            this.numericUpDownIncisionDepthNumberMult.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDepthNumberMult.TabIndex = 10;
            this.numericUpDownIncisionDepthNumberMult.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelIncisionDepthNumberMult
            // 
            this.labelIncisionDepthNumberMult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDepthNumberMult.AutoSize = true;
            this.labelIncisionDepthNumberMult.Location = new System.Drawing.Point(3, 67);
            this.labelIncisionDepthNumberMult.Name = "labelIncisionDepthNumberMult";
            this.labelIncisionDepthNumberMult.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDepthNumberMult.TabIndex = 8;
            this.labelIncisionDepthNumberMult.Text = "Каждые";
            this.labelIncisionDepthNumberMult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDepthNumberWidth
            // 
            this.labelIncisionDepthNumberWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDepthNumberWidth.AutoSize = true;
            this.labelIncisionDepthNumberWidth.Location = new System.Drawing.Point(3, 37);
            this.labelIncisionDepthNumberWidth.Name = "labelIncisionDepthNumberWidth";
            this.labelIncisionDepthNumberWidth.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDepthNumberWidth.TabIndex = 6;
            this.labelIncisionDepthNumberWidth.Text = "Размер";
            this.labelIncisionDepthNumberWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDepthNumberColor
            // 
            this.labelIncisionDepthNumberColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDepthNumberColor.AutoSize = true;
            this.labelIncisionDepthNumberColor.Location = new System.Drawing.Point(3, 7);
            this.labelIncisionDepthNumberColor.Name = "labelIncisionDepthNumberColor";
            this.labelIncisionDepthNumberColor.Size = new System.Drawing.Size(40, 16);
            this.labelIncisionDepthNumberColor.TabIndex = 0;
            this.labelIncisionDepthNumberColor.Text = "Цвет";
            this.labelIncisionDepthNumberColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // colorPickerIncisionDepthNumberColor
            // 
            this.colorPickerIncisionDepthNumberColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.colorPickerIncisionDepthNumberColor.BackColor = System.Drawing.Color.Black;
            this.colorPickerIncisionDepthNumberColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerIncisionDepthNumberColor.Location = new System.Drawing.Point(106, 5);
            this.colorPickerIncisionDepthNumberColor.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerIncisionDepthNumberColor.Name = "colorPickerIncisionDepthNumberColor";
            this.colorPickerIncisionDepthNumberColor.Size = new System.Drawing.Size(72, 20);
            this.colorPickerIncisionDepthNumberColor.TabIndex = 5;
            this.colorPickerIncisionDepthNumberColor.TabStop = false;
            this.colorPickerIncisionDepthNumberColor.Click += new System.EventHandler(this.ColorPicker_Click);
            // 
            // numericUpDownIncisionDepthNumberFontSize
            // 
            this.numericUpDownIncisionDepthNumberFontSize.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDepthNumberFontSize.DecimalPlaces = 2;
            this.numericUpDownIncisionDepthNumberFontSize.Location = new System.Drawing.Point(105, 34);
            this.numericUpDownIncisionDepthNumberFontSize.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthNumberFontSize.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthNumberFontSize.Name = "numericUpDownIncisionDepthNumberFontSize";
            this.numericUpDownIncisionDepthNumberFontSize.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDepthNumberFontSize.TabIndex = 9;
            this.numericUpDownIncisionDepthNumberFontSize.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // checkBoxIncisionDepthLine
            // 
            this.checkBoxIncisionDepthLine.AutoSize = true;
            this.checkBoxIncisionDepthLine.Checked = true;
            this.checkBoxIncisionDepthLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionDepthLine.Location = new System.Drawing.Point(25, 20);
            this.checkBoxIncisionDepthLine.Name = "checkBoxIncisionDepthLine";
            this.checkBoxIncisionDepthLine.Size = new System.Drawing.Size(68, 20);
            this.checkBoxIncisionDepthLine.TabIndex = 10;
            this.checkBoxIncisionDepthLine.Text = "Линии";
            this.checkBoxIncisionDepthLine.UseVisualStyleBackColor = true;
            this.checkBoxIncisionDepthLine.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionDepthLine_CheckedChanged);
            // 
            // groupBoxIncisionDepthLine
            // 
            this.groupBoxIncisionDepthLine.Controls.Add(this.tableLayoutPanelIncisionDepthLine);
            this.groupBoxIncisionDepthLine.Location = new System.Drawing.Point(10, 20);
            this.groupBoxIncisionDepthLine.Name = "groupBoxIncisionDepthLine";
            this.groupBoxIncisionDepthLine.Size = new System.Drawing.Size(190, 125);
            this.groupBoxIncisionDepthLine.TabIndex = 10;
            this.groupBoxIncisionDepthLine.TabStop = false;
            // 
            // tableLayoutPanelIncisionDepthLine
            // 
            this.tableLayoutPanelIncisionDepthLine.AutoSize = true;
            this.tableLayoutPanelIncisionDepthLine.ColumnCount = 2;
            this.tableLayoutPanelIncisionDepthLine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIncisionDepthLine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelIncisionDepthLine.Controls.Add(this.numericUpDownIncisionDepthLineMult, 1, 2);
            this.tableLayoutPanelIncisionDepthLine.Controls.Add(this.labelIncisionDepthLineMult, 0, 2);
            this.tableLayoutPanelIncisionDepthLine.Controls.Add(this.labelIncisionDepthLineWidth, 0, 1);
            this.tableLayoutPanelIncisionDepthLine.Controls.Add(this.labelIncisionDepthLineColor, 0, 0);
            this.tableLayoutPanelIncisionDepthLine.Controls.Add(this.colorPickerIncisionDepthLineColor, 1, 0);
            this.tableLayoutPanelIncisionDepthLine.Controls.Add(this.numericUpDownIncisionDepthLineWidth, 1, 1);
            this.tableLayoutPanelIncisionDepthLine.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanelIncisionDepthLine.Name = "tableLayoutPanelIncisionDepthLine";
            this.tableLayoutPanelIncisionDepthLine.RowCount = 3;
            this.tableLayoutPanelIncisionDepthLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelIncisionDepthLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelIncisionDepthLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelIncisionDepthLine.Size = new System.Drawing.Size(182, 90);
            this.tableLayoutPanelIncisionDepthLine.TabIndex = 3;
            // 
            // numericUpDownIncisionDepthLineMult
            // 
            this.numericUpDownIncisionDepthLineMult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDepthLineMult.Location = new System.Drawing.Point(105, 64);
            this.numericUpDownIncisionDepthLineMult.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthLineMult.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthLineMult.Name = "numericUpDownIncisionDepthLineMult";
            this.numericUpDownIncisionDepthLineMult.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDepthLineMult.TabIndex = 10;
            this.numericUpDownIncisionDepthLineMult.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelIncisionDepthLineMult
            // 
            this.labelIncisionDepthLineMult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDepthLineMult.AutoSize = true;
            this.labelIncisionDepthLineMult.Location = new System.Drawing.Point(3, 67);
            this.labelIncisionDepthLineMult.Name = "labelIncisionDepthLineMult";
            this.labelIncisionDepthLineMult.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDepthLineMult.TabIndex = 8;
            this.labelIncisionDepthLineMult.Text = "Каждые";
            this.labelIncisionDepthLineMult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDepthLineWidth
            // 
            this.labelIncisionDepthLineWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDepthLineWidth.AutoSize = true;
            this.labelIncisionDepthLineWidth.Location = new System.Drawing.Point(3, 37);
            this.labelIncisionDepthLineWidth.Name = "labelIncisionDepthLineWidth";
            this.labelIncisionDepthLineWidth.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDepthLineWidth.TabIndex = 6;
            this.labelIncisionDepthLineWidth.Text = "Размер";
            this.labelIncisionDepthLineWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDepthLineColor
            // 
            this.labelIncisionDepthLineColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDepthLineColor.AutoSize = true;
            this.labelIncisionDepthLineColor.Location = new System.Drawing.Point(3, 7);
            this.labelIncisionDepthLineColor.Name = "labelIncisionDepthLineColor";
            this.labelIncisionDepthLineColor.Size = new System.Drawing.Size(40, 16);
            this.labelIncisionDepthLineColor.TabIndex = 0;
            this.labelIncisionDepthLineColor.Text = "Цвет";
            this.labelIncisionDepthLineColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // colorPickerIncisionDepthLineColor
            // 
            this.colorPickerIncisionDepthLineColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.colorPickerIncisionDepthLineColor.BackColor = System.Drawing.Color.Black;
            this.colorPickerIncisionDepthLineColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerIncisionDepthLineColor.Location = new System.Drawing.Point(106, 5);
            this.colorPickerIncisionDepthLineColor.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerIncisionDepthLineColor.Name = "colorPickerIncisionDepthLineColor";
            this.colorPickerIncisionDepthLineColor.Size = new System.Drawing.Size(72, 20);
            this.colorPickerIncisionDepthLineColor.TabIndex = 5;
            this.colorPickerIncisionDepthLineColor.TabStop = false;
            this.colorPickerIncisionDepthLineColor.Click += new System.EventHandler(this.ColorPicker_Click);
            // 
            // numericUpDownIncisionDepthLineWidth
            // 
            this.numericUpDownIncisionDepthLineWidth.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDepthLineWidth.DecimalPlaces = 2;
            this.numericUpDownIncisionDepthLineWidth.Location = new System.Drawing.Point(105, 34);
            this.numericUpDownIncisionDepthLineWidth.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthLineWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIncisionDepthLineWidth.Name = "numericUpDownIncisionDepthLineWidth";
            this.numericUpDownIncisionDepthLineWidth.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDepthLineWidth.TabIndex = 9;
            this.numericUpDownIncisionDepthLineWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBoxIncisionDistance
            // 
            this.groupBoxIncisionDistance.Controls.Add(this.checkBoxIncisionDistanceNumber);
            this.groupBoxIncisionDistance.Controls.Add(this.groupBoxIncisionDistanceNumber);
            this.groupBoxIncisionDistance.Controls.Add(this.checkBoxIncisionDistanceLine);
            this.groupBoxIncisionDistance.Controls.Add(this.groupBoxIncisionDistanceLine);
            this.groupBoxIncisionDistance.Location = new System.Drawing.Point(10, 170);
            this.groupBoxIncisionDistance.Name = "groupBoxIncisionDistance";
            this.groupBoxIncisionDistance.Size = new System.Drawing.Size(210, 285);
            this.groupBoxIncisionDistance.TabIndex = 2;
            this.groupBoxIncisionDistance.TabStop = false;
            this.groupBoxIncisionDistance.Text = "Расстояние";
            // 
            // checkBoxIncisionDistanceNumber
            // 
            this.checkBoxIncisionDistanceNumber.AutoSize = true;
            this.checkBoxIncisionDistanceNumber.Checked = true;
            this.checkBoxIncisionDistanceNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionDistanceNumber.Location = new System.Drawing.Point(25, 150);
            this.checkBoxIncisionDistanceNumber.Name = "checkBoxIncisionDistanceNumber";
            this.checkBoxIncisionDistanceNumber.Size = new System.Drawing.Size(72, 20);
            this.checkBoxIncisionDistanceNumber.TabIndex = 11;
            this.checkBoxIncisionDistanceNumber.Text = "Цифры";
            this.checkBoxIncisionDistanceNumber.UseVisualStyleBackColor = true;
            this.checkBoxIncisionDistanceNumber.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionDistanceNumber_CheckedChanged);
            // 
            // groupBoxIncisionDistanceNumber
            // 
            this.groupBoxIncisionDistanceNumber.Controls.Add(this.tableLayoutPanelIncisionDistanceNumber);
            this.groupBoxIncisionDistanceNumber.Location = new System.Drawing.Point(10, 150);
            this.groupBoxIncisionDistanceNumber.Name = "groupBoxIncisionDistanceNumber";
            this.groupBoxIncisionDistanceNumber.Size = new System.Drawing.Size(190, 125);
            this.groupBoxIncisionDistanceNumber.TabIndex = 12;
            this.groupBoxIncisionDistanceNumber.TabStop = false;
            // 
            // tableLayoutPanelIncisionDistanceNumber
            // 
            this.tableLayoutPanelIncisionDistanceNumber.AutoSize = true;
            this.tableLayoutPanelIncisionDistanceNumber.ColumnCount = 2;
            this.tableLayoutPanelIncisionDistanceNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIncisionDistanceNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelIncisionDistanceNumber.Controls.Add(this.numericUpDownIncisionDistanceNumberMult, 1, 2);
            this.tableLayoutPanelIncisionDistanceNumber.Controls.Add(this.labelIncisionDistanceNumberMult, 0, 2);
            this.tableLayoutPanelIncisionDistanceNumber.Controls.Add(this.labelIncisionDistanceNumberWidth, 0, 1);
            this.tableLayoutPanelIncisionDistanceNumber.Controls.Add(this.labelIncisionDistanceNumberColor, 0, 0);
            this.tableLayoutPanelIncisionDistanceNumber.Controls.Add(this.colorPickerIncisionDistanceNumberColor, 1, 0);
            this.tableLayoutPanelIncisionDistanceNumber.Controls.Add(this.numericUpDownIncisionDistanceNumberFontSize, 1, 1);
            this.tableLayoutPanelIncisionDistanceNumber.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanelIncisionDistanceNumber.Name = "tableLayoutPanelIncisionDistanceNumber";
            this.tableLayoutPanelIncisionDistanceNumber.RowCount = 3;
            this.tableLayoutPanelIncisionDistanceNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionDistanceNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionDistanceNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIncisionDistanceNumber.Size = new System.Drawing.Size(182, 90);
            this.tableLayoutPanelIncisionDistanceNumber.TabIndex = 3;
            // 
            // numericUpDownIncisionDistanceNumberMult
            // 
            this.numericUpDownIncisionDistanceNumberMult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDistanceNumberMult.Location = new System.Drawing.Point(105, 64);
            this.numericUpDownIncisionDistanceNumberMult.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceNumberMult.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceNumberMult.Name = "numericUpDownIncisionDistanceNumberMult";
            this.numericUpDownIncisionDistanceNumberMult.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDistanceNumberMult.TabIndex = 10;
            this.numericUpDownIncisionDistanceNumberMult.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // labelIncisionDistanceNumberMult
            // 
            this.labelIncisionDistanceNumberMult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDistanceNumberMult.AutoSize = true;
            this.labelIncisionDistanceNumberMult.Location = new System.Drawing.Point(3, 67);
            this.labelIncisionDistanceNumberMult.Name = "labelIncisionDistanceNumberMult";
            this.labelIncisionDistanceNumberMult.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDistanceNumberMult.TabIndex = 8;
            this.labelIncisionDistanceNumberMult.Text = "Каждые";
            this.labelIncisionDistanceNumberMult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDistanceNumberWidth
            // 
            this.labelIncisionDistanceNumberWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDistanceNumberWidth.AutoSize = true;
            this.labelIncisionDistanceNumberWidth.Location = new System.Drawing.Point(3, 37);
            this.labelIncisionDistanceNumberWidth.Name = "labelIncisionDistanceNumberWidth";
            this.labelIncisionDistanceNumberWidth.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDistanceNumberWidth.TabIndex = 6;
            this.labelIncisionDistanceNumberWidth.Text = "Размер";
            this.labelIncisionDistanceNumberWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDistanceNumberColor
            // 
            this.labelIncisionDistanceNumberColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDistanceNumberColor.AutoSize = true;
            this.labelIncisionDistanceNumberColor.Location = new System.Drawing.Point(3, 7);
            this.labelIncisionDistanceNumberColor.Name = "labelIncisionDistanceNumberColor";
            this.labelIncisionDistanceNumberColor.Size = new System.Drawing.Size(40, 16);
            this.labelIncisionDistanceNumberColor.TabIndex = 0;
            this.labelIncisionDistanceNumberColor.Text = "Цвет";
            this.labelIncisionDistanceNumberColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // colorPickerIncisionDistanceNumberColor
            // 
            this.colorPickerIncisionDistanceNumberColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.colorPickerIncisionDistanceNumberColor.BackColor = System.Drawing.Color.Black;
            this.colorPickerIncisionDistanceNumberColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerIncisionDistanceNumberColor.Location = new System.Drawing.Point(106, 5);
            this.colorPickerIncisionDistanceNumberColor.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerIncisionDistanceNumberColor.Name = "colorPickerIncisionDistanceNumberColor";
            this.colorPickerIncisionDistanceNumberColor.Size = new System.Drawing.Size(72, 20);
            this.colorPickerIncisionDistanceNumberColor.TabIndex = 5;
            this.colorPickerIncisionDistanceNumberColor.TabStop = false;
            this.colorPickerIncisionDistanceNumberColor.Click += new System.EventHandler(this.ColorPicker_Click);
            // 
            // numericUpDownIncisionDistanceNumberFontSize
            // 
            this.numericUpDownIncisionDistanceNumberFontSize.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDistanceNumberFontSize.DecimalPlaces = 2;
            this.numericUpDownIncisionDistanceNumberFontSize.Location = new System.Drawing.Point(105, 34);
            this.numericUpDownIncisionDistanceNumberFontSize.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceNumberFontSize.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceNumberFontSize.Name = "numericUpDownIncisionDistanceNumberFontSize";
            this.numericUpDownIncisionDistanceNumberFontSize.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDistanceNumberFontSize.TabIndex = 9;
            this.numericUpDownIncisionDistanceNumberFontSize.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // checkBoxIncisionDistanceLine
            // 
            this.checkBoxIncisionDistanceLine.AutoSize = true;
            this.checkBoxIncisionDistanceLine.Checked = true;
            this.checkBoxIncisionDistanceLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionDistanceLine.Location = new System.Drawing.Point(25, 20);
            this.checkBoxIncisionDistanceLine.Name = "checkBoxIncisionDistanceLine";
            this.checkBoxIncisionDistanceLine.Size = new System.Drawing.Size(68, 20);
            this.checkBoxIncisionDistanceLine.TabIndex = 10;
            this.checkBoxIncisionDistanceLine.Text = "Линии";
            this.checkBoxIncisionDistanceLine.UseVisualStyleBackColor = true;
            this.checkBoxIncisionDistanceLine.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionDistanceLine_CheckedChanged);
            // 
            // groupBoxIncisionDistanceLine
            // 
            this.groupBoxIncisionDistanceLine.Controls.Add(this.tableLayoutPanelDistanceLine);
            this.groupBoxIncisionDistanceLine.Location = new System.Drawing.Point(10, 20);
            this.groupBoxIncisionDistanceLine.Name = "groupBoxIncisionDistanceLine";
            this.groupBoxIncisionDistanceLine.Size = new System.Drawing.Size(190, 125);
            this.groupBoxIncisionDistanceLine.TabIndex = 10;
            this.groupBoxIncisionDistanceLine.TabStop = false;
            // 
            // tableLayoutPanelDistanceLine
            // 
            this.tableLayoutPanelDistanceLine.AutoSize = true;
            this.tableLayoutPanelDistanceLine.ColumnCount = 2;
            this.tableLayoutPanelDistanceLine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDistanceLine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelDistanceLine.Controls.Add(this.numericUpDownIncisionDistanceLineMult, 1, 2);
            this.tableLayoutPanelDistanceLine.Controls.Add(this.labelIncisionDistanceLineMult, 0, 2);
            this.tableLayoutPanelDistanceLine.Controls.Add(this.labelIncisionDistanceLineWidth, 0, 1);
            this.tableLayoutPanelDistanceLine.Controls.Add(this.labelIncisionDistanceLineColor, 0, 0);
            this.tableLayoutPanelDistanceLine.Controls.Add(this.colorPickerIncisionDistanceLineColor, 1, 0);
            this.tableLayoutPanelDistanceLine.Controls.Add(this.numericUpDownIncisionDistanceLineWidth, 1, 1);
            this.tableLayoutPanelDistanceLine.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanelDistanceLine.Name = "tableLayoutPanelDistanceLine";
            this.tableLayoutPanelDistanceLine.RowCount = 3;
            this.tableLayoutPanelDistanceLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelDistanceLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelDistanceLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelDistanceLine.Size = new System.Drawing.Size(182, 90);
            this.tableLayoutPanelDistanceLine.TabIndex = 3;
            // 
            // numericUpDownIncisionDistanceLineMult
            // 
            this.numericUpDownIncisionDistanceLineMult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDistanceLineMult.Location = new System.Drawing.Point(105, 64);
            this.numericUpDownIncisionDistanceLineMult.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceLineMult.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceLineMult.Name = "numericUpDownIncisionDistanceLineMult";
            this.numericUpDownIncisionDistanceLineMult.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDistanceLineMult.TabIndex = 10;
            this.numericUpDownIncisionDistanceLineMult.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelIncisionDistanceLineMult
            // 
            this.labelIncisionDistanceLineMult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDistanceLineMult.AutoSize = true;
            this.labelIncisionDistanceLineMult.Location = new System.Drawing.Point(3, 67);
            this.labelIncisionDistanceLineMult.Name = "labelIncisionDistanceLineMult";
            this.labelIncisionDistanceLineMult.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDistanceLineMult.TabIndex = 8;
            this.labelIncisionDistanceLineMult.Text = "Каждые";
            this.labelIncisionDistanceLineMult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDistanceLineWidth
            // 
            this.labelIncisionDistanceLineWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDistanceLineWidth.AutoSize = true;
            this.labelIncisionDistanceLineWidth.Location = new System.Drawing.Point(3, 37);
            this.labelIncisionDistanceLineWidth.Name = "labelIncisionDistanceLineWidth";
            this.labelIncisionDistanceLineWidth.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionDistanceLineWidth.TabIndex = 6;
            this.labelIncisionDistanceLineWidth.Text = "Размер";
            this.labelIncisionDistanceLineWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionDistanceLineColor
            // 
            this.labelIncisionDistanceLineColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionDistanceLineColor.AutoSize = true;
            this.labelIncisionDistanceLineColor.Location = new System.Drawing.Point(3, 7);
            this.labelIncisionDistanceLineColor.Name = "labelIncisionDistanceLineColor";
            this.labelIncisionDistanceLineColor.Size = new System.Drawing.Size(40, 16);
            this.labelIncisionDistanceLineColor.TabIndex = 0;
            this.labelIncisionDistanceLineColor.Text = "Цвет";
            this.labelIncisionDistanceLineColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // colorPickerIncisionDistanceLineColor
            // 
            this.colorPickerIncisionDistanceLineColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.colorPickerIncisionDistanceLineColor.BackColor = System.Drawing.Color.Black;
            this.colorPickerIncisionDistanceLineColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerIncisionDistanceLineColor.Location = new System.Drawing.Point(106, 5);
            this.colorPickerIncisionDistanceLineColor.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerIncisionDistanceLineColor.Name = "colorPickerIncisionDistanceLineColor";
            this.colorPickerIncisionDistanceLineColor.Size = new System.Drawing.Size(72, 20);
            this.colorPickerIncisionDistanceLineColor.TabIndex = 5;
            this.colorPickerIncisionDistanceLineColor.TabStop = false;
            this.colorPickerIncisionDistanceLineColor.Click += new System.EventHandler(this.ColorPicker_Click);
            // 
            // numericUpDownIncisionDistanceLineWidth
            // 
            this.numericUpDownIncisionDistanceLineWidth.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionDistanceLineWidth.DecimalPlaces = 2;
            this.numericUpDownIncisionDistanceLineWidth.Location = new System.Drawing.Point(105, 34);
            this.numericUpDownIncisionDistanceLineWidth.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceLineWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIncisionDistanceLineWidth.Name = "numericUpDownIncisionDistanceLineWidth";
            this.numericUpDownIncisionDistanceLineWidth.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionDistanceLineWidth.TabIndex = 9;
            this.numericUpDownIncisionDistanceLineWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkBoxIncisionFill
            // 
            this.checkBoxIncisionFill.AutoSize = true;
            this.checkBoxIncisionFill.Checked = true;
            this.checkBoxIncisionFill.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionFill.Location = new System.Drawing.Point(245, 10);
            this.checkBoxIncisionFill.Name = "checkBoxIncisionFill";
            this.checkBoxIncisionFill.Size = new System.Drawing.Size(108, 20);
            this.checkBoxIncisionFill.TabIndex = 3;
            this.checkBoxIncisionFill.Text = "Заполнение";
            this.checkBoxIncisionFill.UseVisualStyleBackColor = true;
            this.checkBoxIncisionFill.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionFill_CheckedChanged);
            // 
            // groupBoxIncisionFill
            // 
            this.groupBoxIncisionFill.Controls.Add(this.tableLayoutPanelIncisionFill);
            this.groupBoxIncisionFill.Controls.Add(this.checkBoxIncisionFillUserColor);
            this.groupBoxIncisionFill.Location = new System.Drawing.Point(230, 10);
            this.groupBoxIncisionFill.Name = "groupBoxIncisionFill";
            this.groupBoxIncisionFill.Size = new System.Drawing.Size(210, 155);
            this.groupBoxIncisionFill.TabIndex = 2;
            this.groupBoxIncisionFill.TabStop = false;
            // 
            // tableLayoutPanelIncisionFill
            // 
            this.tableLayoutPanelIncisionFill.AutoSize = true;
            this.tableLayoutPanelIncisionFill.ColumnCount = 2;
            this.tableLayoutPanelIncisionFill.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIncisionFill.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelIncisionFill.Controls.Add(this.labelIncisionFillAlpha, 0, 1);
            this.tableLayoutPanelIncisionFill.Controls.Add(this.numericUpDownIncisionFillAlpha, 1, 1);
            this.tableLayoutPanelIncisionFill.Controls.Add(this.labelIncisionFillColor, 0, 0);
            this.tableLayoutPanelIncisionFill.Controls.Add(this.colorPickerIncisionFillColor, 1, 0);
            this.tableLayoutPanelIncisionFill.Location = new System.Drawing.Point(10, 55);
            this.tableLayoutPanelIncisionFill.Name = "tableLayoutPanelIncisionFill";
            this.tableLayoutPanelIncisionFill.RowCount = 2;
            this.tableLayoutPanelIncisionFill.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelIncisionFill.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelIncisionFill.Size = new System.Drawing.Size(192, 58);
            this.tableLayoutPanelIncisionFill.TabIndex = 1;
            // 
            // labelIncisionFillAlpha
            // 
            this.labelIncisionFillAlpha.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionFillAlpha.AutoSize = true;
            this.labelIncisionFillAlpha.Location = new System.Drawing.Point(3, 35);
            this.labelIncisionFillAlpha.Name = "labelIncisionFillAlpha";
            this.labelIncisionFillAlpha.Size = new System.Drawing.Size(103, 16);
            this.labelIncisionFillAlpha.TabIndex = 1;
            this.labelIncisionFillAlpha.Text = "Прозрачность";
            this.labelIncisionFillAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownIncisionFillAlpha
            // 
            this.numericUpDownIncisionFillAlpha.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionFillAlpha.DecimalPlaces = 2;
            this.numericUpDownIncisionFillAlpha.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownIncisionFillAlpha.Location = new System.Drawing.Point(115, 32);
            this.numericUpDownIncisionFillAlpha.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIncisionFillAlpha.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownIncisionFillAlpha.Name = "numericUpDownIncisionFillAlpha";
            this.numericUpDownIncisionFillAlpha.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionFillAlpha.TabIndex = 0;
            this.numericUpDownIncisionFillAlpha.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelIncisionFillColor
            // 
            this.labelIncisionFillColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionFillColor.AutoSize = true;
            this.labelIncisionFillColor.Enabled = false;
            this.labelIncisionFillColor.Location = new System.Drawing.Point(3, 6);
            this.labelIncisionFillColor.Name = "labelIncisionFillColor";
            this.labelIncisionFillColor.Size = new System.Drawing.Size(40, 16);
            this.labelIncisionFillColor.TabIndex = 2;
            this.labelIncisionFillColor.Text = "Цвет";
            this.labelIncisionFillColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // colorPickerIncisionFillColor
            // 
            this.colorPickerIncisionFillColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.colorPickerIncisionFillColor.BackColor = System.Drawing.Color.Black;
            this.colorPickerIncisionFillColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerIncisionFillColor.Enabled = false;
            this.colorPickerIncisionFillColor.Location = new System.Drawing.Point(116, 4);
            this.colorPickerIncisionFillColor.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerIncisionFillColor.Name = "colorPickerIncisionFillColor";
            this.colorPickerIncisionFillColor.Size = new System.Drawing.Size(72, 20);
            this.colorPickerIncisionFillColor.TabIndex = 5;
            this.colorPickerIncisionFillColor.TabStop = false;
            this.colorPickerIncisionFillColor.Click += new System.EventHandler(this.ColorPicker_Click);
            // 
            // checkBoxIncisionFillUserColor
            // 
            this.checkBoxIncisionFillUserColor.AutoSize = true;
            this.checkBoxIncisionFillUserColor.Location = new System.Drawing.Point(15, 26);
            this.checkBoxIncisionFillUserColor.Name = "checkBoxIncisionFillUserColor";
            this.checkBoxIncisionFillUserColor.Size = new System.Drawing.Size(189, 20);
            this.checkBoxIncisionFillUserColor.TabIndex = 1;
            this.checkBoxIncisionFillUserColor.Text = "Использовать свой цвет";
            this.checkBoxIncisionFillUserColor.UseVisualStyleBackColor = true;
            this.checkBoxIncisionFillUserColor.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionFillColor_CheckedChanged);
            // 
            // checkBoxIncisionStroke
            // 
            this.checkBoxIncisionStroke.AutoSize = true;
            this.checkBoxIncisionStroke.Checked = true;
            this.checkBoxIncisionStroke.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionStroke.Location = new System.Drawing.Point(25, 10);
            this.checkBoxIncisionStroke.Name = "checkBoxIncisionStroke";
            this.checkBoxIncisionStroke.Size = new System.Drawing.Size(84, 20);
            this.checkBoxIncisionStroke.TabIndex = 1;
            this.checkBoxIncisionStroke.Text = "Обводка";
            this.checkBoxIncisionStroke.UseVisualStyleBackColor = true;
            this.checkBoxIncisionStroke.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionStroke_CheckedChanged);
            // 
            // groupBoxIncisionStroke
            // 
            this.groupBoxIncisionStroke.Controls.Add(this.tableLayoutPanelIncisionStroke);
            this.groupBoxIncisionStroke.Controls.Add(this.checkBoxIncisionStrokeUserColor);
            this.groupBoxIncisionStroke.Location = new System.Drawing.Point(10, 10);
            this.groupBoxIncisionStroke.Name = "groupBoxIncisionStroke";
            this.groupBoxIncisionStroke.Size = new System.Drawing.Size(210, 155);
            this.groupBoxIncisionStroke.TabIndex = 0;
            this.groupBoxIncisionStroke.TabStop = false;
            // 
            // tableLayoutPanelIncisionStroke
            // 
            this.tableLayoutPanelIncisionStroke.AutoSize = true;
            this.tableLayoutPanelIncisionStroke.ColumnCount = 2;
            this.tableLayoutPanelIncisionStroke.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIncisionStroke.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanelIncisionStroke.Controls.Add(this.labelIncisionStrokeAlpha, 0, 2);
            this.tableLayoutPanelIncisionStroke.Controls.Add(this.labelIncisionStrokeWdith, 0, 1);
            this.tableLayoutPanelIncisionStroke.Controls.Add(this.numericUpDownIncisionStrokeAlpha, 1, 2);
            this.tableLayoutPanelIncisionStroke.Controls.Add(this.labelIncisionStrokeColor, 0, 0);
            this.tableLayoutPanelIncisionStroke.Controls.Add(this.numericUpDownIncisionStrokeWidth, 1, 1);
            this.tableLayoutPanelIncisionStroke.Controls.Add(this.colorPickerIncisionStrokeColor, 1, 0);
            this.tableLayoutPanelIncisionStroke.Location = new System.Drawing.Point(10, 55);
            this.tableLayoutPanelIncisionStroke.Name = "tableLayoutPanelIncisionStroke";
            this.tableLayoutPanelIncisionStroke.RowCount = 3;
            this.tableLayoutPanelIncisionStroke.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelIncisionStroke.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelIncisionStroke.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanelIncisionStroke.Size = new System.Drawing.Size(192, 90);
            this.tableLayoutPanelIncisionStroke.TabIndex = 1;
            // 
            // labelIncisionStrokeAlpha
            // 
            this.labelIncisionStrokeAlpha.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionStrokeAlpha.AutoSize = true;
            this.labelIncisionStrokeAlpha.Location = new System.Drawing.Point(3, 67);
            this.labelIncisionStrokeAlpha.Name = "labelIncisionStrokeAlpha";
            this.labelIncisionStrokeAlpha.Size = new System.Drawing.Size(103, 16);
            this.labelIncisionStrokeAlpha.TabIndex = 8;
            this.labelIncisionStrokeAlpha.Text = "Прозрачность";
            this.labelIncisionStrokeAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIncisionStrokeWdith
            // 
            this.labelIncisionStrokeWdith.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionStrokeWdith.AutoSize = true;
            this.labelIncisionStrokeWdith.Location = new System.Drawing.Point(3, 37);
            this.labelIncisionStrokeWdith.Name = "labelIncisionStrokeWdith";
            this.labelIncisionStrokeWdith.Size = new System.Drawing.Size(58, 16);
            this.labelIncisionStrokeWdith.TabIndex = 6;
            this.labelIncisionStrokeWdith.Text = "Размер";
            this.labelIncisionStrokeWdith.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownIncisionStrokeAlpha
            // 
            this.numericUpDownIncisionStrokeAlpha.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionStrokeAlpha.DecimalPlaces = 2;
            this.numericUpDownIncisionStrokeAlpha.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownIncisionStrokeAlpha.Location = new System.Drawing.Point(115, 64);
            this.numericUpDownIncisionStrokeAlpha.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIncisionStrokeAlpha.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownIncisionStrokeAlpha.Name = "numericUpDownIncisionStrokeAlpha";
            this.numericUpDownIncisionStrokeAlpha.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionStrokeAlpha.TabIndex = 1;
            this.numericUpDownIncisionStrokeAlpha.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelIncisionStrokeColor
            // 
            this.labelIncisionStrokeColor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelIncisionStrokeColor.AutoSize = true;
            this.labelIncisionStrokeColor.Location = new System.Drawing.Point(3, 7);
            this.labelIncisionStrokeColor.Name = "labelIncisionStrokeColor";
            this.labelIncisionStrokeColor.Size = new System.Drawing.Size(40, 16);
            this.labelIncisionStrokeColor.TabIndex = 0;
            this.labelIncisionStrokeColor.Text = "Цвет";
            this.labelIncisionStrokeColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownIncisionStrokeWidth
            // 
            this.numericUpDownIncisionStrokeWidth.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDownIncisionStrokeWidth.DecimalPlaces = 2;
            this.numericUpDownIncisionStrokeWidth.Location = new System.Drawing.Point(115, 34);
            this.numericUpDownIncisionStrokeWidth.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.numericUpDownIncisionStrokeWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownIncisionStrokeWidth.Name = "numericUpDownIncisionStrokeWidth";
            this.numericUpDownIncisionStrokeWidth.Size = new System.Drawing.Size(74, 22);
            this.numericUpDownIncisionStrokeWidth.TabIndex = 0;
            this.numericUpDownIncisionStrokeWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // colorPickerIncisionStrokeColor
            // 
            this.colorPickerIncisionStrokeColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.colorPickerIncisionStrokeColor.BackColor = System.Drawing.Color.Black;
            this.colorPickerIncisionStrokeColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPickerIncisionStrokeColor.Location = new System.Drawing.Point(116, 5);
            this.colorPickerIncisionStrokeColor.Margin = new System.Windows.Forms.Padding(4);
            this.colorPickerIncisionStrokeColor.Name = "colorPickerIncisionStrokeColor";
            this.colorPickerIncisionStrokeColor.Size = new System.Drawing.Size(72, 20);
            this.colorPickerIncisionStrokeColor.TabIndex = 5;
            this.colorPickerIncisionStrokeColor.TabStop = false;
            this.colorPickerIncisionStrokeColor.Click += new System.EventHandler(this.ColorPicker_Click);
            // 
            // checkBoxIncisionStrokeUserColor
            // 
            this.checkBoxIncisionStrokeUserColor.AutoSize = true;
            this.checkBoxIncisionStrokeUserColor.Checked = true;
            this.checkBoxIncisionStrokeUserColor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncisionStrokeUserColor.Location = new System.Drawing.Point(15, 26);
            this.checkBoxIncisionStrokeUserColor.Name = "checkBoxIncisionStrokeUserColor";
            this.checkBoxIncisionStrokeUserColor.Size = new System.Drawing.Size(189, 20);
            this.checkBoxIncisionStrokeUserColor.TabIndex = 0;
            this.checkBoxIncisionStrokeUserColor.Text = "Использовать свой цвет";
            this.checkBoxIncisionStrokeUserColor.UseVisualStyleBackColor = true;
            this.checkBoxIncisionStrokeUserColor.CheckedChanged += new System.EventHandler(this.CheckBoxIncisionUserColor_CheckedChanged);
            // 
            // labelZoom
            // 
            this.labelZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelZoom.AutoSize = true;
            this.labelZoom.Location = new System.Drawing.Point(308, 904);
            this.labelZoom.Name = "labelZoom";
            this.labelZoom.Size = new System.Drawing.Size(21, 16);
            this.labelZoom.TabIndex = 10;
            this.labelZoom.Text = "x1";
            // 
            // trackBarZoom
            // 
            this.trackBarZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarZoom.Location = new System.Drawing.Point(14, 904);
            this.trackBarZoom.Maximum = 5;
            this.trackBarZoom.Minimum = 1;
            this.trackBarZoom.Name = "trackBarZoom";
            this.trackBarZoom.Size = new System.Drawing.Size(293, 45);
            this.trackBarZoom.TabIndex = 9;
            this.trackBarZoom.Value = 1;
            this.trackBarZoom.Scroll += new System.EventHandler(this.TrackBarZoom_Scroll);
            // 
            // colorDialog
            // 
            this.colorDialog.AnyColor = true;
            // 
            // tabControlView
            // 
            this.tabControlView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlView.Controls.Add(this.tabPageMap);
            this.tabControlView.Controls.Add(this.tabPageChart);
            this.tabControlView.Location = new System.Drawing.Point(581, 28);
            this.tabControlView.Name = "tabControlView";
            this.tabControlView.SelectedIndex = 0;
            this.tabControlView.Size = new System.Drawing.Size(611, 921);
            this.tabControlView.TabIndex = 11;
            // 
            // tabPageMap
            // 
            this.tabPageMap.BackColor = System.Drawing.Color.Transparent;
            this.tabPageMap.Controls.Add(this.panelImages);
            this.tabPageMap.Location = new System.Drawing.Point(4, 25);
            this.tabPageMap.Name = "tabPageMap";
            this.tabPageMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMap.Size = new System.Drawing.Size(603, 892);
            this.tabPageMap.TabIndex = 0;
            this.tabPageMap.Text = "Карты";
            // 
            // tabPageChart
            // 
            this.tabPageChart.BackColor = System.Drawing.Color.Transparent;
            this.tabPageChart.Controls.Add(this.comboBoxRepeatabilityPeriodChart);
            this.tabPageChart.Controls.Add(this.buttonRepeatabilityPeriodPrev);
            this.tabPageChart.Controls.Add(this.buttonRepeatabilityPeriodNext);
            this.tabPageChart.Controls.Add(this.chartRepeatabilityPeriod);
            this.tabPageChart.Controls.Add(this.tableLayoutPanelRepeatability);
            this.tabPageChart.Controls.Add(this.chartRepeatability);
            this.tabPageChart.Location = new System.Drawing.Point(4, 25);
            this.tabPageChart.Name = "tabPageChart";
            this.tabPageChart.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageChart.Size = new System.Drawing.Size(603, 892);
            this.tabPageChart.TabIndex = 1;
            this.tabPageChart.Text = "График";
            // 
            // comboBoxRepeatabilityPeriodChart
            // 
            this.comboBoxRepeatabilityPeriodChart.FormattingEnabled = true;
            this.comboBoxRepeatabilityPeriodChart.Location = new System.Drawing.Point(134, 653);
            this.comboBoxRepeatabilityPeriodChart.Name = "comboBoxRepeatabilityPeriodChart";
            this.comboBoxRepeatabilityPeriodChart.Size = new System.Drawing.Size(342, 24);
            this.comboBoxRepeatabilityPeriodChart.TabIndex = 32;
            this.comboBoxRepeatabilityPeriodChart.SelectedIndexChanged += new System.EventHandler(this.ComboBoxRepeatabilityPeriodChart_SelectedIndexChanged);
            // 
            // buttonRepeatabilityPeriodPrev
            // 
            this.buttonRepeatabilityPeriodPrev.Enabled = false;
            this.buttonRepeatabilityPeriodPrev.Location = new System.Drawing.Point(28, 652);
            this.buttonRepeatabilityPeriodPrev.Name = "buttonRepeatabilityPeriodPrev";
            this.buttonRepeatabilityPeriodPrev.Size = new System.Drawing.Size(100, 25);
            this.buttonRepeatabilityPeriodPrev.TabIndex = 31;
            this.buttonRepeatabilityPeriodPrev.Text = "Предыдущий";
            this.buttonRepeatabilityPeriodPrev.UseVisualStyleBackColor = true;
            this.buttonRepeatabilityPeriodPrev.Click += new System.EventHandler(this.ButtonRepeatabilityPeriodPrev_Click);
            // 
            // buttonRepeatabilityPeriodNext
            // 
            this.buttonRepeatabilityPeriodNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRepeatabilityPeriodNext.Enabled = false;
            this.buttonRepeatabilityPeriodNext.Location = new System.Drawing.Point(482, 652);
            this.buttonRepeatabilityPeriodNext.Name = "buttonRepeatabilityPeriodNext";
            this.buttonRepeatabilityPeriodNext.Size = new System.Drawing.Size(100, 25);
            this.buttonRepeatabilityPeriodNext.TabIndex = 30;
            this.buttonRepeatabilityPeriodNext.Text = "Следующий";
            this.buttonRepeatabilityPeriodNext.UseVisualStyleBackColor = true;
            this.buttonRepeatabilityPeriodNext.Click += new System.EventHandler(this.ButtonRepeatabilityPeriodNext_Click);
            // 
            // chartRepeatabilityPeriod
            // 
            this.chartRepeatabilityPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chartRepeatabilityPeriod.ChartAreas.Add(chartArea3);
            legend3.Enabled = false;
            legend3.Name = "Legend1";
            this.chartRepeatabilityPeriod.Legends.Add(legend3);
            this.chartRepeatabilityPeriod.Location = new System.Drawing.Point(6, 6);
            this.chartRepeatabilityPeriod.Name = "chartRepeatabilityPeriod";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series4.IsVisibleInLegend = false;
            series4.Legend = "Legend1";
            series4.MarkerSize = 15;
            series4.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series4.Name = "Series1";
            this.chartRepeatabilityPeriod.Series.Add(series4);
            this.chartRepeatabilityPeriod.Size = new System.Drawing.Size(591, 224);
            this.chartRepeatabilityPeriod.TabIndex = 21;
            this.chartRepeatabilityPeriod.Text = "chart1";
            // 
            // tableLayoutPanelRepeatability
            // 
            this.tableLayoutPanelRepeatability.ColumnCount = 4;
            this.tableLayoutPanelRepeatability.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.Controls.Add(this.comboBoxRepeatabilityPeriod, 3, 0);
            this.tableLayoutPanelRepeatability.Controls.Add(this.labelRepeatabilityPeriod, 2, 0);
            this.tableLayoutPanelRepeatability.Controls.Add(this.labelRepeatabilityKBegin, 0, 2);
            this.tableLayoutPanelRepeatability.Controls.Add(this.labelRepeatabilityA, 0, 1);
            this.tableLayoutPanelRepeatability.Controls.Add(this.labelRepeatabilityTypeY, 0, 0);
            this.tableLayoutPanelRepeatability.Controls.Add(this.comboBoxRepeatabilityTypeY, 1, 0);
            this.tableLayoutPanelRepeatability.Controls.Add(this.numericUpDownRepeatabilityA, 1, 1);
            this.tableLayoutPanelRepeatability.Controls.Add(this.checkBoxRepeatabilityKBeginAuto, 1, 2);
            this.tableLayoutPanelRepeatability.Controls.Add(this.numericUpDownRepeatabilityKBegin, 1, 3);
            this.tableLayoutPanelRepeatability.Controls.Add(this.numericUpDownRepeatabilityPeriod, 3, 1);
            this.tableLayoutPanelRepeatability.Controls.Add(this.buttonRepeatabilityCalculate, 3, 3);
            this.tableLayoutPanelRepeatability.Location = new System.Drawing.Point(6, 686);
            this.tableLayoutPanelRepeatability.Name = "tableLayoutPanelRepeatability";
            this.tableLayoutPanelRepeatability.RowCount = 4;
            this.tableLayoutPanelRepeatability.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelRepeatability.Size = new System.Drawing.Size(588, 200);
            this.tableLayoutPanelRepeatability.TabIndex = 20;
            // 
            // comboBoxRepeatabilityPeriod
            // 
            this.comboBoxRepeatabilityPeriod.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxRepeatabilityPeriod.FormattingEnabled = true;
            this.comboBoxRepeatabilityPeriod.Items.AddRange(new object[] {
            "Год",
            "Месяц",
            "День",
            "Час",
            "Минута",
            "Секунда",
            "Мс"});
            this.comboBoxRepeatabilityPeriod.Location = new System.Drawing.Point(444, 13);
            this.comboBoxRepeatabilityPeriod.Name = "comboBoxRepeatabilityPeriod";
            this.comboBoxRepeatabilityPeriod.Size = new System.Drawing.Size(94, 24);
            this.comboBoxRepeatabilityPeriod.TabIndex = 27;
            // 
            // labelRepeatabilityPeriod
            // 
            this.labelRepeatabilityPeriod.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRepeatabilityPeriod.AutoSize = true;
            this.labelRepeatabilityPeriod.Location = new System.Drawing.Point(297, 17);
            this.labelRepeatabilityPeriod.Name = "labelRepeatabilityPeriod";
            this.labelRepeatabilityPeriod.Size = new System.Drawing.Size(58, 16);
            this.labelRepeatabilityPeriod.TabIndex = 26;
            this.labelRepeatabilityPeriod.Text = "Период";
            this.labelRepeatabilityPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRepeatabilityKBegin
            // 
            this.labelRepeatabilityKBegin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRepeatabilityKBegin.AutoSize = true;
            this.labelRepeatabilityKBegin.Location = new System.Drawing.Point(3, 117);
            this.labelRepeatabilityKBegin.Name = "labelRepeatabilityKBegin";
            this.labelRepeatabilityKBegin.Size = new System.Drawing.Size(100, 16);
            this.labelRepeatabilityKBegin.TabIndex = 24;
            this.labelRepeatabilityKBegin.Text = "Минимальный";
            this.labelRepeatabilityKBegin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRepeatabilityA
            // 
            this.labelRepeatabilityA.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRepeatabilityA.AutoSize = true;
            this.labelRepeatabilityA.Location = new System.Drawing.Point(3, 67);
            this.labelRepeatabilityA.Name = "labelRepeatabilityA";
            this.labelRepeatabilityA.Size = new System.Drawing.Size(73, 16);
            this.labelRepeatabilityA.TabIndex = 21;
            this.labelRepeatabilityA.Text = "Значение";
            this.labelRepeatabilityA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRepeatabilityTypeY
            // 
            this.labelRepeatabilityTypeY.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRepeatabilityTypeY.AutoSize = true;
            this.labelRepeatabilityTypeY.Location = new System.Drawing.Point(3, 17);
            this.labelRepeatabilityTypeY.Name = "labelRepeatabilityTypeY";
            this.labelRepeatabilityTypeY.Size = new System.Drawing.Size(64, 16);
            this.labelRepeatabilityTypeY.TabIndex = 20;
            this.labelRepeatabilityTypeY.Text = "Выбрать";
            this.labelRepeatabilityTypeY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxRepeatabilityTypeY
            // 
            this.comboBoxRepeatabilityTypeY.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxRepeatabilityTypeY.FormattingEnabled = true;
            this.comboBoxRepeatabilityTypeY.Items.AddRange(new object[] {
            "A",
            "Y"});
            this.comboBoxRepeatabilityTypeY.Location = new System.Drawing.Point(150, 13);
            this.comboBoxRepeatabilityTypeY.Name = "comboBoxRepeatabilityTypeY";
            this.comboBoxRepeatabilityTypeY.Size = new System.Drawing.Size(94, 24);
            this.comboBoxRepeatabilityTypeY.TabIndex = 1;
            this.comboBoxRepeatabilityTypeY.SelectedIndexChanged += new System.EventHandler(this.ComboBoxRepeatabilityTypeY_SelectedIndexChanged);
            // 
            // numericUpDownRepeatabilityA
            // 
            this.numericUpDownRepeatabilityA.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownRepeatabilityA.Location = new System.Drawing.Point(150, 64);
            this.numericUpDownRepeatabilityA.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownRepeatabilityA.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownRepeatabilityA.Name = "numericUpDownRepeatabilityA";
            this.numericUpDownRepeatabilityA.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownRepeatabilityA.TabIndex = 19;
            this.numericUpDownRepeatabilityA.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // checkBoxRepeatabilityKBeginAuto
            // 
            this.checkBoxRepeatabilityKBeginAuto.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxRepeatabilityKBeginAuto.AutoSize = true;
            this.checkBoxRepeatabilityKBeginAuto.Location = new System.Drawing.Point(150, 115);
            this.checkBoxRepeatabilityKBeginAuto.Name = "checkBoxRepeatabilityKBeginAuto";
            this.checkBoxRepeatabilityKBeginAuto.Size = new System.Drawing.Size(129, 20);
            this.checkBoxRepeatabilityKBeginAuto.TabIndex = 23;
            this.checkBoxRepeatabilityKBeginAuto.Text = "Автоматически";
            this.checkBoxRepeatabilityKBeginAuto.UseVisualStyleBackColor = true;
            this.checkBoxRepeatabilityKBeginAuto.CheckedChanged += new System.EventHandler(this.CheckBoxKBeginAuto_CheckedChanged);
            // 
            // numericUpDownRepeatabilityKBegin
            // 
            this.numericUpDownRepeatabilityKBegin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownRepeatabilityKBegin.Location = new System.Drawing.Point(150, 164);
            this.numericUpDownRepeatabilityKBegin.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownRepeatabilityKBegin.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownRepeatabilityKBegin.Name = "numericUpDownRepeatabilityKBegin";
            this.numericUpDownRepeatabilityKBegin.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownRepeatabilityKBegin.TabIndex = 25;
            this.numericUpDownRepeatabilityKBegin.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // numericUpDownRepeatabilityPeriod
            // 
            this.numericUpDownRepeatabilityPeriod.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownRepeatabilityPeriod.Location = new System.Drawing.Point(444, 64);
            this.numericUpDownRepeatabilityPeriod.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownRepeatabilityPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRepeatabilityPeriod.Name = "numericUpDownRepeatabilityPeriod";
            this.numericUpDownRepeatabilityPeriod.Size = new System.Drawing.Size(94, 22);
            this.numericUpDownRepeatabilityPeriod.TabIndex = 28;
            this.numericUpDownRepeatabilityPeriod.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonRepeatabilityCalculate
            // 
            this.buttonRepeatabilityCalculate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonRepeatabilityCalculate.Location = new System.Drawing.Point(444, 162);
            this.buttonRepeatabilityCalculate.Name = "buttonRepeatabilityCalculate";
            this.buttonRepeatabilityCalculate.Size = new System.Drawing.Size(100, 25);
            this.buttonRepeatabilityCalculate.TabIndex = 29;
            this.buttonRepeatabilityCalculate.Text = "Рассчитать";
            this.buttonRepeatabilityCalculate.UseVisualStyleBackColor = true;
            this.buttonRepeatabilityCalculate.Click += new System.EventHandler(this.ButtonRepeatabilityCalculate_Click);
            // 
            // chartRepeatability
            // 
            this.chartRepeatability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea4.Name = "ChartArea1";
            this.chartRepeatability.ChartAreas.Add(chartArea4);
            legend4.Enabled = false;
            legend4.Name = "Legend1";
            this.chartRepeatability.Legends.Add(legend4);
            this.chartRepeatability.Location = new System.Drawing.Point(6, 235);
            this.chartRepeatability.Name = "chartRepeatability";
            this.chartRepeatability.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series5.IsVisibleInLegend = false;
            series5.Legend = "Legend1";
            series5.MarkerSize = 15;
            series5.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series5.Name = "Series1";
            series6.BorderWidth = 3;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.IsVisibleInLegend = false;
            series6.Legend = "Legend1";
            series6.Name = "Series2";
            this.chartRepeatability.Series.Add(series5);
            this.chartRepeatability.Series.Add(series6);
            this.chartRepeatability.Size = new System.Drawing.Size(591, 408);
            this.chartRepeatability.TabIndex = 0;
            this.chartRepeatability.Text = "chart1";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title2.Name = "Title1";
            title2.Text = "Титул";
            this.chartRepeatability.Titles.Add(title2);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 961);
            this.Controls.Add(this.tabControlView);
            this.Controls.Add(this.labelZoom);
            this.Controls.Add(this.trackBarZoom);
            this.Controls.Add(this.tabMainControl);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1220, 1000);
            this.Name = "FormMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Планшет";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panelImages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegendOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.tabMainControl.ResumeLayout(false);
            this.tabPageSelection.ResumeLayout(false);
            this.tabPageSelection.PerformLayout();
            this.groupBoxTime.ResumeLayout(false);
            this.groupBoxDepth.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEndDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartDepth)).EndInit();
            this.groupBoxDate.ResumeLayout(false);
            this.groupBoxCoord.ResumeLayout(false);
            this.tabControlCoord.ResumeLayout(false);
            this.tabPageRectangle.ResumeLayout(false);
            this.tabPageRectangle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLatEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLatStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLngEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecLngStart)).EndInit();
            this.tabPageCircle.ResumeLayout(false);
            this.tabPageCircle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCenterLat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCenterLng)).EndInit();
            this.tabPagePolygon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPolygon)).EndInit();
            this.tabPageIncision.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIncision)).EndInit();
            this.groupBoxSpeed.ResumeLayout(false);
            this.groupBoxSpeed.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmax)).EndInit();
            this.groupBoxEnergy.ResumeLayout(false);
            this.groupBoxEnergy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarKmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarKmax)).EndInit();
            this.tabPageSelectionAdditional.ResumeLayout(false);
            this.tabPageSelectionAdditional.PerformLayout();
            this.groupBoxNumeration.ResumeLayout(false);
            this.groupBoxNumeration.PerformLayout();
            this.tableLayoutPanelNumeration.ResumeLayout(false);
            this.tableLayoutPanelNumeration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumerationWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerNumeration)).EndInit();
            this.groupBoxNumerationCommon.ResumeLayout(false);
            this.groupBoxCouple.ResumeLayout(false);
            this.groupBoxCouple.PerformLayout();
            this.tableLayoutPanelCouple.ResumeLayout(false);
            this.tableLayoutPanelCouple.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleKEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleKBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoupleDays)).EndInit();
            this.tabPageLegendAndMarker.ResumeLayout(false);
            this.tabPageLegendAndMarker.PerformLayout();
            this.tableLayoutPanelMarkerColor.ResumeLayout(false);
            this.tableLayoutPanelMarkerColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMarkerRadius)).EndInit();
            this.tabPageGrid.ResumeLayout(false);
            this.tabPageGrid.PerformLayout();
            this.groupBoxGridDegrees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridDegreesWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerGridDegrees)).EndInit();
            this.groupBoxGridLines.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridLinesWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerGridLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridLinesLng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGridLinesLat)).EndInit();
            this.tabPageSurfer.ResumeLayout(false);
            this.tabPageSurfer.PerformLayout();
            this.groupBoxSurferFilter.ResumeLayout(false);
            this.groupBoxSurferFilter.PerformLayout();
            this.tableLayoutPanelSurferFilterParam.ResumeLayout(false);
            this.tableLayoutPanelSurferFilterParam.PerformLayout();
            this.tableLayoutPanelSurferParam.ResumeLayout(false);
            this.tableLayoutPanelSurferParam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownParam2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownParam1)).EndInit();
            this.groupBoxSurferType.ResumeLayout(false);
            this.groupBoxSurferType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatCount)).EndInit();
            this.tabPageImage.ResumeLayout(false);
            this.tabControlImage.ResumeLayout(false);
            this.tabPageIncisionImage.ResumeLayout(false);
            this.tabPageIncisionImage.PerformLayout();
            this.groupBoxIncisionPriority.ResumeLayout(false);
            this.groupBoxIncisionPriority.PerformLayout();
            this.tableLayoutPanelIncisionPriority.ResumeLayout(false);
            this.tableLayoutPanelIncisionPriority.PerformLayout();
            this.groupBoxIncisionRadius.ResumeLayout(false);
            this.groupBoxIncisionRadius.PerformLayout();
            this.tableLayoutPanelIncisionRadius.ResumeLayout(false);
            this.tableLayoutPanelIncisionRadius.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionRadiusMult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionRadiusFontSize)).EndInit();
            this.groupBoxIncisionDepth.ResumeLayout(false);
            this.groupBoxIncisionDepth.PerformLayout();
            this.groupBoxIncisionDepthNumber.ResumeLayout(false);
            this.groupBoxIncisionDepthNumber.PerformLayout();
            this.tableLayoutPanelIncisionDepthNumber.ResumeLayout(false);
            this.tableLayoutPanelIncisionDepthNumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthNumberMult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDepthNumberColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthNumberFontSize)).EndInit();
            this.groupBoxIncisionDepthLine.ResumeLayout(false);
            this.groupBoxIncisionDepthLine.PerformLayout();
            this.tableLayoutPanelIncisionDepthLine.ResumeLayout(false);
            this.tableLayoutPanelIncisionDepthLine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthLineMult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDepthLineColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDepthLineWidth)).EndInit();
            this.groupBoxIncisionDistance.ResumeLayout(false);
            this.groupBoxIncisionDistance.PerformLayout();
            this.groupBoxIncisionDistanceNumber.ResumeLayout(false);
            this.groupBoxIncisionDistanceNumber.PerformLayout();
            this.tableLayoutPanelIncisionDistanceNumber.ResumeLayout(false);
            this.tableLayoutPanelIncisionDistanceNumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceNumberMult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDistanceNumberColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceNumberFontSize)).EndInit();
            this.groupBoxIncisionDistanceLine.ResumeLayout(false);
            this.groupBoxIncisionDistanceLine.PerformLayout();
            this.tableLayoutPanelDistanceLine.ResumeLayout(false);
            this.tableLayoutPanelDistanceLine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceLineMult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionDistanceLineColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionDistanceLineWidth)).EndInit();
            this.groupBoxIncisionFill.ResumeLayout(false);
            this.groupBoxIncisionFill.PerformLayout();
            this.tableLayoutPanelIncisionFill.ResumeLayout(false);
            this.tableLayoutPanelIncisionFill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionFillAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionFillColor)).EndInit();
            this.groupBoxIncisionStroke.ResumeLayout(false);
            this.groupBoxIncisionStroke.PerformLayout();
            this.tableLayoutPanelIncisionStroke.ResumeLayout(false);
            this.tableLayoutPanelIncisionStroke.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionStrokeAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncisionStrokeWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorPickerIncisionStrokeColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarZoom)).EndInit();
            this.tabControlView.ResumeLayout(false);
            this.tabPageMap.ResumeLayout(false);
            this.tabPageChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartRepeatabilityPeriod)).EndInit();
            this.tableLayoutPanelRepeatability.ResumeLayout(false);
            this.tableLayoutPanelRepeatability.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatabilityA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatabilityKBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatabilityPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartRepeatability)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelImages;
        private System.Windows.Forms.PictureBox pictureBoxLegendOut;
        private System.Windows.Forms.PictureBox pictureBoxGrid;
        private System.Windows.Forms.PictureBox pictureBoxLegend;
        private System.Windows.Forms.PictureBox pictureBoxMap;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bulletinToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabControl tabMainControl;
        private System.Windows.Forms.TabPage tabPageSelection;
        private System.Windows.Forms.GroupBox groupBoxCoord;
        private System.Windows.Forms.GroupBox groupBoxSpeed;
        private System.Windows.Forms.TrackBar trackBarVmin;
        private System.Windows.Forms.Label labelVMinMax;
        private System.Windows.Forms.TrackBar trackBarVmax;
        private System.Windows.Forms.GroupBox groupBoxEnergy;
        private System.Windows.Forms.TrackBar trackBarKmin;
        private System.Windows.Forms.Label labelKMinMax;
        private System.Windows.Forms.TrackBar trackBarKmax;
        private System.Windows.Forms.TabPage tabPageLegendAndMarker;
        private System.Windows.Forms.Label labelMarkerRadius;
        private System.Windows.Forms.TrackBar trackBarMarkerRadius;
        private System.Windows.Forms.Button buttonMarkerVpVs;
        private System.Windows.Forms.CheckBox checkBoxLegendOut;
        private System.Windows.Forms.CheckBox checkBoxLegend;
        private System.Windows.Forms.TabPage tabPageGrid;
        private System.Windows.Forms.CheckBox checkBoxGridDegrees;
        private System.Windows.Forms.CheckBox checkBoxGridLines;
        private System.Windows.Forms.GroupBox groupBoxGridDegrees;
        private System.Windows.Forms.NumericUpDown numericUpDownGridDegreesWidth;
        private System.Windows.Forms.PictureBox colorPickerGridDegrees;
        private System.Windows.Forms.GroupBox groupBoxGridLines;
        private System.Windows.Forms.NumericUpDown numericUpDownGridLinesWidth;
        private System.Windows.Forms.PictureBox colorPickerGridLines;
        private System.Windows.Forms.NumericUpDown numericUpDownGridLinesLng;
        private System.Windows.Forms.NumericUpDown numericUpDownGridLinesLat;
        private System.Windows.Forms.CheckBox checkBoxCoord;
        private System.Windows.Forms.GroupBox groupBoxDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.CheckBox checkBoxDepth;
        private System.Windows.Forms.GroupBox groupBoxDepth;
        private System.Windows.Forms.NumericUpDown numericUpDownEndDepth;
        private System.Windows.Forms.NumericUpDown numericUpDownStartDepth;
        private System.Windows.Forms.Label labelZoom;
        private System.Windows.Forms.TrackBar trackBarZoom;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.CheckBox checkBoxTime;
        private System.Windows.Forms.GroupBox groupBoxTime;
        private System.Windows.Forms.DateTimePicker timeTimePickerEnd;
        private System.Windows.Forms.DateTimePicker timeTimePickerStart;
        private System.Windows.Forms.Button buttonMarkerClass;
        private System.Windows.Forms.ToolStripMenuItem catalogExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bulletinExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem surferDocToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPageSurfer;
        private System.Windows.Forms.Label labelLngCount;
        private System.Windows.Forms.Label labelLatCount;
        private System.Windows.Forms.NumericUpDown numericUpDownLngCount;
        private System.Windows.Forms.NumericUpDown numericUpDownLatCount;
        private System.Windows.Forms.GroupBox groupBoxSurferType;
        private System.Windows.Forms.RadioButton radioButtonStationV;
        private System.Windows.Forms.RadioButton radioButtonV;
        private System.Windows.Forms.RadioButton radioButtonDensity;
        private System.Windows.Forms.RadioButton radioButtonActivity;
        private System.Windows.Forms.Label labelLatKm;
        private System.Windows.Forms.Label labelLngKm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSurferParam;
        private System.Windows.Forms.Label labelParam1;
        private System.Windows.Forms.Label labelParam2;
        private System.Windows.Forms.NumericUpDown numericUpDownParam2;
        private System.Windows.Forms.NumericUpDown numericUpDownParam1;
        private System.Windows.Forms.TabControl tabControlCoord;
        private System.Windows.Forms.TabPage tabPageRectangle;
        private System.Windows.Forms.TabPage tabPageCircle;
        private System.Windows.Forms.TabPage tabPagePolygon;
        private System.Windows.Forms.Label labelRecLng;
        private System.Windows.Forms.Label labelRecLat;
        private System.Windows.Forms.NumericUpDown numericUpDownRecLatEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownRecLatStart;
        private System.Windows.Forms.NumericUpDown numericUpDownRecLngEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownRecLngStart;
        private System.Windows.Forms.Label labelCircleLat;
        private System.Windows.Forms.NumericUpDown numericUpDownRadius;
        private System.Windows.Forms.NumericUpDown numericUpDownCenterLat;
        private System.Windows.Forms.NumericUpDown numericUpDownCenterLng;
        private System.Windows.Forms.Label labelCircleRadius;
        private System.Windows.Forms.Label labelCircleLng;
        private System.Windows.Forms.DataGridView dataGridViewPolygon;
        private System.Windows.Forms.Button buttonPolygon;
        private System.Windows.Forms.TabPage tabPageIncision;
        private System.Windows.Forms.DataGridView dataGridViewIncision;
        private System.Windows.Forms.Button buttonShowCoord;
        private System.Windows.Forms.Button buttonIncision;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPolygonId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLng;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIncisionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLatStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLngStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLatEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLngEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLength;
        private System.Windows.Forms.ToolStripMenuItem saveImageAsIncisionToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPageImage;
        private System.Windows.Forms.TabControl tabControlImage;
        private System.Windows.Forms.TabPage tabPageIncisionImage;
        private System.Windows.Forms.GroupBox groupBoxIncisionStroke;
        private System.Windows.Forms.PictureBox colorPickerIncisionStrokeColor;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionStrokeAlpha;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionStrokeWidth;
        private System.Windows.Forms.CheckBox checkBoxIncisionStrokeUserColor;
        private System.Windows.Forms.CheckBox checkBoxIncisionStroke;
        private System.Windows.Forms.CheckBox checkBoxIncisionFill;
        private System.Windows.Forms.GroupBox groupBoxIncisionFill;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionFill;
        private System.Windows.Forms.Label labelIncisionFillAlpha;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionFillAlpha;
        private System.Windows.Forms.Label labelIncisionFillColor;
        private System.Windows.Forms.PictureBox colorPickerIncisionFillColor;
        private System.Windows.Forms.CheckBox checkBoxIncisionFillUserColor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionStroke;
        private System.Windows.Forms.Label labelIncisionStrokeAlpha;
        private System.Windows.Forms.Label labelIncisionStrokeWdith;
        private System.Windows.Forms.Label labelIncisionStrokeColor;
        private System.Windows.Forms.GroupBox groupBoxIncisionDistance;
        private System.Windows.Forms.CheckBox checkBoxIncisionDistanceLine;
        private System.Windows.Forms.GroupBox groupBoxIncisionDistanceLine;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDistanceLine;
        private System.Windows.Forms.Label labelIncisionDistanceLineMult;
        private System.Windows.Forms.Label labelIncisionDistanceLineWidth;
        private System.Windows.Forms.Label labelIncisionDistanceLineColor;
        private System.Windows.Forms.PictureBox colorPickerIncisionDistanceLineColor;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDistanceLineWidth;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDistanceLineMult;
        private System.Windows.Forms.CheckBox checkBoxIncisionDistanceNumber;
        private System.Windows.Forms.GroupBox groupBoxIncisionDistanceNumber;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionDistanceNumber;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDistanceNumberMult;
        private System.Windows.Forms.Label labelIncisionDistanceNumberMult;
        private System.Windows.Forms.Label labelIncisionDistanceNumberWidth;
        private System.Windows.Forms.Label labelIncisionDistanceNumberColor;
        private System.Windows.Forms.PictureBox colorPickerIncisionDistanceNumberColor;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDistanceNumberFontSize;
        private System.Windows.Forms.GroupBox groupBoxIncisionDepth;
        private System.Windows.Forms.CheckBox checkBoxIncisionDepthNumber;
        private System.Windows.Forms.GroupBox groupBoxIncisionDepthNumber;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionDepthNumber;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDepthNumberMult;
        private System.Windows.Forms.Label labelIncisionDepthNumberMult;
        private System.Windows.Forms.Label labelIncisionDepthNumberWidth;
        private System.Windows.Forms.Label labelIncisionDepthNumberColor;
        private System.Windows.Forms.PictureBox colorPickerIncisionDepthNumberColor;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDepthNumberFontSize;
        private System.Windows.Forms.CheckBox checkBoxIncisionDepthLine;
        private System.Windows.Forms.GroupBox groupBoxIncisionDepthLine;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionDepthLine;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDepthLineMult;
        private System.Windows.Forms.Label labelIncisionDepthLineMult;
        private System.Windows.Forms.Label labelIncisionDepthLineWidth;
        private System.Windows.Forms.Label labelIncisionDepthLineColor;
        private System.Windows.Forms.PictureBox colorPickerIncisionDepthLineColor;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionDepthLineWidth;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionRadiusMult;
        private System.Windows.Forms.GroupBox groupBoxIncisionRadius;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionRadius;
        private System.Windows.Forms.Label labelIncisionRadiusFontWidth;
        private System.Windows.Forms.Label labelIncisionRadiusMult;
        private System.Windows.Forms.NumericUpDown numericUpDownIncisionRadiusFontSize;
        private System.Windows.Forms.GroupBox groupBoxIncisionPriority;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIncisionPriority;
        private System.Windows.Forms.RadioButton radioButtonIncisionPriorityRadius;
        private System.Windows.Forms.RadioButton radioButtonIncisionPriorityDistance;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMarkerColor;
        private System.Windows.Forms.RadioButton radioButtonMarkerColorVpVs;
        private System.Windows.Forms.RadioButton radioButtonMarkerColorClass;
        private System.Windows.Forms.CheckBox checkBoxDontShowIfZero;
        private System.Windows.Forms.ToolStripMenuItem distributionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem distributionEnergyClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageAsRepeatabilityToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPageSelectionAdditional;
        private System.Windows.Forms.NumericUpDown numericUpDownCoupleDistance;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCouple;
        private System.Windows.Forms.NumericUpDown numericUpDownCoupleDays;
        private System.Windows.Forms.DateTimePicker dateTimePickerCoupleTime;
        private System.Windows.Forms.GroupBox groupBoxCouple;
        private System.Windows.Forms.Label labelCoupleDistance;
        private System.Windows.Forms.Label labelCoupleDays;
        private System.Windows.Forms.Label labelCoupleTime;
        private System.Windows.Forms.Label labelCoupleK;
        private System.Windows.Forms.NumericUpDown numericUpDownCoupleKEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownCoupleKBegin;
        private System.Windows.Forms.Button buttonCoupleApply;
        private System.Windows.Forms.CheckBox checkBoxNumeration;
        private System.Windows.Forms.GroupBox groupBoxNumeration;
        private System.Windows.Forms.Button buttonNumerationApply;
        private System.Windows.Forms.NumericUpDown numericUpDownNumerationWidth;
        private System.Windows.Forms.PictureBox colorPickerNumeration;
        private System.Windows.Forms.RadioButton radioButtonNumerationCommon;
        private System.Windows.Forms.Button buttonNumerationCommonApply;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelNumeration;
        private System.Windows.Forms.Label labelNumerationColor;
        private System.Windows.Forms.Label labelNumerationWidth;
        private System.Windows.Forms.RadioButton radioButtonCouple;
        private System.Windows.Forms.GroupBox groupBoxNumerationCommon;
        private System.Windows.Forms.RadioButton radioButtonNumerationDepth;
        private System.Windows.Forms.CheckBox checkBoxNumerationAutoSize;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControlView;
        private System.Windows.Forms.TabPage tabPageMap;
        private System.Windows.Forms.TabPage tabPageChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartRepeatability;
        private System.Windows.Forms.ComboBox comboBoxRepeatabilityTypeY;
        private System.Windows.Forms.NumericUpDown numericUpDownRepeatabilityA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRepeatability;
        private System.Windows.Forms.Label labelRepeatabilityA;
        private System.Windows.Forms.Label labelRepeatabilityTypeY;
        private System.Windows.Forms.CheckBox checkBoxRepeatabilityKBeginAuto;
        private System.Windows.Forms.Label labelRepeatabilityKBegin;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartRepeatabilityPeriod;
        private System.Windows.Forms.NumericUpDown numericUpDownRepeatabilityKBegin;
        private System.Windows.Forms.ComboBox comboBoxRepeatabilityPeriod;
        private System.Windows.Forms.Label labelRepeatabilityPeriod;
        private System.Windows.Forms.NumericUpDown numericUpDownRepeatabilityPeriod;
        private System.Windows.Forms.Button buttonRepeatabilityCalculate;
        private System.Windows.Forms.Button buttonRepeatabilityPeriodNext;
        private System.Windows.Forms.Button buttonRepeatabilityPeriodPrev;
        private System.Windows.Forms.ComboBox comboBoxRepeatabilityPeriodChart;
        private System.Windows.Forms.ToolStripMenuItem repeatabilityPeriodToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker dateTimePickerFilterParam1;
        private System.Windows.Forms.RadioButton radioButtonScoreField;
        private System.Windows.Forms.GroupBox groupBoxSurferFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSurferFilterParam;
        private System.Windows.Forms.RadioButton radioButtonFilterParam1;
    }
}

