﻿using System;
using System.Collections.Generic;

namespace Seismology.Map
{
    public static class ListExtension
    {
        public static bool Contains<T>(this IList<T> source, IList<T> values)
        {
            if (source != null)
            {
                foreach (var value in values)
                {
                    if (!source.Contains(value))
                    {
                        return false;
                    }
                }

                return true;
            }

            throw new ArgumentNullException(nameof(source));
        }
    }
}