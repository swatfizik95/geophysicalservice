﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.Map
{
    public partial class FormMapAdd : Form
    {
        private Image _img;
        private Size _newSize;
        private Dictionary<string, FormMapSettings> _mapSettings;
        private decimal _ratioM;

        public FormMapAdd(string path, Dictionary<string, FormMapSettings> mapSettings)
        {
            InitializeComponent();

            textBoxName.Text = Path.GetFileNameWithoutExtension(path);
            textBoxFileName.Text = Path.GetFileName(path);
            _img = Image.FromFile(path);
            _mapSettings = mapSettings;
        }

        private void FormMapAdd_Load(object sender, EventArgs e)
        {
            var size = _img.Size;
            int height = 1418;

            _ratioM = decimal.Divide(height, size.Height);
            _newSize.Height = height;
            _newSize.Width = (int)(size.Width * _ratioM);
            numericUpDownRatio.Value = 2;

            numericUpDownLatStartPx.Maximum = size.Height;
            numericUpDownLatEndPx.Maximum = size.Height;
            numericUpDownLatEndPx.Value = size.Height;

            numericUpDownLngStartPx.Maximum = size.Width;
            numericUpDownLngEndPx.Maximum = size.Width;
            numericUpDownLngEndPx.Value = size.Width;
        }

        private void FormMapAdd_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK) return;

            string name = textBoxName.Text;
            string fileName = textBoxFileName.Text;

            if (_mapSettings.ContainsKey(name))
            {
                MessageBox.Show("Такое название карты уже существует!\r\nПридумайте новое неповторяющееся название.", "Внимание!");
                e.Cancel = true;
                return;
            }

            if (_mapSettings.FirstOrDefault(s => s.Value.FileName == fileName).Key != null)
            {
                MessageBox.Show("Такое название файла карты уже существует!\r\nПридумайте новое неповторяющееся название.", "Внимание!");
                e.Cancel = true;
                return;
            }

            if (!Directory.Exists(Settings.MapPath)) Directory.CreateDirectory(Settings.MapPath);
            _img.Resize(_newSize).Save($"{Settings.MapPath}\\{fileName}");
        }

        public string ReturnName()
        {
            return textBoxName.Text;
        }

        public FormMapSettings ReturnMapSettings()
        {
            return new FormMapSettings(
                textBoxFileName.Text,
                (int)(_ratioM * numericUpDownLatStartPx.Value),
                (int)(_ratioM * numericUpDownLatEndPx.Value),
                (int)numericUpDownLatStartDeg.Value,
                (int)numericUpDownLatEndDeg.Value,
                (int)(_ratioM * numericUpDownLngStartPx.Value),
                (int)(_ratioM * numericUpDownLngEndPx.Value),
                (int)numericUpDownLngStartDeg.Value,
                (int)numericUpDownLngEndDeg.Value,
                (float)numericUpDownRatio.Value,
                (int)numericUpDownRatio.Value);
        }
    }
}
