﻿using Seismology.Core.Map;
using Seismology.Map.ColorHelper;
using Seismology.Map.IncisionHelper;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Seismology.Map
{
    public class FormSettings
    {
        private const int ClassStart = 3;
        private const int ClassEnd = 16;
        private const int ClassCount = ClassEnd - ClassStart;

        #region Properies

        #region Marker

        #region Marker Color

        //public Color[] Colors => _colors;

        //public double[] ColorIntervals => _colorIntervals;

        public Color[] ColorsVpVs { get; set; } =
        {
            ColorTranslator.FromHtml("#9cb7bb"),
            ColorTranslator.FromHtml("#9bd8da"),
            ColorTranslator.FromHtml("#65b016"),
            ColorTranslator.FromHtml("#ffb62a"),
            ColorTranslator.FromHtml("#ff6a00"),
        };

        public double[] ColorIntervalsVpVs { get; set; } =
        {
            1.6,
            1.7,
            1.76,
            1.86
        };

        public Color[] ColorsClass { get; set; } = Enumerable
            .Range(0, ClassCount + 1)
            .Select(i =>
            {
                var colorStart = Color.FromArgb(255, 170, 170);
                var colorEnd = Color.FromArgb(80, 0, 0);
                var colorStep = Color.FromArgb(
                    (colorStart.R - colorEnd.R) / (ClassCount + 1),
                    (colorStart.G - colorEnd.G) / (ClassCount + 1),
                    (colorStart.B - colorEnd.B) / (ClassCount + 1));
                return Color.FromArgb(
                    colorStart.R - colorStep.R * i,
                    colorStart.G - colorStep.G * i,
                    colorStart.B - colorStep.B * i);
            })
            .ToArray();

        public double[] ColorIntervalsClass { get; set; } = Enumerable.Range(0, ClassCount).Select(i => i + ClassStart + 0.5).ToArray();

        public MarkerColorType ColorType { get; set; } = MarkerColorType.VpVs;

        #endregion

        public int[] Radiuses { get; set; } =
            Enumerable.Range(0, ClassEnd - ClassStart + 1).Select(i => 15 + i * 5).ToArray();

        public int[] Classes { get; set; } =
            Enumerable.Range(0, ClassEnd - ClassStart + 1).Select(i => i + ClassStart).ToArray();

        #endregion

        #region Selection

        public Coordinate[] PolygonCoordinates { get; set; } = {
            new Coordinate(42, 46),
            new Coordinate(43, 47),
            new Coordinate(42, 47),
            new Coordinate(43, 48),
        };

        public IncisionValue[] IncisionsValues { get; set; } = {
            new IncisionValue
            {
                StartLat = 42,
                StartLng = 46,
                EndLat   = 43,
                EndLng   = 47,
                Length   = 30
            },
        };

        public int NumerationWidth { get; set; } = 18;
        public Color NumerationColor { get; set; } = Color.Black;

        #endregion

        #region Legend

        public double RadiusMult { get; set; } = 1;
        public bool DontShowIfZero { get; set; } = false;

        #endregion

        #region Grid

        public int GridLinesLngCount { get; set; } = 4;
        public int GridLinesLatCount { get; set; } = 4;
        public int GridLinesWidth { get; set; } = 1;
        public Color GridLinesColor { get; set; } = Color.Black;

        public int GridDegreesWidth { get; set; } = 12;
        public Color GridDegreesColor { get; set; } = Color.Black;

        #endregion

        #region Surfer

        public int SurferLatCount { get; set; } = 12;
        public int SurferLngCount { get; set; } = 12;

        #endregion

        #region Image

        #region Incision

        public bool IncisionStroke { get; set; } = true;
        public bool IncisionStrokeUserColor { get; set; } = true;
        public Color IncisionStrokeColor { get; set; } = Color.Black;
        public double IncisionStrokeWidth { get; set; } = 1;
        public double IncisionStrokeAlpha { get; set; } = 1;
        public bool IncisionFill { get; set; } = true;
        public bool IncisionFillUserColor { get; set; } = false;
        public Color IncisionFillColor { get; set; } = Color.Black;
        public double IncisionFillAlpha { get; set; } = 1;
        public bool IncisionDistanceLine { get; set; } = true;
        public Color IncisionDistanceLineColor { get; set; } = Color.Black;
        public double IncisionDistanceLineWidth { get; set; } = 1;
        public int IncisionDistanceLineMult { get; set; } = 10;
        public bool IncisionDistanceNumber { get; set; } = true;
        public Color IncisionDistanceNumberColor { get; set; } = Color.Black;
        public double IncisionDistanceNumberFontSize { get; set; } = 14;
        public int IncisionDistanceNumberMult { get; set; } = 10;
        public bool IncisionDepthLine { get; set; } = true;
        public Color IncisionDepthLineColor { get; set; } = Color.Black;
        public double IncisionDepthLineWidth { get; set; } = 1;
        public int IncisionDepthLineMult { get; set; } = 10;
        public bool IncisionDepthNumber { get; set; } = true;
        public Color IncisionDepthNumberColor { get; set; } = Color.Black;
        public double IncisionDepthNumberFontSize { get; set; } = 14;
        public int IncisionDepthNumberMult { get; set; } = 10;
        public int IncisionRadiusMult { get; set; } = 1;
        public double IncisionRadiusFontSize { get; set; } = 16;
        public IncisionPriority IncisionPriority { get; set; } = IncisionPriority.Distance;

        #endregion

        #endregion

        #region Map

        public Dictionary<string, FormMapSettings> MapSettings { get; set; } = new Dictionary<string, FormMapSettings>
        {
            { "Стандарт", new FormMapSettings("MapDagestan.png", 0, 1418, 41, 45, 0, 1036, 45, 49, 2.0f, 2)},
            { "Стандарт (с морем)", new FormMapSettings("MapDagestanWithSea.png", 0, 1418, 41, 45, 0, 1036, 45, 49, 2.0f, 2)},
        };

        #endregion

        #endregion

        #region Equal

        protected bool Equals(FormSettings other)
        {
            return ColorsVpVs.SequenceEqual(other.ColorsVpVs) && ColorIntervalsVpVs.SequenceEqual(other.ColorIntervalsVpVs) &&
                   ColorsClass.SequenceEqual(other.ColorsClass) && ColorIntervalsClass.SequenceEqual(other.ColorIntervalsClass) &&
                   ColorType == other.ColorType && Radiuses.SequenceEqual(other.Radiuses) && Classes.SequenceEqual(other.Classes) &&
                   PolygonCoordinates.SequenceEqual(other.PolygonCoordinates) &&
                   IncisionsValues.SequenceEqual(other.IncisionsValues) && NumerationWidth == other.NumerationWidth &&
                   NumerationColor.Equals(other.NumerationColor) && RadiusMult.Equals(other.RadiusMult) &&
                   DontShowIfZero == other.DontShowIfZero && GridLinesLngCount == other.GridLinesLngCount &&
                   GridLinesLatCount == other.GridLinesLatCount && GridLinesWidth == other.GridLinesWidth &&
                   GridLinesColor.Equals(other.GridLinesColor) && GridDegreesWidth == other.GridDegreesWidth &&
                   GridDegreesColor.Equals(other.GridDegreesColor) && SurferLatCount == other.SurferLatCount &&
                   SurferLngCount == other.SurferLngCount && IncisionStroke == other.IncisionStroke &&
                   IncisionStrokeUserColor == other.IncisionStrokeUserColor &&
                   IncisionStrokeColor.Equals(other.IncisionStrokeColor) &&
                   IncisionStrokeWidth.Equals(other.IncisionStrokeWidth) &&
                   IncisionStrokeAlpha.Equals(other.IncisionStrokeAlpha) && IncisionFill == other.IncisionFill &&
                   IncisionFillUserColor == other.IncisionFillUserColor &&
                   IncisionFillColor.Equals(other.IncisionFillColor) &&
                   IncisionFillAlpha.Equals(other.IncisionFillAlpha) &&
                   IncisionDistanceLine == other.IncisionDistanceLine &&
                   IncisionDistanceLineColor.Equals(other.IncisionDistanceLineColor) &&
                   IncisionDistanceLineWidth.Equals(other.IncisionDistanceLineWidth) &&
                   IncisionDistanceLineMult == other.IncisionDistanceLineMult &&
                   IncisionDistanceNumber == other.IncisionDistanceNumber &&
                   IncisionDistanceNumberColor.Equals(other.IncisionDistanceNumberColor) &&
                   IncisionDistanceNumberFontSize.Equals(other.IncisionDistanceNumberFontSize) &&
                   IncisionDistanceNumberMult == other.IncisionDistanceNumberMult &&
                   IncisionDepthLine == other.IncisionDepthLine &&
                   IncisionDepthLineColor.Equals(other.IncisionDepthLineColor) &&
                   IncisionDepthLineWidth.Equals(other.IncisionDepthLineWidth) &&
                   IncisionDepthLineMult == other.IncisionDepthLineMult &&
                   IncisionDepthNumber == other.IncisionDepthNumber &&
                   IncisionDepthNumberColor.Equals(other.IncisionDepthNumberColor) &&
                   IncisionDepthNumberFontSize.Equals(other.IncisionDepthNumberFontSize) &&
                   IncisionDepthNumberMult == other.IncisionDepthNumberMult &&
                   IncisionRadiusMult == other.IncisionRadiusMult &&
                   IncisionRadiusFontSize.Equals(other.IncisionRadiusFontSize) &&
                   IncisionPriority == other.IncisionPriority &&
                   MapSettings.SequenceEqual(other.MapSettings);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FormSettings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (ColorsVpVs != null ? ColorsVpVs.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ColorIntervalsVpVs != null ? ColorIntervalsVpVs.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ColorsClass != null ? ColorsClass.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ColorIntervalsClass != null ? ColorIntervalsClass.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)ColorType;
                hashCode = (hashCode * 397) ^ (Radiuses != null ? Radiuses.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Classes != null ? Classes.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PolygonCoordinates != null ? PolygonCoordinates.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IncisionsValues != null ? IncisionsValues.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ NumerationWidth;
                hashCode = (hashCode * 397) ^ NumerationColor.GetHashCode();
                hashCode = (hashCode * 397) ^ RadiusMult.GetHashCode();
                hashCode = (hashCode * 397) ^ DontShowIfZero.GetHashCode();
                hashCode = (hashCode * 397) ^ GridLinesLngCount;
                hashCode = (hashCode * 397) ^ GridLinesLatCount;
                hashCode = (hashCode * 397) ^ GridLinesWidth;
                hashCode = (hashCode * 397) ^ GridLinesColor.GetHashCode();
                hashCode = (hashCode * 397) ^ GridDegreesWidth;
                hashCode = (hashCode * 397) ^ GridDegreesColor.GetHashCode();
                hashCode = (hashCode * 397) ^ SurferLatCount;
                hashCode = (hashCode * 397) ^ SurferLngCount;
                hashCode = (hashCode * 397) ^ IncisionStroke.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionStrokeUserColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionStrokeColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionStrokeWidth.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionStrokeAlpha.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionFill.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionFillUserColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionFillColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionFillAlpha.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceLine.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceLineColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceLineWidth.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceLineMult;
                hashCode = (hashCode * 397) ^ IncisionDistanceNumber.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceNumberColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceNumberFontSize.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDistanceNumberMult;
                hashCode = (hashCode * 397) ^ IncisionDepthLine.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDepthLineColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDepthLineWidth.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDepthLineMult;
                hashCode = (hashCode * 397) ^ IncisionDepthNumber.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDepthNumberColor.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDepthNumberFontSize.GetHashCode();
                hashCode = (hashCode * 397) ^ IncisionDepthNumberMult;
                hashCode = (hashCode * 397) ^ IncisionRadiusMult;
                hashCode = (hashCode * 397) ^ IncisionRadiusFontSize.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)IncisionPriority;
                hashCode = (hashCode * 397) ^ (MapSettings != null ? MapSettings.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(FormSettings left, FormSettings right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FormSettings left, FormSettings right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}