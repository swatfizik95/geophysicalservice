﻿using Seismology.Core.Data;
using Seismology.Core.MathS;
using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable CompareOfFloatsByEqualityOperator
// ReSharper disable NonReadonlyMemberInGetHashCode

namespace Seismology.Core
{
    /// <summary>
    /// Представляет землетрясение
    /// </summary>
    public class Earthquake : IComparable<Earthquake>, ICloneable
    {
        private double _k;
        private int _class;
        private double _magnitude;

        /// <summary>
        /// Номер
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Дата землетрясения
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Время толчка в гипоцентре (по Гринвичу +3-00-00.000)
        /// </summary>
        public DateTime T0 { get; set; }

        /// <summary>
        /// Дата и время толчка
        /// </summary>
        public DateTime DateTime => Date.Date.Add(T0.TimeOfDay);


        /// <summary>
        /// Энергия (класс) 10^<see cref="K"/> Дж
        /// </summary>
        public double K
        {
            get => _k;
            set
            {
                _k = value;
                _class = CalculatorS.CalculateClass(value);
                _magnitude = CalculatorS.CalculateMagnitude(_class);
            }
        }

        /// <summary>
        /// Класс
        /// </summary>
        public int Class => _class;

        /// <summary>
        /// Магнитуда
        /// </summary>
        public double Magnitude => _magnitude;

        /// <summary>
        /// Глубина в км
        /// </summary>
        public double H { get; set; }

        /// <summary>
        /// Широта (F - latitude)
        /// </summary>
        public double Lat { get; set; }

        public double LatError { get; set; }

        /// <summary>
        /// Долгота (L - longitue)
        /// </summary>
        public double Lng { get; set; }

        public double LngError { get; set; }

        /// <summary>
        /// Невязка (погрешность)
        /// </summary>
        public double Dt { get; set; }

        /// <summary>
        /// Среднеквадратичное значение Невязки
        /// </summary>
        public double DtRms { get; set; }

        /// <summary>
        /// Отношение скоростей vp/vs (продольной и поперечной волны)
        /// </summary>
        public double VpVs { get; set; }

        /// <summary>
        /// Данные по станциям (если их нет, значение EarthquakeStations.Count = 0)
        /// </summary>
        public List<EarthquakeStation> Stations { get; set; }
            = new List<EarthquakeStation>();

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="Earthquake"/>
        /// с данными по умолчанию
        /// </summary>
        public Earthquake()
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="Earthquake"/>
        /// с помощью указанных начальных данных
        /// </summary>
        /// <param name="number">Номер</param>
        /// <param name="date">Дата</param>
        /// <param name="t0">Время в гипоцентре</param>
        /// <param name="k">Энергия 10^<see cref="K"/> Дж</param>
        /// <param name="h">Глубина в км</param>
        /// <param name="lat">Широта (latitude)</param>
        /// <param name="lng">Долгота (longitude)</param>
        /// <param name="dt"></param>
        /// <param name="vpvs">Отношение скоростей vp/vs</param>
        public Earthquake(int number, DateTime date, DateTime t0,
            double k, double h, double lat, double lng, double dt, double vpvs)
        {
            Number = number;
            Date = date;
            T0 = t0;
            K = k;
            H = h;
            Lat = lat;
            Lng = lng;
            Dt = dt;
            VpVs = vpvs;
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="Earthquake"/>
        /// с помощью указанных начальных данных
        /// </summary>
        /// <param name="number">Номер</param>
        /// <param name="date">Дата</param>
        /// <param name="t0">Время в гипоцентре</param>
        /// <param name="k">Энергия 10^<see cref="K"/> Дж</param>
        /// <param name="h">Глубина в км</param>
        /// <param name="lat">Широта (latitude)</param>
        /// <param name="lng">Долгота (longitude)</param>
        /// <param name="dt"></param>
        /// <param name="v">Отношение скоростей vp/vs</param>
        /// <param name="eqs">Данные по станциям</param>
        public Earthquake(int number, DateTime date, DateTime t0,
            double k, double h, double lat, double lng, double dt, double v,
            List<EarthquakeStation> eqs)
        {
            Number = number;
            Date = date;
            T0 = t0;
            K = k;
            H = h;
            Lat = lat;
            Lng = lng;
            Dt = dt;
            VpVs = v;
            Stations = new List<EarthquakeStation>(eqs);
        }

        /// <summary>
        /// Преобразует экземпляр замлетрясения класса <see cref="DbEarthquake"/> в экземпляр класса <see cref="Earthquake"/>
        /// </summary>
        /// <param name="earthquake"></param>
        /// <returns></returns>
        public static explicit operator Earthquake(DbEarthquake earthquake)
        {
            return new Earthquake
            {
                Number = earthquake.Id,
                Date = earthquake.Date,
                T0 = earthquake.T0,
                K = earthquake.K,
                H = earthquake.H,
                Lat = earthquake.Lat,
                LatError = earthquake.LatError,
                Lng = earthquake.Lng,
                LngError = earthquake.LngError,
                Dt = earthquake.Dt,
                DtRms = earthquake.DtRms,
                VpVs = earthquake.VpVs,
                Stations = earthquake.Stations.Select(station => (EarthquakeStation)station).ToList()
            };
        }



        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (GetType() != obj.GetType())
                return false;
            Earthquake another = (Earthquake)obj;
            return Date == another.Date &&
                   T0 == another.T0 &&
                   K == another.K &&
                   H == another.H &&
                   Lat == another.Lat &&
                   LatError == another.LatError &&
                   Lng == another.Lng &&
                   LngError == another.LngError &&
                   Dt == another.Dt &&
                   DtRms == another.DtRms &&
                   VpVs == another.VpVs &&
                   Stations.SequenceEqual(another.Stations);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            var hashCode = 614271503;
            hashCode = hashCode * -1521134295 + Number.GetHashCode();
            hashCode = hashCode * -1521134295 + Date.GetHashCode();
            hashCode = hashCode * -1521134295 + T0.GetHashCode();
            hashCode = hashCode * -1521134295 + K.GetHashCode();
            hashCode = hashCode * -1521134295 + H.GetHashCode();
            hashCode = hashCode * -1521134295 + Lat.GetHashCode();
            hashCode = hashCode * -1521134295 + LatError.GetHashCode();
            hashCode = hashCode * -1521134295 + Lng.GetHashCode();
            hashCode = hashCode * -1521134295 + LngError.GetHashCode();
            hashCode = hashCode * -1521134295 + Dt.GetHashCode();
            hashCode = hashCode * -1521134295 + DtRms.GetHashCode();
            hashCode = hashCode * -1521134295 + VpVs.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<List<EarthquakeStation>>.Default.GetHashCode(Stations);
            return hashCode;
        }

        /// <summary>
        /// Сравнивает по дате и времени
        /// </summary>
        /// <param name="other">Другой экземпляр класса</param>
        /// <returns></returns>
        public int CompareTo(Earthquake other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            if (Date.Date > other.Date.Date)
                return 1;
            if (Date.Date < other.Date.Date)
                return -1;
            if (T0 > other.T0)
                return 1;
            if (T0 < other.T0)
                return -1;
            return 0;
        }

        /// <summary>
        /// Возвращает (неглубокую) копию экземпляра класса
        /// </summary>
        /// <returns>Землетрясение</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Возвращает (глубокую) копию экземпляра класса
        /// </summary>
        /// <returns>Землетрясение</returns>
        public Earthquake Copy()
        {
            var earthquake = (Earthquake)MemberwiseClone();
            earthquake.Stations = Stations.Select(station => station.Copy()).ToList();
            return earthquake;
        }
    }
}
