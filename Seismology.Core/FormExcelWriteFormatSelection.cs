﻿using Seismology.Core.IO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.Core
{
    public partial class FormExcelWriteFormatSelection : Form
    {
        private readonly string[] _formats;
        private readonly int _countMin = 1;
        private readonly int _countMax = 40;
        private int _currentCount = 3;
        private List<ComboBox> _comboBoxes = new List<ComboBox>();
        private List<TextBox> _textBoxes = new List<TextBox>();

        /// <summary>
        /// Форма для выбора Excel формата
        /// </summary>
        public FormExcelWriteFormatSelection(bool isBulletin, ExcelType excelType)
        {
            InitializeComponent();

            _formats = isBulletin ? FileS.PossibleBulletinExcelFormats : FileS.PossibleCatalogExcelFormats;

            InitializeComboBoxes();
            InitializeTextBoxes();

            if (excelType == ExcelType.Csv)
            {
                Text = "Выберите формат CSV файла";
                textBoxDelimeter.Visible = true;
                labelDelimeter.Visible = true;
                textBoxWorksheetName.Visible = false;
                labelWorksheetName.Visible = false;
            }
        }

        #region Events

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            var t = _comboBoxes.Select(cb => cb.Location.X).ToList();
            if (_currentCount < _countMax)
            {
                if (_currentCount > 0)
                {
                    _comboBoxes[_currentCount].Left = _comboBoxes[_currentCount - 1].Left + 105;
                    _textBoxes[_currentCount].Left = _textBoxes[_currentCount - 1].Left + 105;
                }
                _comboBoxes[_currentCount].Visible = true;
                _textBoxes[_currentCount].Visible = true;
                _currentCount++;
                CheckAddRemove();
            }
        }

        private void ButtonRemove_Click(object sender, EventArgs e)
        {
            if (_currentCount > _countMin)
            {
                _currentCount--;
                _comboBoxes[_currentCount].Visible = false;
                _textBoxes[_currentCount].Visible = false;
                CheckAddRemove();
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var combobox = (ComboBox)sender;
            int index = Convert.ToInt32(combobox.Tag);
            if (_formats.Any(p => p.ToUpper() == _textBoxes[index].Text.ToUpper()))
            {
                _textBoxes[index].Text = combobox.Text;
            }
        }

        #endregion

        #region Logic

        private void InitializeComboBoxes()
        {
            int startX = 0;
            var point = new Point(startX, 0);
            var size = new Size(100, 30);
            int maxDropDownWidthSize = _formats.Max(f => f.Length) * 10;
            for (int i = 0; i < _countMax; i++)
            {
                point.X = startX + i * 105;
                var comboBox = new ComboBox
                {
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    DropDownWidth = maxDropDownWidthSize,
                    Parent = panelFormat,
                    Location = point,
                    Size = size,
                    Visible = false,
                    Tag = i,
                };
                comboBox.Items.AddRange(_formats);
                comboBox.SelectedIndex = 0;
                comboBox.SelectedIndexChanged += ComboBox_SelectedIndexChanged;

                _comboBoxes.Add(comboBox);
            }

            foreach (var comboBox in _comboBoxes.Take(_currentCount))
            {
                comboBox.Visible = true;
            }
        }

        private void InitializeTextBoxes()
        {
            int startX = 0;
            var point = new Point(startX, 40);
            var size = new Size(100, 30);
            for (int i = 0; i < _countMax; i++)
            {
                point.X = startX + i * 105;
                var textBox = new TextBox
                {
                    Parent = panelFormat,
                    Location = point,
                    Size = size,
                    Visible = false,
                    Text = _comboBoxes[0].Text
                };

                _textBoxes.Add(textBox);
            }

            foreach (var textBox in _textBoxes.Take(_currentCount))
            {
                textBox.Visible = true;
            }
        }

        private void CheckAddRemove()
        {
            buttonAdd.Enabled = _currentCount != _countMax;
            buttonRemove.Enabled = _currentCount != _countMin;
        }

        /// <summary>
        /// Возвращаем формат
        /// </summary>
        /// <returns></returns>
        public string[] GetFormats()
        {
            return _comboBoxes.Take(_currentCount).Select(cb => cb.Text).ToArray();
        }

        /// <summary>
        /// Возвращает заголовки форматов
        /// </summary>
        /// <returns></returns>
        public string[] GetFormatsName()
        {
            return _textBoxes.Take(_currentCount).Select(tb => tb.Text).ToArray();
        }

        /// <summary>
        /// Возвращает разделитель для csv
        /// </summary>
        /// <returns></returns>
        public char GetDelimeter()
        {
            return textBoxDelimeter.Text[0];
        }

        /// <summary>
        /// Возвращает имя литса xlsx
        /// </summary>
        /// <returns></returns>
        public string GetWorksheetName()
        {
            return textBoxWorksheetName.Text;
        }

        #endregion
    }
}
