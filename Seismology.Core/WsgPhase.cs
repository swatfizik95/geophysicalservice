﻿using System;

namespace Seismology.Core
{
    /// <summary>
    /// Класс для хранения станционной фазы из файла WSG
    /// </summary>
    public class WsgPhase
    {
        public string Name { set; get; }
        public double Dist { set; get; }
        public double EvAz { set; get; }
        public string Phaze { set; get; }
        public DateTime Time { set; get; }
        public double TRes { set; get; }
        public double Azim { set; get; }
        public string AzRes { set; get; }
        public string Slow { set; get; }
        public string SRes { set; get; }
        public string Def { set; get; }
        public string SNR { set; get; }
        public double Amp { set; get; }
        public double Per { set; get; }
        public string Qual { set; get; }
        public double Magnitude { get; set; }
        public int ArrID { set; get; }
    }
}
