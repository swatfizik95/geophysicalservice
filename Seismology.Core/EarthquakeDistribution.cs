﻿using System.Linq;

namespace Seismology.Core
{
    public class EarthquakeDistribution
    {

        public int[] Classes { get; }
        public int[] Depths { get; }

        public EarthquakeDistribution(int[] years, int[] classes, int[] depths)
        {
            DistributionValues = new EarthquakeDistributionValue[years.Length];
            Classes = classes.ToArray();
            Depths = depths.ToArray();
        }

        public EarthquakeDistributionValue[] DistributionValues { get; set; }
    }
}