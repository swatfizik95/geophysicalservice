﻿using Seismology.Core.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.Core
{
    public partial class FormBulletinFormatSelection : Form
    {
        private readonly int[] _groups;
        
        public FormBulletinFormatSelection(IList<Earthquake> _earthquakes)
        {
            InitializeComponent();

            _groups = _earthquakes.SelectMany(eq => eq.Stations)
                .Select(s => s.Station.Group)
                .Distinct()
                .ToArray();

            labelGroup.Text = string.Format(L10n.GroupNumbersSelect, string.Join(", ", _groups));

            InitializeTextBoxes();
        }
        
        #region events

        private void FormBulletinFormatSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK && IsSelectedGroupsCorrect() == false)
            {
                MessageBox.Show(L10n.GroupNumbersNotRecognize, L10n.Attention);
                e.Cancel = true;
            }
        }

        #endregion

        #region methods

        public int[] GetSelectedGroups()
        {
            if (TryGetSelectedGroups(out var result) == false)
                throw new Exception(L10n.GroupNumbersNotRecognize);

            return result;
        }

        private bool TryGetSelectedGroups(out int[] result)
        {
            string text = textBoxGroup.Text;

            string[] splitted = text.Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

            result = new int[splitted.Length];

            for (int index = 0; index < splitted.Length; index++)
            {
                if (int.TryParse(splitted[index], out var res) == false)
                {
                    return false;
                }

                result[index] = res;
            }

            return true;
        }

        private void InitializeTextBoxes() =>
            textBoxGroup.Text = string.Join(", ", _groups);

        private bool IsSelectedGroupsCorrect() => 
            TryGetSelectedGroups(out var result);

        #endregion
    }
}
