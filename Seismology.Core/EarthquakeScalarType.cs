﻿using Seismology.Core.Infrastructure.Attributes;

namespace Seismology.Core
{
    public enum EarthquakeScalarType
    {
        /// <summary>
        /// Плотность
        /// </summary>
        [Title("Плотность")]
        Density,
        /// <summary>
        /// Отношение скоростей
        /// </summary>
        [Title("Отношение скоростей")]
        VpVs,
        /// <summary>
        /// Отношение скоростей станционное
        /// </summary>
        [Title("Отношение скоростей станционное")]
        StationVpVs,
        /// <summary>
        /// Активность
        /// </summary>
        [Title("Активность")]
        Activity,
        /// <summary>
        /// Поле бальности
        /// </summary>
        [Title("Поле бальности")]
        ScoreField
    }
}