﻿using System;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет отрезок
    /// </summary>
    public class SegmentS
    {
        /// <summary>
        /// Начало отрезок
        /// </summary>
        public Coordinate Start { get; }

        /// <summary>
        /// Конец отрезка
        /// </summary>
        public Coordinate End { get; }

        public SegmentS(Coordinate start, Coordinate end)
        {
            if (end.Lng < start.Lng)
            {
                Start = new Coordinate(end.Lat, end.Lng);
                End = new Coordinate(start.Lat, start.Lng);
            }
            else
            {
                Start = new Coordinate(start.Lat, start.Lng);
                End = new Coordinate(end.Lat, end.Lng);
            }


            GetValues(this, out var k1, out var b1, out var type1);
        }

        public Coordinate FindIntersection(SegmentS segment)
        {
            GetValues(this, out var k1, out var b1, out var type1);
            GetValues(segment, out var k2, out var b2, out var type2);

            var intersection = new Coordinate();

            if (k1 == k2 ||
                type1 == SegmentSType.Vertical && type2 == SegmentSType.Vertical ||
                type1 == SegmentSType.Horizontal && type2 == SegmentSType.Horizontal)
            {
                if (Start.Equals(segment.Start) || Start.Equals(segment.End))
                {
                    return Start;
                }

                if (End.Equals(segment.Start) || End.Equals(segment.End))
                {
                    return End;
                }

                return null;
            }

            if (type1 == SegmentSType.Vertical)
            {
                intersection.Lng = Start.Lng;
                intersection.Lat = intersection.Lng * k2 + b2;

                if (segment.Start.Lng <= intersection.Lng &&
                    segment.End.Lng >= intersection.Lng &&
                    Math.Min(Start.Lat, End.Lat) <= intersection.Lat &&
                    Math.Max(Start.Lat, End.Lat) >= intersection.Lat)
                {
                    return intersection;
                }
                else
                {
                    return null;
                }
            }

            if (type2 == SegmentSType.Vertical)
            {
                intersection.Lng = segment.Start.Lng;
                intersection.Lat = k1 * intersection.Lng + b1;
                if (Start.Lng <= intersection.Lng &&
                    End.Lng >= intersection.Lng &&
                    Math.Min(segment.Start.Lng, segment.Start.Lat) <= intersection.Lat &&
                    Math.Max(segment.Start.Lng, segment.Start.Lat) >= intersection.Lat)
                {
                    return intersection;
                }
                else
                {
                    return null;
                }
            }

            intersection.Lng = (b1 - b2) / (k2 - k1);
            intersection.Lat = k1 * intersection.Lng + b1;

            if (intersection.Lng < Math.Max(Start.Lng, segment.Start.Lng) ||
                intersection.Lng > Math.Min(End.Lng, segment.End.Lng))
            {
                return null;
            }
            else
            {
                return intersection;
            }
        }

        private void GetValues(SegmentS segment, out double k, out double b, out SegmentSType type)
        {
            k = (segment.End.Lat - segment.Start.Lat) / (segment.End.Lng - segment.Start.Lng);
            b = segment.Start.Lat - k * segment.Start.Lng;
            double kPerpendicular = -1 / k;
            type = SegmentSType.Diagonal;

            if (segment.Start.Lng == segment.End.Lng || double.IsInfinity(k))
            {
                type = SegmentSType.Vertical;
            }
            else if (segment.Start.Lat == segment.End.Lat || double.IsInfinity(kPerpendicular))
            {
                type = SegmentSType.Horizontal;
            }
        }

        private enum SegmentSType
        {
            Vertical,
            Horizontal,
            Diagonal
        }
    }
}