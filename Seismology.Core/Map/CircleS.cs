﻿using Seismology.Core.Map.Interfaces;
using System;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет круг с координатами и радиусом
    /// </summary>
    public class CircleS : IPlaneFigureS, IAreaFigureS
    {
        private double _lat;
        private double _lng;
        private double _radius;
        private double _latRadius;
        private double _lngRadius;

        #region Свойства

        /// <summary>
        /// Широта
        /// </summary>
        public double Lat
        {
            get => _lat;
            set
            {
                _lat = value;
                _lngRadius = GetLngRadius(_radius, _lat);
            }
        }

        /// <summary>
        /// Долгота
        /// </summary>
        public double Lng
        {
            get => _lng;
            set => _lng = value;
        }

        /// <summary>
        /// Радиус в Киллометрах
        /// </summary>
        public double Radius
        {
            get => _radius;
            set
            {
                _radius = value;
                _latRadius = GetLatRadius(_radius);
                _lngRadius = GetLngRadius(_radius, _lat);
            }
        }

        /// <summary>
        /// Длина радиуса в градусах по широте
        /// </summary>
        public double LatRadius => _latRadius;

        /// <summary>
        /// Длина радиуса в градусах по долготе
        /// </summary>
        public double LngRadius => _lngRadius;

        /// <summary>
        /// Центр
        /// </summary>
        public Coordinate Centre => new Coordinate(_lat, _lng);

        #endregion


        #region Конструкторы

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="CircleS"/> со следуюшими значениями
        /// </summary>
        /// <param name="lat">Широта центра</param>
        /// <param name="lng">Долгота центра</param>
        /// <param name="radius">Радиус</param>
        public CircleS(double lat, double lng, double radius)
        {
            _lat = lat;
            _lng = lng;
            _radius = radius;
            _latRadius = GetLatRadius(radius);
            _lngRadius = GetLngRadius(radius, lat);
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="CircleS"/> со следуюшими значениями
        /// </summary>
        /// <param name="coordinate">Координаты центра</param>
        /// <param name="radius">Радиус</param>
        public CircleS(Coordinate coordinate, double radius) : this(coordinate.Lat, coordinate.Lng, radius)
        {
        }

        #endregion

        // Получает радиус в градусах по широте
        private double GetLatRadius(double radius)
        {
            return radius / Coordinate.LatToKm(1);
        }

        // Получает радиус в градусах по долготе
        private double GetLngRadius(double radius, double lat)
        {
            return radius / Coordinate.LngToKm(lat, 1);
        }

        /// <summary>
        /// <inheritdoc />
        /// <remarks>Т.к. на самом деле, при проецировании окружности на реальную карту получится эллипс
        /// Поэтому проверяет находится ли внутри эллипса, а не круга</remarks>
        /// </summary>
        public bool IsInside(double lat, double lng)
        {
            return Math.Sqrt(Math.Pow(lat - _lat, 2) / Math.Pow(_latRadius, 2) +
                             Math.Pow(lng - _lng, 2) / Math.Pow(_lngRadius, 2)) <= 1;
        }

        /// <summary>
        /// <inheritdoc />
        /// <remarks>Т.к. на самом деле, при проецировании окружности на реальную карту получится эллипс
        /// Поэтому проверяет находится ли внутри эллипса, а не круга</remarks>
        /// </summary>
        public bool IsInside(Coordinate coordinate)
        {
            return IsInside(coordinate.Lat, coordinate.Lng);
        }

        /// <inheritdoc />
        public double CalculateArea()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}
