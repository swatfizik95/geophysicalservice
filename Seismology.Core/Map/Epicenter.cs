﻿namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет эпицентр с координатами: широта (Lat) и долгота (Lng)
    /// </summary>
    public struct Epicenter
    {
        /// <summary>
        /// Широта
        /// </summary>
        public double Lat { get; set; }
        /// <summary>
        /// Долгота
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="Epicenter"/> с указанными координатам
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        public Epicenter(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }
    }
}