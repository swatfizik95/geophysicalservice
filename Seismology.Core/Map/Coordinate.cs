﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет координаты
    /// </summary>
    public class Coordinate
    {
        private const double LatMult = 111.111;
        private const double LngMult = 111.3;
        private const double R = 6371.0;
        private const double RadianMult = Math.PI / 180.0;

        /// <summary>
        /// Широта
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// Возвращает длину 1° широты в км
        /// </summary>
        public double LatKm => LatMult;

        /// <summary>
        /// Возвращает длину 1° долготы в км 
        /// </summary>
        public double LngKm => LngMult * Math.Cos(RadianMult * Lat);

        #region Конструкторы

        /// <summary>
        /// Инициализирует класс <see cref="Coordinate"/> со значениями по умолчанию (0, 0)
        /// </summary>
        public Coordinate()
        {
            Lat = 0;
            Lng = 0;
        }

        /// <summary>
        /// Инициализирует класс <see cref="Coordinate"/> со следующими значениями
        /// </summary>
        /// <param name="lat">Долгота</param>
        /// <param name="lng">Широта</param>
        public Coordinate(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }

        /// <summary>
        /// Инициализирует класс <see cref="Coordinate"/> со следующими значениями
        /// </summary>
        /// <param name="coordinate"></param>
        public Coordinate(Coordinate coordinate)
        {
            Lat = coordinate.Lat;
            Lng = coordinate.Lng;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Вычисляет модуль расстояния до точки в км со следующими координатами
        /// </summary>
        /// <param name="lat">Широта в градусах</param>
        /// <param name="lng">Долгота в градусах</param>
        /// <returns>Расстояние в км</returns>
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        public double DistanceTo(double lat, double lng)
        {
            /*
             * Статья по которой идет вычисление
             * http://osiktakan.ru/geo_koor.htm
             */
            if (Lat == lat && Lng == lng)
                return 0.0;

            return R * Math.Abs(Math.Acos(Math.Sin(RadianMult * lat) * Math.Sin(RadianMult * Lat) +
                                          Math.Cos(RadianMult * lat) * Math.Cos(RadianMult * Lat) *
                                          Math.Cos(RadianMult * (lng - Lng))));
        }

        /// <summary>
        /// Вычисляет модуль расстояния до точки point в км
        /// </summary>
        /// <param name="point">Точка, до которой нужно найти расстояние</param>
        /// <returns>Расстояние в км</returns>
        public double DistanceTo(Coordinate point)
        {
            return DistanceTo(point.Lat, point.Lng);
        }

        #endregion

        ///// <summary>
        ///// Вычисляет модуль расстояния до точки point в км
        ///// </summary>
        ///// <param name="point">Точка, до которой нужно найти расстояние</param>
        ///// <returns>Расстояние в км</returns>
        //public double DistanceTo(PointLatLng point)
        //{
        //    return DistanceTo(point.Lat, point.Lng);
        //}

        #region Статичные методы

        /// <summary>
        /// Возвращает длину широты в км
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <returns></returns>
        public static double LatToKm(double lat) => lat * LatMult;

        /// <summary>
        /// Переводит длину долготы в км на указанной широте
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        /// <returns></returns>
        public static double LngToKm(double lat, double lng) => lng * (LngMult * Math.Cos(RadianMult * lat));

        /// <summary>
        /// Переводит длину дуги в км в широту
        /// </summary>
        /// <param name="km">Км</param>
        /// <returns></returns>
        public static double KmToLat(double km) => km / LatMult;

        /// <summary>
        /// Переводит длину дуги в км в долготу
        /// </summary>
        /// <param name="km">Км</param>
        /// <param name="lat">Долгота</param>
        /// <returns></returns>
        public static double KmToLng(double km, double lat) => km / (LngMult * Math.Cos(RadianMult * lat));

        #endregion

        #region Equal

        /// <summary>
        /// Сранивает две координаты по параметрам (Широта и Долгота)
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        protected bool Equals(Coordinate other)
        {
            return Lat.Equals(other.Lat) && Lng.Equals(other.Lng);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Coordinate)obj);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            unchecked
            {
                return (Lat.GetHashCode() * 397) ^ Lng.GetHashCode();
            }
        }

        #endregion
    }
}
