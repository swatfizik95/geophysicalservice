﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Класс для работы с картами GMap
    /// </summary>
    public class GMapS
    {
        private readonly CultureInfo _culture = CultureInfo.InvariantCulture;

        /// <summary>
        /// Возвращает маркер землтерясения типа <see cref="GMarkerGoogle"/>
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        /// <param name="markerGoogleType">Тип гугл маркера</param>
        /// <returns>Маркер землетрясения</returns>
        public GMarkerGoogle GetEarthquakeGMarker(double lat, double lng, GMarkerGoogleType markerGoogleType = GMarkerGoogleType.red_dot)
        {
            return GetEarthquakeGMarker(new PointLatLng(lat, lng), markerGoogleType);
        }

        /// <summary>
        /// Возвращает маркер землтерясения типа <see cref="GMarkerGoogle"/>
        /// </summary>
        /// <param name="point">Координаты землетрясения</param>
        /// <param name="markerGoogleType">Тип гугл маркера</param>
        /// <returns>Маркер землетрясения</returns>
        public GMarkerGoogle GetEarthquakeGMarker(PointLatLng point, GMarkerGoogleType markerGoogleType = GMarkerGoogleType.red_dot)
        {
            return new GMarkerGoogle(point, markerGoogleType)
            {
                ToolTipText = "Землетрясение\r\n" +
                              $"{point.Lat.ToString("f4", _culture)}, " +
                              $"{point.Lng.ToString("f4", _culture)}",
                ToolTip =
                {
                    Format = { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center},
                    Font = new Font("Tahoma", 12, FontStyle.Bold)
                }
            };
        }

        /// <summary>
        /// Возвращает маркер станции в виде треугольника и его имени
        /// </summary>
        /// <param name="station">Станция, маркер которой нужен</param>
        /// <param name="size">Длина стороны квадрта в пикселях, в который будет вписан треугольник</param>
        /// <returns>Возвращает маркер станции</returns>
        public GMarkerGoogle GetStationGMarker(Station station, int size = 20)
        {
            var imgStan = GetTriangleBitmap(station.Color, size);

            var gMarker = new GMarkerGoogle(station.GMapCoordinate, imgStan)
            {
                ToolTipText = $"{station.Name}",
                ToolTipMode = MarkerTooltipMode.Always,
                ToolTip =
                {
                    Fill = new SolidBrush(Color.Transparent),
                    Foreground = new SolidBrush(station.Color),
                    Stroke = new Pen(new SolidBrush(Color.Transparent)),
                    Font = new Font("Tahoma", 12)
                }
            };
            switch (station.Name.Length)
            {
                case 4:
                    gMarker.ToolTip.Offset = new Point(-33, 20);
                    break;
                case 3:
                    gMarker.ToolTip.Offset = new Point(-28, 20);
                    break;
            }

            return gMarker;
        }

        private Bitmap GetTriangleBitmap(Color color, int size)
        {
            var bitmap = new Bitmap(size, size);
            using (var graph = Graphics.FromImage(bitmap))
            {
                Point[] points =
                {
                    new Point(size / 2, 0),
                    new Point(size, size),
                    new Point(0, size)
                };
                graph.FillPolygon(new SolidBrush(color), points);
            }

            return bitmap;
        }

        /// <summary>
        /// Вовзращает маркер станцию "пустышку". Нужен только для вывода информации об станции. 
        /// Для получения маркера самой станции воспользуйтесь методом <see cref="GetStationGMarker"/>
        /// </summary>
        /// <param name="station">Станция, маркер которой нужен</param>
        /// <returns>Возвращает маркер "пустышку" станции</returns>
        public GMarkerGoogle GetEmptyStationGMarker(Station station)
        {
            int size = 20;
            var imgStanEmpty = new Bitmap(size, size);
            using (var graph = Graphics.FromImage(imgStanEmpty))
            {
                Point[] points =
                {
                    new Point(size / 2, 0),
                    new Point(size, size),
                    new Point(0, size)
                };
                graph.FillPolygon(Brushes.Transparent, points);
            }
            var gMarkerEmpty = new GMarkerGoogle(station.GMapCoordinate, imgStanEmpty)
            {
                ToolTipText = $"({station.Name}) {station.RusName}\r\n" +
                              $"{station.Coordinate.Lat.ToString(_culture)}, " +
                              $"{station.Coordinate.Lng.ToString(_culture)}",
                ToolTip =
                {
                    Format = { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center},
                    Font = new Font("Tahoma", 12, FontStyle.Bold)
                }
            };

            return gMarkerEmpty;
        }


        /// <summary>
        /// Возвращает круг типа <see cref="GMapPolygon"/>
        /// </summary>
        /// <param name="centre">Координаты центра</param>
        /// <param name="radiusKm">Радиус (в киллометрах)</param>
        /// <param name="lineColor">Цвет линии</param>
        /// <param name="fillColor">Цвет заполнения</param>
        /// <param name="segments">Количество сегментов</param>
        /// <returns></returns>
        public GMapPolygon GetCircle(PointLatLng centre, double radiusKm,
            Color lineColor, Color fillColor, int segments = 500)
        {
            var points = new List<PointLatLng>();
            double radiusLat = Coordinate.KmToLat(radiusKm);
            double radiusLng = Coordinate.KmToLng(radiusKm, centre.Lat);
            double segment = Math.PI * 2.0 / segments;

            for (int i = 0; i <= segments; i++)
            {
                double theta = segment * i;
                double lat = centre.Lat + Math.Sin(theta) * radiusLat;
                double lng = centre.Lng + Math.Cos(theta) * radiusLng;
                var gPoint = new PointLatLng(lat, lng);
                points.Add(gPoint);
            }

            var gPol = new GMapPolygon(points, "pol")
            {
                Fill = new SolidBrush(fillColor),
                Stroke = new Pen(lineColor, 2)
            };

            return gPol;
        }

        /// <summary>
        /// Возвращает эллипс типа <see cref="GMapPolygon"/>
        /// </summary>
        /// <param name="centre">Координаты центра</param>
        /// <param name="radiusLatKm">Радиус широты (в киллометрах)</param>
        /// <param name="radiusLngKm">Радиус долготы (в киллометрах)</param>
        /// <param name="lineColor">Цвет линии</param>
        /// <param name="fillColor">Цвет заполнения</param>
        /// <param name="segments">Количество сегментов</param>
        /// <returns></returns>
        public GMapPolygon GetEllipse(PointLatLng centre, double radiusLatKm, double radiusLngKm,
            Color lineColor, Color fillColor, int segments = 100)
        {
            var points = new List<PointLatLng>();
            double radiusLat = Coordinate.KmToLat(radiusLatKm);
            double radiusLng = Coordinate.KmToLng(radiusLngKm, centre.Lat);
            double segment = Math.PI * 2 / segments;

            for (int i = 0; i <= segments; i++)
            {
                double theta = segment * i;
                double lat = centre.Lat + Math.Cos(theta) * radiusLat;
                double lng = centre.Lng + Math.Sin(theta) * radiusLng;
                var gPoint = new PointLatLng(lat, lng);
                points.Add(gPoint);
            }

            var gPol = new GMapPolygon(points, "pol")
            {
                Fill = new SolidBrush(fillColor),
                Stroke = new Pen(lineColor, 2)
            };

            return gPol;
        }

        /// <summary>
        /// Возвращет <see cref="GMapPolygon"/> полигон станции с его соответсствующим цветом
        /// </summary>
        /// <param name="station"></param>
        /// <param name="segments">Количество сегментов</param>
        /// <returns></returns>
        public GMapPolygon GetStationRadiusGPolygon(EarthquakeStation station, int segments = 100)
        {
            return GetCircle(
                station.Station.GMapCoordinate,
                station.EpicDistance,
                station.Station.Color,
                Color.Transparent,
                segments);
        }

        /// <summary>
        /// Возвращет <see cref="GMapPolygon"/> полигон станции с его соответсствующим цветом
        /// </summary>
        /// <param name="station"></param>
        /// <param name="isIncludeAlpha">Включить ли прозрачность</param>
        /// <param name="alpha">Прозрачность</param>
        /// <param name="segments">Количество сегментов</param>
        /// <returns></returns>
        public GMapPolygon GetStationRadiusGPolygon(EarthquakeStation station, bool isIncludeAlpha, int alpha = 100, int segments = 100)
        {
            Color color = isIncludeAlpha ? station.Station.Color : Color.FromArgb(100, station.Station.Color);
            return GetCircle(
                station.Station.GMapCoordinate,
                station.EpicDistance,
                color,
                Color.Transparent,
                segments);
        }
    }
}
