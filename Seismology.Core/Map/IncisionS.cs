﻿using Seismology.Core.Map.Interfaces;
using System;
using System.Linq;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет разрез
    /// </summary>
    public class IncisionS : IPlaneFigureS, IAreaFigureS
    {
        private const double Epsilon = -0.01;
        private const int PointsLength = 4;
        private readonly Coordinate[] _points = new Coordinate[PointsLength];
        private readonly PolygonS _polygon;

        /// <summary>
        /// Начало разреза
        /// </summary>
        public Coordinate Start { get; }
        /// <summary>
        /// Конец конец разреза
        /// </summary>
        public Coordinate End { get; }
        /// <summary>
        /// Длина разреза в км.
        /// </summary>
        public double Length { get; set; }

        #region Конструкторы

        public IncisionS(double startLat, double startLng, double endLat, double endLng, double length)
        {
            double tan = (endLat - startLat) / (endLng - startLng);
            double angle = tan < 0 ? Math.Atan(tan) - Math.PI / 2 : Math.Atan(tan) + Math.PI / 2;
            double sin = Math.Sin(angle);
            double cos = Math.Cos(angle);

            Length = length;
            Start = new Coordinate(startLat, startLng);
            End = new Coordinate(endLat, endLng);

            var start = new Coordinate(
                Coordinate.KmToLat(length) * sin,
                Coordinate.KmToLng(length, startLat) * cos);
            var end = new Coordinate(
                Coordinate.KmToLat(length) * sin,
                Coordinate.KmToLng(length, endLat) * cos);

            _points[0] = new Coordinate(
                startLat + start.Lat,
                startLng + start.Lng);
            _points[1] = new Coordinate(
                startLat - start.Lat,
                startLng - start.Lng);
            _points[2] = new Coordinate(
                endLat - end.Lat,
                endLng - end.Lng);
            _points[3] = new Coordinate(
                endLat + end.Lat,
                endLng + end.Lng);

            _polygon = new PolygonS(_points);
        }

        public IncisionS(Coordinate start, Coordinate end, double length) :
            this(start.Lat, start.Lng, end.Lat, end.Lng, length)
        {
        }

        #endregion

        #region Методы

        /// <inheritdoc/>
        public bool IsInside(double lat, double lng)
        {
            return _polygon.IsInside(lat, lng);

            //for (int index = 0, prevIndex = _points.Length - 1; index < _points.Length; index++, prevIndex = index - 1)
            //{
            //    double d = (lng - _points[index].Lng) * (_points[prevIndex].Lat - _points[index].Lat) - 
            //               (lat - _points[index].Lat) * (_points[prevIndex].Lng - _points[index].Lng);
            //    if (d < Epsilon)
            //    {
            //        return false;
            //    }
            //}

            //return true;
        }

        /// <inheritdoc/>
        public bool IsInside(Coordinate coordinate)
        {
            return IsInside(coordinate.Lat, coordinate.Lng);
        }

        public double CalculateArea()
        {
            double e2 = Math.Pow(_points[0].DistanceTo(_points[2]), 2);
            double f2 = Math.Pow(_points[1].DistanceTo(_points[3]), 2);
            double a2 = Math.Pow(_points[0].DistanceTo(_points[1]), 2);
            double b2 = Math.Pow(_points[1].DistanceTo(_points[2]), 2);
            double c2 = Math.Pow(_points[2].DistanceTo(_points[3]), 2);
            double d2 = Math.Pow(_points[3].DistanceTo(_points[0]), 2);
            return 0.25 * Math.Sqrt(4 * e2 * f2 - Math.Pow(b2 + d2 - a2 - c2, 2));
        }

        /// <summary>
        /// Возвращает узлы заданного разреза
        /// </summary>
        /// <returns></returns>
        public Coordinate[] GetPoints()
        {
            return _points.Select(p => new Coordinate(p)).ToArray();
        }

        #endregion
    }
}