﻿using GMap.NET;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Seismology.TestApp")]
namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет станцию
    /// </summary>
    public class Station : ICloneable
    {
        #region Properties

        /// <summary>
        /// Имя станции (учитывает R)
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Международное имя станции (без R)
        /// </summary>
        public string InternationalName { get; }

        /// <summary>
        /// Имя, которое хранится в БД
        /// </summary>
        public string NameInDb { get; }

        /// <summary>
        /// Дополнительное имя для чтения
        /// </summary>
        public string NameForRead { get; }

        /// <summary>
        /// Русское имя станции
        /// </summary>
        public string RusName { get; }

        /// <summary>
        /// Координаты станции
        /// </summary>
        public Coordinate Coordinate { get; }

        /// <summary>
        /// Координаты станции для GMap
        /// </summary>
        public PointLatLng GMapCoordinate { get; }

        /// <summary>
        /// Цвет станции (для обозначения на карте)
        /// </summary>
        public Color Color { get; }

        /// <summary>
        /// Номер группы (нужне для вывода в бюллетенях)
        /// </summary>
        public int Group { get; }

        #endregion

        #region ctors

        /// <summary>
        /// Инициализирует новый объект класса <see cref="Stations"/>
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="internationalName">Интернациональное имя</param>
        /// <param name="nameInDb">Имя для БД</param>
        /// <param name="rusName">Российское имя</param>
        /// <param name="coordinate">Координаты</param>
        /// <param name="color">Цвет</param>
        /// <param name="nameForRead">Дополнительно имя для чтения</param>
        internal Station(string name, string internationalName, string nameInDb, string rusName, Coordinate coordinate, Color color, string nameForRead = null) :
            this(name, internationalName, nameInDb, rusName, coordinate, color, 1, nameForRead)
        {
        }

        /// <summary>
        /// Инициализирует новый объект класса <see cref="Stations"/>
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="internationalName">Интернациональное имя</param>
        /// <param name="nameInDb">Имя для БД</param>
        /// <param name="rusName">Российское имя</param>
        /// <param name="coordinate">Координаты</param>
        /// <param name="color">Цвет</param>
        /// <param name="group">Номер группы</param>
        /// <param name="nameForRead">Дополнительно имя для чтения</param>
        internal Station(string name, string internationalName, string nameInDb, string rusName, Coordinate coordinate, Color color, int group, string nameForRead = null)
        {
            Name = name;
            InternationalName = internationalName;
            NameInDb = nameInDb;
            RusName = rusName;
            Coordinate = coordinate;
            GMapCoordinate = new PointLatLng(coordinate.Lat, coordinate.Lng);
            Color = color;
            NameForRead = nameForRead;
            Group = group;
        }

        #endregion

        /// <summary>
        /// Возвращает копию класса
        /// </summary>
        /// <returns></returns>
        public object Clone() => 
            MemberwiseClone();

        /// <summary>
        /// Возвращает копию класса
        /// </summary>
        /// <returns></returns>
        public Station Copy() =>
            (Station) MemberwiseClone();
    }
}
