﻿using System.Drawing;

namespace Seismology.Core.Map
{
    public class Marker
    {
        public double Lat { get; set; }
        public double Lng { get; set; }

        public float Radius { get; set; } = 15;
        public Color Color { get; set; } = Color.Black;
        public Color FillColor { get; set; } = Color.FromArgb(100, Color.Red);
        public float Width { get; set; } = 1;

        public Marker(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }

        public Marker(double lat, double lng, Color color, int alpha = 100) : this(lat, lng)
        {
            Color = color;
            FillColor = Color.FromArgb(alpha, Color);
        }

        public Marker(Coordinate coordinate)
        {
            Lat = coordinate.Lat;
            Lng = coordinate.Lng;
        }

        public Marker(Coordinate coordinate, Color color, int alpha = 100) : this(coordinate)
        {
            Color = color;
            FillColor = Color.FromArgb(alpha, Color);
        }
    }
}
