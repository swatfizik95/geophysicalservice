﻿using Seismology.Core.Dto;
using Seismology.Core.IO;
using Seismology.Core.IO.Interfaces;
using Seismology.Core.Mapping;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Seismology.Core.Map
{
    /// <inheritdoc />
    public class StationFileRepository : StationRepositoryBase
    {
        private readonly string _filePath;
        private ISettingsReader<List<StationDto>> _reader;
        private ReadOnlyCollection<Station> _stations;

        /// <inheritdoc />
        public StationFileRepository(string filePath)
        {
            _filePath = filePath;
        }

        public ISettingsReader<List<StationDto>> Reader
        {
            get => _reader ?? (_reader = new JsonSettings<List<StationDto>>(_filePath));
            set => _reader = value;
        }

        /// <inheritdoc />
        protected override ReadOnlyCollection<Station> Stations =>
            _stations ?? (_stations = Reader.Load().Select(s => s.ToStation()).ToList().AsReadOnly());
    }
}