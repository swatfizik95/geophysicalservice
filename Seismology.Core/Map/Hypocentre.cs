﻿namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет гипоцентр с координатами: широта (Lat) и долгота (Lng), и с глубиной (Depth)
    /// </summary>
    public struct Hypocentre
    {
        /// <summary>
        /// Широта
        /// </summary>
        public double Lat { get; set; }
        /// <summary>
        /// Долгота
        /// </summary>
        public double Lng { get; set; }
        /// <summary>
        /// Высота
        /// </summary>
        public double Depth { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="Hypocentre"/> с указанными координатам и глубиной
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        /// <param name="depth">Глубина</param>
        public Hypocentre(double lat, double lng, double depth)
        {
            Lat = lat;
            Lng = lng;
            Depth = depth;
        }
    }
}
