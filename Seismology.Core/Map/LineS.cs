﻿namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет линию (kx + b)
    /// </summary>
    public class LineS
    {
        private readonly Coordinate _point;
        private readonly double _k;
        private readonly double _b;
        private readonly double _kPerpendicular;
        private readonly LineSType _type;

        public LineS(double startLat, double startLng, double endLat, double endLng)
        {
            _point = new Coordinate(startLat, startLng);
            _k = (endLat - startLat) / (endLng - startLng);
            _b = _point.Lat - _k * _point.Lng;
            _kPerpendicular = -1 / _k;
            _type = LineSType.Diagonal;

            if (startLng == endLng || double.IsInfinity(_k))
            {
                _type = LineSType.Vertical;

                _k = -1;
                _b = _point.Lat - _k * _point.Lng;
                _kPerpendicular = -1 / _k;
            }
            else if (startLat == endLat || double.IsInfinity(_kPerpendicular))
            {
                _type = LineSType.Horizontal;

                _k = 0;
                _b = _point.Lat - _k * _point.Lng;
                _kPerpendicular = 0;
            }
        }

        public LineS(Coordinate start, Coordinate end) : this(start.Lat, start.Lng, end.Lat, end.Lng)
        {
        }

        /// <summary>
        /// Находит точку пересечения точки с прямой
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns></returns>
        public Coordinate FindIntersection(double lat, double lng)
        {
            var intersection = new Coordinate();

            switch (_type)
            {
                case LineSType.Diagonal:
                    double bPerpendicular = lat - _kPerpendicular * lng;
                    intersection.Lng = (_b - bPerpendicular) / (_kPerpendicular - _k);
                    intersection.Lat = _k * intersection.Lng + _b;
                    break;
                case LineSType.Vertical:
                    intersection.Lng = _point.Lng;
                    intersection.Lat = lat;
                    break;
                case LineSType.Horizontal:
                    intersection.Lng = lng;
                    intersection.Lat = _point.Lat;
                    break;
            }

            return intersection;
        }

        /// <summary>
        /// <inheritdoc cref="FindIntersection(double,double)"/>
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public Coordinate FindIntersection(Coordinate coordinate)
        {
            return FindIntersection(coordinate.Lat, coordinate.Lng);
        }

        private enum LineSType
        {
            Vertical,
            Horizontal,
            Diagonal
        }
    }
}