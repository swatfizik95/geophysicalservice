﻿using Seismology.Core.Map.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет многоугольник
    /// </summary>
    public class PolygonS : IPlaneFigureS, IAreaFigureS
    {
        private const double Epsilon = 0.01;
        private Coordinate[] _coordinates;
        private double[] _latArr;
        private double[] _lngArr;
        private double[] _radArr;

        #region Конструкторы

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="PolygonS"/> с указанными координатами
        /// </summary>
        /// <param name="coordinates">Координаты</param>
        public PolygonS(IList<Coordinate> coordinates)
        {
            if (coordinates == null)
            {
                throw new NullReferenceException("Массив элементов оказался null");
            }

            if (coordinates.Count < 3)
            {
                throw new NullReferenceException("Количество элементов меньше 2х!");
            }

            _coordinates = coordinates.ToArray();
            _latArr = new double[_coordinates.Length];
            _lngArr = new double[_coordinates.Length];
            _radArr = new double[_coordinates.Length];

            for (int index = 0, prevIndex = _coordinates.Length - 1; index < _coordinates.Length; index++, prevIndex = index - 1)
            {
                _latArr[index] = _coordinates[index].Lat - _coordinates[prevIndex].Lat;
                _lngArr[index] = _coordinates[prevIndex].Lng - _coordinates[index].Lng;
                _radArr[index] = _coordinates[index].Lng * _coordinates[prevIndex].Lat -
                               _coordinates[prevIndex].Lng * _coordinates[index].Lat;
            }
        }

        #endregion

        #region Методы

        ///<inheritdoc/>
        public bool IsInside(double lat, double lng)
        {
            bool result = false;
            for (int index = 0, indexPrev = _coordinates.Length - 1; index < _coordinates.Length; index++, indexPrev = index - 1)
            {

                if (lat >= _coordinates[index].Lat && lat <= _coordinates[indexPrev].Lat ||
                    lat <= _coordinates[index].Lat && lat >= _coordinates[indexPrev].Lat &&
                    lng >= _coordinates[index].Lng && lng <= _coordinates[indexPrev].Lng ||
                    lng <= _coordinates[index].Lng && lng >= _coordinates[indexPrev].Lng)
                {
                    if (_coordinates[index].Lat == _coordinates[indexPrev].Lat)
                    {
                        if (lat == _coordinates[index].Lat &&
                            (lng <= _coordinates[index].Lng && lng >= _coordinates[indexPrev].Lng ||
                             lng >= _coordinates[index].Lng && lng <= _coordinates[indexPrev].Lng))
                        {
                            return true;
                        }
                    }
                    if (_coordinates[index].Lng == _coordinates[indexPrev].Lng)
                    {
                        if (lng == _coordinates[index].Lng &&
                            (lat <= _coordinates[index].Lat && lat >= _coordinates[indexPrev].Lat ||
                             lat >= _coordinates[index].Lat && lat <= _coordinates[indexPrev].Lat))
                        {
                            return true;
                        }
                    }

                    if (Math.Abs((lng - _coordinates[index].Lng) / (_coordinates[indexPrev].Lng - _coordinates[index].Lng) -
                                 (lat - _coordinates[index].Lat) / (_coordinates[indexPrev].Lat - _coordinates[index].Lat)) < Epsilon)
                    {
                        return true;
                    }
                }

                if (_coordinates[index].Lat < lat && _coordinates[indexPrev].Lat >= lat ||
                    _coordinates[indexPrev].Lat < lat && _coordinates[index].Lat >= lat)
                {
                    if (_coordinates[index].Lng + (lat - _coordinates[index].Lat) /
                        (_coordinates[indexPrev].Lat - _coordinates[index].Lat) *
                        (_coordinates[indexPrev].Lng - _coordinates[index].Lng) <= lng)
                    {
                        result = !result;
                    }
                }
            }

            return result;
        }

        ///<inheritdoc/>
        public bool IsInside(Coordinate coordinate)
        {
            return IsInside(coordinate.Lat, coordinate.Lng);
        }

        public double CalculateArea()
        {
            //double area = 0.0;

            //for (int index = 1; index < _coordinates.Length - 1; index++)
            //{
            //    area += CalculateTriangleArea(_coordinates[0], _coordinates[index], _coordinates[index + 1]);
            //}

            //return area;

            Coordinate[] points = new Coordinate[_coordinates.Length];
            points[0] = new Coordinate(0, 0);

            for (int index = 1; index < _coordinates.Length; index++)
            {
                points[index] = new Coordinate();
                points[index].Lat = _coordinates[index].LatKm * (_coordinates[index].Lat - _coordinates[0].Lat);
                points[index].Lng = _coordinates[index].LngKm * (_coordinates[index].Lng - _coordinates[0].Lng);
            }

            double sum1 = 0.0;
            double sum2 = 0.0;

            for (int index = 0, indexNext = 1;
                index < _coordinates.Length;
                index++, indexNext = index + 1)
            {
                if (index == _coordinates.Length - 1)
                {
                    indexNext = 0;
                }
                sum1 += points[index].Lng * points[indexNext].Lat;
                sum2 += points[index].Lat * points[indexNext].Lng;
            }

            return Math.Abs(sum1 - sum2) / 2.0;
        }

        #endregion

        private double CalculateTriangleArea(Coordinate c1, Coordinate c2, Coordinate c3)
        {
            double a = Math.Pow(c1.DistanceTo(c2), 2);
            double b = Math.Pow(c2.DistanceTo(c3), 2);
            double c = Math.Pow(c3.DistanceTo(c1), 2);
            return 0.25 * Math.Sqrt(4 * a * b - Math.Pow(a + b - c, 2));
            //double a = c1.DistanceTo(c2);
            //double b = c2.DistanceTo(c3);
            //double c = c3.DistanceTo(c1);
            //double p = (a + b + c) / 2.0;
            //return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
    }
}