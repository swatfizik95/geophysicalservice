﻿namespace Seismology.Core.Map.Interfaces
{
    /// <summary>
    /// Предоставляет базовую оболочку для фигур
    /// </summary>
    public interface IPlaneFigureS
    {
        /// <summary>
        /// Проверяет находится ли координата внутри фигуры
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        /// <returns>true - если находится внутри, false - если не находится внутри</returns>
        bool IsInside(double lat, double lng);

        /// <summary>
        /// Проверяет находится ли координата внутри фигуры
        /// </summary>
        /// <param name="coordinate">Координаты</param>
        /// <returns>true - если находится внутри, false - если не находится внутри</returns>
        bool IsInside(Coordinate coordinate);
    }
}