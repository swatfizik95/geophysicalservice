﻿using System.Collections.ObjectModel;

namespace Seismology.Core.Map.Interfaces
{
    /// <summary>
    /// Репозиторий для станций.
    /// </summary>
    public interface IStationRepository
    {
        /// <summary>
        /// Возвращает первую станцию совпавшую по имени. Если таковой нет, вовзращает null
        /// </summary>
        /// <param name="name">Имя станции</param>
        /// <returns>Станция</returns>
        Station FindStationByName(string name);

        /// <summary>
        /// Возвращается все станции.
        /// </summary>
        /// <returns>Станции</returns>
        ReadOnlyCollection<Station> GetAll();
    }
}