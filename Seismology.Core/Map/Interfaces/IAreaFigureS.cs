﻿namespace Seismology.Core.Map.Interfaces
{
    /// <summary>
    /// Представляет площадь
    /// </summary>
    public interface IAreaFigureS
    {
        /// <summary>
        /// Вычисляет площадь фигуры
        /// </summary>
        /// <returns></returns>
        double CalculateArea();
    }
}