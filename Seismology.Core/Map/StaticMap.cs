﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

// ReSharper disable InconsistentNaming

namespace Seismology.Core.Map
{
    /// <summary>
    /// Класс позволяющий работать с изображением как с статичной картой
    /// </summary>
    public class StaticMap
    {
        // Для рисования на изображении
        protected readonly Graphics _graph;
        // Чистое изображение карты (нужно для чистки)
        protected readonly Image _pureImg;

        // 
        protected Image _img;
        /// <summary>
        /// Изображение карты
        /// </summary>
        public Image Image => _img;

        protected float GetLatPx(double lat)
        {
            return LatEndPx - ((float)lat - LatStart) * LatPx;
        }

        protected float GetLngPx(double lng)
        {
            return LngStartPx + ((float)lng - LngStart) * LngPx;
        }


        #region Широта

        /// <summary>
        /// С какого значения широты (в пикселях) начинается отчет
        /// </summary>
        public int LatStartPx { get; }

        /// <summary>
        /// Где заканчивается градусная мера широты в пикселях
        /// </summary>
        public int LatEndPx { get; }

        /// <summary>
        /// В каком значении широты (в пикселях) заканчивается отчет
        /// </summary>
        public int LatLengthPx { get; }

        /// <summary>
        /// С какого значения широты (в градусах) начинается отчет
        /// </summary>
        public float LatStart { get; }

        /// <summary>
        /// В каком значении широты (в градусах) заканчивается отчет
        /// </summary>
        public float LatEnd { get; }

        /// <summary>
        /// 1 градус широты в пикселях
        /// </summary>
        public float LatPx { get; }

        /// <summary>
        /// 1 градус широты в км
        /// </summary>
        public float LatKm { get; }

        #endregion

        #region Долгота

        /// <summary>
        /// С какого значения долготы (в пикселях) начинается отчет
        /// </summary>
        public int LngStartPx { get; }

        /// <summary>
        /// Где заканчивается градусная мера долготы в пикселях
        /// </summary>
        public int LngEndPx { get; }

        /// <summary>
        /// В каком значении долготы (в пикселях) заканчивается отчет
        /// </summary>
        public int LngLengthPx { get; }

        /// <summary>
        /// С какого значения долготы (в градусах) начинается отчет
        /// </summary>
        public float LngStart { get; }

        /// <summary>
        /// В каком значении долготы (в градусах) заканчивается отчет
        /// </summary>
        public float LngEnd { get; }

        /// <summary>
        /// 1 градус долготы в пикселях
        /// </summary>
        public float LngPx { get; }

        /// <summary>
        /// 1 градус долготы в км
        /// </summary>
        public float LngKm { get; }

        #endregion

        /// <summary>
        /// Инициализирует объект класса <see cref="StaticMap"/>
        /// </summary>
        /// <param name="map">Изображение</param>
        /// <param name="latStartPx">Начало широты в пикселях</param>
        /// <param name="latEndPx">Конец широты в пикселях</param>
        /// <param name="latStart">Начало широты в градусах</param>
        /// <param name="latEnd">Конец широты в градусах</param>
        /// <param name="lngStartPx">Начало долготы в пикселях</param>
        /// <param name="lngEndPx">Конец долготы в пикселях</param>
        /// <param name="lngStart">Начало долготы в градусах</param>
        /// <param name="lngEnd">Конец долготы в градусах</param>
        /// <param name="latKm">1 градус широты в км (111.1 по умолчанию)</param>
        /// <param name="lngKm">1 градус долготы в км (80.5 по умолчанию для Дагестана)</param>
        public StaticMap(Image map,
            int latStartPx, int latEndPx,
            float latStart, float latEnd,
            int lngStartPx, int lngEndPx,
            float lngStart, float lngEnd,
            float latKm = 111.1f, float lngKm = 80.5f)
        {
            _pureImg = (Image)map.Clone();
            _img = (Image)map.Clone();
            _graph = Graphics.FromImage(_img);

            LatStartPx = latStartPx;
            LatEndPx = latEndPx;
            LatLengthPx = latEndPx - latStartPx;
            LatStart = latStart;
            LatEnd = latEnd;
            LatPx = LatLengthPx / (latEnd - latStart);
            LatKm = latKm;

            LngStartPx = lngStartPx;
            LngEndPx = lngEndPx;
            LngLengthPx = lngEndPx - lngStartPx;
            LngStart = lngStart;
            LngEnd = lngEnd;
            LngPx = LngLengthPx / (lngEnd - lngStart);
            LngKm = lngKm;
        }

        /// <summary>
        /// Добавляет маркеры на карту
        /// </summary>
        /// <param name="markers">Маркеры</param>
        public void AddMarkers(IEnumerable<Marker> markers)
        {
            foreach (var marker in markers)
            {
                AddMarker(marker);
            }
        }

        /// <summary>
        /// Добавляет маркер на карту
        /// </summary>
        /// <param name="marker">Маркер</param>
        public void AddMarker(Marker marker)
        {
            float latPx = GetLatPx(marker.Lat);
            float lngPx = GetLngPx(marker.Lng);
            float radius = marker.Radius;
            float radius2 = marker.Radius * 2;

            _graph.FillEllipse(new SolidBrush(marker.FillColor),
                lngPx - radius, latPx - radius,
                radius2, radius2);

            _graph.DrawEllipse(new Pen(marker.Color),
                lngPx - radius, latPx - radius,
                radius2, radius2);
        }

        /// <summary>
        /// Сохраняет изображение карты (с маркерами), автоматически определяет расширение
        /// </summary>
        /// <param name="path">Путь, куда нужно сохранить изображение</param>
        public void SaveMap(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return;

            string extentsion = Path.GetExtension(path).Replace(".", String.Empty);
            SaveMap(path, extentsion);

        }

        /// <summary>
        /// Сохраняет изображение карты (с маркерами), нужно указать расширение
        /// </summary>
        /// <param name="path">Путь, куда нужно сохранить изображение</param>
        /// <param name="type">Тип расширения, в котором нужно сохранить (png, jpg, gif, bmp)</param>
        public void SaveMap(string path, string type)
        {
            string clearPath = Path.ChangeExtension(path, string.Empty);
            string fullPath = $"{clearPath}{type}";
            if (!string.IsNullOrWhiteSpace(clearPath))
            {
                if (type == "png")
                {
                    _img.Save(fullPath, ImageFormat.Png);
                    return;
                }
                if (type == "jpg")
                {
                    _img.Save(fullPath, ImageFormat.Jpeg);
                    return;
                }
                if (type == "gif")
                {
                    _img.Save(fullPath, ImageFormat.Gif);
                    return;
                }
                if (type == "gif")
                {
                    _img.Save(fullPath, ImageFormat.Gif);
                    return;
                }
                _img.Save($"{clearPath}png", ImageFormat.Png);
            }
        }

        /// <summary>
        /// Очищает карту от маркеров
        /// </summary>
        public void ClearMap()
        {
            _graph.Clear(Color.White);
            _graph.DrawImageUnscaled(_pureImg, new Point(0, 0));
        }
    }
}
