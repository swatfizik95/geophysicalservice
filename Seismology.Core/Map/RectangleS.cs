﻿using Seismology.Core.Map.Interfaces;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Представляет прямоугольник с координатам его
    /// </summary>
    public class RectangleS : IPlaneFigureS, IAreaFigureS
    {
        ///// <summary>
        ///// Начало широты
        ///// </summary>
        //public double LatBegin { get; set; }
        ///// <summary>
        ///// Начало долготы
        ///// </summary>
        //public double LngBegin { get; set; }
        ///// <summary>
        ///// Конец широты
        ///// </summary>
        //public double LatEnd { get; set; }
        ///// <summary>
        ///// Конец долготы
        ///// </summary>
        //public double LngEnd { get; set; }
        /// <summary>
        /// Начало координат
        /// </summary>
        public Coordinate Begin { get; set; }
        /// <summary>
        /// Конец координат
        /// </summary>
        public Coordinate End { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="RectangleS"/> с значениями по умолчанию
        /// </summary>
        public RectangleS()
        {
            Begin = new Coordinate();
            End = new Coordinate();
            //LatBegin = 0;
            //LngBegin = 0;
            //LatEnd = 0;
            //LngEnd = 0;
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="RectangleS"/> с указанными значениями
        /// </summary>
        /// <param name="latBegin">Начало широты</param>
        /// <param name="latEnd">Конец широты</param>
        /// <param name="lngBegin">Начало долготы</param>
        /// <param name="lngEnd">Конец долготы</param>
        public RectangleS(double latBegin, double lngBegin, double latEnd, double lngEnd)
        {
            Begin = new Coordinate(latBegin, lngBegin);
            End = new Coordinate(latEnd, lngEnd);
            //LatBegin = latBegin;
            //LngBegin = lngBegin;
            //LatEnd = latEnd;
            //LngEnd = lngEnd;
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="RectangleS"/> с указанными значениями
        /// </summary>
        /// <param name="begin">Начало координат</param>
        /// <param name="end">Конец коордитнат</param>
        public RectangleS(Coordinate begin, Coordinate end)
        {
            Begin = new Coordinate(begin);
            End = new Coordinate(end);
        }

        /// <inheritdoc/>
        public bool IsInside(double lat, double lng)
        {
            return lat >= Begin.Lat &&
                   lat <= End.Lat &&
                   lng >= Begin.Lng &&
                   lng <= End.Lng;
            //return lat >= LatBegin &&
            //       lat <= LatEnd &&
            //       lng >= LngBegin &&
            //       lng <= LngEnd;
        }

        /// <inheritdoc/>
        public bool IsInside(Coordinate coordinate)
        {
            return IsInside(coordinate.Lat, coordinate.Lng);
        }

        /// <inheritdoc/>
        public double CalculateArea()
        {
            return Begin.DistanceTo(Begin.Lat, End.Lng) * Begin.DistanceTo(End.Lat, Begin.Lng);
        }
    }
}
