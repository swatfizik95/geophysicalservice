﻿using System.Drawing;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Класс позволяющий работать с изображением как с статичной картой для землетрясений
    /// </summary>
    public class StaticMapS : StaticMap
    {
        /// <summary>
        /// Инициализирует объект класса <see cref="StaticMapS"/>
        /// </summary>
        /// <param name="map">Изображение</param>
        /// <param name="latStartPx">Начало широты в пикселях</param>
        /// <param name="latEndPx">Конец широты в пикселях</param>
        /// <param name="latStart">Начало широты в градусах</param>
        /// <param name="latEnd">Конец широты в градусах</param>
        /// <param name="lngStartPx">Начало долготы в пикселях</param>
        /// <param name="lngEndPx">Конец долготы в пикселях</param>
        /// <param name="lngStart">Начало долготы в градусах</param>
        /// <param name="lngEnd">Конец долготы в градусах</param>
        /// <param name="latKm">1 градус широты в км (111.1 по умолчанию)</param>
        /// <param name="lngKm">1 градус долготы в км (80.5 по умолчанию для Дагестана)</param>
        public StaticMapS(Image map,
            int latStartPx, int latEndPx, float latStart, float latEnd,
            int lngStartPx, int lngEndPx, float lngStart, float lngEnd,
            float latKm = 111.1f, float lngKm = 80.5f) : base(map,
            latStartPx, latEndPx, latStart, latEnd,
            lngStartPx, lngEndPx, lngStart, lngEnd,
            latKm, lngKm)
        {
        }

        public void AddRadius(Marker marker)
        {
            // Находим 
            float radiusLng = marker.Radius / LngKm * LngPx;
            float radiusLat = marker.Radius / LatKm * LatPx;
            // Находим координаты на изображении
            float latPx = GetLatPx(marker.Lat);
            float lngPx = GetLngPx(marker.Lng);
            // Рисуем радиусы
            _graph.DrawEllipse(
                new Pen(marker.Color, marker.Width),
                lngPx - radiusLng, latPx - radiusLat,
                radiusLng * 2, radiusLat * 2);
        }
        public void AddRadius(double lat, double lng, double radius, Color color, float width)
        {
            AddRadius(new Marker(lat, lng, color)
            {
                FillColor = Color.Transparent,
                Radius = (float)radius,
                Width = width
            });
        }
        public void AddRadius(Coordinate coordinate, double radius, Color color, float width)
        {
            AddRadius(new Marker(coordinate, color)
            {
                FillColor = Color.Transparent,
                Radius = (float)radius,
                Width = width
            });
        }

        public void AddEarhquake(Earthquake earthquake)
        {
            float lat = GetLatPx(earthquake.Lat);
            float lng = GetLngPx(earthquake.Lng);
            _graph.FillPolygon(new SolidBrush(Color.Red), new[]
            {
                new PointF(lng, lat),
                new PointF(lng - 15, lat - 25),
                new PointF(lng + 15, lat - 25),
            });
            _graph.FillPie(new SolidBrush(Color.Red), lng - 15, lat - 40, 30, 30, 180, 180);
        }
    }
}
