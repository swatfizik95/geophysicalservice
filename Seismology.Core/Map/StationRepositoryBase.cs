﻿using Seismology.Core.Map.Interfaces;
using System.Collections.ObjectModel;
using System.Linq;

namespace Seismology.Core.Map
{
    /// <inheritdoc />
    public abstract class StationRepositoryBase : IStationRepository
    {
        // Имена для станций идут следующим образом
        // 1) - Та, что будет в Бюллетене (по идее, рег названия)
        // 2) - Та, что будет в Бюллетене ИФЗ (по идее, международные названия)
        // 3) - Та, что будет в БД ()
        /// <summary>
        /// Станции Дагестана
        /// </summary>
        protected abstract ReadOnlyCollection<Station> Stations { get; }


        /// <inheritdoc />
        public virtual Station FindStationByName(string name) =>
            string.IsNullOrWhiteSpace(name)
                ? null
                : Stations.FirstOrDefault(s => s.Name == name || s.InternationalName == name || s.NameInDb == name || s.NameForRead == name);

        /// <inheritdoc />
        public virtual ReadOnlyCollection<Station> GetAll() => Stations;
    }
}