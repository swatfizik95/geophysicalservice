﻿using Seismology.Core.Map.Interfaces;
using System.Collections.ObjectModel;

namespace Seismology.Core.Map
{
    /// <summary>
    /// Хранилище станций.
    /// </summary>
    public static class StationStorage
    {
        /// <summary>
        /// Репозиторий станций.
        /// </summary>
        internal static IStationRepository StationRepository { get; set; }

        /// <summary>
        /// Возвращает первую станцию совпавшую по имени. Если таковой нет, вовзращает null
        /// </summary>
        /// <param name="name">Имя станции</param>
        /// <returns>Станция</returns>
        public static Station FindStationByName(string name) => StationRepository.FindStationByName(name);

        /// <summary>
        /// Возвращается все станции.
        /// </summary>
        /// <returns>Станции</returns>
        public static ReadOnlyCollection<Station> GetAll() => StationRepository.GetAll();
    }
}