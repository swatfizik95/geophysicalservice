﻿using System.Collections.Generic;

namespace Seismology.Core
{
    public class EarthquakeRepeatabilityPeriod
    {
        public Period Period { get; set; }
        public List<EarthquakeRepeatability> Repeatabilities { get; set; } = new List<EarthquakeRepeatability>();
    }
}