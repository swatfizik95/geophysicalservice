﻿using Seismology.Core.Map.Interfaces;
using Seismology.Core.MathS;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Seismology.Core
{
    public partial class FormChartRepeatability : Form
    {
        private IList<Earthquake> _earthquakes;
        private IAreaFigureS _figure;
        private CalculatorS _calculator = new CalculatorS();

        public FormChartRepeatability(IList<Earthquake> earthquakes, IAreaFigureS figure)
        {
            InitializeComponent();

            _earthquakes = earthquakes;
            _figure = figure;
        }

        private void FormChartRepeatability_Shown(object sender, System.EventArgs e)
        {
            Draw(Calcualte());
        }

        private void ButtonApply_Click(object sender, System.EventArgs e)
        {
            Draw(Calcualte());
        }

        private EarthquakeRepeatability Calcualte()
        {
            int kBegin = (int)numericUpDownKBegin.Value;
            int kBeginOls = (int)numericUpDownKBeginOls.Value;
            return _calculator.FindChartRepeatability(_earthquakes, _figure, kBegin, kBeginOls);
        }

        private void Draw(EarthquakeRepeatability repeatability)
        {
            if (repeatability == null)
            {
                return;
            }

            foreach (var series in chartMain.Series)
            {
                series.Points.Clear();
            }

            for (int index = 0; index < repeatability.ClassRepeatability.Classes.Length; index++)
            {
                chartMain.Series[0].Points.AddXY(
                    repeatability.ClassRepeatability.Classes[index],
                    repeatability.ClassRepeatability.Repeatabilities[index]);
            }

            for (int index = 0; index < repeatability.ClassRepeatabilityCalculate.Classes.Length; index++)
            {
                chartMain.Series[1].Points.AddXY(
                    repeatability.ClassRepeatabilityCalculate.Classes[index],
                    repeatability.Calculate(repeatability.ClassRepeatabilityCalculate.Classes[index]));
            }

            chartMain.Titles[0].Text = $"{repeatability.K:F3} x + {repeatability.B:F3}";
        }

        /// <summary>
        /// Сохраняет изображение
        /// </summary>
        /// <param name="filePath">Путь</param>
        /// <param name="imageFormat">Формат изображения</param>
        public void SaveImage(string filePath, ImageFormat imageFormat)
        {
            chartMain.SaveImage(filePath, imageFormat);
        }
    }
}
