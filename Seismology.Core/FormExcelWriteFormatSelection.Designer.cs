﻿namespace Seismology.Core
{
    partial class FormExcelWriteFormatSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.labelFormat = new System.Windows.Forms.Label();
            this.labelFormatName = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.textBoxWorksheetName = new System.Windows.Forms.TextBox();
            this.textBoxDelimeter = new System.Windows.Forms.TextBox();
            this.labelWorksheetName = new System.Windows.Forms.Label();
            this.labelDelimeter = new System.Windows.Forms.Label();
            this.panelFormat = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // buttonAdd
            // 
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAdd.Location = new System.Drawing.Point(13, 14);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(35, 35);
            this.buttonAdd.TabIndex = 0;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRemove.Location = new System.Drawing.Point(56, 14);
            this.buttonRemove.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(35, 35);
            this.buttonRemove.TabIndex = 1;
            this.buttonRemove.Text = "-";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.ButtonRemove_Click);
            // 
            // labelFormat
            // 
            this.labelFormat.AutoSize = true;
            this.labelFormat.Location = new System.Drawing.Point(12, 65);
            this.labelFormat.Name = "labelFormat";
            this.labelFormat.Size = new System.Drawing.Size(71, 20);
            this.labelFormat.TabIndex = 3;
            this.labelFormat.Text = "Формат";
            // 
            // labelFormatName
            // 
            this.labelFormatName.AutoSize = true;
            this.labelFormatName.Location = new System.Drawing.Point(12, 101);
            this.labelFormatName.Name = "labelFormatName";
            this.labelFormatName.Size = new System.Drawing.Size(115, 20);
            this.labelFormatName.TabIndex = 4;
            this.labelFormatName.Text = "Имя формата";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(773, 169);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(99, 30);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(668, 169);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(99, 30);
            this.buttonOk.TabIndex = 6;
            this.buttonOk.Text = "Выбрать";
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // textBoxWorksheetName
            // 
            this.textBoxWorksheetName.Location = new System.Drawing.Point(131, 162);
            this.textBoxWorksheetName.Name = "textBoxWorksheetName";
            this.textBoxWorksheetName.Size = new System.Drawing.Size(134, 26);
            this.textBoxWorksheetName.TabIndex = 7;
            this.textBoxWorksheetName.Text = "Новый Лист";
            // 
            // textBoxDelimeter
            // 
            this.textBoxDelimeter.Location = new System.Drawing.Point(131, 162);
            this.textBoxDelimeter.Name = "textBoxDelimeter";
            this.textBoxDelimeter.Size = new System.Drawing.Size(134, 26);
            this.textBoxDelimeter.TabIndex = 8;
            this.textBoxDelimeter.Text = ";";
            this.textBoxDelimeter.Visible = false;
            // 
            // labelWorksheetName
            // 
            this.labelWorksheetName.AutoSize = true;
            this.labelWorksheetName.Location = new System.Drawing.Point(12, 165);
            this.labelWorksheetName.Name = "labelWorksheetName";
            this.labelWorksheetName.Size = new System.Drawing.Size(89, 20);
            this.labelWorksheetName.TabIndex = 9;
            this.labelWorksheetName.Text = "Имя листа";
            // 
            // labelDelimeter
            // 
            this.labelDelimeter.AutoSize = true;
            this.labelDelimeter.Location = new System.Drawing.Point(12, 165);
            this.labelDelimeter.Name = "labelDelimeter";
            this.labelDelimeter.Size = new System.Drawing.Size(112, 20);
            this.labelDelimeter.TabIndex = 10;
            this.labelDelimeter.Text = "Разделитель";
            this.labelDelimeter.Visible = false;
            // 
            // panelFormat
            // 
            this.panelFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFormat.AutoScroll = true;
            this.panelFormat.Location = new System.Drawing.Point(131, 65);
            this.panelFormat.Name = "panelFormat";
            this.panelFormat.Size = new System.Drawing.Size(739, 87);
            this.panelFormat.TabIndex = 11;
            // 
            // SelectWriteExcelFormatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 211);
            this.Controls.Add(this.panelFormat);
            this.Controls.Add(this.labelDelimeter);
            this.Controls.Add(this.labelWorksheetName);
            this.Controls.Add(this.textBoxDelimeter);
            this.Controls.Add(this.textBoxWorksheetName);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelFormatName);
            this.Controls.Add(this.labelFormat);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonAdd);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(900, 250);
            this.Name = "SelectWriteExcelFormatForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выберите формат Excel файла";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Label labelFormat;
        private System.Windows.Forms.Label labelFormatName;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.TextBox textBoxWorksheetName;
        private System.Windows.Forms.TextBox textBoxDelimeter;
        private System.Windows.Forms.Label labelWorksheetName;
        private System.Windows.Forms.Label labelDelimeter;
        private System.Windows.Forms.Panel panelFormat;
    }
}