﻿using Seismology.Core.MathS;
using System;
using System.Linq;

namespace Seismology.Core
{
    public class EarthquakeRepeatability
    {
        public ClassRepeatability ClassRepeatability { get; }
        public ClassRepeatability ClassRepeatabilityCalculate { get; }
        public double K { get; }
        public double B { get; }

        public DateTime Date { get; set; }
        public double A { get; set; }

        public EarthquakeRepeatability(
            ClassRepeatability classRepeatability,
            ClassRepeatability classRepeatabilityCalculate)
        {
            ClassRepeatability = classRepeatability;
            ClassRepeatabilityCalculate = classRepeatabilityCalculate;

            var calc = new CalculatorS();
            double[] squares = calc.OrdinaryLeastSquares(
                classRepeatabilityCalculate.Classes.Select(c => (double)c).ToArray(),
                classRepeatabilityCalculate.Repeatabilities);

            K = squares[0];
            B = squares[1];
        }

        public double Calculate(double x)
        {
            return K * x + B;
        }
    }
}