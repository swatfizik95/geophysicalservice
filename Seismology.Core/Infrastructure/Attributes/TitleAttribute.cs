﻿using System;

namespace Seismology.Core.Infrastructure.Attributes
{
    /// <summary>
    /// Аттрибут для наименования
    /// </summary>
    public class TitleAttribute : Attribute
    {
        private readonly string _name;

        public TitleAttribute(string name)
        {
            _name = name;
        }

        /// <summary>
        /// Получает имя
        /// </summary>
        /// <returns></returns>
        public string GetName() => _name;
    }
}