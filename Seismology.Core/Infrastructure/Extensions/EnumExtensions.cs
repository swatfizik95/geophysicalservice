﻿using Seismology.Core.Infrastructure.Attributes;
using System;
using System.Reflection;

namespace Seismology.Core.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Получает название enum <see cref="TitleAttribute"/>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetName(this Enum value)
        {
            if (value == null) return string.Empty;

            return value.GetType().GetField(value.ToString())?.GetCustomAttribute<TitleAttribute>(false)?.GetName() ?? string.Empty;
        }
    }
}