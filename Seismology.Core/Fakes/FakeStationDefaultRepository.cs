﻿using Seismology.Core.Map;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;

namespace Seismology.Core.Fakes
{
    /// <summary>
    /// Фейковый репозиторий.
    /// Используется только для DEBUG и тестов.
    /// </summary>
    public class FakeStationDefaultRepository : StationRepositoryBase
    {
        protected override ReadOnlyCollection<Station> Stations => new List<Station>
            {
                new Station("AKT", "AHT", "AKT", @"Ахты", new Coordinate(41.4792, 47.715),
                    Color.FromArgb(219, 112, 147)),
                new Station("ARKR", "ARK", "ARKR", @"Аракани", new Coordinate(42.6021, 46.9942),
                    Color.FromArgb(250, 128, 114)),
                new Station("BTLR", "BTL", "BTLR", @"Ботлих", new Coordinate(42.6653, 46.219),
                    Color.FromArgb(255, 0, 0)),
                new Station("BUJR", "BUJ", "BUJR", @"Буйнакск", new Coordinate(42.8090, 47.1301),
                    Color.FromArgb(128, 0, 0)),
                new Station("DBC", "DBC", "DBK", @"Дубки", new Coordinate(43.0216, 46.841),
                    Color.FromArgb(255, 167, 0)),
                new Station("DLMR", "DLM", "DLMR", @"Дылым", new Coordinate(43.0730, 46.6187),
                    Color.FromArgb(255, 107, 80)),
                new Station("DRN", "DRN", "DRN", @"Дербент", new Coordinate(41.9976, 48.3322),
                    Color.FromArgb(230, 115, 0)),
                new Station("GNBR", "GNB", "GNBR", @"Гуниб", new Coordinate(42.3893, 46.9638),
                    Color.FromArgb(75, 0, 130)),

                new Station("GROC", "GRO", "GROC", @"Грозный", new Coordinate(43.203, 45.796),
                    Color.FromArgb(144, 238, 144)),
                new Station("KANR", "KRM", "KANR", @"Караман", new Coordinate(43.196, 47.489),
                    Color.FromArgb(0, 255, 127)),
                new Station("KRNR", "KRN", "KRNR", @"Каранай", new Coordinate(42.8267, 46.9053),
                    Color.FromArgb(50, 205, 50)),
                new Station("KSMR", "KSM", "KSMR", @"Касумкент", new Coordinate(41.6023, 48.1246),
                    Color.FromArgb(32, 178, 170)),
                new Station("KMKR", "KUM", "KMKR", @"Кумух", new Coordinate(42.1287, 47.0977),
                    Color.FromArgb(107, 142, 35)),

                new Station("MAK", "MAK", "MAK", @"Махачкала", new Coordinate(42.948, 47.5),
                    Color.FromArgb(135, 206, 250)),
                new Station("SGKR", "SGK", "SGKR", @"Сергокала", new Coordinate(42.4576, 47.655),
                    Color.FromArgb(0, 191, 255)),
                new Station("TRKR", "TRK", "TRKR", @"Терская", new Coordinate(43.723, 44.732),
                    Color.FromArgb(30, 144, 255)),
                new Station("UNCR", "UNC", "UNCR", @"Унцукуль", new Coordinate(42.7155, 46.7929),
                    Color.FromArgb(0, 0, 255)),
                new Station("URKR", "URK", "URKR", @"Уркарах", new Coordinate(42.1645, 47.6316),
                    Color.FromArgb(0, 0, 139)),

                new Station("DVE", "VDN", "DVE", @"Ведено", new Coordinate(42.957, 46.126),
                    Color.FromArgb(123, 104, 238)),

                new Station("XNZR", "XNZ", "HNZR", @"Хунзах", new Coordinate(42.5451, 46.7053),
                    Color.FromArgb(138, 43, 226)),

                new Station("TLTR", "TLTR", "TLTR", @"Тлярата", new Coordinate(42.10586, 46.35440),
                    Color.FromArgb(100, 84, 82)),
            }.AsReadOnly();
    }
}