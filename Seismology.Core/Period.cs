﻿using System;
using System.Collections.Generic;
using System.Windows.Forms.DataVisualization.Charting;

namespace Seismology.Core
{
    public class Period
    {
        private DateTime[] _dateTimes;
        private DateTimeIntervalType _periodType;
        private string _periodTypeString;
        public DateTimeIntervalType PeriodType => _periodType;
        public string PeriodTypeString => _periodTypeString;
        public int Length => _dateTimes.Length;

        public Period(DateTime dateBegin, DateTime dateEnd, DateTimeIntervalType periodType, int periodValue)
        {
            var dateTimes = new List<DateTime>();

            Func<DateTime, int, DateTime> add;

            switch (periodType)
            {
                case DateTimeIntervalType.Years:
                    add = (dateTime, value) => dateTime.AddYears(value);
                    _periodTypeString = "yyyy";
                    break;
                case DateTimeIntervalType.Months:
                    add = (dateTime, value) => dateTime.AddMonths(value);
                    _periodTypeString = "MM.yyyy";
                    break;
                case DateTimeIntervalType.Days:
                    add = (dateTime, value) => dateTime.AddDays(value);
                    _periodTypeString = "dd.MM.yyyy";
                    break;
                case DateTimeIntervalType.Hours:
                    add = (dateTime, value) => dateTime.AddHours(value);
                    _periodTypeString = "HH dd.MM.yyyy";
                    break;
                case DateTimeIntervalType.Minutes:
                    add = (dateTime, value) => dateTime.AddMinutes(value);
                    _periodTypeString = "HH:mm dd.MM.yyyy";
                    break;
                case DateTimeIntervalType.Seconds:
                    add = (dateTime, value) => dateTime.AddSeconds(value);
                    _periodTypeString = "HH:mm:ss dd.MM.yyyy";
                    break;
                case DateTimeIntervalType.Milliseconds:
                    add = (dateTime, value) => dateTime.AddMilliseconds(value);
                    _periodTypeString = "HH:mm:ss.fff dd.MM.yyyy";
                    break;
                default:
                    throw new Exception("Неверный тип периода");
            }

            var dateCurrent = dateBegin;
            dateTimes.Add(dateCurrent);

            while (dateCurrent < dateEnd)
            {
                dateCurrent = add(dateCurrent, periodValue);
                dateTimes.Add(dateCurrent);
            }

            _dateTimes = dateTimes.ToArray();
            _periodType = periodType;
        }

        public DateTime this[int index] => _dateTimes[index];
    }
}