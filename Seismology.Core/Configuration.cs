﻿#if DEBUG
// #define MYTEST
using Seismology.Core.Fakes;
#endif
using Seismology.Core.Map;

namespace Seismology.Core
{
    public static class Configuration
    {
        public static void UseDefaultParams(string stationFilePath)
        {
#if (DEBUG && MYTEST)
            UseFakeParams();
#else
            StationStorage.StationRepository = new StationFileRepository(stationFilePath);
#endif
        }

        public static void UseRusHydroParams(string stationFilePath)
        {
#if (DEBUG && MYTEST)
            UseFakeParams();
#else
            StationStorage.StationRepository = new StationFileRepository(stationFilePath);
#endif
        }

#if DEBUG
        internal static void UseFakeParams()
        {
            StationStorage.StationRepository = new FakeStationDefaultRepository();
        }
#endif
    }
}