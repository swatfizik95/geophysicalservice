﻿using Seismology.Core.Map;

namespace Seismology.Core
{
    /// <summary>
    /// Представляет скалярное значение для землетрясения
    /// </summary>
    public struct EarthquakeScalar
    {
        /// <summary>
        /// Координаты
        /// </summary>
        public Coordinate Coordinate { get; set; }
        
        /// <summary>
        /// Значение
        /// </summary>
        public double Value { get; set; }
    }
}