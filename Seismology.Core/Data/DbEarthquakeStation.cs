﻿using System;

namespace Seismology.Core.Data
{
    /// <summary>
    /// Класс станционного землетрясения для хранения/чтения в/из БД (Access 2002-2003 .mdf)
    /// </summary>
    public class DbEarthquakeStation
    {
        /// <summary>
        /// Уникальный индетификатор землетрясения
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Название станции
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Русское название станции
        /// </summary>
        public string NameRus { get; set; }
        /// <summary>
        /// Указывает, учитывалась ли станция при подсчете (true - учитывалась, false - не учитывалась)
        /// По умолчанию принимает значение true
        /// </summary>
        public bool IsIncluded { get; set; } = true;
        /// <summary>
        /// Время толчка в гипоцентре (по Гринвичу +3-00-00.000)
        /// </summary>
        public DateTime T0 { get; set; }
        /// <summary>
        /// Ep
        /// </summary>
        public string CachP { get; set; }
        /// <summary>
        /// Время распростронения продольной волны
        /// </summary>
        public DateTime Tp { get; set; }
        /// <summary>
        /// Es
        /// </summary>
        public string CachS { get; set; }
        /// <summary>
        /// Время распростронения попереченой волны
        /// </summary>
        public DateTime Ts { get; set; }
        /// <summary>
        /// Разница времени между Ts и Tp (в секундах)
        /// </summary>
        public double Sp { get; set; }
        /// <summary>
        /// Эпицентральное расстояние
        /// </summary>
        public double EpicDistance { get; set; }
        /// <summary>
        /// Невязка (погрешность)
        /// </summary>
        public double Dt { get; set; }
        /// <summary>
        /// Период волны по Z каналу
        /// </summary>
        public double PeriodZ { get; set; }
        /// <summary>
        /// Амплитуда волны по Z каналу
        /// </summary>
        public double AmplitudeZ { get; set; }
        /// <summary>
        /// Период волны по другому каналу
        /// </summary>
        public double Period { get; set; }
        /// <summary>
        /// Амплитуда волны по другому каналу
        /// </summary>
        public double Amplitude { get; set; }
        /// <summary>
        /// Энергия (класс) 10^<see cref="Class"/> Дж
        /// </summary>
        public double Class { get; set; }
        /// <summary>
        /// Отношение скоростей vp/vs (продольной и поперечной волны)
        /// </summary>
        public double VpVs { get; set; }
        /// <summary>
        /// Уникальный индетификатор
        /// </summary>
        public int EarthquakeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="station"></param>
        public static explicit operator DbEarthquakeStation(EarthquakeStation station)
        {
            return new DbEarthquakeStation
            {
                Name = station.Station.NameInDb,
                NameRus = station.Station.RusName,
                //IsIncluded = station.IsIncluded,
                T0 = station.T0,
                CachP = station.Ep,
                Tp = station.Tp,
                CachS = station.Es,
                Ts = station.Ts,
                Sp = Math.Round((station.Ts - station.Tp).TotalMilliseconds, 3),
                EpicDistance = Math.Round(station.EpicDistance, 3),
                Dt = station.Dt,
                PeriodZ = station.PeriodZ,
                AmplitudeZ = station.AmplitudeZ,
                Period = station.Period,
                Amplitude = station.Amplitude,
                Class = Math.Round(station.K, 3),
                VpVs = Math.Round(station.VpVs, 3)
            };
        }
    }
}
