﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.Data
{
    /// <summary>
    /// Класс землетрясения для хранения/чтения в/из БД (Access 2002-2003 .mdf)
    /// </summary>
    public class DbEarthquake
    {
        /// <summary>
        /// Id Землетрясения
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Дата землетрясения
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Время толчка в гипоцентре (по Гринвичу +3-00-00.000)
        /// </summary>
        public DateTime T0 { get; set; }
        /// <summary>
        /// Энергия (класс) 10^<see cref="K"/> Дж
        /// </summary>
        public double K { get; set; }
        /// <summary>
        /// Глубина в км
        /// </summary>
        public double H { get; set; }
        /// <summary>
        /// Широта (F - latitude)
        /// </summary>
        public double Lat { get; set; }
        public double LatError { get; set; }
        /// <summary>
        /// Долгота (L - longitue)
        /// </summary>
        public double Lng { get; set; }
        public double LngError { get; set; }
        /// <summary>
        /// Невязка (погрешность)
        /// </summary>
        public double Dt { get; set; }
        /// <summary>
        /// Среднеквадратичное значение Невязки
        /// </summary>
        public double DtRms { get; set; }
        /// <summary>
        /// Отношение скоростей vp/vs (продольной и поперечной волны)
        /// </summary>
        public double VpVs { get; set; }
        /// <summary>
        /// Имя автора, который занес землетрясение
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Метод вычисления гипоцентра
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// Метод вычисления эпицентрального расстояния
        /// </summary>
        public string HodographMethod { get; set; }

        /// <summary>
        /// Данные по станциям (если их нет, значение EarthquakeStations.Count = 0)
        /// </summary>
        public List<DbEarthquakeStation> Stations { get; set; }
            = new List<DbEarthquakeStation>();

        /// <summary>
        /// Перевод объект замлетрясения класса <see cref="Earthquake"/> в объект класса <see cref="DbEarthquake"/>
        /// </summary>
        /// <param name="earthquake"></param>
        public static explicit operator DbEarthquake(Earthquake earthquake)
        {
            return new DbEarthquake
            {
                Date = earthquake.Date,
                T0 = earthquake.T0,
                K = Math.Round(earthquake.K, 3),
                H = Math.Round(earthquake.H, 3),
                Lat = Math.Round(earthquake.Lat, 3),
                LatError = Math.Round(earthquake.LatError, 2),
                Lng = Math.Round(earthquake.Lng, 3),
                LngError = Math.Round(earthquake.LngError, 2),
                Dt = Math.Round(earthquake.Dt, 3),
                DtRms = Math.Round(earthquake.DtRms, 3),
                VpVs = Math.Round(earthquake.VpVs, 3),
                Stations = earthquake.Stations.Select(s => (DbEarthquakeStation)s).ToList()
            };
        }
    }
}
