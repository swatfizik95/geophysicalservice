﻿namespace Seismology.Core.Data
{
    /// <summary>
    /// Задает индетификаторы, которые определеяют тип операции с БД
    /// </summary>
    public enum DataOperation : byte
    {
        /// <summary>
        /// Возвращаемое значение добавить
        /// </summary>
        Add = 1,
        /// <summary>
        /// Возвращаемое значение получить
        /// </summary>
        Get = 2,
        /// <summary>
        /// Возвращаемое значение обновить
        /// </summary>
        Update = 3,
        /// <summary>
        /// Возвращаемое значение удалить
        /// </summary>
        Delete = 4,
        /// <summary>
        /// Возвращаемое значение ничего не делать
        /// </summary>
        None = 0
    }
}
