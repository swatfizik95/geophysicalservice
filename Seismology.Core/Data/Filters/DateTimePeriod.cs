﻿using System;

namespace Seismology.Core.Data.Filters
{
    public class DateTimePeriod : BasePeriod<DateTime>
    {
        public DateTimePeriod(DateTime begin, DateTime end) : base(begin, end) { }
    }
}