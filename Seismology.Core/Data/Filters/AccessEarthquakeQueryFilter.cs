﻿using Seismology.Core.Map;
using System.Globalization;
using System.Linq;

namespace Seismology.Core.Data.Filters
{
    public class AccessEarthquakeQueryFilter
    {
        private readonly CultureInfo _cultureInvariant = CultureInfo.InvariantCulture;

        public DateTimePeriod DatePeriod { get; set; }
        public DateTimePeriod T0Period { get; set; }
        public DoublePeriod ClassPeriod { get; set; }
        public DoublePeriod DepthPeriod { get; set; }
        public RectangleS Rectangle { get; set; }
        public CircleS Circle { get; set; }
        public string[] Authors { get; set; }

        public AccessEarthquakeQueryFilter(DateTimePeriod datePeriod, DateTimePeriod t0Period, DoublePeriod classPeriod, DoublePeriod depthPeriod, RectangleS rectangle, CircleS circle, string[] authors)
        {
            DatePeriod = datePeriod;
            T0Period = t0Period;
            ClassPeriod = classPeriod;
            DepthPeriod = depthPeriod;
            Rectangle = rectangle;
            Circle = circle;
            Authors = authors;
        }

        public string ToQuery()
        {
            string whereQuery = string.Empty;
            string andQuery = string.Empty;

            // DateTime
            string dateQuery = string.Empty;
            string timeQuery = string.Empty;
            
            // DateAndTime
            if (DatePeriod != null && T0Period != null)
            {
                string beginDateText = DatePeriod.Begin.ToString("MM/dd/yyyy", _cultureInvariant);
                string endDateText = DatePeriod.End.ToString("MM/dd/yyyy", _cultureInvariant);
                string beginTimeText = T0Period.Begin.ToString("HH:mm:ss", _cultureInvariant);
                string endTimeText = T0Period.End.ToString("HH:mm:ss", _cultureInvariant);
                dateQuery = $"(([Date]+[T0]) BETWEEN #{beginDateText} {beginTimeText}# AND #{endDateText} {endTimeText}#)";

                andQuery = "AND";
                whereQuery = "WHERE";
            }
            // Date
            else if (DatePeriod != null)
            {
                string beginDateText = DatePeriod.Begin.ToString("MM/dd/yyyy", _cultureInvariant);
                string endDateText = DatePeriod.End.ToString("MM/dd/yyyy", _cultureInvariant);
                dateQuery = $"Date BETWEEN #{beginDateText}# AND #{endDateText}#";

                andQuery = "AND";
                whereQuery = "WHERE";
            }
            // Time
            else if (T0Period != null)
            {
                string beginTimeText = T0Period.Begin.ToString("HH:mm:ss", _cultureInvariant);
                string endTimeText = T0Period.End.ToString("HH:mm:ss", _cultureInvariant);
                timeQuery = $"T0 BETWEEN #{beginTimeText}# AND #{endTimeText}#";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            // Authors
            string authorsQuery = string.Empty;
            if (Authors != null && Authors.Length > 0)
            {
                var authorsString = Authors
                        .Where(a => !string.IsNullOrWhiteSpace(a))
                        .Select(a => $"'{a}'");
                string authorJoin = string.Join(" OR Author = ", authorsString);
                
                if (!string.IsNullOrWhiteSpace(authorJoin))
                {
                    authorsQuery = $"{andQuery} Author = {authorJoin}";

                    andQuery = "AND";
                    whereQuery = "WHERE";
                }
            }

            // Eenergy
            string energyQuery = string.Empty;
            if (ClassPeriod != null)
            {
                string beginEnergyText = ClassPeriod.Begin.ToString(_cultureInvariant);
                string endEnergyText = ClassPeriod.End.ToString(_cultureInvariant);
                energyQuery = $"{andQuery} Class BETWEEN {beginEnergyText} AND {endEnergyText}";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            // Depth
            string depthQuery = string.Empty;
            if (DepthPeriod != null)
            {
                string beginDepthText = DepthPeriod.Begin.ToString(_cultureInvariant);
                string endDepthText = DepthPeriod.End.ToString(_cultureInvariant);
                depthQuery = $"{andQuery} Depth BETWEEN {beginDepthText} AND {endDepthText}";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            // Coordinates
            string rectQuery = string.Empty;
            string circleQuery = string.Empty;
            // Rectangle
            if (Rectangle != null)
            {
                string
                    latBeginText = Rectangle.Begin.Lat.ToString(_cultureInvariant),
                    lngBeginText = Rectangle.Begin.Lng.ToString(_cultureInvariant),
                    latEndText = Rectangle.End.Lat.ToString(_cultureInvariant),
                    lngEndText = Rectangle.End.Lng.ToString(_cultureInvariant);
                rectQuery = $"{andQuery} Latitude BETWEEN {latBeginText} AND {latEndText} AND Longitude BETWEEN {lngBeginText} AND {lngEndText}";

                andQuery = "AND";
                whereQuery = "WHERE";
            }
            // Circle
            else if (Circle != null)
            {
                string
                    latText = Circle.Lat.ToString(_cultureInvariant),
                    lngText = Circle.Lng.ToString(_cultureInvariant),
                    latRadText = Circle.LatRadius.ToString(_cultureInvariant),
                    lngRadText = Circle.LngRadius.ToString(_cultureInvariant);
                circleQuery =
                    $"{andQuery} SQR((Latitude - {latText})^2 / {latRadText}^2 + (Longitude - {lngText})^2 / {lngRadText}^2) <= 1";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            return $"SELECT * FROM Earthquake {whereQuery} {dateQuery} {timeQuery} {authorsQuery} {energyQuery} {depthQuery} {rectQuery} {circleQuery}";
        }
    }
}