﻿using System;

namespace Seismology.Core.Data.Filters
{
    public abstract class BasePeriod<T> where T : IComparable<T>
    {
        public T Begin { get; }
        public T End{ get; }

        protected BasePeriod(T begin, T end)
        {
            if (end.CompareTo(begin) > 0)
            {
                Begin = begin;
                End = end;
            }
            else
            {
                Begin = end;
                End = begin;
            }
        }
    }
}