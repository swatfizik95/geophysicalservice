﻿namespace Seismology.Core.Data.Filters
{
    public class DoublePeriod : BasePeriod<double>
    {
        public DoublePeriod(double begin, double end) : base(begin, end)
        {
        }
    }
}