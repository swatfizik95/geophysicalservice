﻿using Seismology.Core.Map;
using Seismology.Core.Properties;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Seismology.Core.Data.Access
{
    /// <summary>
    /// Класс, для работы с бд (базой данных) Access 2002-2003
    /// </summary>
    public class DbAccess
    {
        private const string EarthquakeTableName = "Earthquake";
        private const string EarthquakeStationTableName = "EarthquakeStantion";
        
        private const string InsertEarthquakeQuery = "INSERT INTO Earthquake " +
                                                     "([Date],[T0],[T0_ms],[Class],[Depth]," +
                                                     "[Latitude],[LatitudeError],[Longitude],[LongitudeError]," +
                                                     "[Dt],[DtRms],[VpVs],[Author],[Method],[Hodograph_Method]) " +
                                                     "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        private const string UpdateEarthquakeQuery = "UPDATE Earthquake SET " +
                                                     "[Date] = ?,[T0] = ?,[T0_ms] = ?,[Class] = ?,[Depth] = ?," +
                                                     "[Latitude] = ?,[LatitudeError] = ?,[Longitude] = ?,[LongitudeError] = ?," +
                                                     "[Dt] = ?,[DtRms] = ?,[VpVs] = ?,[Author] = ?,[Method] = ?,[Hodograph_Method] = ?";

        private const string IdentityQuery = "SELECT @@Identity";

        private const string InsertStationQuery = "INSERT INTO EarthquakeStantion ([Name],[Is_Included],[NameRus],[T0],[T0_ms]," +
                                                  "[Cach_p],[Tp],[Tp_ms],[Cach_s],[Ts],[Ts_ms],[S_P]," +
                                                  "[EpicDistance],[Dt],[Period_Z],[Amplitude_Z],[Period],[Amplitude]," +
                                                  "[Class],[VpVs],[Earthquake_Id]) " +
                                                  "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        private const string UpdateStationQuery = "UPDATE EarthquakeStantion SET " +
                                                  "[Name] = ?,[Is_Included] = ?,[NameRus] = ?,[T0] = ?,[T0_ms] = ?," +
                                                  "[Cach_p] = ?,[Tp] = ?,[Tp_ms] = ?,[Cach_s] = ?,[Ts] = ?,[Ts_ms] = ?,[S_P] = ?," +
                                                  "[EpicDistance] = ?,[Dt] = ?,[Period_Z] = ?,[Amplitude_Z] = ?,[Period] = ?,[Amplitude] = ?," +
                                                  "[Class] = ?,[VpVs] = ?,[Earthquake_Id] = ? ";


        private readonly OleDbConnectionStringBuilder _sb;
        private readonly CultureInfo _cultureInvariant = CultureInfo.InvariantCulture;
        private readonly CultureInfo _cultureCurrent = CultureInfo.CurrentCulture;

        /// <summary>
        /// Создает объект класса <see cref="DbAccess"/>
        /// </summary>
        /// <param name="dataSource">Источник данных</param>
        public DbAccess(string dataSource)
        {
            _sb = new OleDbConnectionStringBuilder
            {
                DataSource = dataSource,
                Provider = "Microsoft.Jet.OLEDB.4.0"
            };
        }

        /// <summary>
        /// Клонирует новую пустую базу данных
        /// </summary>
        /// <param name="path"></param>
        public static void CloneNewEmptyDb(string path)
        {
            File.WriteAllBytes(path, Resource.MainSeismologyDatabase);
        }

        #region Async

        #region AddAsync

        /// <summary>
        /// Асинхронно добавляет в БД землетрясение со станционными данными
        /// </summary>
        /// <param name="earthquake">Землетрясение</param>
        public async Task AddAsync(DbEarthquake earthquake)
        {
            CheckDataSource();
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var cmd = CreateInsertCmd(connection, earthquake, transaction);
                    var cmdId = CreateIdentityCmd(connection, transaction);

                    await cmd.ExecuteNonQueryAsync();
                    int id = ParseInt(await cmdId.ExecuteScalarAsync());

                    foreach (var station in earthquake.Stations)
                    {
                        var cmdStan = CreateInsertCmd(connection, station, id, transaction);
                        await cmdStan.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Асинхронно добавляет в БД землетрясения со станционными данными
        /// </summary>
        /// <param name="earthquakes">Землетрясения</param>
        public async Task AddAsync(IEnumerable<DbEarthquake> earthquakes)
        {
            CheckDataSource();
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    foreach (var earthquake in earthquakes)
                    {
                        var cmd = CreateInsertCmd(connection, earthquake, transaction);
                        var cmdId = CreateIdentityCmd(connection, transaction);

                        await cmd.ExecuteNonQueryAsync();
                        int id = ParseInt(await cmdId.ExecuteScalarAsync());

                        foreach (var station in earthquake.Stations)
                        {
                            var cmdStan = CreateInsertCmd(connection, station, id, transaction);
                            await cmdStan.ExecuteNonQueryAsync();
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion

        #region UpdateAsync

        /// <summary>
        /// Обновляет асинхронно данные по землетрясению
        /// <para>Если данных по станциям будет меньше, чем в БД, то лишние станционные данные в БД удаляются</para>
        /// <para>Если данных по станциям будет больше, лишние записи буду добавлться в БД</para>
        /// </summary>
        /// <param name="id">id землетрясения, который нужно обновить</param>
        /// <param name="earthquake">Новые данные о землетрясении</param>
        public async Task UpdateAsync(int id, DbEarthquake earthquake)
        {
            CheckDataSource();
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    // earhquake
                    var cmd = connection.CreateCommand();
                    cmd.Transaction = transaction;
                    cmd.CommandText = $"{UpdateEarthquakeQuery} WHERE id = {id}";
                    AddWithValue(cmd, earthquake);
                    await cmd.ExecuteNonQueryAsync();
                    cmd.Parameters.Clear();

                    // id stations
                    cmd.CommandText = $"SELECT Id FROM EarthquakeStantion WHERE Earthquake_Id = {id}";
                    var reader = await cmd.ExecuteReaderAsync();
                    var stationsId = new List<int>();
                    while (reader != null && await reader.ReadAsync())
                    {
                        stationsId.Add(ParseInt(reader["Id"]));
                    }
                    reader?.Close();

                    // stations
                    int min = Math.Min(stationsId.Count, earthquake.Stations.Count);
                    for (int i = 0; i < min; i++)
                    {
                        cmd.CommandText = $"{UpdateStationQuery} WHERE Id = {stationsId[i]}";
                        AddWithValue(cmd, earthquake.Stations[i], id);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    // nothing, if in db as many
                    if (min < stationsId.Count)
                    {
                        // delete, if in db more
                        var cmdStanDelete = connection.CreateCommand();
                        cmdStanDelete.Transaction = transaction;
                        for (int i = min; i < stationsId.Count; i++)
                        {
                            cmdStanDelete.CommandText = $"DELETE * FROM EarthquakeStantion WHERE Id = {stationsId[i]}";
                            await cmdStanDelete.ExecuteNonQueryAsync();
                        }
                    }
                    else if (min < earthquake.Stations.Count)
                    {
                        // add, if in db less
                        cmd.CommandText = InsertStationQuery;
                        for (int i = min; i < earthquake.Stations.Count; i++)
                        {
                            AddWithValue(cmd, earthquake.Stations[i], id);
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Parameters.Clear();
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion

        #region GetAsync

        /// <summary>
        /// Возвращает асинхронно землетрясения
        /// </summary>
        /// <param name="query">Инструкция для SQL</param>
        /// <param name="isIncludeStations">Включить ли станции</param>
        /// <returns></returns>
        private async Task<List<DbEarthquake>> GetDataAsync(string query, bool isIncludeStations, bool isForBulletin = false)
        {
            CheckDataSource();
            var earthquakes = new List<DbEarthquake>();
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                try
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    var cmdStation = connection.CreateCommand();
                    cmd.CommandText = query;

                    var reader = await cmd.ExecuteReaderAsync();
                    while (reader != null && await reader.ReadAsync())
                    {
                        var earthquake = new DbEarthquake
                        {
                            Id = ParseInt(reader["Id"]),
                            Date = ParseDateTime(reader["Date"]),
                            T0 = ParseDateTime(reader["T0"])
                                .AddMilliseconds(ParseInt(reader["T0_ms"])),
                            K = ParseDouble(reader["Class"]),
                            H = ParseDouble(reader["Depth"]),
                            Lat = ParseDouble(reader["Latitude"]),
                            LatError = ParseDouble(reader["LatitudeError"]),
                            Lng = ParseDouble(reader["Longitude"]),
                            LngError = ParseDouble(reader["LongitudeError"]),
                            Dt = ParseDouble(reader["Dt"]),
                            DtRms = ParseDouble(reader["DtRms"]),
                            VpVs = ParseDouble(reader["VpVs"]),
                            Author = reader["Author"].ToString(),
                            Method = reader["Method"].ToString(),
                            HodographMethod = reader["Hodograph_Method"].ToString()
                        };

                        // Нужно ли включить станции?
                        if (isIncludeStations)
                        {
                            if (isForBulletin)
                            {
                                cmdStation.CommandText = $"SELECT * FROM EarthquakeStantion WHERE Earthquake_Id = {earthquake.Id} AND Is_Included = TRUE";
                            }
                            else
                            {
                                cmdStation.CommandText = $"SELECT * FROM EarthquakeStantion WHERE Earthquake_Id = {earthquake.Id}";
                            }

                            var readerStation = await cmdStation.ExecuteReaderAsync();
                            var date = earthquake.Date.Date;
                            while (readerStation != null && await readerStation.ReadAsync())
                            {
                                var t0 = ParseDateTime(readerStation["T0"])
                                    .AddMilliseconds(ParseInt(readerStation["T0_ms"]));
                                var tp = ParseDateTime(readerStation["Tp"])
                                    .AddMilliseconds(ParseInt(readerStation["Tp_ms"]));
                                var ts = ParseDateTime(readerStation["Ts"])
                                    .AddMilliseconds(ParseInt(readerStation["Ts_ms"]));
                                if (tp > t0)
                                {
                                    tp = date
                                        .AddHours(tp.Hour)
                                        .AddMinutes(tp.Minute)
                                        .AddSeconds(tp.Second)
                                        .AddMilliseconds(tp.Millisecond);
                                    ts = date
                                        .AddHours(ts.Hour)
                                        .AddMinutes(ts.Minute)
                                        .AddSeconds(ts.Second)
                                        .AddMilliseconds(ts.Millisecond);
                                }
                                else
                                {
                                    tp = date
                                        .AddDays(1)
                                        .AddHours(tp.Hour)
                                        .AddMinutes(tp.Minute)
                                        .AddSeconds(tp.Second)
                                        .AddMilliseconds(tp.Millisecond);
                                    ts = date
                                        .AddDays(1)
                                        .AddHours(ts.Hour)
                                        .AddMinutes(ts.Minute)
                                        .AddSeconds(ts.Second)
                                        .AddMilliseconds(ts.Millisecond);
                                }

                                t0 = date
                                    .AddHours(t0.Hour)
                                    .AddMinutes(t0.Minute)
                                    .AddSeconds(t0.Second)
                                    .AddMilliseconds(t0.Millisecond);

                                var eqs = new DbEarthquakeStation
                                {
                                    Id = ParseInt(readerStation["Id"]),
                                    Name = readerStation["Name"].ToString(),
                                    IsIncluded = ParseBool(readerStation["Is_Included"]),
                                    NameRus = readerStation["NameRus"].ToString(),
                                    T0 = t0,
                                    CachP = readerStation["Cach_p"].ToString(),
                                    Tp = tp,
                                    CachS = readerStation["Cach_s"].ToString(),
                                    Ts = ts,
                                    Sp = ParseDouble(readerStation["S_P"]),
                                    EpicDistance = ParseDouble(readerStation["EpicDistance"]),
                                    Dt = ParseDouble(readerStation["Dt"]),
                                    PeriodZ = ParseDouble(readerStation["Period_Z"]),
                                    AmplitudeZ = ParseDouble(readerStation["Amplitude_Z"]),
                                    Period = ParseDouble(readerStation["Period"]),
                                    Amplitude = ParseDouble(readerStation["Amplitude"]),
                                    Class = ParseDouble(readerStation["Class"]),
                                    VpVs = ParseDouble(readerStation["VpVs"]),
                                    EarthquakeId = ParseInt(readerStation["Earthquake_Id"])
                                };
                                earthquake.Stations.Add(eqs);
                            }
                            readerStation?.Close();
                        }

                        earthquakes.Add(earthquake);
                    }

                    connection.Close();
                }
                finally
                {
                    connection.Close();
                }
            }

            return earthquakes;
        }

        /// <summary>
        /// Возвращает асинхронно землетрясение по определенной выборке
        /// </summary>
        /// <param name="dateBegin">Начало даты</param>
        /// <param name="dateEnd">Коне даты</param>
        /// <param name="timeBegin">Начало времени</param>
        /// <param name="timeEnd">Конец времени</param>
        /// <param name="energyBegin">Начало энергии</param>
        /// <param name="energyEnd">Конец энергии</param>
        /// <param name="depthBegin">Начало глубины</param>
        /// <param name="depthEnd">Конец глубины</param>
        /// <param name="authors">Авторы</param>
        /// <param name="rectangle">Площадь (прямоугольная)</param>
        /// <param name="circle">Площадь (круг)</param>
        /// <param name="stationCountBegin">Кол-во станций начало</param>
        /// <param name="stationCountEnd">Кол-во станций конец</param>
        /// <param name="includeStations">Включить станции?</param>
        /// <param name="forBulletin">Для бюллетеня? (Тогда станции, которые не учавствуют в рассчете - не будут учтены)</param>
        /// <returns></returns>
        public async Task<List<DbEarthquake>> GetAsync(
            DateTime? dateBegin, DateTime? dateEnd,
            DateTime? timeBegin, DateTime? timeEnd,
            double? energyBegin, double? energyEnd,
            double? depthBegin, double? depthEnd,
            int? stationCountBegin, int? stationCountEnd,
            string[] authors,
            RectangleS rectangle,
            CircleS circle,
            bool includeStations = false,
            bool forBulletin = false)
        {
            string query = GetQuerySelection(
                dateBegin, dateEnd,
                timeBegin, timeEnd,
                energyBegin, energyEnd,
                depthBegin, depthEnd,
                stationCountBegin, stationCountEnd,
                authors,
                rectangle,
                circle);

            return await GetDataAsync(query, includeStations, forBulletin);
        }

        /// <summary>
        /// Возвращает асинхронно землетрясение со станционными данными по id
        /// </summary>
        /// <param name="id">Id землетрясения</param>
        /// <returns></returns>
        public async Task<DbEarthquake> GetAsync(int id)
        {
            string query = $"SELECT * FROM Earthquake WHERE Id = {id}";
            return (await GetDataAsync(query, true)).FirstOrDefault();
        }

        #endregion

        #region DeleteAsync

        /// <summary>
        /// Удаляет землетрясение ( происходит каскадное удаление землетрясения - станционные данные также удаляются)
        /// </summary>
        /// <param name="id">id землетрясений, который нужно удалить</param>
        public async Task DeleteAsync(int id)
        {
            CheckDataSource();
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var cmd = connection.CreateCommand();
                    cmd.Transaction = transaction;
                    cmd.CommandText = $"DELETE * FROM Earthquake WHERE Id = {id}";

                    await cmd.ExecuteNonQueryAsync();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Удаляет землетрясения ( происходит каскадное удаление землетрясений - станционные данные также удаляются)
        /// </summary>
        /// <param name="ids">id землетрясений, которые нужно удалить</param>
        public async Task DeleteAsync(IEnumerable<int> ids)
        {
            CheckDataSource();
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var cmd = connection.CreateCommand();
                    cmd.Transaction = transaction;
                    foreach (var id in ids)
                    {
                        cmd.CommandText = $"DELETE * FROM Earthquake WHERE Id = {id}";
                        await cmd.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion

        #region MatchAsync

        /// <summary>
        /// Указывает, обнаружено ли в БД запись с землетрясением, попавший в промежуток вермени
        /// </summary>
        /// <param name="date">Дата</param>
        /// <param name="t0">Время</param>
        /// <param name="borderSeconds">Разница для проверки (по умолчанию 3 секунды)</param>
        /// <returns>true - если есть совпадение, false - если совпадения нет</returns>
        public async Task<bool> IsMatchAsync(DateTime date, DateTime t0, int borderSeconds = 3)
        {
            CheckDataSource();
            bool result;
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                var dateToCompare = date
                    .AddHours(t0.Hour)
                    .AddMinutes(t0.Minute)
                    .AddSeconds(t0.Second)
                    .AddMilliseconds(t0.Millisecond);
                string starDate = dateToCompare.AddSeconds(-borderSeconds)
                    .ToString("MM/dd/yyyy H:m:s", _cultureInvariant);
                string endDate = dateToCompare.AddSeconds(borderSeconds)
                    .ToString("MM/dd/yyyy H:m:s", _cultureInvariant);
                try
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandText =
                        $"SELECT COUNT(*) FROM Earthquake WHERE((([Date] +[T0]) Between #{starDate}# And #{endDate}#));";
                    result = ParseInt(await cmd.ExecuteScalarAsync()) > 0;
                    connection.Close();
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// Ищет в БД все входящие в промежуток времени землетрясения (возвращаем вместе со станционными данными)
        /// </summary>
        /// <param name="date">Дата</param>
        /// <param name="t0">Время</param>
        /// <param name="borderSeconds">Разница для проверки (по умолчанию 3 секунды)</param>
        /// <returns></returns>
        public async Task<List<DbEarthquake>> MatchesAsync(DateTime date, DateTime t0, int borderSeconds = 3)
        {
            CheckDataSource();
            List<DbEarthquake> result;
            using (var connection = new OleDbConnection(_sb.ConnectionString))
            {
                var dateToCompare = date
                    .AddHours(t0.Hour)
                    .AddMinutes(t0.Minute)
                    .AddSeconds(t0.Second)
                    .AddMilliseconds(t0.Millisecond);
                string starDate = dateToCompare.AddSeconds(-borderSeconds)
                    .ToString("MM/dd/yyyy H:m:s", _cultureInvariant);
                string endDate = dateToCompare.AddSeconds(borderSeconds)
                    .ToString("MM/dd/yyyy H:m:s", _cultureInvariant);
                try
                {
                    result = await GetDataAsync($"SELECT * FROM Earthquake WHERE((([Date] +[T0]) Between #{starDate}# And #{endDate}#));", true);
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        #endregion

        #endregion

        #region Parser

        /// <summary>
        /// Преобразует данные в строку, а потом в вещественное число
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Число</returns>
        private double ParseDouble(object obj)
        {
            return double.TryParse(obj.ToString(), NumberStyles.Float, _cultureCurrent, out var result) ? result : 0.0;
        }

        /// <summary>
        /// Преобразует данные в строку, а потом в целое число
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Число</returns>
        private int ParseInt(object obj)
        {
            return int.TryParse(obj.ToString(), out var result) ? result : 0;
        }

        /// <summary>
        /// Преобразует данные в строку, а потом в дату
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Число</returns>
        private DateTime ParseDateTime(object obj)
        {
            return DateTime.TryParse(obj.ToString(), _cultureCurrent, DateTimeStyles.None, out var result) ? result : DateTime.MinValue;
        }

        /// <summary>
        /// Преобразует данные в строку, а потом в логическое значение
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Логическое значение</returns>
        private bool ParseBool(object obj)
        {
            return bool.TryParse(obj.ToString(), out var result) ? result : throw new Exception();
        }

        #endregion

        private static OleDbCommand CreateInsertCmd(OleDbConnection connection, DbEarthquake earthquake, OleDbTransaction transaction)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = InsertEarthquakeQuery;
            cmd.Transaction = transaction;
            AddWithValue(cmd, earthquake);

            return cmd;
        }

        private static OleDbCommand CreateInsertCmd(OleDbConnection connection, DbEarthquakeStation station, int id, OleDbTransaction transaction)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = InsertStationQuery;
            cmd.Transaction = transaction;
            AddWithValue(cmd, station, id);

            return cmd;
        }

        private static OleDbCommand CreateIdentityCmd(OleDbConnection connection, OleDbTransaction transaction)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = IdentityQuery;
            cmd.Transaction = transaction;

            return cmd;
        }

        private static void AddWithValue(OleDbCommand cmd, DbEarthquake earthquake)
        {
            cmd.Parameters.AddWithValue("@Date", earthquake.Date.Date);
            cmd.Parameters.AddWithValue("@T0", earthquake.T0.ToLongTimeString());
            cmd.Parameters.AddWithValue("@T0_ms", earthquake.T0.Millisecond);
            cmd.Parameters.AddWithValue("@Class", earthquake.K);
            cmd.Parameters.AddWithValue("@Depth", earthquake.H);
            cmd.Parameters.AddWithValue("@Latitude", earthquake.Lat);
            cmd.Parameters.AddWithValue("@LatitudeError", earthquake.LatError);
            cmd.Parameters.AddWithValue("@Longitude", earthquake.Lng);
            cmd.Parameters.AddWithValue("@LongitudeError", earthquake.LngError);
            cmd.Parameters.AddWithValue("@Dt", earthquake.Dt);
            cmd.Parameters.AddWithValue("@DtRms", earthquake.DtRms);
            cmd.Parameters.AddWithValue("@VpVs", earthquake.VpVs);
            cmd.Parameters.AddWithValue("@Author", earthquake.Author);
            cmd.Parameters.AddWithValue("@Method", earthquake.Method);
            cmd.Parameters.AddWithValue("@Hodograph_Method", earthquake.HodographMethod);
        }

        private static void AddWithValue(OleDbCommand cmd, DbEarthquakeStation station, int id)
        {
            cmd.Parameters.AddWithValue("@Name", station.Name);
            cmd.Parameters.AddWithValue("@Is_Included", station.IsIncluded);
            cmd.Parameters.AddWithValue("@NameRus", station.NameRus);
            cmd.Parameters.AddWithValue("@T0", station.T0.ToLongTimeString());
            cmd.Parameters.AddWithValue("@T0_ms", station.T0.Millisecond);
            cmd.Parameters.AddWithValue("@Cach_p", station.CachP);
            cmd.Parameters.AddWithValue("@Tp", station.Tp.ToLongTimeString());
            cmd.Parameters.AddWithValue("@Tp_ms", station.Tp.Millisecond);
            cmd.Parameters.AddWithValue("@Cach_s", station.CachS);
            cmd.Parameters.AddWithValue("@Ts", station.Ts.ToLongTimeString());
            cmd.Parameters.AddWithValue("@Ts_ms", station.Ts.Millisecond);
            cmd.Parameters.AddWithValue("@S_P", (station.Ts - station.Tp).TotalSeconds);
            cmd.Parameters.AddWithValue("@EpicDistance", station.EpicDistance);
            cmd.Parameters.AddWithValue("@Dt", station.Dt);
            cmd.Parameters.AddWithValue("@Period_Z", station.PeriodZ);
            cmd.Parameters.AddWithValue("@Amplitude_Z", station.AmplitudeZ);
            cmd.Parameters.AddWithValue("@Period", station.Period);
            cmd.Parameters.AddWithValue("@Amplitude", station.Amplitude);
            cmd.Parameters.AddWithValue("@Class", station.Class);
            cmd.Parameters.AddWithValue("@VpVs", station.VpVs);
            cmd.Parameters.AddWithValue("@Earthquake_id", id);
        }

        private string GetQuerySelection(
            DateTime? dateBegin, DateTime? dateEnd,
            DateTime? timeBegin, DateTime? timeEnd,
            double? energyBegin, double? energyEnd,
            double? depthBegin, double? depthEnd,
            int? stationCountBegin, int? stationCountEnd,
            string[] authors,
            RectangleS rectangle,
            CircleS circle)
        {
            bool isNeedFilterByStations = false;
            string stationQuery = string.Empty;
            string andStationQuery = string.Empty;

            string whereQuery = string.Empty;
            string andQuery = string.Empty;

            #region stations filter

            if (stationCountBegin.HasValue && stationCountEnd.HasValue)
            {
                isNeedFilterByStations = true;
                stationQuery = $"{stationQuery} {andStationQuery} COUNT(Earthquake_Id) BETWEEN {stationCountBegin.Value} AND {stationCountEnd.Value}";
                andStationQuery = "AND";
                whereQuery = "WHERE";
            }

            #endregion

            string dateQuery = string.Empty;
            string timeQuery = string.Empty;

            #region main filter

            // DateAndTime
            if (dateBegin != null && dateEnd != null && timeBegin != null && timeEnd != null)
            {
                string beginDateText = dateBegin.Value.ToString("MM/dd/yyyy", _cultureInvariant);
                string endDateText = dateEnd.Value.ToString("MM/dd/yyyy", _cultureInvariant);
                string beginTimeText = timeBegin.Value.ToString("HH:mm:ss", _cultureInvariant);
                string endTimeText = timeEnd.Value.ToString("HH:mm:ss", _cultureInvariant);
                dateQuery = $"(([Date]+[T0]) BETWEEN #{beginDateText} {beginTimeText}# AND #{endDateText} {endTimeText}#)";

                andQuery = "AND";
                whereQuery = "WHERE";
            }
            // Date
            else if (dateBegin != null && dateEnd != null)
            {
                string beginDateText = dateBegin.Value.ToString("MM/dd/yyyy", _cultureInvariant);
                string endDateText = dateEnd.Value.ToString("MM/dd/yyyy", _cultureInvariant);
                dateQuery = $"Date BETWEEN #{beginDateText}# AND #{endDateText}#";

                andQuery = "AND";
                whereQuery = "WHERE";
            }
            // Time
            else if (timeBegin != null && timeEnd != null)
            {
                string beginTimeText = timeBegin.Value.ToString("HH:mm:ss", _cultureInvariant);
                string endTimeText = timeEnd.Value.ToString("HH:mm:ss", _cultureInvariant);
                timeQuery = $"T0 BETWEEN #{beginTimeText}# AND #{endTimeText}#";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            // Authors
            string authorsQuery = string.Empty;
            if (authors != null)
            {
                var authorsString = authors
                    .Where(a => !string.IsNullOrWhiteSpace(a))
                    .Select(a => $"'{a}'");
                string authorJoin = string.Join(" OR Author = ", authorsString);
                if (!string.IsNullOrWhiteSpace(authorJoin))
                {
                    authorsQuery = $"{andQuery} Author = {authorJoin}";

                    andQuery = "AND";
                    whereQuery = "WHERE";
                }
            }

            // Eenergy
            string energyQuery = string.Empty;
            if (energyBegin != null && energyEnd != null && energyBegin <= energyEnd)
            {
                string beginEnergyText = energyBegin?.ToString(_cultureInvariant);
                string endEnergyText = energyEnd?.ToString(_cultureInvariant);
                energyQuery = $"{andQuery} Class BETWEEN {beginEnergyText} AND {endEnergyText}";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            // Depth
            string depthQuery = string.Empty;
            if (depthBegin != null && depthEnd != null && depthBegin <= depthEnd)
            {
                string beginDepthText = depthBegin?.ToString(_cultureInvariant);
                string endDepthText = depthEnd?.ToString(_cultureInvariant);
                depthQuery = $"{andQuery} Depth BETWEEN {beginDepthText} AND {endDepthText}";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            // Coordinates
            string rectQuery = string.Empty;
            string circleQuery = string.Empty;
            // Rectangle
            if (rectangle != null)
            {
                string
                    latBeginText = rectangle.Begin.Lat.ToString(_cultureInvariant),
                    lngBeginText = rectangle.Begin.Lng.ToString(_cultureInvariant),
                    latEndText = rectangle.End.Lat.ToString(_cultureInvariant),
                    lngEndText = rectangle.End.Lng.ToString(_cultureInvariant);
                rectQuery = $"{andQuery} Latitude BETWEEN {latBeginText} AND {latEndText} AND Longitude BETWEEN {lngBeginText} AND {lngEndText}";

                andQuery = "AND";
                whereQuery = "WHERE";
            }
            // Circle
            else if (circle != null)
            {
                string
                    latText = circle.Lat.ToString(_cultureInvariant),
                    lngText = circle.Lng.ToString(_cultureInvariant),
                    latRadText = circle.LatRadius.ToString(_cultureInvariant),
                    lngRadText = circle.LngRadius.ToString(_cultureInvariant);
                circleQuery =
                    $"{andQuery} SQR((Latitude - {latText})^2 / {latRadText}^2 + (Longitude - {lngText})^2 / {lngRadText}^2) <= 1";

                andQuery = "AND";
                whereQuery = "WHERE";
            }

            #endregion

            if (isNeedFilterByStations)
                stationQuery = $"{andQuery} Id IN (SELECT Earthquake_Id FROM {EarthquakeStationTableName} GROUP BY Earthquake_Id HAVING {stationQuery})";

            return $"SELECT * FROM {EarthquakeTableName} {whereQuery} {dateQuery} {timeQuery} {authorsQuery} {energyQuery} {depthQuery} {rectQuery} {circleQuery} {stationQuery}";
        }

        /// <summary>
        /// Проверка на корректность ввода БД
        /// </summary>
        private void CheckDataSource()
        {
            if (string.IsNullOrEmpty(_sb.DataSource) ||
                _sb.DataSource == "Не задано" ||
                !_sb.DataSource.Contains(".mdb"))
                throw new Exception("Путь к базе данных не указан или пустой!");
        }
    }
}