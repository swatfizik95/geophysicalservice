﻿namespace Seismology.Core
{
    public class EarthquakeDistributionValue
    {
        /// <summary>
        /// Год
        /// </summary>

        public int Year { get; }
        /// <summary>
        /// Количенство классов
        /// </summary>
        public int[] ClassesCount { get; }

        /// <summary>
        /// Количество глубин
        /// </summary>
        public int[] DepthsCount { get; }

        /// <summary>
        /// Энергия класса
        /// </summary>
        public double ClassesEnergy { get; set; }

        public EarthquakeDistributionValue(int year, int classesLength, int depthsLength)
        {
            Year = year;
            ClassesCount = new int[classesLength];
            DepthsCount = new int[depthsLength];
        }
    }
}