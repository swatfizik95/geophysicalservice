﻿using Seismology.Core.Dto;
using Seismology.Core.Map;

namespace Seismology.Core.Mapping
{
    internal static class StationMapper
    {
        public static Station ToStation(this StationDto station) => 
            new Station(station.Name, station.InternationalName, station.NameInDb, station.RusName, new Coordinate(station.Lat, station.Lng), station.Color, station.Group, station.NameForRead);

        public static StationDto ToStationDto(this Station station) =>
            new StationDto
            {
                Name = station.Name,
                NameInDb = station.NameInDb,
                InternationalName = station.InternationalName,
                NameForRead = station.NameForRead,
                RusName = station.RusName,
                Color = station.Color,
                Lat = station.Coordinate.Lat,
                Lng = station.Coordinate.Lng,
                Group = station.Group,
            };
    }
}