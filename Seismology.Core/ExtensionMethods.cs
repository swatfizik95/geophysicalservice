﻿using System;

namespace Seismology.Core
{
    /// <summary>
    /// Методы расширения
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Меняет год
        /// </summary>
        /// <param name="source"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static DateTime ChangeYears(this DateTime source, int year)
        {
            return source.AddYears(year - source.Year);
        }

        public static DateTime ChangeMonths(this DateTime source, int month)
        {
            return source.AddMonths(month - source.Month);
        }

        public static DateTime ChangeDays(this DateTime source, int day)
        {
            return source.AddDays(day - source.Day);
        }

        public static DateTime ChangeHours(this DateTime source, int hour)
        {
            return source.AddHours(hour - source.Hour);
        }

        public static DateTime ChangeMinutes(this DateTime source, int minute)
        {
            return source.AddMinutes(minute - source.Minute);
        }

        public static DateTime ChangeSeconds(this DateTime source, double second)
        {
            return source.AddSeconds(second - source.Second);
        }
    }
}