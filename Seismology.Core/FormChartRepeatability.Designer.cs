﻿namespace Seismology.Core
{
    partial class FormChartRepeatability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.chartMain = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.numericUpDownKBegin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownKBeginOls = new System.Windows.Forms.NumericUpDown();
            this.buttonApply = new System.Windows.Forms.Button();
            this.labelKBegin = new System.Windows.Forms.Label();
            this.labelKBeginOls = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKBeginOls)).BeginInit();
            this.SuspendLayout();
            // 
            // chartMain
            // 
            this.chartMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chartMain.ChartAreas.Add(chartArea2);
            legend2.Enabled = false;
            legend2.Name = "Legend1";
            this.chartMain.Legends.Add(legend2);
            this.chartMain.Location = new System.Drawing.Point(12, 12);
            this.chartMain.Name = "chartMain";
            this.chartMain.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series3.Legend = "Legend1";
            series3.MarkerSize = 20;
            series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series3.Name = "Series1";
            series4.BorderWidth = 2;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Series2";
            this.chartMain.Series.Add(series3);
            this.chartMain.Series.Add(series4);
            this.chartMain.Size = new System.Drawing.Size(809, 453);
            this.chartMain.TabIndex = 0;
            this.chartMain.Text = "График";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title2.Name = "Title1";
            title2.Text = "Титул";
            this.chartMain.Titles.Add(title2);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(722, 471);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(99, 30);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(617, 471);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(99, 30);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // numericUpDownKBegin
            // 
            this.numericUpDownKBegin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownKBegin.Location = new System.Drawing.Point(37, 474);
            this.numericUpDownKBegin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownKBegin.Name = "numericUpDownKBegin";
            this.numericUpDownKBegin.Size = new System.Drawing.Size(60, 26);
            this.numericUpDownKBegin.TabIndex = 3;
            this.numericUpDownKBegin.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // numericUpDownKBeginOls
            // 
            this.numericUpDownKBeginOls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownKBeginOls.Location = new System.Drawing.Point(245, 474);
            this.numericUpDownKBeginOls.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownKBeginOls.Name = "numericUpDownKBeginOls";
            this.numericUpDownKBeginOls.Size = new System.Drawing.Size(60, 26);
            this.numericUpDownKBeginOls.TabIndex = 4;
            this.numericUpDownKBeginOls.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.Location = new System.Drawing.Point(491, 471);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(120, 30);
            this.buttonApply.TabIndex = 5;
            this.buttonApply.Text = "Применить";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.ButtonApply_Click);
            // 
            // labelKBegin
            // 
            this.labelKBegin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelKBegin.AutoSize = true;
            this.labelKBegin.Location = new System.Drawing.Point(12, 476);
            this.labelKBegin.Name = "labelKBegin";
            this.labelKBegin.Size = new System.Drawing.Size(19, 20);
            this.labelKBegin.TabIndex = 6;
            this.labelKBegin.Text = "K";
            // 
            // labelKBeginOls
            // 
            this.labelKBeginOls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelKBeginOls.AutoSize = true;
            this.labelKBeginOls.Location = new System.Drawing.Point(120, 476);
            this.labelKBeginOls.Name = "labelKBeginOls";
            this.labelKBeginOls.Size = new System.Drawing.Size(119, 20);
            this.labelKBeginOls.TabIndex = 7;
            this.labelKBeginOls.Text = "K для расчета";
            // 
            // FormChartRepeatability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 513);
            this.Controls.Add(this.labelKBeginOls);
            this.Controls.Add(this.labelKBegin);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.numericUpDownKBeginOls);
            this.Controls.Add(this.numericUpDownKBegin);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.chartMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormChartRepeatability";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "График повторяемости";
            this.Shown += new System.EventHandler(this.FormChartRepeatability_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.chartMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKBeginOls)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartMain;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.NumericUpDown numericUpDownKBegin;
        private System.Windows.Forms.NumericUpDown numericUpDownKBeginOls;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Label labelKBegin;
        private System.Windows.Forms.Label labelKBeginOls;
    }
}