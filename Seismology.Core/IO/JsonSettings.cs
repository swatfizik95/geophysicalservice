﻿using Newtonsoft.Json;
using Seismology.Core.IO.Interfaces;
using System.IO;
using System.Text;

namespace Seismology.Core.IO
{
    public class JsonSettings<T> : ISettingsReader<T>, ISettingsWriter<T>
    {
        private readonly string _filePath;

        public JsonSettings(string filePath)
        {
            _filePath = filePath;
        }

        /// <inheritdoc />
        public T Load() =>
            JsonConvert.DeserializeObject<T>(File.ReadAllText(_filePath, Encoding.UTF8));

        /// <inheritdoc />
        public void Save(T data) =>
            File.WriteAllText(_filePath, JsonConvert.SerializeObject(data), Encoding.UTF8);
    }
}