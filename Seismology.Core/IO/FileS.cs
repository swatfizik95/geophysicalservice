﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

[assembly: InternalsVisibleTo("SeismologyClassLibraryTests")]

namespace Seismology.Core.IO
{
    /// <summary>
    /// Реализует <see cref="IEarthquakeFile"/> для записи и чтения 
    /// Землетрясений форматов .txt, .csv, .wsg (.txt)
    /// </summary>
    public class FileS : IEarthquakeFile
    {
        private static readonly CultureInfo _invariantCulture = CultureInfo.InvariantCulture;

        #region Const

        #region Const for reading and writing

        // Константы для чтения и записи//
        private const int _SPACE = 6;
        private const int _SPACE_SIGN = 7;
        private const int DATE_SPACE = 40;

        private const int _LEN = 6;
        private const int T0_START = 3;
        private const int K_START = 18;
        private const int H_START = 28;
        private const int F_START = 38;
        private const int L_START = 48;
        private const int DT_START = 59;
        private const int DT_LEN = 7;
        private const int V_START = 74;

        private const int _STAN_LEN = 5;
        private const int NAME_START = 0;
        private const int NAME_LEN = 4;
        private const int EP_START = 16;
        private const int TP_START = 25;
        private const int ES_START = 16;
        private const int TS_START = 25;
        private const int AMPLZ_START = 69;
        private const int PERZ_START = 77;
        private const int AMPL_START = 2;
        private const int PER_START = 10;
        private const int T0_STAN_START = 48;
        private const int EPDIS_START = 61;
        private const int EPDIS_LEN = 7;
        private const int DT_STAN_START = 71;
        private const int K_STAN_START = 17;
        private const int V_STAN_START = 25;

        #endregion

        #region Const for writing

        // Константы для записи //
        private const int NAME_SPACE = -16;
        private const int EP_SPACE = -3;
        private const int TP_SPACE = 17;
        private const int AMPLZ_SPACE = 38;
        private const int AMPL_SPACE = 7;
        private const int ES_SPACE = -3;
        private const int TS_SPACE = 17;
        private const int DIFF_SPACE = 9;
        private const int T0_SPACE = 14;
        private const int EPDIS_SPACE = 9;
        private const int DT_SPACE = 10;
        private const int PER_SPACE = 7;
        private const int K_SPACE = 8;
        private const int V_SPACE = 8;

        #endregion
        
        #endregion

        #region Variables

        private readonly IStorageReader _storageReader;
        private readonly IStorageWriter _storageWriter;

        private struct QuakeData
        {
            public string FirstLine;
            public string SecondLine;
            public List<QuakeStationData> QuakeStationData;
        }
        private struct QuakeStationData
        {
            public string FirstLine;
            public string SecondLine;
            public string ThirdLine;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует пустой экземпляр <see cref="FileS"/>
        /// </summary>
        public FileS()
        {
            _storageReader = new StorageReader();
            _storageWriter = new StorageWriter();
        }

        /// <summary>
        /// Инициализирует экземпляр <see cref="FileS"/> 
        /// со следующими параметрами
        /// </summary>
        /// <param name="storageReader">Хранилище для чтения</param>
        /// <param name="storageWriter">Хранилище для записи</param>
        public FileS(IStorageReader storageReader, IStorageWriter storageWriter)
        {
            _storageReader = storageReader;
            _storageWriter = storageWriter;
        }

        #endregion

        #region Methods

        private IEnumerable<QuakeData> StorageRead(string filePath)
        {
            _storageReader.Open(filePath);
            string line = _storageReader.ReadLine();
            while (line != null)
            {
                QuakeData data;
                data.FirstLine = line;
                data.SecondLine = line = _storageReader.ReadLine();
                line = _storageReader.ReadLine();
                data.QuakeStationData = new List<QuakeStationData>();
                if (line == StationNotFound)
                    line = _storageReader.ReadLine();
                else
                {
                    while (line != null && !IsArrivalDate(line))
                    {
                        QuakeStationData stationData;
                        stationData.FirstLine = line;
                        stationData.SecondLine = line = _storageReader.ReadLine();
                        stationData.ThirdLine = line = _storageReader.ReadLine();
                        line = _storageReader.ReadLine();
                        data.QuakeStationData.Add(stationData);
                    }
                }

                yield return data;
            }
            _storageReader.Close();
        }

        #region Catalog

        /// <summary>
        /// Чтение землетрясений в виде каталога (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, по которому требуется выполнить чтение</param>
        /// <returns>Список землетрясений</returns>
        public IEnumerable<Earthquake> ReadCatalogTxt(string filePath)
        {
            int numeration = 0;
            return StorageRead(filePath).Select(
                data =>
                {
                    var date = GetDate(data.FirstLine);
                    var time = GetTime(data.SecondLine, T0_START);
                    return new Earthquake
                    {
                        Date = date,
                        T0 = date.Add(time),
                        K = GetNumber(data.SecondLine, K_START, _LEN),
                        H = GetNumber(data.SecondLine, H_START, _LEN),
                        Lat = GetNumber(data.SecondLine, F_START, _LEN),
                        Lng = GetNumber(data.SecondLine, L_START, _LEN),
                        Dt = GetNumber(data.SecondLine, DT_START, DT_LEN),
                        VpVs = GetNumber(data.SecondLine, V_START, _LEN),
                        Number = numeration++
                    };
                });
        }

        /// <summary>
        /// Запись землетрясений в виде каталога (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Список землетрясений, которые нужно записать</param>
        public void WriteCatalogTxt(string filePath, IEnumerable<Earthquake> earthquakes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var eq in earthquakes)
            {
                sb.AppendLine($"{eq.Date,DATE_SPACE:dd.MM.yyyy}");

                sb.Append($"0: {FormatTime(eq.T0),_SPACE};");
                sb.Append($" k={FormatF3(eq.K),_SPACE};");
                sb.Append($" h={FormatF3(eq.H),_SPACE};");
                sb.Append($" f={FormatF3(eq.Lat),_SPACE};");
                sb.Append($" l={FormatF3(eq.Lng),_SPACE};");
                sb.Append($" dt={FormatF3(eq.Dt),_SPACE_SIGN};");
                sb.AppendLine($" vp/vs={FormatF3(eq.VpVs),_SPACE}");
            }
            _storageWriter.Write(filePath, sb.ToString());
        }

        /// <summary>
        /// Запись землетрясение в виде каталога (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquake">Землетрясение</param>
        public void WriteCatalogTxt(string filePath, Earthquake earthquake)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"{earthquake.Date,DATE_SPACE:dd.MM.yyyy}");

            sb.Append($"0: {FormatTime(earthquake.T0),_SPACE};");
            sb.Append($" k={FormatF3(earthquake.K),_SPACE};");
            sb.Append($" h={FormatF3(earthquake.H),_SPACE};");
            sb.Append($" f={FormatF3(earthquake.Lat),_SPACE};");
            sb.Append($" l={FormatF3(earthquake.Lng),_SPACE};");
            sb.Append($" dt={FormatF3(earthquake.Dt),_SPACE_SIGN};");
            sb.AppendLine($" vp/vs={FormatF3(earthquake.VpVs),_SPACE}");
            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region Bulletin

        private const string StationNotFound = "Данных по станциям не обнаружено";

        /// <summary>
        /// Чтение землетрясений в виде бюллетеня (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, по которому требуется выполнить чтение</param>
        /// <returns>Список землетрясений</returns>
        public IEnumerable<Earthquake> ReadBulletinTxt(string filePath)
        {
            int numeration = 0;
            return StorageRead(filePath).Select(
                data =>
                {
                    var date = GetDate(data.FirstLine);
                    var time = GetTime(data.SecondLine, T0_START);
                    return new Earthquake
                    {
                        Date = date,
                        T0 = date.Add(time),
                        K = GetNumber(data.SecondLine, K_START, _LEN),
                        H = GetNumber(data.SecondLine, H_START, _LEN),
                        Lat = GetNumber(data.SecondLine, F_START, _LEN),
                        Lng = GetNumber(data.SecondLine, L_START, _LEN),
                        Dt = GetNumber(data.SecondLine, DT_START, DT_LEN),
                        VpVs = GetNumber(data.SecondLine, V_START, _LEN),
                        Number = numeration++,
                        Stations = data.QuakeStationData.Select(
                                stanData =>
                                {
                                    var tp = GetTime(stanData.FirstLine, TP_START);
                                    var ts = GetTime(stanData.SecondLine, TS_START);
                                    var t0 = GetTime(stanData.SecondLine, T0_STAN_START);
                                    return new EarthquakeStation
                                    {
                                        Name = GetText(stanData.FirstLine, NAME_START, NAME_LEN),
                                        Ep = GetText(stanData.FirstLine, EP_START, 3),
                                        Tp = tp > time ? date.Add(tp) : date.AddDays(1).Add(tp),
                                        AmplitudeZ = GetNumber(stanData.FirstLine, AMPLZ_START, _STAN_LEN),
                                        PeriodZ = GetNumber(stanData.FirstLine, PERZ_START, _STAN_LEN),

                                        Es = GetText(stanData.SecondLine, ES_START, 3),
                                        Ts = ts > time ? date.Add(ts) : date.AddDays(1).Add(ts),
                                        T0 = t0 > time ? date.Add(t0) : date.AddDays(1).Add(t0),
                                        EpicDistance = GetNumber(stanData.SecondLine, EPDIS_START, EPDIS_LEN),
                                        Dt = GetNumber(stanData.SecondLine, DT_STAN_START, DT_LEN),

                                        Amplitude = GetNumber(stanData.ThirdLine, AMPL_START, _STAN_LEN),
                                        Period = GetNumber(stanData.ThirdLine, PER_START, _STAN_LEN),
                                        K = GetNumber(stanData.ThirdLine, K_STAN_START, _LEN),
                                        VpVs = GetNumber(stanData.ThirdLine, V_STAN_START, _LEN)
                                    };
                                })
                            .ToList()
                    };
                });
        }

        /// <summary>
        /// Запись землетрясений в виде бюллетеня (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Список землетрясений, которые нужно записать</param>
        public void WriteBulletinTxt(string filePath, IEnumerable<Earthquake> earthquakes)
        {
            var sb = new StringBuilder();
            foreach (var eq in earthquakes)
            {
                sb.AppendLine($"{eq.Date,DATE_SPACE:dd.MM.yyyy}");

                sb.Append($"0: {FormatTime(eq.T0),_SPACE};");
                sb.Append($" k={FormatF3(eq.K),_SPACE};");
                sb.Append($" h={FormatF3(eq.H),_SPACE};");
                sb.Append($" f={FormatF3(eq.Lat),_SPACE};");
                sb.Append($" l={FormatF3(eq.Lng),_SPACE};");
                sb.Append($" dt= {FormatF3(eq.Dt),_SPACE};");
                sb.AppendLine($" vp/vs={FormatF3(eq.VpVs),_SPACE}");

                if (eq.Stations.Count == 0)
                    sb.AppendLine();
                else
                    foreach (var eqs in eq.Stations)
                    {
                        string stationName = eqs.Station?.Name ?? eqs.Name;
                        sb.Append($"{stationName,NAME_SPACE}");
                        sb.Append($"{eqs.Ep,EP_SPACE}");
                        sb.Append($"{FormatTime(eqs.Tp),TP_SPACE}");
                        sb.Append($"{FormatF3(eqs.AmplitudeZ),AMPLZ_SPACE}/");
                        sb.AppendLine($"{FormatF3(eqs.PeriodZ),PER_SPACE}");

                        sb.Append($"{string.Empty,NAME_SPACE}");
                        sb.Append($"{eqs.Es,ES_SPACE}");
                        sb.Append($"{FormatTime(eqs.Ts),TS_SPACE}");
                        sb.Append($"{FormatF2(eqs.Sp),DIFF_SPACE}");
                        sb.Append($"{FormatTime(eqs.T0),T0_SPACE}");
                        sb.Append($"{FormatF3(eqs.EpicDistance),EPDIS_SPACE}");
                        sb.AppendLine($"{FormatF3(eqs.Dt),DT_SPACE}");

                        sb.Append($"{FormatF3(eqs.Amplitude),AMPL_SPACE}/");
                        sb.Append($"{FormatF3(eqs.Period),PER_SPACE}");
                        sb.Append($"{FormatF3(eqs.K),K_SPACE}");
                        sb.AppendLine($"{FormatF3(eqs.VpVs),V_SPACE}");
                    }
            }
            sb.Length--;
            _storageWriter.Write(filePath, sb.ToString());
        }

        /// <summary>
        /// Запись землетрясений в виде бюллетеня (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquake">Землетрясение, которое нужно записать</param>
        public void WriteBulletinTxt(string filePath, Earthquake earthquake) => 
            WriteBulletinTxt(filePath, new[] { earthquake });

        #endregion

        #region BulletinIfz

        /// <summary>
        /// Запись землетрясений в виде бюллетеня ИФЗ (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Список землетрясений, которые нужно записать</param>
        public void WriteBulletinIfzTxt(string filePath, IEnumerable<Earthquake> earthquakes)
        {
            var sb = new StringBuilder();
            foreach (var eq in earthquakes)
            {
                string beginText = $"{eq.Date:yyyyMMdd}{eq.T0:HHmm}8," +
                                   $@"""{eq.Date:yyyyMMdd}""," +
                                   $"{eq.T0:HH,mm,ss.ff}," +
                                   $"{FormatF2(eq.Lat)},{FormatF2(eq.Lng)},{FormatF1(eq.H)},{FormatF2(eq.K)},";
                foreach (var eqs in eq.Stations)
                {
                    sb.Append(beginText);
                    string stationName;
                    if (eqs.Station != null)
                    {
                        // Используем интернациональное имя для бюллетеня ИФЗ
                        stationName = eqs.Station.InternationalName;
                    }
                    else
                    {
                        stationName = eqs.Name;
                        // Частные случаи
                        switch (stationName)
                        {
                            case "AKT":
                                stationName = "AHT";
                                break;
                            case "KMKR":
                                stationName = "KUM";
                                break;
                            case "GROC":
                                stationName = "GRO";
                                break;
                            case "DBK":
                                stationName = "DBK";
                                break;
                            default:
                                // Если есть R в конце, то удаляем(в ИФЗ не учитывается R - региональный)
                                if (stationName.Length == 4 && stationName[3] == 'R')
                                {
                                    stationName = stationName.Remove(3);
                                }

                                // Очень редкий частный случай
                                if (stationName == "HNZ")
                                    stationName = "XNZ";
                                break;
                        }
                    }

                    sb.Append($@"""{stationName}"",");
                    sb.Append($"{eqs.Tp:s.ff},");
                    sb.Append($"{eqs.Ts:s.ff},");
                    sb.Append($"{FormatF2(eqs.Sp)},");
                    sb.AppendLine($"{FormatF3(eqs.VpVs)}");
                }
            }

            sb.Length--;
            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region BulletinIfzDagestan

        /// <summary>
        /// Запись землетрясений в виде бюллетеня ИФЗ (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Список землетрясений, которые нужно записать</param>
        public void WriteBulletinIfzTxtDagestan(string filePath, IEnumerable<Earthquake> earthquakes)
        {
            // Если на территории Дагестана, то мы ограничиваем широту и долготу вхождения (41-45, 45-49)
            earthquakes = earthquakes.Where(eq => eq.Lat >= 41 && eq.Lat <= 45 && eq.Lng >= 45 && eq.Lng <= 49);
            WriteBulletinIfzTxt(filePath, earthquakes);
        }

        #endregion

        #region Format

        /// <summary>
        /// Возвращает форматированное значение числа
        /// </summary>
        /// <param name="number">Число</param>
        /// <returns>Форматированное число</returns>
        private string FormatF3(double number)
        {
            // 23,12341 ->  23.123
            // 4,23     ->   4.230
            return String.Format(_invariantCulture, "{0:F3}", number);
        }
        private string FormatF2(double number)
        {
            // 23,12341 ->   23.12
            // 4,23     ->    4.23
            return string.Format(CultureInfo.InvariantCulture, "{0:F2}", number);
        }
        private string FormatF1(double number)
        {
            // 23,12341 ->   23.1
            // 4,23     ->    4.2
            return string.Format(_invariantCulture, "{0:F1}", number);
        }

        private string FormatC2(double number)
        {
            // 23,12341 ->   23,12
            // 4,23     ->    4,23
            return string.Format(CultureInfo.CurrentCulture, "{0:F2}", number);
        }

        /// <summary>
        /// Возвращает форматированное значение времени (06.33.09:023 ->  6-33- 9:02)
        /// </summary>
        /// <param name="time">Время</param>
        /// <returns>Форматированное время</returns>
        private string FormatTime(DateTime time)
        {
            // 14.07.53:231 -> 14- 7-51:23
            // 06.33.09:023 ->  6-33- 9:02
            return $"{time.Hour,2}-{time.Minute,2}-{time.Second,2}.{time:ff}";
        }

        #endregion

        #region CSV JournalMonitoring

        /// <summary>
        /// Запись землетрясений в виде каталога (формат .csv) для Мониторинга Журнала
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="earthquakes"></param>
        /// <param name="delimeter"></param>
        public void WriteCsvCatalogJournalMonitoring(string filePath, IEnumerable<Earthquake> earthquakes, char delimeter = ';')
        {
            var sb = new StringBuilder();
            sb.Append($"№{delimeter}");
            sb.Append($"Data d m g{delimeter}");
            sb.Append($"t0{ delimeter}");
            sb.Append($"dt{delimeter}");
            sb.Append($"N{delimeter}");
            sb.Append($"E{delimeter}");
            sb.Append($"h{delimeter}");
            sb.Append($"VpkVs{delimeter}");
            sb.Append($"Kp{delimeter}");
            sb.Append($"M{delimeter}");
            sb.AppendLine($"n{delimeter}");
            int index = 1;
            foreach (var earthquake in earthquakes)
            {
                sb.Append($"{index++}{delimeter}");
                sb.Append($"{earthquake.Date:dd.MM.yy}{delimeter}");
                sb.Append($"{earthquake.T0:HH-mm-ss.ff}{delimeter}");
                sb.Append($"{FormatF2(earthquake.Dt)}{delimeter}");
                sb.Append($"{FormatF2(earthquake.Lat)}{delimeter}");
                sb.Append($"{FormatF2(earthquake.Lng)}{delimeter}");
                sb.Append($"{FormatF2(earthquake.H)}{delimeter}");
                sb.Append($"{FormatF2(earthquake.VpVs)}{delimeter}");
                sb.Append($"{FormatF2(earthquake.K)}{delimeter}");
                sb.Append($"{FormatF2((earthquake.K - 4) / 1.8)}{delimeter}");
                sb.AppendLine($"{earthquake.Stations.Count}{delimeter}");
            }
            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region Xlsx JournalMonitoring

        /// <summary>
        /// Запись землетрясений в виде каталога (формат .xlsx) для Мониторинга Журнала
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="earthquakes"></param>
        /// <param name="delimeter"></param>
        public void WriteXlsxCatalogJournalMonitoring(string filePath, IEnumerable<Earthquake> earthquakes)
        {
            string worksheetsName = "Новый Лист";
            var file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }

            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add(worksheetsName);
                worksheet.Cells[1, 1].Value = "№";
                worksheet.Cells[1, 2].Value = "Data d m g";
                worksheet.Cells[1, 3].Value = "t0";
                worksheet.Cells[1, 4].Value = "dt";
                worksheet.Cells[1, 5].Value = "N";
                worksheet.Cells[1, 6].Value = "E";
                worksheet.Cells[1, 7].Value = "h";
                worksheet.Cells[1, 8].Value = "VpkVs";
                worksheet.Cells[1, 9].Value = "Kp";
                worksheet.Cells[1, 10].Value = "M";
                worksheet.Cells[1, 11].Value = "n";
                int index = 1;
                int rowIndex = 2;
                foreach (var earthquake in earthquakes)
                {
                    worksheet.Cells[rowIndex, 1].Value = $"{index++}";
                    worksheet.Cells[rowIndex, 2].Value = $"{earthquake.Date:dd.MM.yy}";
                    worksheet.Cells[rowIndex, 3].Value = $"{earthquake.T0:HH-mm-ss.ff}";
                    worksheet.Cells[rowIndex, 4].Value = $"{FormatF2(earthquake.Dt)}";
                    worksheet.Cells[rowIndex, 5].Value = $"{FormatF2(earthquake.Lat)}";
                    worksheet.Cells[rowIndex, 6].Value = $"{FormatF2(earthquake.Lng)}";
                    worksheet.Cells[rowIndex, 7].Value = $"{FormatF2(earthquake.H)}";
                    worksheet.Cells[rowIndex, 8].Value = $"{FormatF2(earthquake.VpVs)}";
                    worksheet.Cells[rowIndex, 9].Value = $"{FormatF2(earthquake.K)}";
                    worksheet.Cells[rowIndex, 10].Value = $"{FormatF2((earthquake.K - 4) / 1.8)}";
                    worksheet.Cells[rowIndex, 11].Value = $"{earthquake.Stations.Count}";
                    rowIndex++;
                }

                package.Save();
            }
        }

        #endregion

        #region Excel Format

        private static readonly List<EarthquakeExcelFormat> ExcelFormat = new List<EarthquakeExcelFormat>
        {
            new EarthquakeExcelFormat("Номер", eq => $"{eq.Number}", (eq, s) => { eq.Number = ParseInt(s); }),
            new EarthquakeExcelFormat("Дата", eq => $"{eq.Date:dd.MM.yyyy}",
                (eq, s) => { eq.Date = DateTime.ParseExact(s, "dd.MM.yyyy", _invariantCulture); }),
            new EarthquakeExcelFormat("Год", eq => $"{eq.Date.Year}",
                (eq, s) => { eq.Date = eq.Date.ChangeYears(ParseInt(s)); }),
            new EarthquakeExcelFormat("Месяц", eq => $"{eq.Date.Month}",
                (eq, s) => { eq.Date = eq.Date.ChangeMonths(ParseInt(s)); }),
            new EarthquakeExcelFormat("День", eq => $"{eq.Date.Day}",
                (eq, s) => { eq.Date = eq.Date.ChangeDays(ParseInt(s)); }),
            new EarthquakeExcelFormat("Время", eq => $"{eq.T0:H-m-s.ff}",
                (eq, s) => { eq.T0 = DateTime.ParseExact(s, "H-m-s.ff", _invariantCulture); }),
            new EarthquakeExcelFormat("Час", eq => $"{eq.T0.Hour}",
                (eq, s) => { eq.T0 = eq.T0.ChangeHours(ParseInt(s)); }),
            new EarthquakeExcelFormat("Минуты", eq => $"{eq.T0.Minute}",
                (eq, s) => { eq.T0 = eq.T0.ChangeMinutes(ParseInt(s)); }),
            new EarthquakeExcelFormat("Секунды", eq => $"{eq.T0:s.ff}",
                (eq, s) => { eq.T0 = eq.T0.ChangeSeconds(ParseDouble(s)); }),
            new EarthquakeExcelFormat("Невязка", eq => $"{eq.Dt}", (eq, s) => { eq.Dt = ParseDouble(s); }),
            new EarthquakeExcelFormat("Среднее квадратическое Dt", eq => $"{eq.DtRms}", (eq, s) => { eq.DtRms = ParseDouble(s); }),
            new EarthquakeExcelFormat("Класс", eq => $"{eq.K}", (eq, s) => { eq.K = ParseDouble(s); }),
            new EarthquakeExcelFormat("Долгота", eq => $"{eq.Lng}", (eq, s) => { eq.Lng = ParseDouble(s); }),
            new EarthquakeExcelFormat("Долгота (погрешность)", eq => $"{eq.LngError}", (eq, s) => { eq.LngError = ParseDouble(s); }),
            new EarthquakeExcelFormat("Широта", eq => $"{eq.Lat}", (eq, s) => { eq.Lat = ParseDouble(s); }),
            new EarthquakeExcelFormat("Широта (погрешность)", eq => $"{eq.LatError}", (eq, s) => { eq.LatError = ParseDouble(s); }),
            new EarthquakeExcelFormat("Глубина", eq => $"{eq.H}", (eq, s) => { eq.H = ParseDouble(s); }),
            new EarthquakeExcelFormat("Скорость", eq => $"{eq.VpVs}", (eq, s) => { eq.VpVs = ParseDouble(s); }),
            new EarthquakeExcelFormat("Магнитуда", eq => $"{(eq.K - 4) / 1.8}",
                (eq, s) => { eq.K = ParseDouble(s) * 1.8 + 4; }),
            new EarthquakeExcelFormat("Количество станций", eq => $"{eq.Stations.Count}", (eq, s) => { }),
            new EarthquakeExcelFormat("Игнорировать", eq => string.Empty, (eq, s) => { }),
        };

        private static readonly List<EarthquakeStationExcelFormat> ExcelStationFormats = new List<EarthquakeStationExcelFormat>
        {
            new EarthquakeStationExcelFormat("Станция", s => s.Name),
            new EarthquakeStationExcelFormat("T0", s => $"{s.T0:H-m-s.ff}"),
            new EarthquakeStationExcelFormat("Tsp", s => $"{s.Sp}"),
            new EarthquakeStationExcelFormat("Tp", s => $"{s.Tp:H-m-s.ff}"),
            new EarthquakeStationExcelFormat("Ts", s => $"{s.Ts:H-m-s.ff}"),
            new EarthquakeStationExcelFormat("Tp-T0", s => $"{(s.Tp - s.T0).TotalSeconds}"),
            new EarthquakeStationExcelFormat("Ts-T0", s => $"{(s.Ts - s.T0).TotalSeconds}"),
            new EarthquakeStationExcelFormat("Dt", s => $"{s.Dt}"),
            new EarthquakeStationExcelFormat("Эпиц. расстояние", s => $"{s.EpicDistance}"),
            new EarthquakeStationExcelFormat("Az", s => $"{s.AmplitudeZ}"),
            new EarthquakeStationExcelFormat("Tz", s => $"{s.PeriodZ}"),
            new EarthquakeStationExcelFormat("A", s => $"{s.Amplitude}"),
            new EarthquakeStationExcelFormat("T", s => $"{s.Period}"),
            new EarthquakeStationExcelFormat("K", s => $"{s.K}"),
            new EarthquakeStationExcelFormat("Vps", s => $"{s.VpVs}"),
            new EarthquakeStationExcelFormat("Ep", s => $"{s.Ep}"),
            new EarthquakeStationExcelFormat("Es", s => $"{s.Es}"),
            new EarthquakeStationExcelFormat("Знак P", s =>
            {
                if (s.Ep.Contains("+")) return "+";
                if (s.Ep.Contains("-")) return "-";
                return string.Empty;
            }),
            new EarthquakeStationExcelFormat("Знак S", s =>
            {
                if (s.Es.Contains("+")) return "+";
                if (s.Es.Contains("-")) return "-";
                return string.Empty;
            }),
        };

        #endregion

        #region Excel Xlsx



        /// <summary>
        /// Возможные свойства каталога, которые могут быть в первой строке (для .csv и .excel)
        /// </summary>
        public static readonly string[] PossibleCatalogExcelFormats = ExcelFormat.Select(excelFormat => excelFormat.Name).ToArray();

        /// <summary>
        /// Возможные свойства  бюлетеня, которые могут быть в первой строке (для .csv и .excel)
        /// </summary>
        public static readonly string[] PossibleBulletinExcelFormats =
            ExcelStationFormats.Select(ef => ef.Name).Union(PossibleCatalogExcelFormats).ToArray();

        /// <summary>
        /// Запись землетрясений в виде каталога (формат .xlsx; .csv)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Землетрясение, которое нужно записать</param>
        /// <param name="formats">Форматы</param>
        /// <param name="formatsName">Наименования форматов</param>
        /// <param name="worksheetsName">Имя листа</param>
        public void WriteCatalogExcel(string filePath, IEnumerable<Earthquake> earthquakes, string[] formats,
            string[] formatsName, string worksheetsName = "Новый Лист")
        {
            var file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }

            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add(worksheetsName);
                for (int i = 0; i < formatsName.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = formatsName[i];
                }

                int number = 1;
                foreach (var earthquake in earthquakes)
                {
                    earthquake.Number = number++;
                    int index = 1;
                    foreach (var format in formats)
                    {
                        foreach (var excelFormat in ExcelFormat)
                        {
                            if (format == excelFormat.Name)
                            {
                                worksheet.Cells[number, index].Value = excelFormat.GetFormat(earthquake);
                                break;
                            }
                        }
                        index++;
                    }
                }

                package.Save();
            }
        }

        /// <summary>
        /// Запись землетрясений в виде бюллетеня (формат .xlsx; .csv)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Землетрясение, которое нужно записать</param>
        /// <param name="formats">Форматы</param>
        /// <param name="formatsName">Наименования форматов</param>
        /// <param name="worksheetsName">Имя листа</param>
        public void WriteBulletinExcel(string filePath, IEnumerable<Earthquake> earthquakes, string[] formats,
            string[] formatsName, string worksheetsName = "Новый Лист")
        {
            var file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }

            using (var excel = new ExcelPackage(file))
            {
                var worksheet = excel.Workbook.Worksheets.Add(worksheetsName);

                int startRow = 1;
                int startColumn = 1;
                int column = startColumn;
                int row = startRow;

                // Пишем заголовок
                foreach (var name in formatsName)
                {
                    worksheet.Cells[startRow, column].Value = name;
                    column++;
                }
                row++;

                int number = 1;
                foreach (var earthquake in earthquakes)
                {
                    earthquake.Number = number++;
                    foreach (var station in earthquake.Stations)
                    {
                        column = startColumn;
                        foreach (var format in formats)
                        {
                            var findStationFormat = ExcelStationFormats.FirstOrDefault(f => f.Name == format);
                            if (findStationFormat != null)
                            {
                                worksheet.Cells[row, column].Value = findStationFormat.GetFormat(station);
                            }
                            else
                            {
                                var findFormat = ExcelFormat.FirstOrDefault(f => f.Name == format);
                                if (findFormat != null)
                                {
                                    worksheet.Cells[row, column].Value = findFormat.GetFormat(earthquake);
                                }
                            }

                            column++;
                        }

                        row++;
                    }
                }

                excel.Save();
            }
        }

        public IEnumerable<Earthquake> ReadExcel(string filePath, string[] formats, bool isFirstTitle)
        {
            var file = new FileInfo(filePath);
            if (file.Exists)
            {
                using (var excel = new ExcelPackage(file))
                {
                    var worksheet = excel.Workbook.Worksheets.FirstOrDefault();
                    if (worksheet != null)
                    {
                        int rowStart = worksheet.Dimension.Start.Row;
                        if (isFirstTitle) rowStart++;
                        int rowEnd = worksheet.Dimension.End.Row;
                        int columnStart = worksheet.Dimension.Start.Column;
                        for (int row = rowStart; row <= rowEnd; row++)
                        {
                            int column = columnStart;
                            var earthquake = new Earthquake
                            {
                                Date = DateTime.Today,
                            };
                            foreach (var format in formats)
                            {
                                string value = worksheet.Cells[row, column].Text;
                                var findFormat = ExcelFormat.FirstOrDefault(f => f.Name == format);
                                findFormat?.SetValue(earthquake, value);
                                column++;
                            }

                            yield return earthquake;
                        }
                    }
                }
            }
        }

        #endregion

        #region Repeatability Period

        public void WriteRepeatabilityPeriod(string filePath, EarthquakeRepeatabilityPeriod repeatabilityPeriod)
        {
            string worksheetsName = "Новый Лист";
            var file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }

            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add(worksheetsName);

                worksheet.Cells[1, 1].Value = "№";
                worksheet.Cells[1, 2].Value = "Дата";
                worksheet.Cells[1, 3].Value = "Период";
                worksheet.Cells[1, 4].Value = "A";
                worksheet.Cells[1, 5].Value = "Y";

                int rowIndex = 2;

                for (var index = 0; index < repeatabilityPeriod.Repeatabilities.Count; index++)
                {
                    var repeatability = repeatabilityPeriod.Repeatabilities[index];
                    string periodTypeString = repeatabilityPeriod.Period.PeriodTypeString;

                    worksheet.Cells[rowIndex, 1].Value = $"{index + 1}";
                    worksheet.Cells[rowIndex, 2].Value = $"{repeatabilityPeriod.Period[index].ToString(periodTypeString)}";
                    worksheet.Cells[rowIndex, 3].Value = $"{repeatabilityPeriod.Period[index].ToString(periodTypeString)} - " +
                                                         $"{repeatabilityPeriod.Period[index + 1].ToString(periodTypeString)}";
                    worksheet.Cells[rowIndex, 4].Value = $"{repeatability.A}";
                    worksheet.Cells[rowIndex, 5].Value = $"{repeatability.K}";
                    rowIndex++;
                }

                package.Save();
            }
        }

        #endregion

        #region Csv

        public void WriteCatalogCsv(string filePath, IEnumerable<Earthquake> earthquakes, string[] formats,
            string[] formatsName, char delimiter = ';')
        {
            if (formats.Length != formatsName.Length)
            {
                throw new Exception("Длина формата и именований форматов должна быть равна!");
            }

            var sb = new StringBuilder();
            foreach (var name in formatsName)
            {
                sb.Append($"{name}{delimiter}");
            }

            sb.Length--;
            sb.AppendLine();
            int number = 1;
            foreach (var earthquake in earthquakes)
            {
                earthquake.Number = number++;
                foreach (var format in formats)
                {
                    foreach (var csvFormat in ExcelFormat)
                    {
                        if (format == csvFormat.Name)
                        {
                            sb.Append($"{csvFormat.GetFormat(earthquake)}{delimiter}");
                            break;
                        }
                    }
                }
                sb.Length--;
                sb.AppendLine();
            }
            sb.Length--;
            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region Wsg

        private const int WsgLatStart = 37;
        private const int WsgLngStart = 47;
        private const int WsgCoordLength = 7;
        private const int WsgDepthStart = 72;
        private const int WsgDepthLength = 4;

        /// <summary>
        /// Чтение WSG землетрясения
        /// </summary>
        /// <param name="filePath">Путь, по которому требуется выполнить чтение</param>
        /// <returns>WSG землетрясение</returns>
        public WsgEarthquake ReadWsgEarthquake(string filePath)
        {
            var wsgData = new WsgEarthquake();
            _storageReader.Open(filePath);
            string line = _storageReader.ReadLine();
            while (line != null && !line.StartsWith("Date"))
            {
                line = _storageReader.ReadLine();
            }

            line = _storageReader.ReadLine();
            if (line == null) return wsgData;
            var date =
                DateTime.ParseExact(
                    line.Substring(0, 22),
                    "yyyy/MM/dd HH:mm:ss.ff",
                    _invariantCulture);
            wsgData.DateAndTime = date.Date;
            var time = DateTime.MinValue
                .AddHours(date.Hour)
                .AddMinutes(date.Minute)
                .AddSeconds(date.Second)
                .AddMilliseconds(date.Millisecond);
            wsgData.Time = time;

            wsgData.Latitude = ParseDouble(line.Substring(WsgLatStart, WsgCoordLength).Trim());
            wsgData.Longitude = ParseDouble(line.Substring(WsgLngStart, WsgCoordLength).Trim());
            wsgData.Depth = ParseDouble(line.Substring(WsgDepthStart, WsgDepthLength).Trim());

            while (line != null && !line.StartsWith("Sta") && !line.EndsWith("ArrID"))
            {
                line = _storageReader.ReadLine();
            }

            line = _storageReader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                wsgData.AddPhase(ParseWsgPhase(line, date.Date, time));
                line = _storageReader.ReadLine();
            }
            _storageReader.Close();

            return wsgData;
        }

        #endregion

        #region EnergyClass

        /// <summary>
        /// Запись энергетического класса (формат .txt)
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="energy">Энергетический класс</param>
        public void WriteEnergyClass(string filePath, double energy)
        {
            var sb = new StringBuilder();

            sb.Append(FormatE1(energy));

            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region Distribution

        public void WriteDistribution(string filePath, EarthquakeDistribution distribution)
        {
            if (distribution == null)
            {
                _storageWriter.Write(filePath, " ");
                return;
            }

            var sb = new StringBuilder();

            const int tab = 6;
            const int mult = 4;

            sb.AppendLine($"{"Классы",40}");
            sb.Append($"{"Год",tab}{"Всего",tab}");
            foreach (var @class in distribution.Classes)
            {
                sb.Append($"{@class,tab}");
            }
            sb.AppendLine($"{"Энергия за год",tab * mult}");
            sb.AppendLine(new string('-', tab * 2 + tab * distribution.Classes.Length + tab * mult));
            foreach (var value in distribution.DistributionValues)
            {
                sb.Append($"{value.Year,tab}{value.ClassesCount.Sum(),tab}");
                foreach (var count in value.ClassesCount)
                {
                    sb.Append($"{count,tab}");
                }

                sb.Append($"{string.Empty,tab}");
                sb.AppendLine($"{FormatE(value.ClassesEnergy),-tab * (mult - 1)}");
            }

            sb.Append($"{"Итог",tab}{distribution.DistributionValues.Sum(v => v.ClassesCount.Sum()),tab}");

            for (var index = 0; index < distribution.Classes.Length; index++)
            {
                sb.Append($"{distribution.DistributionValues.Sum(v => v.ClassesCount[index]),tab}");
            }

            sb.Append($"{string.Empty,tab}");
            sb.AppendLine($"{FormatE(distribution.DistributionValues.Sum(v => v.ClassesEnergy)),-tab * (mult - 1)}");

            sb.AppendLine();
            sb.AppendLine();

            sb.AppendLine($"{"Глубины",40}");

            sb.Append($"{"Год",tab}{"Всего",tab}");
            for (var index = 0; index < distribution.Depths.Length - 1; index++)
            {
                sb.Append($"{$"{distribution.Depths[index]}-{distribution.Depths[index + 1]}",tab}");
            }
            sb.AppendLine($"{$"{distribution.Depths.Last()} <",tab}");

            sb.AppendLine(new string('-', tab * 2 + tab * distribution.Depths.Length));
            foreach (var value in distribution.DistributionValues)
            {
                sb.Append($"{value.Year,tab}{value.DepthsCount.Sum(),tab}");
                foreach (var count in value.DepthsCount)
                {
                    sb.Append($"{count,tab}");
                }

                sb.AppendLine();
            }

            sb.Append($"{"Итог",tab}{distribution.DistributionValues.Sum(v => v.DepthsCount.Sum()),tab}");
            for (var index = 0; index < distribution.Depths.Length; index++)
            {
                sb.Append($"{distribution.DistributionValues.Sum(v => v.DepthsCount[index]),tab}");
            }

            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region Surfer

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="scalars"></param>
        public void WriteScalarData(string filePath, IEnumerable<EarthquakeScalar> scalars)
        {
            var sb = new StringBuilder();
            foreach (var scalar in scalars)
            {
                sb.AppendLine($"{scalar.Coordinate.Lng.ToString("0.00", _invariantCulture)} {scalar.Coordinate.Lat.ToString("0.00", _invariantCulture)} {scalar.Value.ToString("0.###", _invariantCulture)}");
            }
            _storageWriter.Write(filePath, sb.ToString());
        }

        #endregion

        #region Parser

        /// <summary>
        /// Проверяет наличие даты формата dd.mm.yyyy в строке
        /// </summary>
        /// <param name="text">Строка</param>
        /// <returns>Результат проверки: true - присутствует дата, false - отсутствует дата</returns>
        private bool IsArrivalDate(string text)
        {
            // Регулярное выражение для определения записана ли дата
            // "'пробелы' 'дата' 'пробелы'" -> true
            return Regex.IsMatch(text, @"\s*\d{2}[.]\d{2}[.]\d{4}\s*");
        }

        /// <summary>
        /// Получает дату формата dd.mm.yyyy из строки
        /// </summary>
        /// <param name="text">Строка</param>
        /// <returns>Дата</returns>
        private DateTime GetDate(string text)
        {
            // Получаем дату и удаляем все пробелы
            return DateTime.ParseExact(text.Substring(30, 10), "dd.MM.yyyy", _invariantCulture);
        }

        private TimeSpan GetTime(string text, int start)
        {
            return TimeSpan.ParseExact(
                text
                    .Substring(start, 11)
                    .Replace(" ", "0"), // Удаляем пробелы
                "hh\\-mm\\-ss\\.ff",
                _invariantCulture);
        }

        /// <summary>
        /// Получаем число из строки
        /// </summary>
        /// <param name="text">Строка</param>
        /// <param name="start">Начало считывания</param>
        /// <param name="length">Количество символов</param>
        /// <returns>Число</returns>
        private double GetNumber(string text, int start, int length)
        {
            return double.Parse(
                text
                    .Substring(start, length)
                    .Replace(" ", ""), // Удаляем пробелы
                _invariantCulture);
        }

        /// <summary>
        /// Получает строку из строки
        /// </summary>
        /// <param name="text">Строка</param>
        /// <param name="start">Начало считывания</param>
        /// <param name="length">Количество символов</param>
        /// <returns>Название станции</returns>
        private string GetText(string text, int start, int length)
        {
            return text.Substring(start, length).Replace(" ", "");
        }

        /// <summary>
        /// Преобразует строку line в wsg фазу <see cref="WsgPhase"/>
        /// </summary>
        /// <param name="line">Строка</param>
        /// <param name="date">Дата</param>
        /// <param name="time">Время t0</param>
        /// <returns></returns>
        private WsgPhase ParseWsgPhase(string line, DateTime date, DateTime time)
        {
            var timeSpanPhase = TimeSpan.ParseExact(line.Substring(28, 11), "hh\\:mm\\:ss\\.ff", _invariantCulture);
            var timePhase = DateTime.MinValue.Add(timeSpanPhase);
            if (timePhase > time)
                timePhase = date.Date.Add(timeSpanPhase);
            else
                timePhase = date.Date.AddDays(1).Add(timeSpanPhase);
            var phase = new WsgPhase
            {
                Name = line.Substring(0, 4).Trim(),
                Dist = ParseDouble(line.Substring(6, _LEN)),
                EvAz = ParseDouble(line.Substring(12, _LEN)),
                Phaze = line.Substring(19, 9).Trim(),
                Time = timePhase,
                TRes = ParseDouble(line.Substring(40, _LEN)),
                Azim = ParseDouble(line.Substring(46, _LEN)),
                AzRes = line.Substring(53, _LEN).Trim(),
                Slow = line.Substring(59, _LEN).Trim(),
                SRes = line.Substring(66, _LEN).Trim(),
                Def = line.Substring(73, 3).Trim(),
                Amp = ParseDouble(line.Substring(82, 10)),
                Per = ParseDouble(line.Substring(92, _LEN)),
                Qual = line.Substring(99, 4).Trim(),
                Magnitude = ParseDouble(line.Substring(109, 4)),
                ArrID = ParseInt(line.Substring(115, 7))
            };
            return phase;
        }

        /// <summary>
        /// Преобразует строку в вещественное число
        /// </summary>
        /// <param name="line">Строка</param>
        /// <returns>Число</returns>
        private static double ParseDouble(string line)
        {
            if (double.TryParse(line, NumberStyles.Float, _invariantCulture, out var result))
            {
                return result;
            }

            if (double.TryParse(line.Replace(",", "."), NumberStyles.Float, _invariantCulture, out result))
            {
                return result;
            }

            return 0.0;
        }

        /// <summary>
        /// Преобразует строку line в целое число
        /// </summary>
        /// <param name="line">Строка</param>
        /// <returns>Число</returns>
        private static int ParseInt(string line)
        {
            return int.TryParse(line, out var result) ? result : 0;
        }

        #endregion

        #region Format

        private string FormatE(object value)
        {
            return string.Format(_invariantCulture, "{0:0.0000000000  E+0}", value);
        }

        private string FormatE1(object value)
        {
            return string.Format(_invariantCulture, "{0:0.0#########  E+0}", value);
        }

        #endregion

        #endregion
    }
}
