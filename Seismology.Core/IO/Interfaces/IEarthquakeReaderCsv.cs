﻿using System.Collections.Generic;

namespace Seismology.Core.IO
{
    public interface IEarthquakeReaderCsv
    {
        IEnumerable<Earthquake> ReadCsv(string filePath, string format, char delimiter);
    }
}
