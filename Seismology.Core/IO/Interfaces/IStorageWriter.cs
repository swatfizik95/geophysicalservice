﻿namespace Seismology.Core.IO
{
    public interface IStorageWriter
    {
        void Write(string filePath, string data);
    }
}
