﻿using System.Collections.Generic;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Представляет "писатель", который поддерживает простую
    /// запись землетрясений в форматах .txt, .csv
    /// </summary>
    interface IEarthquakeWriterTxt
    {
        /// <summary>
        /// Запись землетрясений в виде каталога в формате .txt
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Список землетрясений, которые нужно записать</param>
        void WriteCatalogTxt(string filePath, IEnumerable<Earthquake> earthquakes);

        /// <summary>
        /// Запись землетрясений в виде бюллетеня в формате .txt
        /// </summary>
        /// <param name="filePath">Путь, в который требуется выполнить запись</param>
        /// <param name="earthquakes">Список землетрясений, которые нужно записать</param>
        void WriteBulletinTxt(string filePath, IEnumerable<Earthquake> earthquakes);
    }
}
