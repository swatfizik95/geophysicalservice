﻿using System.Collections.Generic;

namespace Seismology.Core.IO
{
    public interface IEarthquakeWriterCsv
    {
        void WriteCsv(string filePath, IEnumerable<Earthquake> earthquakes, string format, char delimiter);
    }
}
