﻿namespace Seismology.Core.IO
{
    public interface IStorageReader
    {
        void Open(string filePath);
        string ReadLine();
        void Close();
    }
}
