﻿namespace Seismology.Core.IO.Interfaces
{
    public interface ISettingsReader<out T>
    {
        T Load();
    }
}