﻿namespace Seismology.Core.IO.Interfaces
{
    public interface ISettingsWriter<in T>
    {
        void Save(T data);
    }
}