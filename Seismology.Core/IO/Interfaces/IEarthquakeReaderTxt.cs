﻿using System.Collections.Generic;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Предоставляет "читатель", который поддерживает простое
    /// чтение землетрясений форматов .txt, .csv
    /// </summary>
    interface IEarthquakeReaderTxt
    {
        /// <summary>
        /// Чтение каталога замлетрясений формата .txt
        /// </summary>
        /// <param name="filePath">Путь, по которому требуется выполнить чтение</param>
        /// <returns></returns>
        IEnumerable<Earthquake> ReadCatalogTxt(string filePath);

        /// <summary>
        /// Чтение бюллетеня землетрясений формата .txt
        /// </summary>
        /// <param name="filePath">Путь, по которому требуется выполнить чтение</param>
        /// <returns></returns>
        IEnumerable<Earthquake> ReadBulletinTxt(string filePath);
    }
}
