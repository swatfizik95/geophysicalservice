﻿namespace Seismology.Core.IO
{
    /// <summary>
    /// Предоставляет "читатель" и "писатель", который поддерживает простое
    /// чтение и запись землетрясений форматов .txt, .csv
    /// </summary>
    interface IEarthquakeFile : IEarthquakeReaderTxt, IEarthquakeWriterTxt
    {
    }
}
