﻿using System;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Класс для создания формата вывода в экзель документ
    /// </summary>
    public class EarthquakeStationExcelFormat
    {
        /// <summary>
        /// Имя формата
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получаем нужные нам данные
        /// </summary>
        public Func<EarthquakeStation, string> GetFormat { get; set; }

        public EarthquakeStationExcelFormat(string name, Func<EarthquakeStation, string> format)
        {
            Name = name;
            GetFormat = format;
        }
    }
}