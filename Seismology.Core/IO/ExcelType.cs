﻿namespace Seismology.Core.IO
{
    public enum ExcelType
    {
        /// <summary>
        /// Тип xlsx
        /// </summary>
        Xlsx = 1,
        /// <summary>
        /// Тип csv
        /// </summary>
        Csv = 2
    }
}