﻿using System.IO;
using System.Text;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Хранилище, которое использует экземпляр <see cref="StreamWriter"/> для 
    /// реализации интерфейса <see cref="IStorageWriter"/>
    /// </summary>
    class StorageWriter : IStorageWriter
    {
        /// <summary>
        /// Записывает в заданный поток строку с данными
        /// </summary>
        /// <param name="filePath">Файл, в который нужно записать</param>
        /// <param name="data">Строка с данными</param>
        public void Write(string filePath, string data)
        {
            using (var sw = new StreamWriter(filePath, false, Encoding.UTF8))
            {
                sw.Write(data);
            }
        }
    }
}
