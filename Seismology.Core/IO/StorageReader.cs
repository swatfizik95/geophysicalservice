﻿using System.IO;
using System.Text;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Хранилище, которое использует экземпляр <see cref="StreamReader"/> для 
    /// реализации интерфейса <see cref="IStorageReader"/>
    /// </summary>
    class StorageReader : IStorageReader
    {
        private StreamReader _sr;

        /// <summary>
        /// Открывает поток, который инициализирует новый
        /// экземпляр класса <see cref="StreamReader"/> 
        /// для указанного файла
        /// </summary>
        /// <param name="filePath">Файл, который нужно считать</param>
        public void Open(string filePath)
        {
            _sr = new StreamReader(filePath, Encoding.UTF8);
        }

        /// <summary>
        /// Выполняет чтение строки и возвращает данные в
        /// виде строки
        /// </summary>
        /// <returns></returns>
        string IStorageReader.ReadLine()
        {
            return _sr.ReadLine();
        }

        /// <summary>
        /// Закрывает поток <see cref="StreamReader"/> 
        /// и освобождает все ресурсы , связанные с 
        /// устройством чтения
        /// </summary>
        public void Close()
        {
            _sr.Close();
        }
    }
}
