﻿using Seismology.Core.IO.Interfaces;
using System.IO;
using System.Xml.Serialization;

namespace Seismology.Core.IO
{
    /// <inheritdoc />
    public class XmlSettings<T> : ISettingsReader<T>, ISettingsWriter<T>
    {
        private readonly string _filePath;

        public XmlSettings(string filePath)
        {
            _filePath = filePath;
        }

        /// <inheritdoc />
        public T Load()
        {
            using (var fs = new FileStream(_filePath, FileMode.Open))
            {
                var xml = new XmlSerializer(typeof(T));
                return (T) xml.Deserialize(fs);
            }
        }

        /// <inheritdoc />
        public void Save(T data)
        {
            using (var fs = new FileStream(_filePath, FileMode.Create))
            {
                var xml = new XmlSerializer(typeof(T));
                xml.Serialize(fs, this);
            }
        }
    }
}