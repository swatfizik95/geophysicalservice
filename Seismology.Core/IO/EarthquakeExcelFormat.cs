﻿using System;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Класс для создания формата вывода в экзель документ
    /// </summary>
    public class EarthquakeExcelFormat
    {
        /// <summary>
        /// Имя формата
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получаем нужные нам данные
        /// </summary>
        public Func<Earthquake, string> GetFormat { get; set; }

        /// <summary>
        /// Устанавливаем значение
        /// </summary>
        public Action<Earthquake, string> SetValue { get; set; }

        public EarthquakeExcelFormat(string name, Func<Earthquake, string> getFormat, Action<Earthquake, string> setValue)
        {
            Name = name;
            GetFormat = getFormat;
            SetValue = setValue;
        }
    }
}