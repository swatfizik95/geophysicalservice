﻿using Seismology.Core.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.IO
{
    /// <summary>
    /// Хелпер для файловых операций
    /// </summary>
    public static class FileSHlp
    {
        #region Const (Filters)

        /// <summary>
        /// Фильтр для текстовых документов (.txt)
        /// </summary>
        public const string TxtFilter = "Текстовые документы (*.txt) | *.txt;";

        /// <summary>
        /// Фильтр для CSV документа (.csv)
        /// </summary>
        public const string CsvFilter = "CSV документ (*.csv) | *.csv;";

        /// <summary>
        /// Фильтр для Excel документа (..xlsx)
        /// </summary>
        public const string ExcelFilter = "Excel документ (*.xlsx) | *.xlsx;";

        /// <summary>
        /// Фильтр для все возможных типов документов (.txt, .csv, .xlsx)
        /// </summary>
        public const string DocFilter = "Документы (*.txt, *.csv, *.xlsx)|*.txt;*.csv;*.xlsx";

        #endregion

        #region Methods

        /// <summary>
        /// Возвращает полное имя формата "Каталог с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="earthquakes">Список землетрясений</param>
        /// <returns>Имя</returns>
        public static string GetCatalogFullName(IList<Earthquake> earthquakes) => GetFullName("Каталог", earthquakes);

        /// <summary>
        /// Возврает полное имя формата "Бюллетень с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="earthquakes">Список землетрясений</param>
        /// <returns>Имя</returns>
        public static string GetBulletinFullName(IList<Earthquake> earthquakes) => GetFullName("Бюллетень", earthquakes);

        /// <summary>
        /// Возврает полное имя формата "Бюллетень ИФЗ с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="earthquakes">Список землетрясений</param>
        /// <returns>Имя</returns>
        public static string GetBulletinIfzFullName(IList<Earthquake> earthquakes) => GetFullName("Бюллетень ИФЗ", earthquakes);

        /// <summary>
        /// Возвращает полное имя формата "Энергетический класс с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="earthquakes">Список землетрясений</param>
        /// <returns>Имя</returns>
        public static string GetEnergyClassFullName(IList<Earthquake> earthquakes) => GetFullName("Энергетический класс", earthquakes);

        /// <summary>
        /// Возвращает полное имя формата "Период повторяемости с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="repeatabilityPeriod">Период</param>
        /// <returns>Имя</returns>
        public static string GetRepeatabilityPeriodFullName(EarthquakeRepeatabilityPeriod repeatabilityPeriod) => $"Период повторяемости {GetPeriod(repeatabilityPeriod)}";

        /// <summary>
        /// Возвращает полное имя формата "Для Surfer (тип) с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="earthquakes"></param>
        /// <param name="scalarType"></param>
        /// <returns></returns>
        public static string GetSurferClassFullName(IList<Earthquake> earthquakes, EarthquakeScalarType scalarType) =>
            GetFullName($"Для Surfer ({scalarType.GetName()})", earthquakes);

        /// <summary>
        /// Возвращает полное имя формата "{<see cref="text"/>} с год-месяц-день по год-месяц-день"
        /// </summary>
        /// <param name="text"></param>
        /// <param name="earthquakes"></param>
        /// <returns></returns>
        public static string GetFullName(string text, IList<Earthquake> earthquakes)
        {
            if (earthquakes == null) throw new NullReferenceException("Значение было null");

            return $"{text.TrimEnd()} {GetPeriod(earthquakes)}";
        }

        /// <summary>
        /// Возвращает период
        /// </summary>
        /// <param name="earthquakes">Список землетрясений</param>
        /// <returns>Период</returns>
        public static string GetPeriod(IList<Earthquake> earthquakes)
        {
            if (!earthquakes.Any())
                return "Пустой";

            var firstDate = earthquakes.Min(e => e.Date).Date;
            var lastDate = earthquakes.Max(e => e.Date).Date;

            return GetPeriod(firstDate, lastDate);
        }

        /// <summary>
        /// Возвращает период
        /// </summary>
        /// <param name="repeatabilityPeriod">Период повторяемости</param>
        /// <returns>Период</returns>
        public static string GetPeriod(EarthquakeRepeatabilityPeriod repeatabilityPeriod) =>
            GetPeriod(repeatabilityPeriod.Period[0], repeatabilityPeriod.Period[repeatabilityPeriod.Period.Length - 1]);

        public static string GetPeriod(DateTime firstDate, DateTime lastDate)
        {
            string firstYear = string.Empty;
            string lastYear;
            if (firstDate.Year == lastDate.Year) lastYear = $" {lastDate:yyyy}";
            else
            {
                firstYear = $" {firstDate:yyyy}";
                lastYear = $" {lastDate:yyyy}";
            }

            string firstMonth = string.Empty;
            string lastMonth;
            if (firstDate.Month == lastDate.Month && string.IsNullOrEmpty(firstYear)) lastMonth = $" {firstDate:MMM}";
            else
            {
                firstMonth = $" {firstDate:MMM}";
                lastMonth = $" {lastDate:MMM}";
            }

            string firstDay = string.Empty;
            string lastDay;
            if (firstDate.Day == lastDate.Day && string.IsNullOrEmpty(firstMonth)) lastDay = $"{firstDate:dd}";
            else
            {
                firstDay = $"{firstDate:dd}";
                lastDay = $" - {lastDate:dd}";
            }

            return $"{firstDay}{firstMonth}{firstYear}{lastDay}{lastMonth}{lastYear}";
        }

        #endregion
    }
}