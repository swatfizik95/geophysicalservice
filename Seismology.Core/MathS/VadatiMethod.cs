﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Представляет метод Вадати вычисления эпицентра
    /// </summary>
    public class VadatiMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        /// <inheritdoc />
        public string Name { get; } = "Метод Вадати";

        /// <inheritdoc />
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <inheritdoc />
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            return Calculate(stations, depth, out var h);
        }

        public Epicenter Calculate(List<EarthquakeStation> stations, double depth, out double h)
        {
            if (stations.Count != 4)
            {
                throw new Exception("При вычислении методом Вадати количество станций не было равно 4м!");
            }

            stations = stations.OrderBy(s => s.Sp).ToList();

            var circles = new CircleS[3];
            var mArr = new double[3];
            var dArr = new double[3];

            var pairs = new (int Index1, int Index2)[]
            {
                (0, 1),
                (0, 2),
                (0, 3),
            };

            for (int index = 0; index < pairs.Length; index++)
            {
                var station1 = stations[pairs[index].Index1];
                var station2 = stations[pairs[index].Index2];

                double m = station1.Sp / station2.Sp;
                double m2 = Math.Pow(m, 2);

                //double d = Math.Sqrt(
                //    Math.Pow(station1.Station.Coordinate.Lng - station2.Station.Coordinate.Lng, 2) +
                //    Math.Pow(station1.Station.Coordinate.Lat - station2.Station.Coordinate.Lat, 2)
                //);

                double d = station1.Station.Coordinate.DistanceTo(station2.Station.Coordinate);

                circles[index] =
                    new CircleS(
                        (station1.Station.Coordinate.Lat - m2 * station2.Station.Coordinate.Lat) / (1 - m2),
                        (station1.Station.Coordinate.Lng - m2 * station2.Station.Coordinate.Lng) / (1 - m2),
                        m * d / (1 - m2));

                mArr[index] = m;
                dArr[index] = d;
            }

            var calc = new CalculatorS();

            var intersectionOfCircles = calc.FindIntersectionOfCircles(circles.ToList());

            if (intersectionOfCircles.Count < 2)
            {
                throw new Exception("Некорректно выбраны или метод расчета или станции (количество хорд < 2)\r\n" +
                                    $"Количество хорд = {intersectionOfCircles.Count}\r\n" +
                                    $"Для метода Вадати нужно минимум 2 или 3 хорды");
            }

            var intersections = calc.FindIntersectionOfSegments(intersectionOfCircles);

            if (intersections.Count == 0)
            {
                throw new Exception("Некорректно выбраны или метод расчета или станции (количество точек = 0)\r\n" +
                                    $"Количество точек = {intersections.Count}\r\n" +
                                    $"Для метода Вадати нужно минимум 1 точка (2 пересикающиеся хорды)");
            }

            var epicenter = new Epicenter
            {
                Lat = intersections.Average(i => i.Lat),
                Lng = intersections.Average(i => i.Lng)
            };

            var circle = circles[0];
            double distanceToEpicenter = circle.Centre.DistanceTo(epicenter.Lat, epicenter.Lng);
            h = Math.Sqrt(Math.Pow(circle.Radius, 2) - Math.Pow(distanceToEpicenter, 2));

            return epicenter;
        }
    }
}