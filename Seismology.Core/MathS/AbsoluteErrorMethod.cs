﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления абсолютной погрешности эпицентра
    /// </summary>
    public class AbsoluteErrorMethod : IEpicenterErrorCalculateMethod, ICoordinatesErrorCalculateMethod
    {
        /// <inheritdoc />
        public string Name { get; } = "Абсолютная погрешность";

        /// <inheritdoc />
        public double Calculate(IEnumerable<EarthquakeStation> stations, double lat, double lng)
        {
            return stations.Select(s => Math.Abs(s.EpicDistance - s.Station.Coordinate.DistanceTo(lat, lng))).Average();
        }

        /// <inheritdoc />
        public double Calculate(IEnumerable<EarthquakeStation> stations, Epicenter epicenter)
        {
            return Calculate(stations, epicenter.Lat, epicenter.Lng);
        }

        /// <inheritdoc />
        public void CalculateCoordinatesError(IEnumerable<EarthquakeStation> stations, double lat, double lng, out double latError, out double lngError)
        {
            latError = 0;
            lngError = 0;

            latError = stations.Select(s => Math.Abs(s.EpicDistance - s.Station.Coordinate.DistanceTo(s.Station.Coordinate.Lat, lng))).Average();
            lngError = stations.Select(s => Math.Abs(s.EpicDistance - s.Station.Coordinate.DistanceTo(lat, s.Station.Coordinate.Lng))).Average();
        }
    }
}