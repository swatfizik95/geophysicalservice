﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод Средних вычисления эпицентра
    /// </summary>
    public class BinaryMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        /// <summary>
        /// Имя метода
        /// </summary>
        public string Name { get; } = "Двоичный Метод";

        /// <summary>
        /// Метод, с помощью которого будет вычеслять погрешность при уточнении погрешности
        /// </summary>
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Вычисляет эпицентр на основании станционных данных
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            var beginCoord = new Epicenter();
            var endCoord = new Epicenter();
            var stepCoord = new Epicenter();

            beginCoord.Lat = stations.Select(s =>
                s.Station.Coordinate.Lat - Coordinate.KmToLat(s.EpicDistance)).Min();
            beginCoord.Lng = stations.Select(s =>
                s.Station.Coordinate.Lng - Coordinate.KmToLng(s.EpicDistance, s.Station.Coordinate.Lat)).Min();

            endCoord.Lat = stations.Select(s =>
                s.Station.Coordinate.Lat + Coordinate.KmToLat(s.EpicDistance)).Max();
            endCoord.Lng = stations.Select(s =>
                s.Station.Coordinate.Lng + Coordinate.KmToLng(s.EpicDistance, s.Station.Coordinate.Lat)).Max();

            stepCoord.Lat = (endCoord.Lat - beginCoord.Lat) / 2.0;
            stepCoord.Lng = (endCoord.Lng - beginCoord.Lng) / 2.0;

            double minErrorOnStep = double.PositiveInfinity;
            for (int step = 0; step < 100; step++)
            {
                // 1) Смотрим слева сверху '+
                var coord = new Epicenter();
                var updEndCoord = new Epicenter();
                var updStartCoord = new Epicenter();
                coord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                coord.Lng = beginCoord.Lng + stepCoord.Lng / 2.0;
                double errorOnStep = _errorCalculateMethod.Calculate(stations, coord);

                // Берем значения по дефолту
                updStartCoord.Lat = endCoord.Lat - stepCoord.Lat;
                updEndCoord.Lat = endCoord.Lat;
                updStartCoord.Lng = beginCoord.Lng;
                updEndCoord.Lng = beginCoord.Lng + stepCoord.Lng;

                // 2) Смотрим справа сверху +'
                coord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                coord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;
                double newError = _errorCalculateMethod.Calculate(stations, coord);
                // Если ошибка на новом положении меньше, то меняем значения
                if (newError < errorOnStep)
                {
                    errorOnStep = newError;
                    updStartCoord.Lat = endCoord.Lat - stepCoord.Lat;
                    updEndCoord.Lat = endCoord.Lat;
                    updStartCoord.Lng = endCoord.Lng - stepCoord.Lng;
                    updEndCoord.Lng = endCoord.Lng;
                }

                // 3) Смотрим слева снизу .+
                coord.Lat = beginCoord.Lat + stepCoord.Lat / 2.0;
                coord.Lng = beginCoord.Lng + stepCoord.Lng / 2.0;
                newError = _errorCalculateMethod.Calculate(stations, coord);
                if (newError < errorOnStep)
                {
                    errorOnStep = newError;
                    updStartCoord.Lat = beginCoord.Lat;
                    updEndCoord.Lat = beginCoord.Lat + stepCoord.Lat;
                    updStartCoord.Lng = beginCoord.Lng;
                    updEndCoord.Lng = beginCoord.Lng + stepCoord.Lng;
                }

                // 4) Смотрим справа снизу +.
                coord.Lat = beginCoord.Lat + stepCoord.Lat / 2.0;
                coord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;
                newError = _errorCalculateMethod.Calculate(stations, coord);
                if (newError < errorOnStep)
                {
                    errorOnStep = newError;
                    updStartCoord.Lat = beginCoord.Lat;
                    updEndCoord.Lat = beginCoord.Lat + stepCoord.Lat;
                    updStartCoord.Lng = endCoord.Lng - stepCoord.Lng;
                    updEndCoord.Lng = endCoord.Lng;
                }

                // Если ошибка на прошлом шаге больше, чем на нынешнем, то
                if (minErrorOnStep > errorOnStep)
                {
                    endCoord.Lat = updEndCoord.Lat;
                    beginCoord.Lat = updStartCoord.Lat;
                    beginCoord.Lng = updStartCoord.Lng;
                    endCoord.Lng = updEndCoord.Lng;

                    stepCoord.Lat = (endCoord.Lat - beginCoord.Lat) / 2.0;
                    stepCoord.Lng = (endCoord.Lng - beginCoord.Lng) / 2.0;

                    minErrorOnStep = errorOnStep;
                }
                // Иначе сужаем область поиска
                else
                {
                    endCoord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                    beginCoord.Lat = beginCoord.Lat + stepCoord.Lat / 2.0;
                    beginCoord.Lng = beginCoord.Lng + stepCoord.Lng / 2.0;
                    endCoord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;

                    stepCoord.Lat = (endCoord.Lat - beginCoord.Lat) / 2.0;
                    stepCoord.Lng = (endCoord.Lng - beginCoord.Lng) / 2.0;
                }
            }

            return new Epicenter
            {
                Lat = (endCoord.Lat + beginCoord.Lat) / 2.0,
                Lng = (endCoord.Lng + beginCoord.Lng) / 2.0
            };
        }
    }
}