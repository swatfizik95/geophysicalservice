﻿using Seismology.Core.Map;
using System.Collections.Generic;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления погрешности эпицентра
    /// </summary>
    public interface IEpicenterErrorCalculateMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Вычисляет погрешность нахождения эпицентра
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        /// <returns></returns>
        double Calculate(IEnumerable<EarthquakeStation> stations, double lat, double lng);

        /// <summary>
        /// Вычисляет погрешность нахождения эпицентра
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="epicenter">Эпицентр</param>
        /// <returns></returns>
        double Calculate(IEnumerable<EarthquakeStation> stations, Epicenter epicenter);
    }
}