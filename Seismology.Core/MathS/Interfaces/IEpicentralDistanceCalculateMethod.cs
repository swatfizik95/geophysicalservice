﻿using System;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления эпицентрального расстояния
    /// </summary>
    public interface IEpicentralDistanceCalculateMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Вычисляет эпицентральное расстояние
        /// </summary>
        /// <param name="station">Станционные данные</param>
        /// <param name="t0">Время t0</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        double Calculate(EarthquakeStation station, DateTime t0, double depth);

        /// <summary>
        /// Проверка на выход за границы дозволенного времени
        /// <para>true - если вышла за пределы, false - если внутри границ</para>
        /// </summary>
        /// <param name="station"></param>
        /// <param name="t0"></param>
        /// <returns></returns>
        bool IsInsideTime(EarthquakeStation station, DateTime t0, double depth);
    }
}