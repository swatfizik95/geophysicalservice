﻿using System;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет годограф, который вычисляет эпицентральное расстояние на основе ts-tp
    /// </summary>
    public class SpMethod : IEpicentralDistanceCalculateMethod
    {
        /// <inheritdoc />
        public string Name { get; } = "Метод Sp";

        /// <inheritdoc />
        public double Calculate(EarthquakeStation station, DateTime t0, double depth)
        {
            double sec = station.Sp;
            if (depth < 3.5)
                return 0.040168633 * Math.Pow(sec, 3) - 0.652912686 * Math.Pow(sec, 2) + 8.696383686 * sec -
                       5.133437934;
            if (depth < 6)
                return 0.274260796 * Math.Pow(sec, 3) - 3.500738150 * Math.Pow(sec, 2) + 19.857869531 * sec -
                       17.674823657;
            if (depth < 9)
                return 0.044097161 * Math.Pow(sec, 3) - 0.840491918 * Math.Pow(sec, 2) + 11.777812100 * sec -
                       13.214412375;
            if (depth < 13)
                return 0.071400829 * Math.Pow(sec, 3) - 1.543451549 * Math.Pow(sec, 2) + 17.883387215 * sec -
                       28.366314112;
            if (depth < 18)
                return 0.208844427 * Math.Pow(sec, 3) - 3.886804265 * Math.Pow(sec, 2) + 31.056542250 * sec -
                       54.153325953;
            if (depth < 23)
                return 0.025565304 * Math.Pow(sec, 3) - 0.753829716 * Math.Pow(sec, 2) + 15.565409894 * sec -
                       32.544392123;
            if (depth < 28)
                return 0.020876630 * Math.Pow(sec, 3) - 0.656018635 * Math.Pow(sec, 2) + 15.539610682 * sec -
                       34.105521926;
            if (depth < 33)
                return 0.031914802 * Math.Pow(sec, 3) - 1.058293168 * Math.Pow(sec, 2) + 20.702052877 * sec -
                       63.175275628;
            if (depth < 38)
                return 0.035863094 * Math.Pow(sec, 3) - 1.321194248 * Math.Pow(sec, 2) + 26.199153243 * sec -
                       100.401879366;
            if (depth < 43)
                return 0.035212876 * Math.Pow(sec, 3) - 1.130674723 * Math.Pow(sec, 2) + 22.700653155 * sec -
                       96.124663833;
            if (depth < 48)
                return 0.034248987 * Math.Pow(sec, 3) - 1.339987698 * Math.Pow(sec, 2) + 25.339408089 * sec -
                       101.815238915;
            if (depth < 53)
                return 0.024598453 * Math.Pow(sec, 3) - 1.013036713 * Math.Pow(sec, 2) + 21.298299820 * sec -
                       67.110376337;
            if (depth < 58)
                return 0.019949115 * Math.Pow(sec, 3) - 1.161087526 * Math.Pow(sec, 2) + 30.613725424 * sec -
                       149.978594349;
            if (depth < 63)
                return 0.058606625 * Math.Pow(sec, 3) - 2.831081093 * Math.Pow(sec, 2) + 51.920159956 * sec -
                       224.901906568;
            if (depth < 68)
                return 0.011105130 * Math.Pow(sec, 3) - 0.491824760 * Math.Pow(sec, 2) + 15.393929612 * sec -
                       80.236969788;
            if (depth < 73)
                return 0.025019645 * Math.Pow(sec, 3) - 1.475430382 * Math.Pow(sec, 2) + 37.175981571 * sec -
                       182.290230859;
            return 0.099991524 * Math.Pow(sec, 3) - 4.289225235 * Math.Pow(sec, 2) + 70.850198050 * sec - 340.031842236;
        }

        /// <inheritdoc />
        public bool IsInsideTime(EarthquakeStation station, DateTime t0, double depth)
        {
            double sec = station.Sp;
            if (depth < 3.5) return sec > 0.639 && sec < 9.0765;
            if (depth < 6) return sec > 1.1 && sec < 7.4984;
            if (depth < 9) return sec > 1.25 && sec < 10.6342;
            if (depth < 13) return sec > 1.81 && sec < 13.9804;
            if (depth < 18) return sec > 2.36 && sec < 7.7144;
            if (depth < 23) return sec > 2.38 && sec < 16.98592;
            if (depth < 28) return sec > 2.47 && sec < 17.59;
            if (depth < 33) return sec > 3.71 && sec < 18.46776;
            if (depth < 38) return sec > 4.92 && sec < 19.81248;
            if (depth < 43) return sec > 5.37 && sec < 20.462;
            if (depth < 48) return sec > 5.12 && sec < 24.00744;
            if (depth < 53) return sec > 3.78 && sec < 25.28109;
            if (depth < 58) return sec > 6.63 && sec < 28.54776;
            if (depth < 63) return sec > 6.86 && sec < 25.37875;
            if (depth < 68) return sec > 6.25 && sec < 27.21;
            if (depth < 73) return sec > 6.8 && sec < 27.404288;
            return sec > 7.78 && sec < 22.06783;
        }
    }
}
