﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Представляет метод Пересечений вычисления эпицентра
    /// </summary>
    public class IntersectionMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        /// <inheritdoc />
        public string Name { get; } = "Метод Пересечений";

        /// <inheritdoc />
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <inheritdoc />
        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            var sortedStations = stations.OrderBy(s => s.EpicDistance).ToList();

            // Невязка для предыдущей гулбины
            double error = double.PositiveInfinity;

            // Список пар точек пересечения для текущей глубины
            var intersections = new List<(Coordinate P1, Coordinate P2)>();
            var math = new CalculatorS();

            for (int i = 0; i < sortedStations.Count - 1; i++)
            {
                var point1 = sortedStations[i].Station.Coordinate;
                var radius1 = sortedStations[i].EpicDistance;

                for (int j = i + 1; j < sortedStations.Count; j++)
                {
                    var point2 = sortedStations[j].Station.Coordinate;
                    var radius2 = sortedStations[j].EpicDistance;

                    // Находим точки пересечения двух эллипсов
                    var res = math.FindEllipseIntersectionPoints(point1, radius1, point2, radius2);

                    // Если есть, то добавляем их
                    if (res.Count != 0)
                    {
                        intersections.Add((res[0], res[1]));
                    }
                }
            }

            var finalyPoints = new List<Coordinate>();

            foreach (var (p1, p2) in intersections)
            {
                double sum1 = 0.0;
                double sum2 = 0.0;
                foreach (var (p3, p4) in intersections)
                {
                    if (p3.Lat != -1 && p3.Lng != -1)
                    {
                        sum1 += p1.DistanceTo(p3);
                        sum2 += p2.DistanceTo(p3);
                    }

                    if (p4.Lat != -1 && p4.Lng != -1)
                    {
                        sum1 += p1.DistanceTo(p4);
                        sum2 += p2.DistanceTo(p4);
                    }
                }

                if (sum1 < sum2)
                {
                    p2.Lat = -1;
                    p2.Lng = -1;
                    finalyPoints.Add(new Coordinate(p1));
                }
                else
                {
                    p1.Lat = -1;
                    p1.Lng = -1;
                    finalyPoints.Add(new Coordinate(p2));
                }
            }

            if (finalyPoints.Count < 1)
            {
                return new Epicenter(0, 0);
            }

            return new Epicenter(finalyPoints.Average(p => p.Lat), finalyPoints.Average(p => p.Lng));
        }
    }
}
