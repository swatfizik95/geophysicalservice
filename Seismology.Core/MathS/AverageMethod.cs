﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод Средних вычисления эпицентра
    /// </summary>
    public class AverageMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        /// <inheritdoc />
        public string Name { get; } = "Метод Средних";

        /// <inheritdoc />
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Вычисляет эпицентр на основании станционных данных
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            // Невязка для предыдущей гулбины
            double error = double.PositiveInfinity;

            var calc = new CalculatorS();
            // Список пар точек пересечения для текущей глубины
            var intersections = calc.FindIntersectionOfCircles(stations);

            double a, b;
            /*
             * Создаем списки пересечений:
             * Для каждой точки находим среди всех пар точек ближайшие.
             */
            var pointSets = new List<List<Coordinate>>();
            foreach (var segment1 in intersections)
            {
                var pointSetP3 = new List<Coordinate>();
                var pointSetP4 = new List<Coordinate>();
                foreach (var segment2 in intersections)
                {
                    a = segment1.Start.DistanceTo(segment2.Start);
                    b = segment1.Start.DistanceTo(segment2.End);
                    pointSetP3.Add(new Coordinate(a < b ? segment2.Start : segment2.End));

                    a = segment1.End.DistanceTo(segment2.Start);
                    b = segment1.End.DistanceTo(segment2.End);
                    pointSetP4.Add(new Coordinate(a < b ? segment2.Start : segment2.End));
                }
                pointSets.Add(pointSetP3);
                pointSets.Add(pointSetP4);
            }


            var epicenter = new Epicenter();
            /*
             * Находим эпицентр по средним значениями intersections
             * сравниваем с предыдущим значением на невязку
             * если невязка меньше, сохраняем вместо предыдущего варианта.
             */
            foreach (var points in pointSets)
            {
                var epicenterOnPoints = new Epicenter()
                {
                    Lng = points.Average(p => p.Lng),
                    Lat = points.Average(p => p.Lat)
                };
                double errorOnPoints = _errorCalculateMethod.Calculate(stations, epicenterOnPoints);
                if (errorOnPoints < error)
                {
                    error = errorOnPoints;

                    epicenter.Lat = epicenterOnPoints.Lat;
                    epicenter.Lng = epicenterOnPoints.Lng;
                }
            }

            return epicenter;
        }
    }
}