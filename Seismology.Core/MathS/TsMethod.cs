﻿using System;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет годограф, который вычисляет эпицентральное расстояние на основе ts-t0
    /// </summary>
    public class TsMethod : IEpicentralDistanceCalculateMethod
    {
        /// <inheritdoc />
        public string Name { get; } = "Метод Ts";

        /// <inheritdoc />
        public double Calculate(EarthquakeStation station, DateTime t0, double depth)
        {
            // Разница времени ts и t0 (в секундах)
            double sec = (station.Ts - t0).TotalSeconds;
            if (depth < 3.5) return 0.001063687 * Math.Pow(sec, 3) - 0.052988050 * Math.Pow(sec, 2) + 3.153505017 * sec - 3.839588190;
            if (depth < 6) return 0.002775378 * Math.Pow(sec, 3) - 0.108088996 * Math.Pow(sec, 2) + 4.062379093 * sec - 7.832594365;
            if (depth < 9) return 0.002117786 * Math.Pow(sec, 3) - 0.098308154 * Math.Pow(sec, 2) + 4.370412322 * sec - 11.297581653;
            if (depth < 13) return 0.001885399 * Math.Pow(sec, 3) - 0.111157886 * Math.Pow(sec, 2) + 5.291007057 * sec - 19.401679817;
            if (depth < 18) return 0.006603036 * Math.Pow(sec, 3) - 0.319784779 * Math.Pow(sec, 2) + 8.402029048 * sec - 35.692655759;
            if (depth < 23) return 0.002448698 * Math.Pow(sec, 3) - 0.171615867 * Math.Pow(sec, 2) + 7.206592803 * sec - 36.913641474;
            if (depth < 28) return 0.004942042 * Math.Pow(sec, 3) - 0.336759293 * Math.Pow(sec, 2) + 10.811448078 * sec - 58.264881960;
            if (depth < 33) return 0.001686262 * Math.Pow(sec, 3) - 0.137460915 * Math.Pow(sec, 2) + 7.429620032 * sec - 52.223442907;
            if (depth < 38) return 0.004436584 * Math.Pow(sec, 3) - 0.342416651 * Math.Pow(sec, 2) + 12.836054706 * sec - 105.428355109;
            if (depth < 43) return 0.001691144 * Math.Pow(sec, 3) - 0.142168862 * Math.Pow(sec, 2) + 8.735129966 * sec - 98.386442224;
            if (depth < 48) return 0.001851346 * Math.Pow(sec, 3) - 0.174647708 * Math.Pow(sec, 2) + 9.074866391 * sec - 93.824940001;
            if (depth < 53) return 0.002396183 * Math.Pow(sec, 3) - 0.257460124 * Math.Pow(sec, 2) + 12.103918381 * sec - 99.074969858;
            if (depth < 58) return 0.001274349 * Math.Pow(sec, 3) - 0.173659195 * Math.Pow(sec, 2) + 11.256798429 * sec - 118.026876969;
            if (depth < 63) return 0.003386427 * Math.Pow(sec, 3) - 0.346439171 * Math.Pow(sec, 2) + 14.918748707 * sec - 127.586933802;
            if (depth < 68) return 0.002005062 * Math.Pow(sec, 3) - 0.198425969 * Math.Pow(sec, 2) + 9.898135886 * sec - 110.555530694;
            if (depth < 73) return 0.002431353 * Math.Pow(sec, 3) - 0.315638520 * Math.Pow(sec, 2) + 17.370092760 * sec - 196.074381899;
            return 0.005943899 * Math.Pow(sec, 3) - 0.656716372 * Math.Pow(sec, 2) + 28.355072683 * sec - 351.207113072;
        }

        /// <inheritdoc />
        public bool IsInsideTime(EarthquakeStation station, DateTime t0, double depth)
        {
            // Разница времени ts и t0 (в секундах)
            double sec = (station.Ts - t0).TotalSeconds;
            if (depth < 3.5) return sec > 1.26 && sec < 21.4975;
            if (depth < 6) return sec > 2.06 && sec < 22.364;
            if (depth < 9) return sec > 2.78 && sec < 25.1233;
            if (depth < 13) return sec > 4.01 && sec < 36.9428;
            if (depth < 18) return sec > 5.23 && sec < 27.253;
            if (depth < 23) return sec > 6.02 && sec < 41.0312;
            if (depth < 28) return sec > 6.89 && sec < 39.275;
            if (depth < 33) return sec > 8.24 && sec < 45.4112;
            if (depth < 38) return sec > 10.9 && sec < 44.10712;
            if (depth < 43) return sec > 13.8 && sec < 50.6;
            if (depth < 48) return sec > 12.9 && sec < 58.95832;
            if (depth < 53) return sec > 11.1 && sec < 60.65166;
            if (depth < 58) return sec > 13.7 && sec < 67.37552;
            if (depth < 63) return sec > 14.9 && sec < 49.03125;
            if (depth < 68) return sec > 14.7 && sec < 58.9;
            if (depth < 73) return sec > 19.6 && sec < 61.03744;
            return sec > 20.3 && sec < 53.3648;
        }
    }
}
