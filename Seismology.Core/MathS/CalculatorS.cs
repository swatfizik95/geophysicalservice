﻿using Seismology.Core.Map;
using Seismology.Core.Map.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Представляет математику для сейсмологии
    /// </summary>
    public class CalculatorS
    {
        /// <summary>
        /// Находит отношение скоростей и время в очаге с помощью МНК по данным прихода T и S волн
        /// </summary>
        /// <param name="earthquake">Данные об землетрясении</param>
        public double[][] FindVpVsAndT0(ref Earthquake earthquake)
        {
            var tpArr = earthquake.Stations.Select(s => s.Tp).ToArray();
            var tsArr = earthquake.Stations.Select(s => s.Ts).ToArray();

            if (tpArr.Length != tsArr.Length)
                throw new Exception("Размеры массивов не совпадают");
            int count = tpArr.Length;
            var minTp = tpArr.Min();
            var timeToCompare = new DateTime(
                minTp.Year,
                minTp.Month,
                minTp.Day,
                minTp.Hour,
                minTp.Minute,
                0,
                0);
            if (timeToCompare == minTp)
            {
                timeToCompare = timeToCompare.AddSeconds(-1);
            }
            var tpArrS = Enumerable
                .Range(0, count)
                .Select(i => (tpArr[i] - timeToCompare).TotalSeconds)
                .ToArray();
            var tsArrS = Enumerable
                .Range(0, count)
                .Select(i => (tsArr[i] - timeToCompare).TotalSeconds)
                .ToArray();
            var spArrS = Enumerable
                .Range(0, count)
                .Select(i => tsArrS[i] - tpArrS[i])
                .ToArray();
            double[] coeffP = OrdinaryLeastSquares(tpArrS, spArrS);
            double[] coeffS = OrdinaryLeastSquares(tsArrS, spArrS);
            double t0Ps = -1 * (coeffP[1] / coeffP[0]);
            double t0Ss = -1 * (coeffS[1] / coeffS[0]);
            double t0S = (t0Ps + t0Ss) / 2.0;

            earthquake.T0 = timeToCompare.AddSeconds(t0S);


            for (int index = 0; index < count; index++)
            {
                earthquake.Stations[index].VpVs = 1.0 + spArrS[index] / (tpArrS[index] - t0S);
                earthquake.Stations[index].T0 =
                    earthquake.Stations[index].Tp.AddSeconds(-earthquake.Stations[index].Sp / 0.73);
            }

            earthquake.VpVs = earthquake.Stations.Average(s => s.VpVs);

            return new[] { coeffP, coeffS };
        }

        /// <summary>
        /// МНК (Метод наименьнших квадратов)
        /// </summary>
        /// <param name="x">Значения X</param>
        /// <param name="y">Значения Y</param>
        /// <returns>Коэффициенты</returns>
        public double[] OrdinaryLeastSquares(double[] x, double[] y)
        {
            if (x.Length != y.Length)
                throw new Exception("Размеры массивов не совпадают");

            double xSum, ySum, x2Sum, xySum;
            xSum = ySum = x2Sum = xySum = 0;
            int n = x.Length;
            for (int i = 0; i < n; i++)
            {
                xSum += x[i];
                ySum += y[i];
                x2Sum += x[i] * x[i];
                xySum += x[i] * y[i];
            }

            double delta = xSum * xSum - n * x2Sum;
            double[] coefficients = new double[2];

            coefficients[0] = (ySum * xSum - xySum * n) / delta;
            coefficients[1] = (xSum * xySum - x2Sum * ySum) / delta;

            return coefficients;
        }

        /// <summary>
        /// Находит точку пересечения двух эллипсов на Земном Шаре численным методом
        /// </summary>
        /// <param name="c1">Координаты центра 1го эллипса</param>
        /// <param name="r1">Радиус (в км) 1го эллипса</param>
        /// <param name="c2">Координаты центра 2го эллипса</param>
        /// <param name="r2">Радиус (в км) 2го эллипса</param>
        /// <returns></returns>
        public List<Coordinate> FindEllipseIntersectionPoints(Coordinate c1, double r1, Coordinate c2, double r2)
        {
            var result = new List<Coordinate>();

            Coordinate center1;
            Coordinate center2;
            double radius1;
            double radius2;

            if (r1 > r2)
            {
                radius1 = r1;
                center1 = new Coordinate(c1);
                radius2 = r2;
                center2 = new Coordinate(c2);
            }
            else
            {
                radius1 = r2;
                center1 = new Coordinate(c2);
                radius2 = r1;
                center2 = new Coordinate(c1);
            }

            var radius = new Coordinate(Coordinate.KmToLat(radius1), Coordinate.KmToLng(radius1, center1.Lat));

            // Находим точку, в которая будет ближе всего к центру 2го эллипса
            double bestDistanceTo = double.PositiveInfinity;
            var bestPoint = new Coordinate(center1.Lat, center1.Lng);
            double degreeMax = 2 * Math.PI;
            double degreeStep = degreeMax * 5 / 360.0;
            double degreeCenter = 0.0;
            for (double degreeCurrent = 0; degreeCurrent < degreeMax; degreeCurrent += degreeStep)
            {
                var point = new Coordinate(
                    center1.Lat + radius.Lat * Math.Sin(degreeCurrent),
                    center1.Lng + radius.Lng * Math.Cos(degreeCurrent)
                );
                double distanceTo = center2.DistanceTo(point);
                if (distanceTo < bestDistanceTo)
                {
                    bestDistanceTo = distanceTo;
                    bestPoint.Lat = point.Lat;
                    bestPoint.Lng = point.Lng;
                    degreeCenter = degreeCurrent;
                }
            }

            if (radius2 - bestDistanceTo < -2.5)
            {
                return result;
            }

            // Ищем правую точку
            var rightPoint = new Coordinate();
            double bestError = double.PositiveInfinity;
            double degree = degreeCenter;
            degreeStep = degreeMax / 1440.0;
            while (bestError > 0.05)
            {
                var point = new Coordinate(
                    center1.Lat + radius.Lat * Math.Sin(degree),
                    center1.Lng + radius.Lng * Math.Cos(degree)
                );

                double error = Math.Abs(radius2 - center2.DistanceTo(point));
                if (error < bestError)
                {
                    bestError = error;
                    rightPoint.Lat = point.Lat;
                    rightPoint.Lng = point.Lng;
                }

                degree += degreeStep;
                if (degree > degreeCenter + Math.PI)
                {
                    break;
                }
            }
            result.Add(rightPoint);
            // Ищем левую точку
            var leftPoint = new Coordinate();
            bestError = double.PositiveInfinity;
            degree = degreeCenter;
            while (bestError > 0.05)
            {
                var point = new Coordinate(
                    center1.Lat + radius.Lat * Math.Sin(degree),
                    center1.Lng + radius.Lng * Math.Cos(degree)
                );
                double error = Math.Abs(radius2 - center2.DistanceTo(point));
                if (error < bestError)
                {
                    bestError = error;
                    leftPoint.Lat = point.Lat;
                    leftPoint.Lng = point.Lng;
                }

                degree -= degreeStep;
                if (degree < degreeCenter - Math.PI)
                {
                    break;
                }
            }
            result.Add(leftPoint);

            return result;
        }

        /// <summary>
        /// Находит энергетический класс землетрясений
        /// </summary>
        /// <param name="earthquakes"></param>
        /// <returns></returns>
        public double FindEnergyClass(IList<Earthquake> earthquakes)
        {
            if (earthquakes == null || earthquakes.Count < 1)
            {
                return 0;
            }

            int classMin = earthquakes.Min(eq => eq.Class);
            int classMax = earthquakes.Max(eq => eq.Class);

            var classes = Enumerable
                .Range(classMin, classMax - classMin + 1)
                .Select(x => x)
                .ToArray();

            int factor = classMin;
            double energy = 0.0;
            foreach (var @class in classes)
            {
                int count = earthquakes.Count(eq => eq.Class == @class);
                energy += count * Math.Pow(10, factor);
                factor++;
            }

            return energy;
        }

        public EarthquakeDistribution FindEnergyClasses(IList<Earthquake> earthquakes,
            int classMin, int classMax,
            int depthMin, int depthMax, int depthStep)
        {
            if (earthquakes == null || earthquakes.Count < 1)
            {
                return null;
            }

            int yearMin = earthquakes.Min(eq => eq.Date).Year;
            int yearMax = earthquakes.Max(eq => eq.Date).Year;

            //int classMin = earthquakes.Min(eq => eq.Class);
            //int classMax = earthquakes.Max(eq => eq.Class);

            int[] years = Enumerable
                .Range(0, yearMax - yearMin + 1)
                .Select(y => yearMin + y)
                .ToArray();

            int[] classes = Enumerable
                .Range(0, classMax - classMin + 1)
                .Select(c => classMin + c)
                .ToArray();

            int[] depths = Enumerable
                .Range(0, (depthMax - depthMin) / depthStep + 1)
                .Select(d => depthMin + d * depthStep)
                .ToArray();

            var distribution = new EarthquakeDistribution(years, classes, depths);

            for (var yearIndex = 0; yearIndex < years.Length; yearIndex++)
            {
                int year = years[yearIndex];
                var currentEarthquakes = earthquakes
                    .Where(eq => eq.Date.Year == year)
                    .ToList();

                var distributionValue = new EarthquakeDistributionValue(year, classes.Length, depths.Length);


                for (int classIndex = 1; classIndex < classes.Length; classIndex++)
                {
                    distributionValue.ClassesCount[classIndex] = currentEarthquakes
                        .Count(eq => eq.Class == classes[classIndex]);
                }

                distributionValue.ClassesCount[0] += currentEarthquakes
                    .Count(eq => eq.Class <= classes.First());
                distributionValue.ClassesCount[classes.Length - 1] += currentEarthquakes
                    .Count(eq => eq.Class >= classes.Last());

                double classesEnergy = 0.0;
                int factor = classMin;
                foreach (var count in distributionValue.ClassesCount)
                {
                    classesEnergy += count * Math.Pow(10, factor);
                    factor++;
                }
                distributionValue.ClassesEnergy = classesEnergy;

                for (int depthIndex = 1; depthIndex < depths.Length - 1; depthIndex++)
                {
                    distributionValue.DepthsCount[depthIndex] = currentEarthquakes
                        .Count(eq => eq.H > depths[depthIndex] && eq.H <= depths[depthIndex + 1]);
                }

                distributionValue.DepthsCount[0] =
                    currentEarthquakes.Count(eq => eq.H <= depths[1]);
                distributionValue.DepthsCount[depths.Length - 1] =
                    currentEarthquakes.Count(eq => eq.H > depths.Last());

                distribution.DistributionValues[yearIndex] = distributionValue;
            }

            return distribution;
        }

        public EarthquakeRepeatabilityPeriod FindChartRepeatabilityPeriod(IList<Earthquake> earthquakes, IAreaFigureS figure, DateTimeIntervalType periodType, int periodValue, int a, int kBegin = 7)
        {
            if (earthquakes == null || earthquakes.Count < 1)
            {
                return null;
            }

            var earthquakesForFind = earthquakes.Where(eq => eq.Class >= kBegin).ToList();

            var dateMin = earthquakesForFind.Min(eq => eq.Date);
            var dateMax = earthquakesForFind.Max(eq => eq.Date).AddDays(1);

            var period = new Period(dateMin, dateMax, periodType, periodValue);

            var repeatabilityPeriod = new EarthquakeRepeatabilityPeriod
            {
                Period = period
            };

            for (int index = 0; index < period.Length - 1; index++)
            {
                var earthquakesCurrent = earthquakesForFind.Where(eq => eq.DateTime >= period[index] && eq.DateTime <= period[index + 1]).ToList();

                if (earthquakesCurrent.Count >= 1)
                {
                    var classesDistinct = earthquakesCurrent
                        .Select(eq => eq.Class)
                        .Distinct()
                        .OrderBy(c => c)
                        .ToList();

                    int[] classesCalculate = classesDistinct.Where(c => c >= kBegin).ToArray();
                    int[] classes = classesDistinct.Where(c => c >= kBegin).ToArray();

                    double periodRatio = FindPeriodRatio(earthquakesCurrent);
                    double area = figure.CalculateArea();

                    var repeatability = new ClassRepeatability(earthquakesCurrent, classes, periodRatio, area);
                    var repeatabilityCalculate = new ClassRepeatability(earthquakesCurrent, classesCalculate, periodRatio, area);

                    var result = new EarthquakeRepeatability(repeatability, repeatabilityCalculate);
                    result.Date = period[index];
                    double y = result.K;
                    result.A = (1 - Math.Pow(10, -Math.Abs(y))) / Math.Pow(10, -Math.Abs(y) * (kBegin - a)) * (1000.0 / (periodRatio * area)) * earthquakesCurrent.Count;

                    repeatabilityPeriod.Repeatabilities.Add(result);
                }
            }

            return repeatabilityPeriod;
        }
        public EarthquakeRepeatability FindChartRepeatability(IList<Earthquake> earthquakes, IAreaFigureS figure, int kBegin = 7, int kBeginCalculate = 7)
        {
            if (earthquakes == null || earthquakes.Count < 1)
            {
                return null;
            }

            var earthquakesForFind = earthquakes.Where(eq => eq.Class >= kBegin).ToList();

            var classesDistinct = earthquakesForFind
                .Select(eq => eq.Class)
                .Distinct()
                .OrderBy(c => c)
                .ToList();

            int[] classesCalculate = classesDistinct.Where(c => c >= kBeginCalculate).ToArray();
            int[] classes = classesDistinct.Where(c => c >= kBegin).ToArray();

            double periodRatio = FindPeriodRatio(earthquakesForFind);
            double area = figure.CalculateArea();

            var repeatability = new ClassRepeatability(earthquakesForFind, classes, periodRatio, area);
            var repeatabilityCalculate = new ClassRepeatability(earthquakesForFind, classesCalculate, periodRatio, area);

            return new EarthquakeRepeatability(repeatability, repeatabilityCalculate);
        }

        #region Intersection

        /// <summary>
        /// Находит точки пересечения отрезков
        /// </summary>
        /// <param name="segments"></param>
        /// <returns></returns>
        public List<Coordinate> FindIntersectionOfSegments(List<SegmentS> segments)
        {
            var intersections = new List<Coordinate>();

            for (int i = 0; i < segments.Count - 1; i++)
            {
                for (int j = i + 1; j < segments.Count; j++)
                {
                    var coordinate = segments[i].FindIntersection(segments[j]);
                    if (coordinate != null)
                    {
                        intersections.Add(coordinate);
                    }
                }
            }

            return intersections;
        }

        /// <summary>
        /// Находит точки пересечения (хорды) окружностей
        /// </summary>
        /// <param name="stations"></param>
        /// <returns></returns>
        public List<SegmentS> FindIntersectionOfCircles(List<EarthquakeStation> stations)
        {
            return FindIntersectionOfCircles(stations.Select(
                s => new CircleS(s.Station.Coordinate, s.EpicDistance)).ToList());
        }

        /// <summary>
        /// Находит точки пересечения (хорды) окружностей
        /// </summary>
        /// <param name="circles"></param>
        /// <returns></returns>
        public List<SegmentS> FindIntersectionOfCircles(List<CircleS> circles)
        {
            // Список пар точек пересечения для текущей глубины
            var intersections = new List<SegmentS>();

            double a, b;
            for (int i = 0; i < circles.Count - 1; i++)
            {
                var p1 = circles[i].Centre;
                if (p1 == null) continue;
                double r1 = circles[i].Radius;
                for (int j = i + 1; j < circles.Count; j++)
                {
                    var p2 = circles[j].Centre;
                    if (p2 == null) continue;
                    double dist = p1.DistanceTo(p2);
                    double r2 = circles[j].Radius;
                    if (r1 + r2 <= dist || Math.Abs(r1 - r2) > dist) continue;

                    // Следуя нашей статье, находим две точки пересечения окружностей,
                    b = (Math.Pow(r2, 2) - Math.Pow(r1, 2) + Math.Pow(dist, 2)) / (2 * dist);
                    a = dist - b;
                    double hh = Math.Sqrt(Math.Pow(r2, 2) - Math.Pow(b, 2));
                    var p0 = new Coordinate
                    {
                        Lat = p1.Lat + a / dist * (p2.Lat - p1.Lat),
                        Lng = p1.Lng + a / dist * (p2.Lng - p1.Lng)
                    };
                    var p3 = new Coordinate
                    {
                        Lat = p0.Lat - (p2.Lng - p1.Lng) / dist * hh,
                        Lng = p0.Lng + (p2.Lat - p1.Lat) / dist * hh
                    };
                    var p4 = new Coordinate
                    {
                        Lat = p0.Lat + (p2.Lng - p1.Lng) / dist * hh,
                        Lng = p0.Lng - (p2.Lat - p1.Lat) / dist * hh
                    };

                    intersections.Add(new SegmentS(p3, p4));
                }
            }

            return intersections;
        }

        #endregion

        #region Surfer

        /// <summary>
        /// Минимальный класс для активности
        /// </summary>
        private const int ActivityClassMin = 7;

        /// <summary>
        /// Находит скалярное значение
        /// </summary>
        /// <param name="earthquakes"></param>
        /// <param name="latStart"></param>
        /// <param name="latEnd"></param>
        /// <param name="latCount"></param>
        /// <param name="lngStart"></param>
        /// <param name="lngEnd"></param>
        /// <param name="lngCount"></param>
        /// <param name="scalarType"></param>
        /// <param name="inputParams"></param>
        /// <returns></returns>
        public IEnumerable<EarthquakeScalar> FindScalars(List<Earthquake> earthquakes,
            double latStart, double latEnd, int latCount,
            double lngStart, double lngEnd, int lngCount,
            EarthquakeScalarType scalarType, params decimal[] inputParams)
        {
            var currentEarthquakes = earthquakes
                .Where(eq =>
                    eq.Lat >= 41 &&
                    eq.Lat <= 45 &&
                    eq.Lng >= 45 &&
                    eq.Lng <= 49)
                .ToList();

            var first = earthquakes.FirstOrDefault();

            double gamma = 0;
            double period = 0;
            switch (scalarType)
            {
                case EarthquakeScalarType.Activity:
                    currentEarthquakes = currentEarthquakes
                        .Where(eq => eq.Class >= ActivityClassMin)
                        .ToList();
                    gamma = FindGamma(currentEarthquakes);
                    period = FindPeriodRatio(currentEarthquakes);
                    break;
            }

            double latStep = (latEnd - latStart) / latCount;
            double lngStep = (lngEnd - lngStart) / lngCount;
            for (int latIndex = 0; latIndex < latCount; latIndex++)
            {
                double latIntervalStart = latStart + latStep * latIndex;
                double latIntervalEnd = latIntervalStart + latStep;
                for (int lngIndex = 0; lngIndex < lngCount; lngIndex++)
                {
                    double lngIntervalStart = lngStart + lngStep * lngIndex;
                    double lngIntervalEnd = lngIntervalStart + lngStep;
                    var scalar = new EarthquakeScalar
                    {
                        Coordinate = new Coordinate((latIntervalEnd + latIntervalStart) / 2.0, (lngIntervalEnd + lngIntervalStart) / 2.0)
                    };
                    var filtered = currentEarthquakes.Where(eq =>
                        eq.Lat > latIntervalStart &&
                        eq.Lat <= latIntervalEnd &&
                        eq.Lng > lngIntervalStart &&
                        eq.Lng <= lngIntervalEnd);

                    switch (scalarType)
                    {
                        case EarthquakeScalarType.Density:
                            scalar.Value = filtered.Count();
                            break;
                        case EarthquakeScalarType.VpVs:
                            scalar.Value = filtered.Select(eq => eq.VpVs).DefaultIfEmpty(0).Average();
                            break;
                        case EarthquakeScalarType.StationVpVs:
                            scalar.Value = filtered.SelectMany(eq => eq.Stations.Select(s => s.VpVs))
                                .DefaultIfEmpty(0)
                                .Average();
                            break;
                        case EarthquakeScalarType.Activity:
                            int count = (int)inputParams[0];
                            int class0 = (int)inputParams[1];
                            scalar.Value = FindActivity(currentEarthquakes, scalar.Coordinate.Lat, scalar.Coordinate.Lng, count, class0, gamma, period);
                            break;
                        case EarthquakeScalarType.ScoreField:
                            scalar.Value = first == null ? 0 : FindScoreField(first, scalar.Coordinate.DistanceTo(first.Lat, first.Lng));
                            break;
                    }

                    yield return scalar;
                }
            }
        }

        private double FindGamma(List<Earthquake> earthquakes)
        {
            int[] classArr = earthquakes
                .Where(eq => eq.Class >= ActivityClassMin)
                .Select(eq => eq.Class)
                .Distinct()
                .OrderBy(c => c)
                .ToArray();

            for (int index = 1; index < classArr.Length; index++)
            {
                if (classArr[index - 1] + 2 < classArr[index])
                {
                    classArr = classArr.Take(index).ToArray();
                    break;
                }
            }

            int length = classArr.Length;
            int[] countArr = new int[length];
            int[] squareClassArr = new int[length];
            double[] lgCountArr = new double[length];
            double[] lgClassCountArr = new double[length];

            for (int index = 0; index < length; index++)
            {
                countArr[index] = earthquakes.Count(e => e.Class == classArr[index]);
                squareClassArr[index] = classArr[index] * classArr[index];
                lgCountArr[index] = Math.Log10(countArr[index]);
                lgClassCountArr[index] = classArr[index] * lgCountArr[index];
            }

            int classSum = classArr.Sum();
            int squareClassSum = squareClassArr.Sum();
            double lgCountSum = lgCountArr.Sum();
            double lgClassCountSum = lgClassCountArr.Sum();

            return (length * lgClassCountSum - classSum * lgCountSum) / (length * squareClassSum - classSum * classSum);
        }

        private double FindPeriodRatio(IList<Earthquake> earthquakes)
        {
            var dateMin = earthquakes.Min(eq => eq.Date);
            var dateMax = earthquakes.Max(eq => eq.Date);

            if (dateMin == dateMax)
            {
                dateMax = dateMax.AddDays(1);
            }

            int days = (dateMax - dateMin).Days;

            if (days == 364 || days == 365)
            {
                return 1;
            }

            return days / 365.0;
        }

        /// <summary>
        /// Находит активность
        /// </summary>
        /// <param name="earthquakes"></param>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <param name="n"></param>
        /// <param name="k0"></param>
        /// <param name="g"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private double FindActivity(List<Earthquake> earthquakes, double lat, double lng, int n, int k0, double g, double t)
        {
            double radiusStep = 1;
            const double radiusStart = 1;
            const double aMin = 0.01;
            var circle = new CircleS(lat, lng, radiusStart);
            double mult = 1000 * n * Math.Pow(10, Math.Abs(g) * (ActivityClassMin - k0)) / (Math.PI * t);
            while (true)
            {
                int count = earthquakes.Count(eq => circle.IsInside(eq.Lat, eq.Lng));
                double a = mult / Math.Pow(circle.Radius, 2);

                if (a < aMin)
                {
                    return 0;
                }

                #region check count

                if (count == n)
                {
                    return a;
                }
                if (count < n)
                {
                    circle.Radius += radiusStep;
                }
                else if (count > n)
                {
                    radiusStep /= 2;
                    circle.Radius -= radiusStep;
                }

                #endregion
            }
        }

        /// <summary>
        /// Находит поле бальности
        /// </summary>
        /// <param name="earthquake"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public double FindScoreField(Earthquake earthquake, double distance) =>
            1.5 * earthquake.Magnitude -
            3.9 * Math.Log10(Math.Sqrt(Math.Pow(distance, 2) + Math.Pow(earthquake.H, 2))) + 3.2;
        
        #endregion

        /// <summary>
        /// Возвращает строку линейного уравнения
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public string LinearEquationFormat(string y, string x, double k, double b)
        {
            char sign = Math.Sign(b) == 0 ? '+' : '-';
            return $"{y} = {k:F3} {x} {sign} {Math.Abs(b):F3}";
        }

        /// <summary>
        /// Находит класс
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public static int CalculateClass(double k)
        {
            int min = 1;
            int max = 20;
            double border = 0.5;

            if (k < min + border)
            {
                return min;
            }

            if (k > max + border)
            {
                return max;
            }

            int whole = (int)Math.Truncate(k);
            double fractional = k - whole;

            return fractional < 0.5 ? whole : whole + 1;
        }

        /// <summary>
        /// Находит Класс (для тестов)
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public static int CalculateClassForTest(double k)
        {
            int min = 1;
            int max = 20;
            double border = 0.5;

            if (k < min + border)
            {
                return min;
            }

            for (int index = min; index < max; index++)
            {
                if (k < index + border)
                {
                    return index;
                }
            }

            return max;
        }

        /// <summary>
        /// Находит Магнитуду
        /// </summary>
        /// <param name="class"></param>
        /// <returns></returns>
        public static double CalculateMagnitude(int @class) => (@class - 4) / 1.8;
    }
}