﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет Мульти метод вычисления эпицентра
    /// </summary>
    public class MultiMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        /// <inheritdoc />
        public string Name { get; } = "Мульти Метод";

        /// <inheritdoc />
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <inheritdoc />
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            // Невязка для предыдущей гулбины
            var error = double.PositiveInfinity;

            var calc = new CalculatorS();
            // Список пар точек пересечения для текущей глубины
            var intersections = calc.FindIntersectionOfCircles(stations);

            double a, b;
            /*
             * Создаем списки пересечений:
             * Для каждой точки находим среди всех пар точек ближайшие.
             */
            var pointSets = new List<List<Coordinate>>();
            foreach (var segment1 in intersections)
            {
                var pointSetP3 = new List<Coordinate>();
                var pointSetP4 = new List<Coordinate>();
                foreach (var segment2 in intersections)
                {
                    a = segment1.Start.DistanceTo(segment2.Start);
                    b = segment1.Start.DistanceTo(segment2.End);
                    pointSetP3.Add(new Coordinate(a < b ? segment2.Start : segment2.End));

                    a = segment1.End.DistanceTo(segment2.Start);
                    b = segment1.End.DistanceTo(segment2.End);
                    pointSetP4.Add(new Coordinate(a < b ? segment2.Start : segment2.End));
                }
                pointSets.Add(pointSetP3);
                pointSets.Add(pointSetP4);
            }


            var epicenter = new Epicenter();
            /*
             * Находим эпицентр по средним значениями intersections
             * сравниваем с предыдущим значением на невязку
             * если невязка меньше, сохраняем вместо предыдущего варианта.
             */
            foreach (var points in pointSets)
            {
                var hypocentreOnPoints = new Epicenter()
                {
                    Lng = points.Average(p => p.Lng),
                    Lat = points.Average(p => p.Lat)
                };
                double errorOnPoints = RefinementError(stations, ref hypocentreOnPoints);
                if (errorOnPoints < error)
                {
                    error = errorOnPoints;

                    epicenter.Lat = hypocentreOnPoints.Lat;
                    epicenter.Lng = hypocentreOnPoints.Lng;
                }
            }

            return epicenter;
        }

        private double RefinementError(List<EarthquakeStation> stations, ref Epicenter epicenter)
        {
            double currentError = _errorCalculateMethod.Calculate(stations, epicenter);
            double minError = currentError;
            double lastError = minError;
            double lng = epicenter.Lng;
            double lat = epicenter.Lat;
            int i1 = 1, j1 = 1;
            double step = 0.5;
            // TODO: Оптимизировать этот кусок кода
            for (int iteration = 0; iteration < 50; iteration++)
            {
                if (Math.Abs(lastError - minError) < 0.01)
                    break;
                lastError = minError;

                for (int i = 0; i <= 2; i++)
                    for (int j = 0; j <= 2; j++)
                    {
                        currentError = _errorCalculateMethod.Calculate(
                            stations,
                            lat + (j - 1.0) * step,
                            lng + (i - 1.0) * step);
                        if (currentError <= minError)
                        {
                            i1 = i; j1 = j;
                            minError = currentError;
                        }
                    }
                if (i1 == 1 && j1 == 1)
                {
                    step = step / 2.0;
                }
                else
                {
                    lng = lng + (i1 - 1.0) * step;
                    lat = lat + (j1 - 1.0) * step;
                }
            }

            epicenter.Lng = lng;
            epicenter.Lat = lat;
            return minError;
        }
    }
}