﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Основной класс для вычисления данных землетрясения
    /// </summary>
    public class EarthquakeCalculate
    {
        private Earthquake _earthquake;
        private IEpicenterCalculateMethod _epicenterMethod;
        private IEpicenterErrorCalculateMethod _errorMethod;
        private IEpicentralDistanceCalculateMethod _distanceMethod;
        private ICoordinatesErrorCalculateMethod _coordinatesErrorMethod = new AbsoluteErrorMethod();

        #region Properties

        /// <summary>
        /// Метод вычисления эпицентра
        /// </summary>
        public IEpicenterCalculateMethod EpicenterMethod
        {
            get => _epicenterMethod;
            set => _epicenterMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Метод вычисления погрешности эпицентра
        /// </summary>
        public IEpicenterErrorCalculateMethod ErrorMethod
        {
            get => _errorMethod;
            set
            {
                _errorMethod = value ?? throw new ArgumentNullException(nameof(value));
                _epicenterMethod.ErrorCalculateMethod = value;
            }
        }

        /// <summary>
        /// Метод вычисления эпицентрального расстояния
        /// </summary>
        public IEpicentralDistanceCalculateMethod DistanceMethod
        {
            get => _distanceMethod;
            set => _distanceMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Землетрясение
        /// </summary>
        public Earthquake Earthquake
        {
            get => _earthquake;
            set => _earthquake = value;
        }

        /// <summary>
        /// С какого значения глубины мы начинаем
        /// </summary>
        public double BeginDepth { get; set; } = 3;

        /// <summary>
        /// До какого значения мы доходим
        /// </summary>
        public double EndDepth { get; set; } = 80;

        /// <summary>
        /// Шаг глубины
        /// </summary>
        public double StepDepth { get; set; } = 3;

        /// <summary>
        /// Коэффициенты метода Вадати
        /// </summary>
        public double[][] WadatiData { get; set; }

        /// <summary>
        /// Указывает является ли wsg типом данных.
        /// </summary>
        public WsgTypeEnum? WsgType { get; set; }

        #endregion

        /// <summary>
        /// Создает объект класса <see cref="EarthquakeCalculate"/> со следующими параметрами
        /// <remarks>
        /// Методы нахождения:</remarks>
        /// <remarks>Радиус: годограф</remarks>
        /// <para>Гипоцентр: сеток</para> 
        /// Погрешность: дисперсия
        /// </summary>
        public EarthquakeCalculate()
        {
            _epicenterMethod = new AutomaticMethod();
            _errorMethod = new DispersionMethod();
            _distanceMethod = new TpMethod();
        }

        /// <summary>
        /// Вычисляет землетрясение
        /// </summary>
        public Earthquake Calculate()
        {
            //// Вычисляем T0
            //CalculateT0();

            //// Вычисляем невязку (Dt) и отношение скоростей (VpVs)
            //CalculateDtAndVpVs();

            if (WsgType != WsgTypeEnum.FewStations) CalculateVpVsT0AndDt();

            if (WsgType != null)
            {
                _epicenterMethod = new WsgDataMethod();
                foreach (var station in _earthquake.Stations)
                {
                    station.EpicDistance = station.Station.Coordinate.DistanceTo(_earthquake.Lat, _earthquake.Lng);
                }
            }
            else
            {
                // Вычисляем гипоцентр
                CalculateHypocentre();

                // Вычисляем эпицентральное расстояние
                CalculateEpicDist();
            }

            // Вычисляем энергию или класс
            CalculateClass();

            CalculateCoordinatesError();

            _earthquake.Stations = _earthquake.Stations.OrderBy(s => s.Sp).ToList();
            return _earthquake;
        }

        private void CalculateCoordinatesError()
        {
            _coordinatesErrorMethod.CalculateCoordinatesError(_earthquake.Stations, _earthquake.Lat, _earthquake.Lng, out var latError, out var lngError);
            _earthquake.LatError = latError;
            _earthquake.LngError = lngError;
        }

        /// <summary>
        /// Вычисляет VpVs, T0 и Dt
        /// </summary>
        protected void CalculateVpVsT0AndDt()
        {
            var calc = new CalculatorS();
            WadatiData = calc.FindVpVsAndT0(ref _earthquake);

            foreach (var station in _earthquake.Stations)
            {
                station.Dt = (station.T0 - _earthquake.T0).TotalSeconds;
            }

            _earthquake.Dt = _earthquake.Stations.Average(s => s.Dt);
            _earthquake.DtRms = _earthquake.Stations.Average(s => Math.Pow(s.Dt, 2));
        }

        /// <summary>
        /// Вычисляет T0
        /// </summary>
        protected void CalculateT0()
        {
            // Находим t0 для каждой станции
            foreach (var station in _earthquake.Stations)
            {
                station.T0 = station.Tp.AddSeconds(-1 * (station.Ts - station.Tp).TotalSeconds / (1.73 - 1.0));
            }
            // Новый метод нахождения t0 и vpvs
            var stationsT0 = new List<DateTime>(_earthquake.Stations.Select(s => s.T0)).ToList();
            var minT0 = stationsT0.Min();
            var t0ToCompare = new DateTime(
                minT0.Year,
                minT0.Month,
                minT0.Day,
                minT0.Hour, 0, 0, 0);
            double lymbda = 0.5;
            int min = 2;
            while (true)
            {
                int count = stationsT0.Count;
                var stationsT0Sec = Enumerable
                    .Range(0, count)
                    .Select(i => (stationsT0[i] - t0ToCompare).TotalSeconds)
                    .ToList();
                var t0Sec = stationsT0Sec.Sum() / count;
                var diff = Enumerable
                    .Range(0, count)
                    .Select(i => Math.Abs(stationsT0Sec[i] - t0Sec))
                    .ToList();

                int maxIndex = 0;
                double max = diff[maxIndex];
                for (int i = 1; i < count; i++)
                {
                    if (max < diff[i])
                    {
                        maxIndex = i;
                        max = diff[maxIndex];
                    }
                }

                if (max < lymbda || count <= min)
                {
                    _earthquake.T0 = t0ToCompare.AddSeconds(t0Sec);
                    _earthquake.Date = _earthquake.T0.Date;
                    break;
                }
                stationsT0.RemoveAt(maxIndex);
            }
        }

        /// <summary>
        /// Вычисляет невязку, отношение скоростей
        /// </summary>
        protected void CalculateDtAndVpVs()
        {
            foreach (var station in _earthquake.Stations)
            {
                // Dt
                station.Dt = (station.T0 - _earthquake.T0).TotalSeconds;
                // VpVs
                station.VpVs = (station.Ts - station.Tp).TotalSeconds / (station.Tp - _earthquake.T0).TotalSeconds + 1;
            }

            int count = _earthquake.Stations.Count;
            _earthquake.Dt = _earthquake.Stations.Sum(s => s.Dt) / count;
            _earthquake.DtRms = _earthquake.Stations.Sum(s => Math.Pow(s.Dt, 2)) / count;
            _earthquake.VpVs = _earthquake.Stations.Sum(s => s.VpVs) / count;
        }

        /// <summary>
        /// Вычисляет гипоцентр
        /// </summary>
        protected void CalculateHypocentre()
        {
            double error = double.MaxValue;

            // Если выбран метод автоматический
            if (_epicenterMethod is AutomaticMethod)
            {
                double bestAutoError = error;
                // Смотрим при годографе Tp
                IEpicentralDistanceCalculateMethod distanceMethod = new TpMethod();
                error = CalculateError(bestAutoError, _epicenterMethod, distanceMethod, _errorMethod);
                if (error < bestAutoError)
                {
                    bestAutoError = error;
                    _distanceMethod = new TpMethod();
                }
                var bestMethod = MethodType.GetEpicenterMethod(_epicenterMethod.Name);

                // Смотрим при годографе SP
                distanceMethod = new SpMethod();
                error = CalculateError(bestAutoError, bestMethod, distanceMethod, _errorMethod);
                if (error < bestAutoError)
                {
                    bestAutoError = error;
                    _distanceMethod = new SpMethod();
                }

                // Смотрим при годографе Ts
                distanceMethod = new TsMethod();
                error = CalculateError(bestAutoError, bestMethod, distanceMethod, _errorMethod);
                if (error < bestAutoError)
                {
                    _distanceMethod = new TsMethod();
                }
            }

            if (_epicenterMethod is VadatiMethod vadatiMethod)
            {
                var currentEpicenter = vadatiMethod.Calculate(_earthquake.Stations, 0, out var h);
                _earthquake.Lat = currentEpicenter.Lat;
                _earthquake.Lng = currentEpicenter.Lng;
                _earthquake.H = h;
            }
            // Если выбран любой другой метод
            else
            {
                CalculateError(error, _epicenterMethod, _distanceMethod, _errorMethod);
            }
        }

        private double CalculateError(
            double error,
            IEpicenterCalculateMethod epicenterMethod,
            IEpicentralDistanceCalculateMethod distanceMethod,
            IEpicenterErrorCalculateMethod errorMethod)
        {
            double bestError = error;

            double endDepth = EndDepth;
            for (double depth = BeginDepth; depth < endDepth; depth += StepDepth)
            {

                var correctStations =
                    _earthquake.Stations.Where(s => distanceMethod.IsInsideTime(s, _earthquake.T0, depth)).ToList();

                if (correctStations.Count < 3)
                {
                    continue;
                }

                foreach (var station in correctStations)
                {
                    station.EpicDistance = distanceMethod.Calculate(station, _earthquake.T0, depth);
                }

                var currentEpicenter = epicenterMethod.Calculate(correctStations, depth);
                var currentError = errorMethod.Calculate(correctStations, currentEpicenter);
                if (currentError < bestError - bestError * 0.07)
                {
                    bestError = currentError;

                    _earthquake.Lat = currentEpicenter.Lat;
                    _earthquake.Lng = currentEpicenter.Lng;
                    _earthquake.H = depth;
                }
            }

            return bestError;
        }

        /// <summary>
        /// Вычисляет эпицентральное расстояние
        /// </summary>
        protected void CalculateEpicDist()
        {
            foreach (var station in _earthquake.Stations)
            {
                station.EpicDistance = _distanceMethod.Calculate(station, _earthquake.T0, _earthquake.H);
            }

            foreach (var station in _earthquake.Stations.Where(s => !_distanceMethod.IsInsideTime(s, _earthquake.T0, _earthquake.H)))
            {
                station.EpicDistance = station.Station.Coordinate.DistanceTo(_earthquake.Lat, _earthquake.Lng);
            }
        }

        /// <summary>
        /// Вычисляет класс землятресения
        /// </summary>
        protected void CalculateClass()
        {
            int count = 0;
            foreach (var station in _earthquake.Stations)
            {
                if (station.AmplitudeZ == 0 || station.Amplitude == 0)
                {
                    station.K = 0;
                    continue;
                }
                station.K = 2.4 + 1.83 * (Math.Log10(station.AmplitudeZ + station.Amplitude) + 2.0 * Math.Log10(station.EpicDistance));
                count++;
            }

            if (_earthquake.Stations.All(s => s.K == 0))
            {
                _earthquake.K = 0;
            }
            else
            {
                _earthquake.K = _earthquake.Stations.Select(s => s.K).Sum() / count;
            }
        }
    }
}