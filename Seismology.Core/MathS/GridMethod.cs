﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод Сеток вычисления эпицентра
    /// </summary>
    public class GridMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorMethod = new DispersionMethod();
        // дополнительная погрешность
        private IEpicenterErrorCalculateMethod _errorAbsMethod = new AbsoluteErrorMethod();

        /// <summary>
        /// Имя метода
        /// </summary>
        public string Name { get; } = "Метод Сеток";
        /// <summary>
        /// Количество делений
        /// </summary>
        public int Count { get; set; } = 100;
        /// <summary>
        /// Количество итераций
        /// </summary>
        public int IterationCount { get; set; } = 2;

        /// <summary>
        /// Метод, с помощью которого будет вычеслять погрешность при уточнении погрешности
        /// </summary>
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorMethod;
            set => _errorMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Вычисляет эпицентр на основании станционных данных
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            var beginCoord = new Epicenter();
            var endCoord = new Epicenter();
            var stepCoord = new Epicenter();

            beginCoord.Lat = stations.Select(s =>
                s.Station.Coordinate.Lat - Coordinate.KmToLat(s.EpicDistance)).Min();
            beginCoord.Lng = stations.Select(s =>
                s.Station.Coordinate.Lng - Coordinate.KmToLng(s.EpicDistance, s.Station.Coordinate.Lat)).Min();

            endCoord.Lat = stations.Select(s =>
                s.Station.Coordinate.Lat + Coordinate.KmToLat(s.EpicDistance)).Max();
            endCoord.Lng = stations.Select(s =>
                s.Station.Coordinate.Lng + Coordinate.KmToLng(s.EpicDistance, s.Station.Coordinate.Lat)).Max();

            stepCoord.Lat = (endCoord.Lat - beginCoord.Lat) / Count;
            stepCoord.Lng = (endCoord.Lng - beginCoord.Lng) / Count;

            double minErrorOnIteration = double.PositiveInfinity;
            double minAbsErrorOnIteration = double.PositiveInfinity;
            var coord = new Coordinate();

            for (int iteration = 0; iteration < IterationCount; iteration++)
            {
                for (int i = 0; i < Count; i++)
                {
                    for (int j = 0; j < Count; j++)
                    {
                        double currentError = _errorMethod.Calculate(
                            stations,
                            new Epicenter
                            {
                                Lat = beginCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5,
                                Lng = beginCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5
                            }
                            );
                        double currentAbsError = _errorAbsMethod.Calculate(
                            stations,
                            new Epicenter
                            {
                                Lat = beginCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5,
                                Lng = beginCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5
                            });

                        if (minErrorOnIteration > currentError && currentAbsError < 10 /*minAbsErrorOnIteration > currentAbsError*/)
                        {
                            coord.Lat = beginCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5;
                            coord.Lng = beginCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5;

                            minErrorOnIteration = currentError;
                            minAbsErrorOnIteration = currentAbsError;
                        }
                    }
                }

                beginCoord.Lat = coord.Lat - stepCoord.Lat * 0.5;
                beginCoord.Lng = coord.Lng - stepCoord.Lng * 0.5;

                endCoord.Lat = coord.Lat + stepCoord.Lat * 0.5;
                endCoord.Lng = coord.Lng + stepCoord.Lng * 0.5;

                stepCoord.Lat = (endCoord.Lat - beginCoord.Lat) / Count;
                stepCoord.Lng = (endCoord.Lng - beginCoord.Lng) / Count;
            }

            return new Epicenter()
            {
                Lat = coord.Lat,
                Lng = coord.Lng
            };
        }
    }
}