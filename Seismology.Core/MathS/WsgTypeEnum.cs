﻿namespace Seismology.Core.MathS
{
    /// <summary>
    /// Тип WSG.
    /// </summary>
    public enum WsgTypeEnum
    {
        /// <summary>
        /// Далекие землетрясения (stations.Count(s => s.Sp < 25) < 3).
        /// </summary>
        DistantEarthquakes,

        /// <summary>
        /// Мало станций (stations.Count < 3).
        /// </summary>
        FewStations
    }
}