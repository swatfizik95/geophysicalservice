﻿using System;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет годограф, который вычисляет эпицентральное расстояние на основе tp-t0
    /// </summary>
    public class TpMethod : IEpicentralDistanceCalculateMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        public string Name { get; } = "Метод Tp";

        /// <inheritdoc />
        public double Calculate(EarthquakeStation station, DateTime t0, double depth)
        {
            // Разница времени tp и t0 (в секундах и мс)
            double sec = (station.Tp - t0).TotalSeconds;
            if (depth < 3.5) return 0.028340888 * Math.Pow(sec, 3) - 0.480679824 * Math.Pow(sec, 2) + 6.767549955 * sec - 5.289201559;
            if (depth < 6) return 0.000792504 * Math.Pow(sec, 3) - 0.025771604 * Math.Pow(sec, 2) + 5.170443666 * sec - 4.841787677;
            if (depth < 9) return 0.007019577 * Math.Pow(sec, 3) - 0.189323767 * Math.Pow(sec, 2) + 6.836705976 * sec - 9.921594306;
            if (depth < 13) return 0.003326113 * Math.Pow(sec, 3) - 0.157087417 * Math.Pow(sec, 2) + 7.711772313 * sec - 15.890216279;
            if (depth < 18) return 0.038725552 * Math.Pow(sec, 3) - 1.142889507 * Math.Pow(sec, 2) + 16.296744369 * sec - 39.556524948;
            if (depth < 23) return 0.014015191 * Math.Pow(sec, 3) - 0.570648044 * Math.Pow(sec, 2) + 13.015761592 * sec - 39.263715296;
            if (depth < 28) return 0.036151846 * Math.Pow(sec, 3) - 1.409923872 * Math.Pow(sec, 2) + 23.208597630 * sec - 73.202366375;
            if (depth < 33) return 0.010941804 * Math.Pow(sec, 3) - 0.535920231 * Math.Pow(sec, 2) + 14.755766528 * sec - 60.367662433;
            if (depth < 38) return 0.039232278 * Math.Pow(sec, 3) - 1.717274686 * Math.Pow(sec, 2) + 31.378664311 * sec - 141.454037055;
            if (depth < 43) return 0.022717774 * Math.Pow(sec, 3) - 1.177117804 * Math.Pow(sec, 2) + 28.051378778 * sec - 170.498860686;
            if (depth < 48) return 0.008474662 * Math.Pow(sec, 3) - 0.492409279 * Math.Pow(sec, 2) + 15.876903653 * sec - 101.862689766;
            if (depth < 53) return 0.014329911 * Math.Pow(sec, 3) - 0.921301236 * Math.Pow(sec, 2) + 24.322088518 * sec - 117.909303704;
            if (depth < 58) return 0.005603476 * Math.Pow(sec, 3) - 0.447220877 * Math.Pow(sec, 2) + 17.597723686 * sec - 98.542502788;
            if (depth < 63) return 0.024449388 * Math.Pow(sec, 3) - 1.322310243 * Math.Pow(sec, 2) + 28.937597807 * sec - 129.378081375;
            if (depth < 68) return 0.017109327 * Math.Pow(sec, 3) - 0.939860660 * Math.Pow(sec, 2) + 22.951623333 * sec - 140.047268484;
            if (depth < 73) return 0.017005204 * Math.Pow(sec, 3) - 1.159753780 * Math.Pow(sec, 2) + 32.745681482 * sec - 196.426176510;
            return 0.039026286 * Math.Pow(sec, 3) - 2.583918758 * Math.Pow(sec, 2) + 63.528023449 * sec - 461.012070104;
        }

        /// <inheritdoc />
        public bool IsInsideTime(EarthquakeStation station, DateTime t0, double depth)
        {
            // Разница времени tp и t0 (в секундах и мс)
            double sec = (station.Tp - t0).TotalSeconds;
            if (depth < 3.5) return sec > 0.828 && sec < 11.1655;
            if (depth < 6) return sec > 0.941 && sec < 13.04924;
            if (depth < 9) return sec > 1.52 && sec < 14.36339;
            if (depth < 13) return sec > 2.19 && sec < 23.22168;
            if (depth < 18) return sec > 2.93 && sec < 18.3188;
            if (depth < 23) return sec > 3.6 && sec < 24.02488;
            if (depth < 28) return sec > 4.28 && sec < 22.1375;
            if (depth < 33) return sec > 5 && sec < 26.67424;
            if (depth < 38) return sec > 6.42 && sec < 24.20616;
            if (depth < 43) return sec > 8.69 && sec < 28.27;
            if (depth < 48) return sec > 8.09 && sec < 35.0972;
            if (depth < 53) return sec > 7.14 && sec < 35.29407;
            if (depth < 58) return sec > 7.14 && sec < 39.24624;
            if (depth < 63) return sec > 8.13 && sec < 23.13;
            if (depth < 68) return sec > 8.5 && sec < 31.74;
            if (depth < 73) return sec > 8.79 && sec < 33.7752;
            return sec > 12.5 && sec < 31.59943;
        }
    }
}