﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет Автоматический метод вычисления эпицентра
    /// </summary>
    public class AutomaticMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        private readonly List<IEpicenterCalculateMethod> _epicenterMethods = new List<IEpicenterCalculateMethod>()
        {
            new GridMethod(),
            //new AverageMethod(),
            new BinaryMethod(),
            new MultiMethod(),
            new IntersectionMethod()
        };

        /// <summary>
        /// Имя метода
        /// </summary>
        public string Name { get; private set; } = "Не определен";

        /// <summary>
        /// Метод, с помощью которого будет вычеслять погрешность при уточнении погрешности
        /// </summary>
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Вычисляет эпицентр на основании станционных данных
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            var bestEpicenter = new Epicenter();
            double bestError = double.PositiveInfinity;

            foreach (var method in _epicenterMethods)
            {
                var epicenter = method.Calculate(stations, depth);
                double error = _errorCalculateMethod.Calculate(stations, epicenter);
                if (bestError > error)
                {
                    bestError = error;
                    bestEpicenter = epicenter;
                    Name = method.Name;
                }
            }

            return bestEpicenter;
        }
    }
}
