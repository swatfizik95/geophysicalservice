﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Предоставляет метод дисперсии вычисления погрешности эпицентра
    /// </summary>
    public class DispersionMethod : IEpicenterErrorCalculateMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        public string Name { get; } = "Метод дисперсий";

        /// <summary>
        /// Вычисляет погрешность нахождения эпицентра
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        /// <returns></returns>
        public double Calculate(IEnumerable<EarthquakeStation> stations, double lat, double lng)
        {
            var differences = stations.Select(s => Math.Abs(s.EpicDistance - s.Station.Coordinate.DistanceTo(lat, lng))).ToList();
            double mat = Math.Pow(differences.Average(), 2);

            return differences.Select(val => Math.Pow(val, 2) - mat).Average();
        }

        /// <summary>
        /// Вычисляет погрешность нахождения эпицентра
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="epicenter">Эпицентр</param>
        /// <returns></returns>
        public double Calculate(IEnumerable<EarthquakeStation> stations, Epicenter epicenter)
        {
            return Calculate(stations, epicenter.Lat, epicenter.Lng);
        }
    }
}
