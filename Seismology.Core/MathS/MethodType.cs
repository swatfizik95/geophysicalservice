﻿using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core.MathS
{
    /// <summary>
    /// Тип методов
    /// </summary>
    public static class MethodType
    {
        private static readonly List<IEpicenterCalculateMethod> Methods = new List<IEpicenterCalculateMethod>
        {
            new MultiMethod(),
            new BinaryMethod(),
            //new AverageMethod(),
            new GridMethod(),
            new IntersectionMethod(),
            new WsgDataMethod(),
            new VadatiMethod()
        };

        private static readonly List<IEpicentralDistanceCalculateMethod> DistanceMethods = new List<IEpicentralDistanceCalculateMethod>
        {
            new TpMethod(),
            new SpMethod(),
            new TsMethod()
        };

        /// <summary>
        /// Возвращаем метод реализующий интерфейс <see cref="IEpicenterCalculateMethod"/> с указаным именем
        /// </summary>
        /// <param name="name">Имя</param>
        /// <returns></returns>
        public static IEpicenterCalculateMethod GetEpicenterMethod(string name)
        {
            string upper = name.ToUpper();
            return Methods.FirstOrDefault(m => m.Name.ToUpper() == upper);
        }

        /// <summary>
        /// Возвращаем метод реализующий интерфейс <see cref="IEpicentralDistanceCalculateMethod"/> с указаным именем
        /// </summary>
        /// <param name="name">Имя</param>
        /// <returns></returns>
        public static IEpicentralDistanceCalculateMethod GetDistanceMethod(string name)
        {
            string upper = name.ToUpper();
            return DistanceMethods.FirstOrDefault(d => d.Name.ToUpper() == upper);
        }
    }
}
