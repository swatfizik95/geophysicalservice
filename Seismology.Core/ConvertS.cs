﻿using GMap.NET;
using Seismology.Core.Map;

namespace Seismology.Core
{
    /// <summary>
    /// Преобразует значение одного базового типа землетрясения к другому базовому типу землетрясения
    /// </summary>
    public static class ConvertS
    {
        /// <summary>
        /// Преобразует экземпляр замлетрясения класса <see cref="WsgEarthquake"/> в экземпляр класса <see cref="Earthquake"/>
        /// </summary>
        /// <param name="wsgEarthquake">ЦWSG землетрясение</param>
        /// <returns></returns>
        public static Earthquake ToEarthquake(WsgEarthquake wsgEarthquake)
        {
            var earthquake = new Earthquake
            {
                Date = wsgEarthquake.DateAndTime.Date,
                Lat = wsgEarthquake.Latitude,
                Lng = wsgEarthquake.Longitude,
                H = wsgEarthquake.Depth
            };
            foreach (var station in wsgEarthquake.WsgStations)
            {
                var phaseP = station.GetPhaseP();
                var phaseS = station.GetPhaseS();
                earthquake.Stations.Add(new EarthquakeStation
                {
                    Name = station.Name,
                    Tp = phaseP.Time,
                    Ts = phaseS.Time,
                    PeriodZ = phaseP.Per,
                    AmplitudeZ = phaseP.Amp / 1000.0,
                    Period = phaseS.Per,
                    Amplitude = phaseS.Amp / 1000.0,
                    Ep = SelectP(phaseP.Qual),
                    Es = SelectS(phaseS.Qual),
                    T0 = station.T0
                });
            }

            return earthquake;
        }

        private static string SelectP(string phase)
        {
            if (phase.Length == 3)
            {
                switch (phase[1])
                {
                    case '_':
                        return $"{phase[2]}p";
                    case 'c':
                        return $"+{phase[2]}p";
                    case 'd':
                        return $"-{phase[2]}p";
                    default:
                        return $"{phase[2]}p";
                }
            }

            return "ep";
        }

        private static string SelectS(string phase)
        {
            if (phase.Length == 3)
            {
                switch (phase[1])
                {
                    case '_':
                        return $"{phase[2]}s";
                    case 'c':
                        return $"+{phase[2]}s";
                    case 'd':
                        return $"-{phase[2]}s";
                    default:
                        return $"{phase[2]}s";
                }
            }

            return "es";
        }

        /// <summary>
        /// Преобразует значение заданного <see cref="Coordinate"/> в точку <see cref="PointLatLng"/>
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public static PointLatLng ToPointLatLng(Coordinate coordinate)
        {
            return new PointLatLng(coordinate.Lat, coordinate.Lng);
        }

        /// <summary>
        /// Преобразует значение заданного <see cref="PointLatLng"/> в координаты <see cref="Coordinate"/>
        /// </summary>
        /// <param name="pointLatLng"></param>
        /// <returns></returns>
        public static Coordinate ToCoordinate(PointLatLng pointLatLng)
        {
            return new Coordinate(pointLatLng.Lat, pointLatLng.Lng);
        }
    }
}
