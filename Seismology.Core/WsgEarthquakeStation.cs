﻿using Seismology.Core.Map;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core
{
    /// <summary>
    /// Класс для хранения станции с его фазами <see cref="WsgPhase"/> из файла WSG
    /// </summary>
    public class WsgEarthquakeStation
    {
        /// <summary>
        /// Станция
        /// </summary>
        public Station Station { get; private set; }

        private string _name;
        /// <summary>
        /// Имя станции
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                Station = StationStorage.FindStationByName(_name);
            }
        }

        /// <summary>
        /// Список фаз станции
        /// </summary>
        public List<WsgPhase> Phases = new List<WsgPhase>();

        /// <summary>
        /// Возвращает время Tp
        /// </summary>
        /// <returns>Время Tp</returns>
        public DateTime? GetTp()
        {
            return Phases.FirstOrDefault(p => p.Phaze == "P")?.Time;
        }

        /// <summary>
        /// Возвращает время Ts
        /// </summary>
        /// <returns>Время Ts</returns>
        public DateTime? GetTs()
        {
            return Phases.FirstOrDefault(p => p.Phaze == "S")?.Time;
        }


        /// <summary>
        /// Возвращает фазу P
        /// </summary>
        /// <returns>Фаза P</returns>
        public WsgPhase GetPhaseP()
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return Phases.FirstOrDefault(p => p.Phaze == "P" && p.Amp != 0.0) ?? Phases.FirstOrDefault(p => p.Phaze == "P");
        }

        /// <summary>
        /// Возвращает фазу S. По умолчанию i = 1, возвращает фазу с максимальной амплитудой.
        /// При i = 2 возвращает фазу с минимальной амплитудой
        /// </summary>
        /// <param name="i"> Какую по счёту фазу вернуть</param>
        /// <returns>Фаза S</returns>
        public WsgPhase GetPhaseS(int i = 1)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            var phases = Phases.Where(p => p.Phaze == "S" && p.Amp != 0.0).OrderBy(p => p.Amp);
            switch (i)
            {
                case 1:
                    return phases.LastOrDefault() ?? Phases.FirstOrDefault(p => p.Phaze == "S");
                case 2:
                    return phases.FirstOrDefault() ?? Phases.FirstOrDefault(p => p.Phaze == "S");
                default:
                    return phases.LastOrDefault() ?? Phases.FirstOrDefault(p => p.Phaze == "S");
            }
        }

        public DateTime T0 { get; set; }
    }
}
