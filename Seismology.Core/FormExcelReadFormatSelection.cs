﻿using OfficeOpenXml;
using Seismology.Core.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.Core
{
    public partial class FormExcelReadFormatSelection : Form
    {
        private string[] headers;
        private readonly List<string[]> data = new List<string[]>();
        private readonly ExcelType type;
        private readonly string filePath;

        private readonly string[] possibleFormats;

        public FormExcelReadFormatSelection(string filePath, bool isBulletin, ExcelType type)
        {
            InitializeComponent();

            this.type = type;
            this.filePath = filePath;

            possibleFormats = isBulletin ? FileS.PossibleBulletinExcelFormats : FileS.PossibleCatalogExcelFormats;
        }

        #region Events

        private void CheckBoxIsHeader_CheckedChanged(object sender, System.EventArgs e)
        {
            for (int index = 0; index < dataGridViewShow.Columns.Count; index++)
            {
                dataGridViewShow.Columns[index].HeaderText = checkBoxIsHeader.Checked ? headers[index] : string.Empty;
            }

            SetDataGridView();
        }

        private void ExcelReadFormatSelectionForm_Load(object sender, System.EventArgs e)
        {
        }

        private void ExcelReadFormatSelectionForm_Shown(object sender, System.EventArgs e)
        {
            var fileInfo = new FileInfo(filePath);

            if (fileInfo.Exists)
            {
                if (type == ExcelType.Xlsx)
                {
                    using (var excel = new ExcelPackage(fileInfo))
                    {
                        var w = excel.Workbook.Worksheets.First();
                        int columnStart = w.Dimension.Start.Column,
                            columnEnd = w.Dimension.End.Column,
                            rowStart = w.Dimension.Start.Row,
                            rowEnd = rowStart + 4;
                        int columnLength = columnEnd - columnStart + 1;

                        int rowIndex = 0;
                        for (int row = rowStart; row <= rowEnd; row++)
                        {
                            data.Add(new string[columnLength]);
                            int columnIndex = 0;
                            for (int column = columnStart; column <= columnEnd; column++)
                            {
                                data[rowIndex][columnIndex] = w.Cells[row, column].Text;
                                columnIndex++;
                            }

                            rowIndex++;
                        }
                    }
                }

                headers = data[0].ToArray();

                InitDataGridView();
            }
            else
            {
                MessageBox.Show("Файл не найден или был удален!");
                Close();
            }
        }

        private void ExcelReadFormatSelectionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                return;
            var cells = dataGridViewShow.Rows[0].Cells.Cast<DataGridViewCell>().ToList();
            if (cells.Any(c => c.Value == null))
            {
                MessageBox.Show($"В столбцах: {string.Join(", ", cells.Where(c => c.Value == null).Select(c => c.ColumnIndex + 1))} не выбран формат");
                e.Cancel = true;
                return;
            }

            isFirstHeader = checkBoxIsHeader.Checked;
            formats = cells.Select(c => c.Value.ToString()).ToArray();
        }

        #endregion

        #region Logic

        private void InitDataGridView()
        {
            for (var index = 0; index < headers.Length; index++)
            {
                dataGridViewShow.Columns.Add(new DataGridViewColumn
                {
                    HeaderText = headers[index],
                    Name = $"column{index}",
                    SortMode = DataGridViewColumnSortMode.NotSortable,
                    CellTemplate = new DataGridViewTextBoxCell()
                });
            }

            dataGridViewShow.Rows.Add(1);
            for (int index = 0; index < headers.Length; index++)
            {
                var comboBoxCell = new DataGridViewComboBoxCell();
                comboBoxCell.Items.AddRange(possibleFormats);
                dataGridViewShow.Rows[0].Cells[index] = comboBoxCell;
                var findHeader = possibleFormats.FirstOrDefault(f => f.Equals(headers[index], StringComparison.OrdinalIgnoreCase));
                if (findHeader != null)
                {
                    dataGridViewShow.Rows[0].Cells[index].Value = findHeader;
                }
            }

            int count = 3;
            dataGridViewShow.Rows.Add(count);
            int skipCount = checkBoxIsHeader.Checked ? 1 : 0;
            int rowIndex = 1;
            foreach (var d in data.Skip(skipCount).Take(count))
            {
                dataGridViewShow.Rows[rowIndex].ReadOnly = true;
                dataGridViewShow.Rows[rowIndex].SetValues(d);
                rowIndex++;
            }
        }

        private void SetDataGridView()
        {
            int count = 3;
            int skipCount = checkBoxIsHeader.Checked ? 1 : 0;
            int rowIndex = 1;
            foreach (var d in data.Skip(skipCount).Take(count))
            {
                dataGridViewShow.Rows[rowIndex].SetValues(d);
                rowIndex++;
            }
        }

        #endregion

        private bool isFirstHeader;
        private string[] formats;

        public bool IsFirstHeader => isFirstHeader;
        public string[] Formats => formats;
    }
}
