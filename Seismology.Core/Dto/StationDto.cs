﻿using System.Drawing;

namespace Seismology.Core.Dto
{
    public class StationDto
    {
        /// <summary>
        /// Имя станции (учитывает R)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Международное имя станции (без R)
        /// </summary>
        public string InternationalName { get; set; }

        /// <summary>
        /// Имя, которое хранится в БД
        /// </summary>
        public string NameInDb { get; set; }

        /// <summary>
        /// Дополнительное имя для чтения
        /// </summary>
        public string NameForRead { get; set; }

        /// <summary>
        /// Русское имя станции
        /// </summary>
        public string RusName { get; set; }

        /// <summary>
        /// Координаты станции (широта)
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// Координаты станции (долгота)
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// Цвет станции
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Номер группы (нужне для вывода в бюллетенях)
        /// </summary>
        public int Group { get; set; }
    }
}