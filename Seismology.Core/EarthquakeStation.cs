﻿using Seismology.Core.Data;
using Seismology.Core.Map;
using System;
using System.Collections.Generic;

// ReSharper disable CompareOfFloatsByEqualityOperator
// ReSharper disable NonReadonlyMemberInGetHashCode

namespace Seismology.Core
{
    /// <summary>
    /// Представляет станционное землетрясение
    /// </summary>
    public class EarthquakeStation : ICloneable
    {
        /// <summary>
        /// Данные станции для карт
        /// </summary>
        public Station Station { get; private set; }

        /// <summary>
        /// Указывает, учитывалась ли станция при подсчете (true - учитывалась, false - не учитывалась)
        /// По умолчанию принимает значение true
        /// </summary>
        //public bool IsIncluded { get; set; } = true;


        private string _name;
        /// <summary>
        /// Название станции
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                Station = StationStorage.FindStationByName(_name);
            }
        }

        /// <summary>
        /// Ep
        /// </summary>
        public string Ep { get; set; } = "--";

        private DateTime _tp;
        /// <summary>
        /// Время распростронения продольной волны
        /// </summary>
        public DateTime Tp
        {
            get => _tp;
            set
            {
                _tp = value;
                if (_tp < _ts)
                {
                    _sp = (_ts - _tp).TotalSeconds;
                }
                else
                {
                    _sp = (_ts.AddDays(1) - _tp).TotalSeconds;
                }
            }
        }

        /// <summary>
        /// Es
        /// </summary>
        public string Es { get; set; } = "--";


        private DateTime _ts;
        /// <summary>
        /// Время распростронения попереченой волны
        /// </summary>
        public DateTime Ts
        {
            get => _ts;
            set
            {
                _ts = value;
                if (_tp < _ts)
                {
                    _sp = (_ts - _tp).TotalSeconds;
                }
                else
                {
                    _sp = (_ts.AddDays(1) - _tp).TotalSeconds;
                }
            }
        }

        private double _sp;

        /// <summary>
        /// Разница времени между Ts и Tp (в секундах)
        /// </summary>
        public double Sp
        {
            get => _sp;
            set
            {
                _sp = value;
                _ts = _tp.AddSeconds(value);
            }
        }


        /// <summary>
        /// Время толчка в гипоцентре (по Гринвичу +3-00-00.000)
        /// </summary>
        public DateTime T0 { get; set; }

        /// <summary>
        /// Эпицентральное расстояние
        /// </summary>
        public double EpicDistance { get; set; }

        /// <summary>
        /// Невязка (погрешность) в секундах
        /// </summary>
        public double Dt { get; set; }

        /// <summary>
        /// Энергия 10^<see cref="K"/> Дж
        /// </summary>
        public double K { get; set; }

        /// <summary>
        /// Отношение скоростей vp/vs (продольной и поперечной волны)
        /// </summary>
        public double VpVs { get; set; }

        /// <summary>
        /// Период волны по Z каналу
        /// </summary>
        public double PeriodZ { get; set; }

        /// <summary>
        /// Амплитуда волны по Z каналу
        /// </summary>
        public double AmplitudeZ { get; set; }

        /// <summary>
        /// Период волны по другому каналу
        /// </summary>
        public double Period { get; set; }

        /// <summary>
        /// Амплитуда волны по другому каналу
        /// </summary>
        public double Amplitude { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="EarthquakeStation"/>
        /// с данными по умолчанию
        /// </summary>
        public EarthquakeStation()
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="EarthquakeStation"/>
        /// с помощью указанных начальных данных
        /// </summary>
        /// <param name="name">Имя станции</param>
        /// <param name="tp">Время продольной волны</param>
        /// <param name="ts">Время поперечной волны</param>
        /// <param name="t0">Время толчка в гипоцентре (по Гринвичу +3-00-00.000)</param>
        /// <param name="epicDis">Эпицентральное расстояние</param>
        /// <param name="dt">Невязка</param>
        /// <param name="k">Энергия</param>
        /// <param name="vpvs">Отношение скоростей</param>
        /// <param name="prdZ">Период волны по Z каналу</param>
        /// <param name="ampltdZ">Амплитуда волны по Z каналу</param>
        /// <param name="prd">Период волны по другому каналу</param>
        /// <param name="ampltd">Амплитуда волны по другому каналу</param>
        public EarthquakeStation(string name, DateTime tp, DateTime ts,
            DateTime t0, double epicDis, double dt, double k,
            double vpvs, double prdZ, double ampltdZ, double prd, double ampltd)
        {
            Name = name;
            Tp = tp;
            Ts = ts;
            T0 = t0;
            EpicDistance = epicDis;
            Dt = dt;
            K = k;
            VpVs = vpvs;
            PeriodZ = prdZ;
            AmplitudeZ = ampltdZ;
            Period = prd;
            Amplitude = ampltd;
            Ep = "ep";
            Es = "es";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="station"></param>
        public static explicit operator EarthquakeStation(DbEarthquakeStation station)
        {
            return new EarthquakeStation
            {
                Name = station.Name,
                //IsIncluded = station.IsIncluded,
                T0 = station.T0,
                Ep = station.CachP,
                Tp = station.Tp,
                Es = station.CachS,
                Ts = station.Ts,
                EpicDistance = station.EpicDistance,
                Dt = station.Dt,
                PeriodZ = station.PeriodZ,
                AmplitudeZ = station.AmplitudeZ,
                Period = station.Period,
                Amplitude = station.Amplitude,
                K = station.Class,
                VpVs = station.VpVs
            };
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (GetType() != obj.GetType())
                return false;
            EarthquakeStation another = (EarthquakeStation)obj;
            return Name == another.Name &&
                   Ep == another.Ep &&
                   Tp == another.Tp &&
                   Es == another.Es &&
                   Ts == another.Ts &&
                   Sp == another.Sp &&
                   T0 == another.T0 &&
                   EpicDistance == another.EpicDistance &&
                   Dt == another.Dt &&
                   K == another.K &&
                   VpVs == another.VpVs &&
                   PeriodZ == another.PeriodZ &&
                   AmplitudeZ == another.AmplitudeZ &&
                   Period == another.Period &&
                   Amplitude == another.Amplitude;
        }

        public override int GetHashCode()
        {
            var hashCode = -1248782768;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + Ep.GetHashCode();
            hashCode = hashCode * -1521134295 + Tp.GetHashCode();
            hashCode = hashCode * -1521134295 + Es.GetHashCode();
            hashCode = hashCode * -1521134295 + Ts.GetHashCode();
            hashCode = hashCode * -1521134295 + Sp.GetHashCode();
            hashCode = hashCode * -1521134295 + T0.GetHashCode();
            hashCode = hashCode * -1521134295 + EpicDistance.GetHashCode();
            hashCode = hashCode * -1521134295 + Dt.GetHashCode();
            hashCode = hashCode * -1521134295 + K.GetHashCode();
            hashCode = hashCode * -1521134295 + VpVs.GetHashCode();
            hashCode = hashCode * -1521134295 + PeriodZ.GetHashCode();
            hashCode = hashCode * -1521134295 + AmplitudeZ.GetHashCode();
            hashCode = hashCode * -1521134295 + Period.GetHashCode();
            hashCode = hashCode * -1521134295 + Amplitude.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// Возвращает (неглубокую) копию данного объекта
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Возвращает (глубокую) копию данного объекта
        /// </summary>
        /// <returns></returns>
        public EarthquakeStation Copy()
        {
            var earthquakeStation = (EarthquakeStation)MemberwiseClone();
            earthquakeStation.Station = earthquakeStation.Station.Copy();
            return earthquakeStation;
        }
    }
}
