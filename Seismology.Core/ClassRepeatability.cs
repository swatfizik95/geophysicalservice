﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Seismology.Core
{
    public class ClassRepeatability
    {
        public int[] Classes { get; }
        public double[] Repeatabilities { get; }

        public ClassRepeatability(IList<Earthquake> earthquakes, int[] classes, double periodRatio, double area)
        {
            Classes = classes;
            Repeatabilities = new double[classes.Length];
            for (int index = 0; index < classes.Length; index++)
            {
                int count = earthquakes.Count(eq => eq.Class == classes[index]);
                Repeatabilities[index] = Math.Log10(1000.0 / (periodRatio * area) * count);
            }
        }
    }
}