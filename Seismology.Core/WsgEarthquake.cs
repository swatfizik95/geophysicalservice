﻿using System;
using System.Collections.Generic;

namespace Seismology.Core
{
    /// <summary>
    /// Класс для хранения данных из файла WSG
    /// </summary>
    public class WsgEarthquake
    {
        public DateTime DateAndTime { set; get; }
        public DateTime Time { set; get; }
        public double Longitude { set; get; }
        public double Latitude { set; get; }
        public double Rms { set; get; }
        public double Smaj { set; get; }
        public double Smin { set; get; }
        public double Az { set; get; }
        public double Depth { set; get; }
        public double Err { set; get; }
        public double NDef { set; get; }
        public double Nsta { set; get; }
        public double Mmdist { set; get; }
        public double Mdist { set; get; }
        public string Qual { set; get; }
        public string Author { set; get; }
        public int OrigId { set; get; }
        public double Ml { set; get; }
        public double MlErr { set; get; }


        private List<WsgEarthquakeStation> _wsgStations = new List<WsgEarthquakeStation>();

        /// <summary>
        /// Список станций с их фазами
        /// </summary>
        public List<WsgEarthquakeStation> WsgStations
        {
            get => _wsgStations;
            set => _wsgStations = value;
        }

        /// <summary>
        /// Добавленяет фазу в список фаз станции
        /// </summary>
        /// <param name="phase">Фаза для добавления</param>
        public void AddPhase(WsgPhase phase)
        {
            foreach (var station in _wsgStations)
            {
                if (station.Name == phase.Name)
                {
                    station.Phases.Add(phase);
                    return;
                }
            }

            var newStationPhase = new WsgEarthquakeStation()
            {
                Name = phase.Name,
                T0 = phase.Time
            };
            newStationPhase.Phases.Add(phase);
            _wsgStations.Add(newStationPhase);
        }
    }
}
