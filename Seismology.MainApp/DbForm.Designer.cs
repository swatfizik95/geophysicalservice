﻿namespace Seismology.MainApp
{
    partial class DbForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelDateBegin = new System.Windows.Forms.Label();
            this.labelDateEnd = new System.Windows.Forms.Label();
            this.datePickerBegin = new System.Windows.Forms.DateTimePicker();
            this.datePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewEarthquake = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LatError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lngDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LngError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DtRms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vpVsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.methodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dbEarthquakeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonOpen = new System.Windows.Forms.Button();
            this.buttonShow = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.groupBoxSelection = new System.Windows.Forms.GroupBox();
            this.labelStationCountEnd = new System.Windows.Forms.Label();
            this.labelStationCountBegin = new System.Windows.Forms.Label();
            this.numericUpDownStationCountEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStationCountBegin = new System.Windows.Forms.NumericUpDown();
            this.checkBoxStationsCount = new System.Windows.Forms.CheckBox();
            this.labelSelectedSelection = new System.Windows.Forms.Label();
            this.checkBoxCircle = new System.Windows.Forms.CheckBox();
            this.checkBoxRectangle = new System.Windows.Forms.CheckBox();
            this.groupBoxCircle = new System.Windows.Forms.GroupBox();
            this.numericUpDownCircleRadius = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCircleLng = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCircleLat = new System.Windows.Forms.NumericUpDown();
            this.labelCircleRadius = new System.Windows.Forms.Label();
            this.labelCircleLng = new System.Windows.Forms.Label();
            this.labelCircleLat = new System.Windows.Forms.Label();
            this.groupBoxRectangle = new System.Windows.Forms.GroupBox();
            this.labelRectLat = new System.Windows.Forms.Label();
            this.numericUpDownLatBegin = new System.Windows.Forms.NumericUpDown();
            this.labelRectLng = new System.Windows.Forms.Label();
            this.numericUpDownLatEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLngBegin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLngEnd = new System.Windows.Forms.NumericUpDown();
            this.labelRectLngBegin = new System.Windows.Forms.Label();
            this.labelRectLatStart = new System.Windows.Forms.Label();
            this.labelRectLngEnd = new System.Windows.Forms.Label();
            this.labelRectLatEnd = new System.Windows.Forms.Label();
            this.labelDislplayBy = new System.Windows.Forms.Label();
            this.numericUpDownDisplayBy = new System.Windows.Forms.NumericUpDown();
            this.labelDepthEnd = new System.Windows.Forms.Label();
            this.labelDepthBegin = new System.Windows.Forms.Label();
            this.numericUpDownDepthEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownDepthBegin = new System.Windows.Forms.NumericUpDown();
            this.checkBoxDepth = new System.Windows.Forms.CheckBox();
            this.labelEnergyEnd = new System.Windows.Forms.Label();
            this.labelEnergyBegin = new System.Windows.Forms.Label();
            this.labelTimeEnd = new System.Windows.Forms.Label();
            this.labelTimeBegin = new System.Windows.Forms.Label();
            this.numericUpDownEnergyEnd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownEnergyBegin = new System.Windows.Forms.NumericUpDown();
            this.checkBoxEnergy = new System.Windows.Forms.CheckBox();
            this.timePickerBegin = new System.Windows.Forms.DateTimePicker();
            this.timePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.checkBoxTime = new System.Windows.Forms.CheckBox();
            this.textBoxAuthor = new System.Windows.Forms.TextBox();
            this.checkBoxAuthors = new System.Windows.Forms.CheckBox();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.numericUpDownPage = new System.Windows.Forms.NumericUpDown();
            this.labelPageMax = new System.Windows.Forms.Label();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulletinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulletinIFZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulletinIfzDagestanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csvCatalogJournalMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csvCatalogUserFormatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulettinExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogExcelJournalMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.energyClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialogDocument = new System.Windows.Forms.SaveFileDialog();
            this.groupBoxSort = new System.Windows.Forms.GroupBox();
            this.radioButtonDt = new System.Windows.Forms.RadioButton();
            this.radioButtonDepth = new System.Windows.Forms.RadioButton();
            this.radioButtonMethod = new System.Windows.Forms.RadioButton();
            this.radioButtonClass = new System.Windows.Forms.RadioButton();
            this.comboBoxSortType = new System.Windows.Forms.ComboBox();
            this.radioButtonAuthor = new System.Windows.Forms.RadioButton();
            this.radioButtonId = new System.Windows.Forms.RadioButton();
            this.radioButtonDateAndTime = new System.Windows.Forms.RadioButton();
            this.buttonRecalculate = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonSplitOpenClose = new System.Windows.Forms.Button();
            this.bulletinCustomToolStripMenuItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEarthquake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbEarthquakeBindingSource)).BeginInit();
            this.groupBoxSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStationCountEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStationCountBegin)).BeginInit();
            this.groupBoxCircle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCircleRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCircleLng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCircleLat)).BeginInit();
            this.groupBoxRectangle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDisplayBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDepthEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDepthBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEnergyEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEnergyBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPage)).BeginInit();
            this.menuStripMain.SuspendLayout();
            this.groupBoxSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDateBegin
            // 
            this.labelDateBegin.AutoSize = true;
            this.labelDateBegin.Location = new System.Drawing.Point(170, 33);
            this.labelDateBegin.Name = "labelDateBegin";
            this.labelDateBegin.Size = new System.Drawing.Size(20, 20);
            this.labelDateBegin.TabIndex = 100;
            this.labelDateBegin.Tag = "alwaysVisible";
            this.labelDateBegin.Text = "С";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.AutoSize = true;
            this.labelDateEnd.Location = new System.Drawing.Point(402, 33);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(30, 20);
            this.labelDateEnd.TabIndex = 99;
            this.labelDateEnd.Tag = "alwaysVisible";
            this.labelDateEnd.Text = "По";
            // 
            // datePickerBegin
            // 
            this.datePickerBegin.Location = new System.Drawing.Point(196, 30);
            this.datePickerBegin.Name = "datePickerBegin";
            this.datePickerBegin.Size = new System.Drawing.Size(200, 26);
            this.datePickerBegin.TabIndex = 0;
            this.datePickerBegin.Tag = "alwaysVisible";
            this.datePickerBegin.Value = new System.DateTime(2017, 11, 1, 0, 0, 0, 0);
            this.datePickerBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // datePickerEnd
            // 
            this.datePickerEnd.Location = new System.Drawing.Point(438, 30);
            this.datePickerEnd.Name = "datePickerEnd";
            this.datePickerEnd.Size = new System.Drawing.Size(200, 26);
            this.datePickerEnd.TabIndex = 1;
            this.datePickerEnd.Tag = "alwaysVisible";
            this.datePickerEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // dataGridViewEarthquake
            // 
            this.dataGridViewEarthquake.AllowUserToAddRows = false;
            this.dataGridViewEarthquake.AllowUserToDeleteRows = false;
            this.dataGridViewEarthquake.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEarthquake.AutoGenerateColumns = false;
            this.dataGridViewEarthquake.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEarthquake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEarthquake.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.t0DataGridViewTextBoxColumn,
            this.kDataGridViewTextBoxColumn,
            this.hDataGridViewTextBoxColumn,
            this.latDataGridViewTextBoxColumn,
            this.LatError,
            this.lngDataGridViewTextBoxColumn,
            this.LngError,
            this.dtDataGridViewTextBoxColumn,
            this.DtRms,
            this.vpVsDataGridViewTextBoxColumn,
            this.authorDataGridViewTextBoxColumn,
            this.methodDataGridViewTextBoxColumn});
            this.dataGridViewEarthquake.DataSource = this.dbEarthquakeBindingSource;
            this.dataGridViewEarthquake.Location = new System.Drawing.Point(12, 99);
            this.dataGridViewEarthquake.Name = "dataGridViewEarthquake";
            this.dataGridViewEarthquake.ReadOnly = true;
            this.dataGridViewEarthquake.RowHeadersVisible = false;
            this.dataGridViewEarthquake.Size = new System.Drawing.Size(960, 394);
            this.dataGridViewEarthquake.TabIndex = 7;
            this.dataGridViewEarthquake.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewEarthquake_CellContentDoubleClick);
            this.dataGridViewEarthquake.SelectionChanged += new System.EventHandler(this.DataGridViewEarthquake_SelectionChanged);
            this.dataGridViewEarthquake.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridViewEarthquake_KeyDown);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.FillWeight = 50F;
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.FillWeight = 90F;
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // t0DataGridViewTextBoxColumn
            // 
            this.t0DataGridViewTextBoxColumn.DataPropertyName = "T0";
            dataGridViewCellStyle2.Format = "HH:mm:ss.fff";
            this.t0DataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.t0DataGridViewTextBoxColumn.FillWeight = 110F;
            this.t0DataGridViewTextBoxColumn.HeaderText = "T0";
            this.t0DataGridViewTextBoxColumn.Name = "t0DataGridViewTextBoxColumn";
            this.t0DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // kDataGridViewTextBoxColumn
            // 
            this.kDataGridViewTextBoxColumn.DataPropertyName = "K";
            this.kDataGridViewTextBoxColumn.FillWeight = 80F;
            this.kDataGridViewTextBoxColumn.HeaderText = "K";
            this.kDataGridViewTextBoxColumn.Name = "kDataGridViewTextBoxColumn";
            this.kDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hDataGridViewTextBoxColumn
            // 
            this.hDataGridViewTextBoxColumn.DataPropertyName = "H";
            this.hDataGridViewTextBoxColumn.FillWeight = 40F;
            this.hDataGridViewTextBoxColumn.HeaderText = "H";
            this.hDataGridViewTextBoxColumn.Name = "hDataGridViewTextBoxColumn";
            this.hDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // latDataGridViewTextBoxColumn
            // 
            this.latDataGridViewTextBoxColumn.DataPropertyName = "Lat";
            this.latDataGridViewTextBoxColumn.FillWeight = 80F;
            this.latDataGridViewTextBoxColumn.HeaderText = "Lat";
            this.latDataGridViewTextBoxColumn.Name = "latDataGridViewTextBoxColumn";
            this.latDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // LatError
            // 
            this.LatError.DataPropertyName = "LatError";
            this.LatError.HeaderText = "LatError";
            this.LatError.Name = "LatError";
            this.LatError.ReadOnly = true;
            // 
            // lngDataGridViewTextBoxColumn
            // 
            this.lngDataGridViewTextBoxColumn.DataPropertyName = "Lng";
            this.lngDataGridViewTextBoxColumn.FillWeight = 80F;
            this.lngDataGridViewTextBoxColumn.HeaderText = "Lng";
            this.lngDataGridViewTextBoxColumn.Name = "lngDataGridViewTextBoxColumn";
            this.lngDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // LngError
            // 
            this.LngError.DataPropertyName = "LngError";
            this.LngError.HeaderText = "LngError";
            this.LngError.Name = "LngError";
            this.LngError.ReadOnly = true;
            // 
            // dtDataGridViewTextBoxColumn
            // 
            this.dtDataGridViewTextBoxColumn.DataPropertyName = "Dt";
            this.dtDataGridViewTextBoxColumn.FillWeight = 80F;
            this.dtDataGridViewTextBoxColumn.HeaderText = "Dt";
            this.dtDataGridViewTextBoxColumn.Name = "dtDataGridViewTextBoxColumn";
            this.dtDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // DtRms
            // 
            this.DtRms.DataPropertyName = "DtRms";
            this.DtRms.HeaderText = "DtRms";
            this.DtRms.Name = "DtRms";
            this.DtRms.ReadOnly = true;
            // 
            // vpVsDataGridViewTextBoxColumn
            // 
            this.vpVsDataGridViewTextBoxColumn.DataPropertyName = "VpVs";
            this.vpVsDataGridViewTextBoxColumn.FillWeight = 80F;
            this.vpVsDataGridViewTextBoxColumn.HeaderText = "VpVs";
            this.vpVsDataGridViewTextBoxColumn.Name = "vpVsDataGridViewTextBoxColumn";
            this.vpVsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // authorDataGridViewTextBoxColumn
            // 
            this.authorDataGridViewTextBoxColumn.DataPropertyName = "Author";
            this.authorDataGridViewTextBoxColumn.FillWeight = 110F;
            this.authorDataGridViewTextBoxColumn.HeaderText = "Author";
            this.authorDataGridViewTextBoxColumn.Name = "authorDataGridViewTextBoxColumn";
            this.authorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // methodDataGridViewTextBoxColumn
            // 
            this.methodDataGridViewTextBoxColumn.DataPropertyName = "Method";
            this.methodDataGridViewTextBoxColumn.FillWeight = 110F;
            this.methodDataGridViewTextBoxColumn.HeaderText = "Method";
            this.methodDataGridViewTextBoxColumn.Name = "methodDataGridViewTextBoxColumn";
            this.methodDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dbEarthquakeBindingSource
            // 
            this.dbEarthquakeBindingSource.DataSource = typeof(Seismology.Core.Data.DbEarthquake);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOpen.Location = new System.Drawing.Point(12, 505);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(110, 30);
            this.buttonOpen.TabIndex = 0;
            this.buttonOpen.Text = "Открыть";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.ButtonOpen_Click);
            // 
            // buttonShow
            // 
            this.buttonShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShow.Enabled = false;
            this.buttonShow.Location = new System.Drawing.Point(746, 505);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(110, 30);
            this.buttonShow.TabIndex = 2;
            this.buttonShow.Text = "Показать";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.ButtonShow_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.BackColor = System.Drawing.Color.IndianRed;
            this.buttonDelete.Enabled = false;
            this.buttonDelete.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Firebrick;
            this.buttonDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Location = new System.Drawing.Point(862, 505);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(110, 30);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // groupBoxSelection
            // 
            this.groupBoxSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxSelection.Controls.Add(this.labelStationCountEnd);
            this.groupBoxSelection.Controls.Add(this.labelStationCountBegin);
            this.groupBoxSelection.Controls.Add(this.numericUpDownStationCountEnd);
            this.groupBoxSelection.Controls.Add(this.numericUpDownStationCountBegin);
            this.groupBoxSelection.Controls.Add(this.checkBoxStationsCount);
            this.groupBoxSelection.Controls.Add(this.labelSelectedSelection);
            this.groupBoxSelection.Controls.Add(this.checkBoxCircle);
            this.groupBoxSelection.Controls.Add(this.checkBoxRectangle);
            this.groupBoxSelection.Controls.Add(this.groupBoxCircle);
            this.groupBoxSelection.Controls.Add(this.groupBoxRectangle);
            this.groupBoxSelection.Controls.Add(this.labelDislplayBy);
            this.groupBoxSelection.Controls.Add(this.numericUpDownDisplayBy);
            this.groupBoxSelection.Controls.Add(this.labelDepthEnd);
            this.groupBoxSelection.Controls.Add(this.labelDepthBegin);
            this.groupBoxSelection.Controls.Add(this.numericUpDownDepthEnd);
            this.groupBoxSelection.Controls.Add(this.numericUpDownDepthBegin);
            this.groupBoxSelection.Controls.Add(this.checkBoxDepth);
            this.groupBoxSelection.Controls.Add(this.labelEnergyEnd);
            this.groupBoxSelection.Controls.Add(this.labelEnergyBegin);
            this.groupBoxSelection.Controls.Add(this.labelTimeEnd);
            this.groupBoxSelection.Controls.Add(this.labelTimeBegin);
            this.groupBoxSelection.Controls.Add(this.numericUpDownEnergyEnd);
            this.groupBoxSelection.Controls.Add(this.numericUpDownEnergyBegin);
            this.groupBoxSelection.Controls.Add(this.checkBoxEnergy);
            this.groupBoxSelection.Controls.Add(this.timePickerBegin);
            this.groupBoxSelection.Controls.Add(this.timePickerEnd);
            this.groupBoxSelection.Controls.Add(this.checkBoxTime);
            this.groupBoxSelection.Controls.Add(this.textBoxAuthor);
            this.groupBoxSelection.Controls.Add(this.checkBoxAuthors);
            this.groupBoxSelection.Controls.Add(this.checkBoxDate);
            this.groupBoxSelection.Controls.Add(this.labelDateBegin);
            this.groupBoxSelection.Controls.Add(this.datePickerBegin);
            this.groupBoxSelection.Controls.Add(this.labelDateEnd);
            this.groupBoxSelection.Controls.Add(this.datePickerEnd);
            this.groupBoxSelection.Location = new System.Drawing.Point(12, 3);
            this.groupBoxSelection.Name = "groupBoxSelection";
            this.groupBoxSelection.Size = new System.Drawing.Size(960, 330);
            this.groupBoxSelection.TabIndex = 100;
            this.groupBoxSelection.TabStop = false;
            this.groupBoxSelection.Text = "Выборка";
            // 
            // labelStationCountEnd
            // 
            this.labelStationCountEnd.AutoSize = true;
            this.labelStationCountEnd.Enabled = false;
            this.labelStationCountEnd.Location = new System.Drawing.Point(402, 292);
            this.labelStationCountEnd.Name = "labelStationCountEnd";
            this.labelStationCountEnd.Size = new System.Drawing.Size(30, 20);
            this.labelStationCountEnd.TabIndex = 140;
            this.labelStationCountEnd.Text = "По";
            // 
            // labelStationCountBegin
            // 
            this.labelStationCountBegin.AutoSize = true;
            this.labelStationCountBegin.Enabled = false;
            this.labelStationCountBegin.Location = new System.Drawing.Point(202, 292);
            this.labelStationCountBegin.Name = "labelStationCountBegin";
            this.labelStationCountBegin.Size = new System.Drawing.Size(20, 20);
            this.labelStationCountBegin.TabIndex = 139;
            this.labelStationCountBegin.Text = "С";
            // 
            // numericUpDownStationCountEnd
            // 
            this.numericUpDownStationCountEnd.Enabled = false;
            this.numericUpDownStationCountEnd.Location = new System.Drawing.Point(438, 291);
            this.numericUpDownStationCountEnd.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownStationCountEnd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStationCountEnd.Name = "numericUpDownStationCountEnd";
            this.numericUpDownStationCountEnd.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownStationCountEnd.TabIndex = 138;
            this.numericUpDownStationCountEnd.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownStationCountEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownStationCountBegin
            // 
            this.numericUpDownStationCountBegin.Enabled = false;
            this.numericUpDownStationCountBegin.Location = new System.Drawing.Point(228, 291);
            this.numericUpDownStationCountBegin.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownStationCountBegin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStationCountBegin.Name = "numericUpDownStationCountBegin";
            this.numericUpDownStationCountBegin.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownStationCountBegin.TabIndex = 137;
            this.numericUpDownStationCountBegin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStationCountBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxStationsCount
            // 
            this.checkBoxStationsCount.AutoSize = true;
            this.checkBoxStationsCount.Location = new System.Drawing.Point(24, 291);
            this.checkBoxStationsCount.Name = "checkBoxStationsCount";
            this.checkBoxStationsCount.Size = new System.Drawing.Size(171, 24);
            this.checkBoxStationsCount.TabIndex = 135;
            this.checkBoxStationsCount.Text = "По кол-ву станций:";
            this.checkBoxStationsCount.UseVisualStyleBackColor = true;
            this.checkBoxStationsCount.CheckedChanged += new System.EventHandler(this.CheckBoxStationsCount_CheckedChanged);
            // 
            // labelSelectedSelection
            // 
            this.labelSelectedSelection.AutoSize = true;
            this.labelSelectedSelection.Location = new System.Drawing.Point(126, 0);
            this.labelSelectedSelection.Name = "labelSelectedSelection";
            this.labelSelectedSelection.Size = new System.Drawing.Size(241, 20);
            this.labelSelectedSelection.TabIndex = 106;
            this.labelSelectedSelection.Tag = "";
            this.labelSelectedSelection.Text = "Выбраны: по дате, по авторам";
            this.labelSelectedSelection.Visible = false;
            // 
            // checkBoxCircle
            // 
            this.checkBoxCircle.AutoSize = true;
            this.checkBoxCircle.Location = new System.Drawing.Point(442, 189);
            this.checkBoxCircle.Name = "checkBoxCircle";
            this.checkBoxCircle.Size = new System.Drawing.Size(123, 24);
            this.checkBoxCircle.TabIndex = 133;
            this.checkBoxCircle.Text = "Окружность:";
            this.checkBoxCircle.UseVisualStyleBackColor = true;
            this.checkBoxCircle.CheckedChanged += new System.EventHandler(this.CheckBoxCircle_CheckedChanged);
            // 
            // checkBoxRectangle
            // 
            this.checkBoxRectangle.AutoSize = true;
            this.checkBoxRectangle.Location = new System.Drawing.Point(24, 189);
            this.checkBoxRectangle.Name = "checkBoxRectangle";
            this.checkBoxRectangle.Size = new System.Drawing.Size(150, 24);
            this.checkBoxRectangle.TabIndex = 131;
            this.checkBoxRectangle.Text = "Прямоугольник:";
            this.checkBoxRectangle.UseVisualStyleBackColor = true;
            this.checkBoxRectangle.CheckedChanged += new System.EventHandler(this.CheckBoxRectangle_CheckedChanged);
            // 
            // groupBoxCircle
            // 
            this.groupBoxCircle.Controls.Add(this.numericUpDownCircleRadius);
            this.groupBoxCircle.Controls.Add(this.numericUpDownCircleLng);
            this.groupBoxCircle.Controls.Add(this.numericUpDownCircleLat);
            this.groupBoxCircle.Controls.Add(this.labelCircleRadius);
            this.groupBoxCircle.Controls.Add(this.labelCircleLng);
            this.groupBoxCircle.Controls.Add(this.labelCircleLat);
            this.groupBoxCircle.Location = new System.Drawing.Point(438, 190);
            this.groupBoxCircle.Name = "groupBoxCircle";
            this.groupBoxCircle.Size = new System.Drawing.Size(420, 95);
            this.groupBoxCircle.TabIndex = 134;
            this.groupBoxCircle.TabStop = false;
            // 
            // numericUpDownCircleRadius
            // 
            this.numericUpDownCircleRadius.DecimalPlaces = 3;
            this.numericUpDownCircleRadius.Enabled = false;
            this.numericUpDownCircleRadius.Location = new System.Drawing.Point(306, 25);
            this.numericUpDownCircleRadius.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownCircleRadius.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownCircleRadius.Name = "numericUpDownCircleRadius";
            this.numericUpDownCircleRadius.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownCircleRadius.TabIndex = 140;
            this.numericUpDownCircleRadius.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownCircleRadius.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridViewEarthquake_KeyDown);
            // 
            // numericUpDownCircleLng
            // 
            this.numericUpDownCircleLng.DecimalPlaces = 3;
            this.numericUpDownCircleLng.Enabled = false;
            this.numericUpDownCircleLng.Location = new System.Drawing.Point(92, 57);
            this.numericUpDownCircleLng.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownCircleLng.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownCircleLng.Name = "numericUpDownCircleLng";
            this.numericUpDownCircleLng.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownCircleLng.TabIndex = 139;
            this.numericUpDownCircleLng.Value = new decimal(new int[] {
            47,
            0,
            0,
            0});
            this.numericUpDownCircleLng.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownCircleLat
            // 
            this.numericUpDownCircleLat.DecimalPlaces = 3;
            this.numericUpDownCircleLat.Enabled = false;
            this.numericUpDownCircleLat.Location = new System.Drawing.Point(92, 25);
            this.numericUpDownCircleLat.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownCircleLat.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownCircleLat.Name = "numericUpDownCircleLat";
            this.numericUpDownCircleLat.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownCircleLat.TabIndex = 138;
            this.numericUpDownCircleLat.Value = new decimal(new int[] {
            43,
            0,
            0,
            0});
            this.numericUpDownCircleLat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // labelCircleRadius
            // 
            this.labelCircleRadius.AutoSize = true;
            this.labelCircleRadius.Enabled = false;
            this.labelCircleRadius.Location = new System.Drawing.Point(229, 27);
            this.labelCircleRadius.Name = "labelCircleRadius";
            this.labelCircleRadius.Size = new System.Drawing.Size(67, 20);
            this.labelCircleRadius.TabIndex = 137;
            this.labelCircleRadius.Text = "Радиус:";
            // 
            // labelCircleLng
            // 
            this.labelCircleLng.AutoSize = true;
            this.labelCircleLng.Enabled = false;
            this.labelCircleLng.Location = new System.Drawing.Point(13, 59);
            this.labelCircleLng.Name = "labelCircleLng";
            this.labelCircleLng.Size = new System.Drawing.Size(78, 20);
            this.labelCircleLng.TabIndex = 136;
            this.labelCircleLng.Text = "Долгота:";
            // 
            // labelCircleLat
            // 
            this.labelCircleLat.AutoSize = true;
            this.labelCircleLat.Enabled = false;
            this.labelCircleLat.Location = new System.Drawing.Point(13, 27);
            this.labelCircleLat.Name = "labelCircleLat";
            this.labelCircleLat.Size = new System.Drawing.Size(71, 20);
            this.labelCircleLat.TabIndex = 135;
            this.labelCircleLat.Text = "Широта:";
            // 
            // groupBoxRectangle
            // 
            this.groupBoxRectangle.Controls.Add(this.labelRectLat);
            this.groupBoxRectangle.Controls.Add(this.numericUpDownLatBegin);
            this.groupBoxRectangle.Controls.Add(this.labelRectLng);
            this.groupBoxRectangle.Controls.Add(this.numericUpDownLatEnd);
            this.groupBoxRectangle.Controls.Add(this.numericUpDownLngBegin);
            this.groupBoxRectangle.Controls.Add(this.numericUpDownLngEnd);
            this.groupBoxRectangle.Controls.Add(this.labelRectLngBegin);
            this.groupBoxRectangle.Controls.Add(this.labelRectLatStart);
            this.groupBoxRectangle.Controls.Add(this.labelRectLngEnd);
            this.groupBoxRectangle.Controls.Add(this.labelRectLatEnd);
            this.groupBoxRectangle.Location = new System.Drawing.Point(20, 190);
            this.groupBoxRectangle.Name = "groupBoxRectangle";
            this.groupBoxRectangle.Size = new System.Drawing.Size(412, 95);
            this.groupBoxRectangle.TabIndex = 132;
            this.groupBoxRectangle.TabStop = false;
            // 
            // labelRectLat
            // 
            this.labelRectLat.AutoSize = true;
            this.labelRectLat.Enabled = false;
            this.labelRectLat.Location = new System.Drawing.Point(13, 27);
            this.labelRectLat.Name = "labelRectLat";
            this.labelRectLat.Size = new System.Drawing.Size(71, 20);
            this.labelRectLat.TabIndex = 134;
            this.labelRectLat.Text = "Широта:";
            // 
            // numericUpDownLatBegin
            // 
            this.numericUpDownLatBegin.DecimalPlaces = 3;
            this.numericUpDownLatBegin.Enabled = false;
            this.numericUpDownLatBegin.Location = new System.Drawing.Point(121, 25);
            this.numericUpDownLatBegin.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownLatBegin.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownLatBegin.Name = "numericUpDownLatBegin";
            this.numericUpDownLatBegin.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownLatBegin.TabIndex = 123;
            this.numericUpDownLatBegin.Value = new decimal(new int[] {
            41,
            0,
            0,
            0});
            this.numericUpDownLatBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // labelRectLng
            // 
            this.labelRectLng.AutoSize = true;
            this.labelRectLng.Enabled = false;
            this.labelRectLng.Location = new System.Drawing.Point(13, 59);
            this.labelRectLng.Name = "labelRectLng";
            this.labelRectLng.Size = new System.Drawing.Size(78, 20);
            this.labelRectLng.TabIndex = 133;
            this.labelRectLng.Text = "Долгота:";
            // 
            // numericUpDownLatEnd
            // 
            this.numericUpDownLatEnd.DecimalPlaces = 3;
            this.numericUpDownLatEnd.Enabled = false;
            this.numericUpDownLatEnd.Location = new System.Drawing.Point(293, 25);
            this.numericUpDownLatEnd.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDownLatEnd.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDownLatEnd.Name = "numericUpDownLatEnd";
            this.numericUpDownLatEnd.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownLatEnd.TabIndex = 124;
            this.numericUpDownLatEnd.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.numericUpDownLatEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownLngBegin
            // 
            this.numericUpDownLngBegin.DecimalPlaces = 3;
            this.numericUpDownLngBegin.Enabled = false;
            this.numericUpDownLngBegin.Location = new System.Drawing.Point(121, 57);
            this.numericUpDownLngBegin.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownLngBegin.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownLngBegin.Name = "numericUpDownLngBegin";
            this.numericUpDownLngBegin.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownLngBegin.TabIndex = 125;
            this.numericUpDownLngBegin.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.numericUpDownLngBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownLngEnd
            // 
            this.numericUpDownLngEnd.DecimalPlaces = 3;
            this.numericUpDownLngEnd.Enabled = false;
            this.numericUpDownLngEnd.Location = new System.Drawing.Point(293, 57);
            this.numericUpDownLngEnd.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownLngEnd.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownLngEnd.Name = "numericUpDownLngEnd";
            this.numericUpDownLngEnd.Size = new System.Drawing.Size(100, 26);
            this.numericUpDownLngEnd.TabIndex = 126;
            this.numericUpDownLngEnd.Value = new decimal(new int[] {
            49,
            0,
            0,
            0});
            this.numericUpDownLngEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // labelRectLngBegin
            // 
            this.labelRectLngBegin.AutoSize = true;
            this.labelRectLngBegin.Enabled = false;
            this.labelRectLngBegin.Location = new System.Drawing.Point(95, 59);
            this.labelRectLngBegin.Name = "labelRectLngBegin";
            this.labelRectLngBegin.Size = new System.Drawing.Size(20, 20);
            this.labelRectLngBegin.TabIndex = 130;
            this.labelRectLngBegin.Text = "С";
            // 
            // labelRectLatStart
            // 
            this.labelRectLatStart.AutoSize = true;
            this.labelRectLatStart.Enabled = false;
            this.labelRectLatStart.Location = new System.Drawing.Point(95, 27);
            this.labelRectLatStart.Name = "labelRectLatStart";
            this.labelRectLatStart.Size = new System.Drawing.Size(20, 20);
            this.labelRectLatStart.TabIndex = 127;
            this.labelRectLatStart.Text = "С";
            // 
            // labelRectLngEnd
            // 
            this.labelRectLngEnd.AutoSize = true;
            this.labelRectLngEnd.Enabled = false;
            this.labelRectLngEnd.Location = new System.Drawing.Point(259, 59);
            this.labelRectLngEnd.Name = "labelRectLngEnd";
            this.labelRectLngEnd.Size = new System.Drawing.Size(30, 20);
            this.labelRectLngEnd.TabIndex = 129;
            this.labelRectLngEnd.Text = "По";
            // 
            // labelRectLatEnd
            // 
            this.labelRectLatEnd.AutoSize = true;
            this.labelRectLatEnd.Enabled = false;
            this.labelRectLatEnd.Location = new System.Drawing.Point(259, 27);
            this.labelRectLatEnd.Name = "labelRectLatEnd";
            this.labelRectLatEnd.Size = new System.Drawing.Size(30, 20);
            this.labelRectLatEnd.TabIndex = 128;
            this.labelRectLatEnd.Text = "По";
            // 
            // labelDislplayBy
            // 
            this.labelDislplayBy.AutoSize = true;
            this.labelDislplayBy.Location = new System.Drawing.Point(647, 32);
            this.labelDislplayBy.Name = "labelDislplayBy";
            this.labelDislplayBy.Size = new System.Drawing.Size(109, 20);
            this.labelDislplayBy.TabIndex = 104;
            this.labelDislplayBy.Tag = "alwaysVisible";
            this.labelDislplayBy.Text = "Выводить по";
            // 
            // numericUpDownDisplayBy
            // 
            this.numericUpDownDisplayBy.Location = new System.Drawing.Point(762, 30);
            this.numericUpDownDisplayBy.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownDisplayBy.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownDisplayBy.Name = "numericUpDownDisplayBy";
            this.numericUpDownDisplayBy.Size = new System.Drawing.Size(60, 26);
            this.numericUpDownDisplayBy.TabIndex = 103;
            this.numericUpDownDisplayBy.Tag = "alwaysVisible";
            this.numericUpDownDisplayBy.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownDisplayBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumericUpDownDisplayBy_KeyDown);
            // 
            // labelDepthEnd
            // 
            this.labelDepthEnd.AutoSize = true;
            this.labelDepthEnd.Enabled = false;
            this.labelDepthEnd.Location = new System.Drawing.Point(402, 159);
            this.labelDepthEnd.Name = "labelDepthEnd";
            this.labelDepthEnd.Size = new System.Drawing.Size(30, 20);
            this.labelDepthEnd.TabIndex = 121;
            this.labelDepthEnd.Text = "По";
            // 
            // labelDepthBegin
            // 
            this.labelDepthBegin.AutoSize = true;
            this.labelDepthBegin.Enabled = false;
            this.labelDepthBegin.Location = new System.Drawing.Point(170, 159);
            this.labelDepthBegin.Name = "labelDepthBegin";
            this.labelDepthBegin.Size = new System.Drawing.Size(20, 20);
            this.labelDepthBegin.TabIndex = 120;
            this.labelDepthBegin.Text = "С";
            // 
            // numericUpDownDepthEnd
            // 
            this.numericUpDownDepthEnd.Enabled = false;
            this.numericUpDownDepthEnd.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownDepthEnd.Location = new System.Drawing.Point(438, 158);
            this.numericUpDownDepthEnd.Maximum = new decimal(new int[] {
            102,
            0,
            0,
            0});
            this.numericUpDownDepthEnd.Name = "numericUpDownDepthEnd";
            this.numericUpDownDepthEnd.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownDepthEnd.TabIndex = 119;
            this.numericUpDownDepthEnd.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.numericUpDownDepthEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownDepthBegin
            // 
            this.numericUpDownDepthBegin.Enabled = false;
            this.numericUpDownDepthBegin.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownDepthBegin.Location = new System.Drawing.Point(196, 158);
            this.numericUpDownDepthBegin.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownDepthBegin.Name = "numericUpDownDepthBegin";
            this.numericUpDownDepthBegin.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownDepthBegin.TabIndex = 118;
            this.numericUpDownDepthBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxDepth
            // 
            this.checkBoxDepth.AutoSize = true;
            this.checkBoxDepth.Location = new System.Drawing.Point(24, 157);
            this.checkBoxDepth.Name = "checkBoxDepth";
            this.checkBoxDepth.Size = new System.Drawing.Size(117, 24);
            this.checkBoxDepth.TabIndex = 117;
            this.checkBoxDepth.Text = "По глубине:";
            this.checkBoxDepth.UseVisualStyleBackColor = true;
            this.checkBoxDepth.CheckedChanged += new System.EventHandler(this.CheckBoxDepth_CheckedChanged);
            // 
            // labelEnergyEnd
            // 
            this.labelEnergyEnd.AutoSize = true;
            this.labelEnergyEnd.Enabled = false;
            this.labelEnergyEnd.Location = new System.Drawing.Point(402, 127);
            this.labelEnergyEnd.Name = "labelEnergyEnd";
            this.labelEnergyEnd.Size = new System.Drawing.Size(30, 20);
            this.labelEnergyEnd.TabIndex = 116;
            this.labelEnergyEnd.Text = "По";
            // 
            // labelEnergyBegin
            // 
            this.labelEnergyBegin.AutoSize = true;
            this.labelEnergyBegin.Enabled = false;
            this.labelEnergyBegin.Location = new System.Drawing.Point(170, 127);
            this.labelEnergyBegin.Name = "labelEnergyBegin";
            this.labelEnergyBegin.Size = new System.Drawing.Size(20, 20);
            this.labelEnergyBegin.TabIndex = 115;
            this.labelEnergyBegin.Text = "С";
            // 
            // labelTimeEnd
            // 
            this.labelTimeEnd.AutoSize = true;
            this.labelTimeEnd.Enabled = false;
            this.labelTimeEnd.Location = new System.Drawing.Point(402, 95);
            this.labelTimeEnd.Name = "labelTimeEnd";
            this.labelTimeEnd.Size = new System.Drawing.Size(30, 20);
            this.labelTimeEnd.TabIndex = 114;
            this.labelTimeEnd.Text = "По";
            // 
            // labelTimeBegin
            // 
            this.labelTimeBegin.AutoSize = true;
            this.labelTimeBegin.Enabled = false;
            this.labelTimeBegin.Location = new System.Drawing.Point(170, 95);
            this.labelTimeBegin.Name = "labelTimeBegin";
            this.labelTimeBegin.Size = new System.Drawing.Size(20, 20);
            this.labelTimeBegin.TabIndex = 113;
            this.labelTimeBegin.Text = "С";
            // 
            // numericUpDownEnergyEnd
            // 
            this.numericUpDownEnergyEnd.DecimalPlaces = 1;
            this.numericUpDownEnergyEnd.Enabled = false;
            this.numericUpDownEnergyEnd.Location = new System.Drawing.Point(438, 126);
            this.numericUpDownEnergyEnd.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numericUpDownEnergyEnd.Name = "numericUpDownEnergyEnd";
            this.numericUpDownEnergyEnd.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownEnergyEnd.TabIndex = 112;
            this.numericUpDownEnergyEnd.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.numericUpDownEnergyEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // numericUpDownEnergyBegin
            // 
            this.numericUpDownEnergyBegin.DecimalPlaces = 1;
            this.numericUpDownEnergyBegin.Enabled = false;
            this.numericUpDownEnergyBegin.Location = new System.Drawing.Point(196, 126);
            this.numericUpDownEnergyBegin.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numericUpDownEnergyBegin.Name = "numericUpDownEnergyBegin";
            this.numericUpDownEnergyBegin.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownEnergyBegin.TabIndex = 111;
            this.numericUpDownEnergyBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxEnergy
            // 
            this.checkBoxEnergy.AutoSize = true;
            this.checkBoxEnergy.Location = new System.Drawing.Point(24, 125);
            this.checkBoxEnergy.Name = "checkBoxEnergy";
            this.checkBoxEnergy.Size = new System.Drawing.Size(107, 24);
            this.checkBoxEnergy.TabIndex = 110;
            this.checkBoxEnergy.Text = "По классу:";
            this.checkBoxEnergy.UseVisualStyleBackColor = true;
            this.checkBoxEnergy.CheckedChanged += new System.EventHandler(this.CheckBoxEnergy_CheckedChanged);
            // 
            // timePickerBegin
            // 
            this.timePickerBegin.Enabled = false;
            this.timePickerBegin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timePickerBegin.Location = new System.Drawing.Point(196, 94);
            this.timePickerBegin.Name = "timePickerBegin";
            this.timePickerBegin.ShowUpDown = true;
            this.timePickerBegin.Size = new System.Drawing.Size(200, 26);
            this.timePickerBegin.TabIndex = 106;
            this.timePickerBegin.Value = new System.DateTime(2018, 5, 21, 0, 0, 0, 0);
            this.timePickerBegin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // timePickerEnd
            // 
            this.timePickerEnd.Enabled = false;
            this.timePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timePickerEnd.Location = new System.Drawing.Point(438, 94);
            this.timePickerEnd.Name = "timePickerEnd";
            this.timePickerEnd.ShowUpDown = true;
            this.timePickerEnd.Size = new System.Drawing.Size(200, 26);
            this.timePickerEnd.TabIndex = 107;
            this.timePickerEnd.Value = new System.DateTime(2018, 5, 21, 23, 59, 59, 59);
            this.timePickerEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxTime
            // 
            this.checkBoxTime.AutoSize = true;
            this.checkBoxTime.Location = new System.Drawing.Point(24, 95);
            this.checkBoxTime.Name = "checkBoxTime";
            this.checkBoxTime.Size = new System.Drawing.Size(122, 24);
            this.checkBoxTime.TabIndex = 105;
            this.checkBoxTime.Text = "По времени:";
            this.checkBoxTime.UseVisualStyleBackColor = true;
            this.checkBoxTime.CheckedChanged += new System.EventHandler(this.CheckBoxTime_CheckedChanged);
            // 
            // textBoxAuthor
            // 
            this.textBoxAuthor.Location = new System.Drawing.Point(196, 62);
            this.textBoxAuthor.Name = "textBoxAuthor";
            this.textBoxAuthor.Size = new System.Drawing.Size(442, 26);
            this.textBoxAuthor.TabIndex = 104;
            this.textBoxAuthor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Selection_KeyDown);
            // 
            // checkBoxAuthors
            // 
            this.checkBoxAuthors.AutoSize = true;
            this.checkBoxAuthors.Checked = true;
            this.checkBoxAuthors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAuthors.Location = new System.Drawing.Point(24, 63);
            this.checkBoxAuthors.Name = "checkBoxAuthors";
            this.checkBoxAuthors.Size = new System.Drawing.Size(122, 24);
            this.checkBoxAuthors.TabIndex = 102;
            this.checkBoxAuthors.Text = "По авторам:";
            this.checkBoxAuthors.UseVisualStyleBackColor = true;
            this.checkBoxAuthors.CheckedChanged += new System.EventHandler(this.CheckBoxAuthor_CheckedChanged);
            // 
            // checkBoxDate
            // 
            this.checkBoxDate.AutoSize = true;
            this.checkBoxDate.Checked = true;
            this.checkBoxDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDate.Location = new System.Drawing.Point(24, 31);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(95, 24);
            this.checkBoxDate.TabIndex = 101;
            this.checkBoxDate.Tag = "alwaysVisible";
            this.checkBoxDate.Text = "По дате:";
            this.checkBoxDate.UseVisualStyleBackColor = true;
            this.checkBoxDate.CheckedChanged += new System.EventHandler(this.CheckBoxDate_CheckedChanged);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonNext.Enabled = false;
            this.buttonNext.Location = new System.Drawing.Point(555, 506);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(30, 30);
            this.buttonNext.TabIndex = 6;
            this.buttonNext.Text = ">";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPrevious.Enabled = false;
            this.buttonPrevious.Location = new System.Drawing.Point(415, 506);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(30, 30);
            this.buttonPrevious.TabIndex = 4;
            this.buttonPrevious.Text = "<";
            this.buttonPrevious.UseVisualStyleBackColor = true;
            this.buttonPrevious.Click += new System.EventHandler(this.ButtonPrevious_Click);
            // 
            // numericUpDownPage
            // 
            this.numericUpDownPage.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.numericUpDownPage.Location = new System.Drawing.Point(451, 508);
            this.numericUpDownPage.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPage.Name = "numericUpDownPage";
            this.numericUpDownPage.Size = new System.Drawing.Size(50, 26);
            this.numericUpDownPage.TabIndex = 5;
            this.numericUpDownPage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPage.ValueChanged += new System.EventHandler(this.NumericUpDownPage_ValueChanged);
            // 
            // labelPageMax
            // 
            this.labelPageMax.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelPageMax.AutoSize = true;
            this.labelPageMax.Location = new System.Drawing.Point(509, 510);
            this.labelPageMax.Name = "labelPageMax";
            this.labelPageMax.Size = new System.Drawing.Size(26, 20);
            this.labelPageMax.TabIndex = 7;
            this.labelPageMax.Text = "/ 1";
            // 
            // menuStripMain
            // 
            this.menuStripMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(984, 29);
            this.menuStripMain.TabIndex = 101;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(59, 25);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogToolStripMenuItem,
            this.bulletinToolStripMenuItem,
            this.bulletinCustomToolStripMenuItemToolStripMenuItem,
            this.bulletinIFZToolStripMenuItem,
            this.bulletinIfzDagestanToolStripMenuItem,
            this.csvCatalogJournalMonitoringToolStripMenuItem,
            this.csvCatalogUserFormatToolStripMenuItem,
            this.bulettinExcelToolStripMenuItem,
            this.catalogExcelToolStripMenuItem,
            this.catalogExcelJournalMonitoringToolStripMenuItem,
            this.energyClassToolStripMenuItem});
            this.saveAsToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_save;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.saveAsToolStripMenuItem.Text = "Сохранить как";
            // 
            // catalogToolStripMenuItem
            // 
            this.catalogToolStripMenuItem.Name = "catalogToolStripMenuItem";
            this.catalogToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.catalogToolStripMenuItem.Text = "Каталог";
            this.catalogToolStripMenuItem.Click += new System.EventHandler(this.CatalogToolStripMenuItem_Click);
            // 
            // bulletinToolStripMenuItem
            // 
            this.bulletinToolStripMenuItem.Name = "bulletinToolStripMenuItem";
            this.bulletinToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.bulletinToolStripMenuItem.Text = "Бюллетень";
            this.bulletinToolStripMenuItem.Click += new System.EventHandler(this.BulletinToolStripMenuItem_Click);
            // 
            // bulletinIFZToolStripMenuItem
            // 
            this.bulletinIFZToolStripMenuItem.Name = "bulletinIFZToolStripMenuItem";
            this.bulletinIFZToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.bulletinIFZToolStripMenuItem.Text = "Бюллетень ИФЗ";
            this.bulletinIFZToolStripMenuItem.Click += new System.EventHandler(this.BulletinIFZToolStripMenuItem_Click);
            // 
            // bulletinIfzDagestanToolStripMenuItem
            // 
            this.bulletinIfzDagestanToolStripMenuItem.Name = "bulletinIfzDagestanToolStripMenuItem";
            this.bulletinIfzDagestanToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.bulletinIfzDagestanToolStripMenuItem.Text = "Бюллетень ИФЗ Дагестан";
            this.bulletinIfzDagestanToolStripMenuItem.Click += new System.EventHandler(this.BulletinIfzDagestanToolStripMenuItem_Click);
            // 
            // csvCatalogJournalMonitoringToolStripMenuItem
            // 
            this.csvCatalogJournalMonitoringToolStripMenuItem.Name = "csvCatalogJournalMonitoringToolStripMenuItem";
            this.csvCatalogJournalMonitoringToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.csvCatalogJournalMonitoringToolStripMenuItem.Text = "Каталог CSV (Для мониторинга журнала)";
            this.csvCatalogJournalMonitoringToolStripMenuItem.Click += new System.EventHandler(this.CsvCatalogJournalMonitoringToolStripMenuItem_Click);
            // 
            // csvCatalogUserFormatToolStripMenuItem
            // 
            this.csvCatalogUserFormatToolStripMenuItem.Name = "csvCatalogUserFormatToolStripMenuItem";
            this.csvCatalogUserFormatToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.csvCatalogUserFormatToolStripMenuItem.Text = "Каталог CSV (Свой формат)";
            this.csvCatalogUserFormatToolStripMenuItem.Click += new System.EventHandler(this.CsvCatalogUserFormatToolStripMenuItem_Click);
            // 
            // bulettinExcelToolStripMenuItem
            // 
            this.bulettinExcelToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_excel;
            this.bulettinExcelToolStripMenuItem.Name = "bulettinExcelToolStripMenuItem";
            this.bulettinExcelToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.bulettinExcelToolStripMenuItem.Text = "Бюллетень Excel (Свой формат)";
            this.bulettinExcelToolStripMenuItem.Click += new System.EventHandler(this.BulettinExcelToolStripMenuItem_Click);
            // 
            // catalogExcelToolStripMenuItem
            // 
            this.catalogExcelToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_excel;
            this.catalogExcelToolStripMenuItem.Name = "catalogExcelToolStripMenuItem";
            this.catalogExcelToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.catalogExcelToolStripMenuItem.Text = "Каталог Excel (Свой формат)";
            this.catalogExcelToolStripMenuItem.Click += new System.EventHandler(this.CatalogExcelToolStripMenuItem_Click);
            // 
            // catalogExcelJournalMonitoringToolStripMenuItem
            // 
            this.catalogExcelJournalMonitoringToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_excel;
            this.catalogExcelJournalMonitoringToolStripMenuItem.Name = "catalogExcelJournalMonitoringToolStripMenuItem";
            this.catalogExcelJournalMonitoringToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.catalogExcelJournalMonitoringToolStripMenuItem.Text = "Каталог Excel (Для мониторинга журнала)";
            this.catalogExcelJournalMonitoringToolStripMenuItem.Click += new System.EventHandler(this.catalogExcelJournalMonitoringToolStripMenuItem_Click);
            // 
            // energyClassToolStripMenuItem
            // 
            this.energyClassToolStripMenuItem.Name = "energyClassToolStripMenuItem";
            this.energyClassToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.energyClassToolStripMenuItem.Text = "Энергетический класс";
            this.energyClassToolStripMenuItem.Click += new System.EventHandler(this.EnergyClassToolStripMenuItem_Click);
            // 
            // saveFileDialogDocument
            // 
            this.saveFileDialogDocument.FileName = "Документ";
            this.saveFileDialogDocument.Filter = "Текстовые документы (*.txt) | *.txt;";
            // 
            // groupBoxSort
            // 
            this.groupBoxSort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSort.Controls.Add(this.radioButtonDt);
            this.groupBoxSort.Controls.Add(this.radioButtonDepth);
            this.groupBoxSort.Controls.Add(this.radioButtonMethod);
            this.groupBoxSort.Controls.Add(this.radioButtonClass);
            this.groupBoxSort.Controls.Add(this.comboBoxSortType);
            this.groupBoxSort.Controls.Add(this.radioButtonAuthor);
            this.groupBoxSort.Controls.Add(this.radioButtonId);
            this.groupBoxSort.Controls.Add(this.radioButtonDateAndTime);
            this.groupBoxSort.Location = new System.Drawing.Point(12, 6);
            this.groupBoxSort.Name = "groupBoxSort";
            this.groupBoxSort.Size = new System.Drawing.Size(960, 79);
            this.groupBoxSort.TabIndex = 102;
            this.groupBoxSort.TabStop = false;
            this.groupBoxSort.Text = "Сортировка";
            // 
            // radioButtonDt
            // 
            this.radioButtonDt.AutoSize = true;
            this.radioButtonDt.Location = new System.Drawing.Point(565, 33);
            this.radioButtonDt.Name = "radioButtonDt";
            this.radioButtonDt.Size = new System.Drawing.Size(91, 24);
            this.radioButtonDt.TabIndex = 114;
            this.radioButtonDt.Text = "Невязка";
            this.radioButtonDt.UseVisualStyleBackColor = true;
            this.radioButtonDt.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // radioButtonDepth
            // 
            this.radioButtonDepth.AutoSize = true;
            this.radioButtonDepth.Location = new System.Drawing.Point(470, 33);
            this.radioButtonDepth.Name = "radioButtonDepth";
            this.radioButtonDepth.Size = new System.Drawing.Size(89, 24);
            this.radioButtonDepth.TabIndex = 113;
            this.radioButtonDepth.Text = "Глубина";
            this.radioButtonDepth.UseVisualStyleBackColor = true;
            this.radioButtonDepth.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // radioButtonMethod
            // 
            this.radioButtonMethod.AutoSize = true;
            this.radioButtonMethod.Location = new System.Drawing.Point(742, 33);
            this.radioButtonMethod.Name = "radioButtonMethod";
            this.radioButtonMethod.Size = new System.Drawing.Size(78, 24);
            this.radioButtonMethod.TabIndex = 112;
            this.radioButtonMethod.Text = "Метод";
            this.radioButtonMethod.UseVisualStyleBackColor = true;
            this.radioButtonMethod.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // radioButtonClass
            // 
            this.radioButtonClass.AutoSize = true;
            this.radioButtonClass.Location = new System.Drawing.Point(392, 33);
            this.radioButtonClass.Name = "radioButtonClass";
            this.radioButtonClass.Size = new System.Drawing.Size(72, 24);
            this.radioButtonClass.TabIndex = 111;
            this.radioButtonClass.Text = "Класс";
            this.radioButtonClass.UseVisualStyleBackColor = true;
            this.radioButtonClass.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // comboBoxSortType
            // 
            this.comboBoxSortType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSortType.FormattingEnabled = true;
            this.comboBoxSortType.Items.AddRange(new object[] {
            "По возрастанию",
            "По убыванию"});
            this.comboBoxSortType.Location = new System.Drawing.Point(24, 32);
            this.comboBoxSortType.Name = "comboBoxSortType";
            this.comboBoxSortType.Size = new System.Drawing.Size(175, 28);
            this.comboBoxSortType.TabIndex = 107;
            this.comboBoxSortType.SelectedIndexChanged += new System.EventHandler(this.ComboBoxSortType_SelectedIndexChanged);
            // 
            // radioButtonAuthor
            // 
            this.radioButtonAuthor.AutoSize = true;
            this.radioButtonAuthor.Location = new System.Drawing.Point(662, 33);
            this.radioButtonAuthor.Name = "radioButtonAuthor";
            this.radioButtonAuthor.Size = new System.Drawing.Size(74, 24);
            this.radioButtonAuthor.TabIndex = 110;
            this.radioButtonAuthor.Text = "Автор";
            this.radioButtonAuthor.UseVisualStyleBackColor = true;
            this.radioButtonAuthor.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // radioButtonId
            // 
            this.radioButtonId.AutoSize = true;
            this.radioButtonId.Checked = true;
            this.radioButtonId.Location = new System.Drawing.Point(209, 33);
            this.radioButtonId.Name = "radioButtonId";
            this.radioButtonId.Size = new System.Drawing.Size(41, 24);
            this.radioButtonId.TabIndex = 108;
            this.radioButtonId.TabStop = true;
            this.radioButtonId.Text = "Id";
            this.radioButtonId.UseVisualStyleBackColor = true;
            this.radioButtonId.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // radioButtonDateAndTime
            // 
            this.radioButtonDateAndTime.AutoSize = true;
            this.radioButtonDateAndTime.Location = new System.Drawing.Point(256, 33);
            this.radioButtonDateAndTime.Name = "radioButtonDateAndTime";
            this.radioButtonDateAndTime.Size = new System.Drawing.Size(130, 24);
            this.radioButtonDateAndTime.TabIndex = 109;
            this.radioButtonDateAndTime.Text = "Дата и время";
            this.radioButtonDateAndTime.UseVisualStyleBackColor = true;
            this.radioButtonDateAndTime.CheckedChanged += new System.EventHandler(this.RadioButtonSort_CheckedChanged);
            // 
            // buttonRecalculate
            // 
            this.buttonRecalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRecalculate.Enabled = false;
            this.buttonRecalculate.Location = new System.Drawing.Point(142, 505);
            this.buttonRecalculate.Name = "buttonRecalculate";
            this.buttonRecalculate.Size = new System.Drawing.Size(120, 30);
            this.buttonRecalculate.TabIndex = 1;
            this.buttonRecalculate.Text = "Пересчитать";
            this.buttonRecalculate.UseVisualStyleBackColor = true;
            this.buttonRecalculate.Click += new System.EventHandler(this.ButtonRecalculate_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 29);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBoxSelection);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.buttonOpen);
            this.splitContainer1.Panel2.Controls.Add(this.buttonShow);
            this.splitContainer1.Panel2.Controls.Add(this.buttonDelete);
            this.splitContainer1.Panel2.Controls.Add(this.buttonRecalculate);
            this.splitContainer1.Panel2.Controls.Add(this.buttonNext);
            this.splitContainer1.Panel2.Controls.Add(this.buttonPrevious);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownPage);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewEarthquake);
            this.splitContainer1.Panel2.Controls.Add(this.labelPageMax);
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxSort);
            this.splitContainer1.Size = new System.Drawing.Size(984, 890);
            this.splitContainer1.SplitterDistance = 345;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 105;
            // 
            // buttonSplitOpenClose
            // 
            this.buttonSplitOpenClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.buttonSplitOpenClose.Location = new System.Drawing.Point(100, 30);
            this.buttonSplitOpenClose.Name = "buttonSplitOpenClose";
            this.buttonSplitOpenClose.Size = new System.Drawing.Size(30, 30);
            this.buttonSplitOpenClose.TabIndex = 105;
            this.buttonSplitOpenClose.Text = "-";
            this.buttonSplitOpenClose.UseVisualStyleBackColor = true;
            this.buttonSplitOpenClose.Click += new System.EventHandler(this.ButtonSplitOpenClose_Click);
            // 
            // bulletinCustomToolStripMenuItemToolStripMenuItem
            // 
            this.bulletinCustomToolStripMenuItemToolStripMenuItem.Name = "bulletinCustomToolStripMenuItemToolStripMenuItem";
            this.bulletinCustomToolStripMenuItemToolStripMenuItem.Size = new System.Drawing.Size(379, 26);
            this.bulletinCustomToolStripMenuItemToolStripMenuItem.Text = "Бюллетень (свой формат)";
            this.bulletinCustomToolStripMenuItemToolStripMenuItem.Click += new System.EventHandler(this.bulletinCustomToolStripMenuItemToolStripMenuItem_Click);
            // 
            // DbForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 919);
            this.Controls.Add(this.buttonSplitOpenClose);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStripMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.menuStripMain;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(880, 400);
            this.Name = "DbForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "База данных";
            this.Load += new System.EventHandler(this.DbForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEarthquake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbEarthquakeBindingSource)).EndInit();
            this.groupBoxSelection.ResumeLayout(false);
            this.groupBoxSelection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStationCountEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStationCountBegin)).EndInit();
            this.groupBoxCircle.ResumeLayout(false);
            this.groupBoxCircle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCircleRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCircleLng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCircleLat)).EndInit();
            this.groupBoxRectangle.ResumeLayout(false);
            this.groupBoxRectangle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLatEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLngEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDisplayBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDepthEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDepthBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEnergyEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEnergyBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPage)).EndInit();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.groupBoxSort.ResumeLayout(false);
            this.groupBoxSort.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDateBegin;
        private System.Windows.Forms.Label labelDateEnd;
        private System.Windows.Forms.DateTimePicker datePickerBegin;
        private System.Windows.Forms.DateTimePicker datePickerEnd;
        private System.Windows.Forms.DataGridView dataGridViewEarthquake;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.BindingSource dbEarthquakeBindingSource;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.GroupBox groupBoxSelection;
        private System.Windows.Forms.Button buttonRecalculate;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.NumericUpDown numericUpDownPage;
        private System.Windows.Forms.Label labelPageMax;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialogDocument;
        private System.Windows.Forms.ToolStripMenuItem catalogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bulletinToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxSort;
        private System.Windows.Forms.RadioButton radioButtonClass;
        private System.Windows.Forms.ComboBox comboBoxSortType;
        private System.Windows.Forms.RadioButton radioButtonAuthor;
        private System.Windows.Forms.RadioButton radioButtonId;
        private System.Windows.Forms.RadioButton radioButtonDateAndTime;
        private System.Windows.Forms.CheckBox checkBoxAuthors;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.TextBox textBoxAuthor;
        private System.Windows.Forms.RadioButton radioButtonMethod;
        private System.Windows.Forms.NumericUpDown numericUpDownDisplayBy;
        private System.Windows.Forms.Label labelDislplayBy;
        private System.Windows.Forms.CheckBox checkBoxTime;
        private System.Windows.Forms.DateTimePicker timePickerBegin;
        private System.Windows.Forms.DateTimePicker timePickerEnd;
        private System.Windows.Forms.CheckBox checkBoxEnergy;
        private System.Windows.Forms.NumericUpDown numericUpDownEnergyEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownEnergyBegin;
        private System.Windows.Forms.Label labelEnergyEnd;
        private System.Windows.Forms.Label labelEnergyBegin;
        private System.Windows.Forms.Label labelTimeEnd;
        private System.Windows.Forms.Label labelTimeBegin;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonSplitOpenClose;
        private System.Windows.Forms.Label labelDepthEnd;
        private System.Windows.Forms.Label labelDepthBegin;
        private System.Windows.Forms.NumericUpDown numericUpDownDepthEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownDepthBegin;
        private System.Windows.Forms.CheckBox checkBoxDepth;
        private System.Windows.Forms.RadioButton radioButtonDepth;
        private System.Windows.Forms.Label labelRectLngBegin;
        private System.Windows.Forms.Label labelRectLngEnd;
        private System.Windows.Forms.Label labelRectLatEnd;
        private System.Windows.Forms.Label labelRectLatStart;
        private System.Windows.Forms.NumericUpDown numericUpDownLngEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownLngBegin;
        private System.Windows.Forms.NumericUpDown numericUpDownLatEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownLatBegin;
        private System.Windows.Forms.GroupBox groupBoxRectangle;
        private System.Windows.Forms.Label labelRectLat;
        private System.Windows.Forms.Label labelRectLng;
        private System.Windows.Forms.CheckBox checkBoxRectangle;
        private System.Windows.Forms.GroupBox groupBoxCircle;
        private System.Windows.Forms.CheckBox checkBoxCircle;
        private System.Windows.Forms.NumericUpDown numericUpDownCircleLng;
        private System.Windows.Forms.NumericUpDown numericUpDownCircleLat;
        private System.Windows.Forms.Label labelCircleRadius;
        private System.Windows.Forms.Label labelCircleLng;
        private System.Windows.Forms.Label labelCircleLat;
        private System.Windows.Forms.NumericUpDown numericUpDownCircleRadius;
        private System.Windows.Forms.Label labelSelectedSelection;
        private System.Windows.Forms.ToolStripMenuItem bulletinIFZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csvCatalogJournalMonitoringToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButtonDt;
        private System.Windows.Forms.ToolStripMenuItem bulletinIfzDagestanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csvCatalogUserFormatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem energyClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bulettinExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogExcelJournalMonitoringToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn latDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LatError;
        private System.Windows.Forms.DataGridViewTextBoxColumn lngDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LngError;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DtRms;
        private System.Windows.Forms.DataGridViewTextBoxColumn vpVsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn methodDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox checkBoxStationsCount;
        private System.Windows.Forms.Label labelStationCountEnd;
        private System.Windows.Forms.Label labelStationCountBegin;
        private System.Windows.Forms.NumericUpDown numericUpDownStationCountEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownStationCountBegin;
        private System.Windows.Forms.ToolStripMenuItem bulletinCustomToolStripMenuItemToolStripMenuItem;
    }
}