﻿using PagedList;
using Seismology.Core;
using Seismology.Core.Data;
using Seismology.Core.Data.Access;
using Seismology.Core.IO;
using Seismology.Core.Map;
using Seismology.Core.MathS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

// ReSharper disable LocalizableElement

namespace Seismology.MainApp
{
    public partial class DbForm : Form
    {
        private IPagedList<DbEarthquake> _list;

        public DbForm()
        {
            InitializeComponent();

            _sortType = 0;
        }

        private void DbForm_Load(object sender, EventArgs e)
        {
            CloseSplit();
            comboBoxSortType.SelectedIndex = 0;
            datePickerBegin.Value = DateTime.Now.AddYears(-1);
            textBoxAuthor.Text = Settings.MySettings.UserName;
            _oldDisplayByValue = (int)numericUpDownDisplayBy.Value;
        }

        #region Button Events

        private void ButtonOpen_Click(object sender, EventArgs e)
        {
            OpenAsync();
        }

        private async void ButtonRecalculate_Click(object sender, EventArgs e)
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                int id = Convert.ToInt32(dataGridViewEarthquake.CurrentRow.Cells[0].Value);
                var db = new DbAccess(Settings.MySettings.CurrentDataSource);
                buttonRecalculate.Enabled = false;
                var earthquake = await db.GetAsync(id);
                buttonRecalculate.Enabled = true;
                var calcDbForm = new CalculateDbForm(earthquake);
                calcDbForm.ShowDialog();
                if (calcDbForm.DialogResult == DialogResult.OK)
                {
                    UpdateDataGridAsync();
                }
            }
        }

        private void ButtonShow_Click(object sender, EventArgs e)
        {
            OpenStationsDataAsync();
        }

        private async void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                var idList = dataGridViewEarthquake.SelectedCells
                    .Cast<DataGridViewCell>()
                    .Select(dgvc =>
                        Convert.ToInt32(dataGridViewEarthquake.Rows[dgvc.RowIndex].Cells[0].Value))
                    .OrderBy(id => id)
                    .ToList();

                string message = string.Empty;

                if (idList.Count > 1)
                    message = "Вы точно хотите удалить следующие записи, под номерами:\r\n";
                else if (idList.Count == 1)
                    message = "Вы точно хотите удалить следующую запись, под номером:\r\n";

                message += $"{string.Join(",", idList)} ?";
                if (MessageBox.Show(
                        message,
                        "Внимание",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var db = new DbAccess(Settings.MySettings.CurrentDataSource);
                    await db.DeleteAsync(idList);
                    UpdateDataGridAfterDeleteAsync();
                }
            }
        }

        #endregion

        #region Logic

        private int _sortType;

        private IEnumerable<DbEarthquake> SortData(IEnumerable<DbEarthquake> earthquakes)
        {
            if (radioButtonId.Checked)
            {
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.Id);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.Id);
                }
            }
            else if (radioButtonDateAndTime.Checked)
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.Date).ThenBy(e => e.T0);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.Date).ThenBy(e => e.T0);
                }
            else if (radioButtonClass.Checked)
            {
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.K);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.K);
                }
            }
            else if (radioButtonAuthor.Checked)
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.Author);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.Author);
                }
            else if (radioButtonMethod.Checked)
            {
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.Method);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.Method);
                }
            }
            else if (radioButtonDepth.Checked)
            {
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.H);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.H);
                }
            }
            else if (radioButtonDt.Checked)
            {
                switch (_sortType)
                {
                    case 0:
                        return earthquakes.OrderBy(e => e.Dt);
                    case 1:
                        return earthquakes.OrderByDescending(e => e.Dt);
                }
            }
            return earthquakes;
        }

        private async Task<IEnumerable<DbEarthquake>> GetDataFromDbAsync(bool isIncludeStations, bool isForBulletin)
        {
            var db = new DbAccess(Settings.MySettings.CurrentDataSource);
            // Date
            DateTime? dateBegin = null, dateEnd = null;
            if (checkBoxDate.Checked)
            {
                dateBegin = datePickerBegin.Value.Date;
                dateEnd = datePickerEnd.Value.Date;
            }
            // Time
            DateTime? timeBegin = null, timeEnd = null;
            if (checkBoxTime.Checked)
            {
                timeBegin = timePickerBegin.Value;
                timeEnd = timePickerEnd.Value;
            }
            // Authors
            string[] authors = null;
            if (checkBoxAuthors.Checked)
                authors = textBoxAuthor.Text.Trim().Split();
            // Energy
            double? energyBegin = null, energyEnd = null;
            if (checkBoxEnergy.Checked)
            {
                energyBegin = (double)numericUpDownEnergyBegin.Value;
                energyEnd = (double)numericUpDownEnergyEnd.Value;
            }
            // Depth
            double? depthBegin = null, depthEnd = null;
            if (checkBoxDepth.Checked)
            {
                depthBegin = (double)numericUpDownDepthBegin.Value;
                depthEnd = (double)numericUpDownDepthEnd.Value;
            }
            // StationsCount
            int? stationCountBegin = null, stationCountEnd = null;
            if (checkBoxStationsCount.Checked)
            {
                stationCountBegin = (int)numericUpDownStationCountBegin.Value;
                stationCountEnd = (int)numericUpDownStationCountEnd.Value;
            }
            // Rectangle
            RectangleS rectangle = null;
            if (checkBoxRectangle.Checked)
            {
                rectangle = new RectangleS(
                    new Coordinate((double)numericUpDownLatBegin.Value, (double)numericUpDownLngBegin.Value),
                    new Coordinate((double)numericUpDownLatEnd.Value, (double)numericUpDownLngEnd.Value));
            }
            // Circle
            CircleS circle = checkBoxCircle.Checked ?
                new CircleS((double)numericUpDownCircleLat.Value, (double)numericUpDownCircleLng.Value, (double)numericUpDownCircleRadius.Value) :
                null;

            return SortData(await db.GetAsync(dateBegin, dateEnd, timeBegin, timeEnd, energyBegin, energyEnd, depthBegin, depthEnd, stationCountBegin, stationCountEnd, authors, rectangle, circle, isIncludeStations, isForBulletin));
        }

        private async void OpenStationsDataAsync()
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                int id = Convert.ToInt32(dataGridViewEarthquake.CurrentRow.Cells[0].Value);
                var db = new DbAccess(Settings.MySettings.CurrentDataSource);
                var dbEarthquake = await db.GetAsync(id);
                var stationForm = new DbStationForm(dbEarthquake);
                stationForm.Show();
            }
        }

        #endregion

        #region Pagination

        private async Task<IPagedList<DbEarthquake>> GetDataAsync(int pageNumber, int pageSize)
        {
            var earthquakes = await GetDataFromDbAsync(false, false);
            return await Task.Factory.StartNew(() => earthquakes.ToPagedList(pageNumber, pageSize));
        }

        private async void UpdateDataGridAsync()
        {
            _list = await GetDataAsync((int)numericUpDownPage.Value, (int)numericUpDownDisplayBy.Value);
            dataGridViewEarthquake.DataSource = _list.ToList();

            buttonPrevious.Enabled = _list.HasPreviousPage;
            buttonNext.Enabled = _list.HasNextPage;
        }

        private async void UpdateDataGridAfterDeleteAsync()
        {
            _list = await GetDataAsync((int)numericUpDownPage.Value, (int)numericUpDownDisplayBy.Value);
            dataGridViewEarthquake.DataSource = _list.ToList();

            if (_list.PageCount == 0) return;

            buttonPrevious.Enabled = _list.HasPreviousPage;
            buttonNext.Enabled = _list.HasNextPage;
            numericUpDownPage.Maximum = _list.PageCount;
            labelPageMax.Text = $"/ {_list.PageCount}";
        }

        private async void OpenAsync()
        {
            _list = await GetDataAsync(1, (int)numericUpDownDisplayBy.Value);
            dataGridViewEarthquake.DataSource = _list.ToList();

            if (_list.PageCount == 0) return;

            buttonPrevious.Enabled = _list.HasPreviousPage;
            buttonNext.Enabled = _list.HasNextPage;
            numericUpDownPage.Value = 1;
            numericUpDownPage.Maximum = _list.PageCount;
            labelPageMax.Text = $"/ {_list.PageCount}";
        }

        #endregion

        #region File Events

        private bool IsEmptyList()
        {
            if (_list == null)
            {
                MessageBox.Show("Данные, которые нужно сохранить, не выбраны!");
                return true;
            }

            return false;
        }

        private async void CatalogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(false, false)).Select(data => (Earthquake)data).ToList();
            saveFileDialogDocument.FileName = FileSHlp.GetCatalogFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.TxtFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var fileS = new FileS();
            fileS.WriteCatalogTxt(path, earthquakes);
        }

        private async void BulletinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
            saveFileDialogDocument.FileName = FileSHlp.GetBulletinFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.TxtFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var fileS = new FileS();
            fileS.WriteBulletinTxt(path, earthquakes);
        }

        private async void bulletinCustomToolStripMenuItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();

            var formatForm = new FormBulletinFormatSelection(earthquakes);
            formatForm.ShowDialog();
            if (formatForm.DialogResult == DialogResult.OK)
            {
                var groups = formatForm.GetSelectedGroups();

                earthquakes.ForEach(eq =>
                    eq.Stations = eq.Stations.Where(s => groups.Contains(s.Station.Group)).ToList());
                
                saveFileDialogDocument.FileName = FileSHlp.GetBulletinFullName(earthquakes);
                saveFileDialogDocument.Filter = FileSHlp.TxtFilter;
                if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
                string path = saveFileDialogDocument.FileName;
                var fileS = new FileS();
                fileS.WriteBulletinTxt(path, earthquakes);
            }
        }

        private async void BulletinIFZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
            saveFileDialogDocument.FileName = FileSHlp.GetBulletinIfzFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.TxtFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var fileS = new FileS();
            fileS.WriteBulletinIfzTxt(path, earthquakes);
        }

        private async void BulletinIfzDagestanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
            saveFileDialogDocument.FileName = FileSHlp.GetBulletinIfzFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.TxtFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var fileS = new FileS();
            fileS.WriteBulletinIfzTxtDagestan(path, earthquakes);
        }

        private async void CsvCatalogJournalMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;
            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();

            saveFileDialogDocument.FileName = FileSHlp.GetCatalogFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.CsvFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var fileS = new FileS();
            fileS.WriteCsvCatalogJournalMonitoring(path, earthquakes);
        }

        private async void CsvCatalogUserFormatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var formatForm = new FormExcelWriteFormatSelection(false, ExcelType.Csv);
            formatForm.ShowDialog();
            if (formatForm.DialogResult == DialogResult.OK)
            {
                var formats = formatForm.GetFormats();
                var formatsName = formatForm.GetFormatsName();
                var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
                char delimiter = formatForm.GetDelimeter();

                saveFileDialogDocument.FileName = FileSHlp.GetCatalogFullName(earthquakes);
                saveFileDialogDocument.Filter = FileSHlp.CsvFilter;
                if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
                string path = saveFileDialogDocument.FileName;
                var fileS = new FileS();
                fileS.WriteCatalogCsv(path, earthquakes, formats, formatsName, delimiter);
            }
        }

        private async void CatalogExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            this.Enabled = false;
            var formatForm = new FormExcelWriteFormatSelection(false, ExcelType.Xlsx);
            formatForm.ShowDialog();
            if (formatForm.DialogResult == DialogResult.OK)
            {
                var formats = formatForm.GetFormats();
                var formatsName = formatForm.GetFormatsName();
                var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
                string worksheetName = formatForm.GetWorksheetName();
                saveFileDialogDocument.FileName = FileSHlp.GetCatalogFullName(earthquakes);
                saveFileDialogDocument.Filter = FileSHlp.ExcelFilter;
                if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
                string path = saveFileDialogDocument.FileName;
                var fileS = new FileS();
                fileS.WriteCatalogExcel(path, earthquakes, formats, formatsName, worksheetName);
            }

            this.Enabled = true;
        }

        private async void BulettinExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            this.Enabled = false;
            var formatForm = new FormExcelWriteFormatSelection(true, ExcelType.Xlsx);
            formatForm.ShowDialog();
            if (formatForm.DialogResult == DialogResult.OK)
            {
                var formats = formatForm.GetFormats();
                var formatsName = formatForm.GetFormatsName();
                var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
                string worksheetName = formatForm.GetWorksheetName();
                saveFileDialogDocument.FileName = FileSHlp.GetBulletinFullName(earthquakes);
                saveFileDialogDocument.Filter = FileSHlp.ExcelFilter;
                if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
                string path = saveFileDialogDocument.FileName;
                var fileS = new FileS();
                fileS.WriteBulletinExcel(path, earthquakes, formats, formatsName, worksheetName);
            }

            this.Enabled = true;
        }

        private async void catalogExcelJournalMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
            saveFileDialogDocument.FileName = FileSHlp.GetCatalogFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.ExcelFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var fileS = new FileS();
            fileS.WriteXlsxCatalogJournalMonitoring(path, earthquakes);
        }

        private async void EnergyClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsEmptyList()) return;

            var earthquakes = (await GetDataFromDbAsync(true, true)).Select(data => (Earthquake)data).ToList();
            saveFileDialogDocument.FileName = FileSHlp.GetEnergyClassFullName(earthquakes);
            saveFileDialogDocument.Filter = FileSHlp.TxtFilter;
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var math = new CalculatorS();
            double energy = math.FindEnergyClass(earthquakes);
            var fileS = new FileS();
            fileS.WriteEnergyClass(path, energy);
        }

        #endregion

        #region dataGridView Операции

        private void DataGridViewEarthquake_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OpenStationsDataAsync();
        }

        private void DataGridViewEarthquake_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OpenStationsDataAsync();
            }
        }

        private void DataGridViewEarthquake_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                buttonDelete.Enabled = true;
                buttonRecalculate.Enabled = true;
                buttonShow.Enabled = true;
            }
        }

        #endregion

        #region Пагинация

        private void ButtonNext_Click(object sender, EventArgs e)
        {
            if (numericUpDownPage.Value < numericUpDownPage.Maximum)
            {
                numericUpDownPage.Value++;
            }
        }

        private void ButtonPrevious_Click(object sender, EventArgs e)
        {
            if (numericUpDownPage.Value > numericUpDownPage.Minimum)
            {
                numericUpDownPage.Value--;
            }
        }

        private void NumericUpDownPage_ValueChanged(object sender, EventArgs e)
        {
            UpdateDataGridAsync();
        }

        #endregion



        #region Selection Events

        private void Selection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;

            OpenAsync();
            e.SuppressKeyPress = true;
        }

        private void CheckBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            labelDateBegin.Enabled = checkBoxDate.Checked;
            datePickerBegin.Enabled = checkBoxDate.Checked;
            labelDateEnd.Enabled = checkBoxDate.Checked;
            datePickerEnd.Enabled = checkBoxDate.Checked;
        }

        private void CheckBoxAuthor_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonAuthor.Checked)
                radioButtonId.Checked = true;
            textBoxAuthor.Enabled = checkBoxAuthors.Checked;
        }

        private void CheckBoxTime_CheckedChanged(object sender, EventArgs e)
        {
            var check = checkBoxTime.Checked;
            labelTimeBegin.Enabled = check;
            timePickerBegin.Enabled = check;
            labelTimeEnd.Enabled = check;
            timePickerEnd.Enabled = check;
        }

        private void CheckBoxEnergy_CheckedChanged(object sender, EventArgs e)
        {
            bool check = checkBoxEnergy.Checked;
            labelEnergyBegin.Enabled = check;
            numericUpDownEnergyBegin.Enabled = check;
            labelEnergyEnd.Enabled = check;
            numericUpDownEnergyEnd.Enabled = check;
        }

        private void CheckBoxDepth_CheckedChanged(object sender, EventArgs e)
        {
            bool check = checkBoxDepth.Checked;
            labelDepthBegin.Enabled = check;
            numericUpDownDepthBegin.Enabled = check;
            labelDepthEnd.Enabled = check;
            numericUpDownDepthEnd.Enabled = check;
        }

        private void CheckBoxRectangle_CheckedChanged(object sender, EventArgs e)
        {
            bool check = checkBoxRectangle.Checked;
            if (check)
            {
                checkBoxCircle.Checked = false;
            }
            foreach (Control control in groupBoxRectangle.Controls)
            {
                control.Enabled = check;
            }
        }

        private void CheckBoxCircle_CheckedChanged(object sender, EventArgs e)
        {
            bool check = checkBoxCircle.Checked;
            if (check)
            {
                checkBoxRectangle.Checked = false;
            }
            foreach (Control control in groupBoxCircle.Controls)
            {
                control.Enabled = check;
            }
        }

        private void CheckBoxStationsCount_CheckedChanged(object sender, EventArgs e)
        {
            bool check = checkBoxStationsCount.Checked;

            labelStationCountBegin.Enabled = check;
            numericUpDownStationCountBegin.Enabled = check;
            labelStationCountEnd.Enabled = check;
            numericUpDownStationCountEnd.Enabled = check;
        }

        #endregion

        #region Sort Events

        private void ComboBoxSortType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_list != null)
            {
                _sortType = comboBoxSortType.SelectedIndex;
                UpdateDataGridAsync();
            }
        }

        private void RadioButtonSort_CheckedChanged(object sender, EventArgs e)
        {
            if (_list != null)
            {
                UpdateDataGridAsync();
            }
        }

        #endregion

        private int _oldDisplayByValue;
        private async void NumericUpDownDisplayBy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _list = await GetDataAsync(1, (int)numericUpDownDisplayBy.Value);

                if (_list.PageCount == 0) return;

                buttonPrevious.Enabled = _list.HasPreviousPage;
                buttonNext.Enabled = _list.HasNextPage;
                int index = ((int)numericUpDownPage.Value - 1) * _oldDisplayByValue + 1;
                int pos = index / (int)numericUpDownDisplayBy.Value + 1;
                numericUpDownPage.Maximum = _list.PageCount;
                numericUpDownPage.Value = pos;
                labelPageMax.Text = $"/ {_list.PageCount}";
                _oldDisplayByValue = (int)numericUpDownDisplayBy.Value;
                UpdateDataGridAsync();
            }
        }

        #region Split

        private void ButtonSplitOpenClose_Click(object sender, EventArgs e)
        {
            if (splitContainer1.SplitterDistance == 86)
            {
                OpenSplit();
            }
            else
            {
                CloseSplit();
            }
        }

        private void CloseSplit()
        {
            splitContainer1.SplitterDistance = 86;
            foreach (Control control in groupBoxSelection.Controls)
            {
                if (control.Tag?.ToString() != "alwaysVisible")
                {
                    control.Visible = false;
                }
            }

            var names = groupBoxSelection.Controls.Cast<Control>().Where(c => c is CheckBox && ((CheckBox)c).Checked)
                .Select(c => c.Text.Replace(":", string.Empty).ToLower()).Reverse().ToList();

            labelSelectedSelection.Text = $"Выбраны: {string.Join(", ", names)}";
            labelSelectedSelection.Visible = true;
            buttonSplitOpenClose.Text = "+";
        }

        private void OpenSplit()
        {
            splitContainer1.SplitterDistance = 345;
            foreach (Control control in groupBoxSelection.Controls)
            {
                if (control.Tag?.ToString() != "alwaysVisible")
                {
                    control.Visible = true;
                }
            }

            labelSelectedSelection.Visible = false;
            buttonSplitOpenClose.Text = "-";
        }

        #endregion


    }
}
