﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
#if !DEBUG
using System.IO;
#endif
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using Seismology.MainApp.Model;
using Seismology.MainApp.Properties;
using Seismology.Core;
using Seismology.Core.Data;
using Seismology.Core.IO;
using Seismology.Core.Map;
using Seismology.Core.MathS;

namespace Seismology.MainApp
{
    public partial class CalculateForm : Form
    {
        // Наше рассчитанное землетрясение
        //private Earthquake _earthquake;
        // Список wsg станций, считанных из файла (нужен для выборки)
        protected List<EarthquakeStation> _initialStations;
        // Метод, который используется и который будет хранить рассчитанное землетрясение 
        protected EarthquakeCalculate _calculateMethod;
        // Оверлей для GMap
        protected readonly GMapOverlay _overlay = new GMapOverlay("markers");
        // Статичная карта Дагестана
        protected StaticMapS _staticMap;

        public CalculateForm()
        {
            InitializeComponent();

            InitStaticMap();
            InitGMap();
            InitChart();

            _calculateMethod = new EarthquakeCalculate();
        }

        #region Logic

        #region Logic Main

        private void Collect()
        {
            #region Method

            // Выбираем метод
            int checkedIndex = methodToolStripMenuItem
                .DropDownItems
                .Cast<ToolStripMenuItem>()
                .ToList()
                .FindIndex(tool => tool.Checked);

            switch (checkedIndex)
            {
                case 0:
                    _calculateMethod.EpicenterMethod = new AutomaticMethod();
                    break;
                case 1:
                    _calculateMethod.EpicenterMethod = new MultiMethod();
                    break;
                case 2:
                    _calculateMethod.EpicenterMethod = new BinaryMethod();
                    break;
                case 3:
                    _calculateMethod.EpicenterMethod = new GridMethod();
                    break;
                case 4:
                    _calculateMethod.EpicenterMethod = new IntersectionMethod();
                    break;
                case 5:
                    _calculateMethod.EpicenterMethod = new VadatiMethod();
                    break;
            }

            #endregion

            #region Hodograph

            // Выбираем годограф
            checkedIndex = hodographToolStripMenuItem
                .DropDownItems
                .Cast<ToolStripMenuItem>()
                .ToList()
                .FindIndex(tool => tool.Checked);

            switch (checkedIndex)
            {
                case 0:
                    _calculateMethod.DistanceMethod = new TpMethod();
                    break;
                case 1:
                    _calculateMethod.DistanceMethod = new SpMethod();
                    break;
                case 2:
                    _calculateMethod.DistanceMethod = new TsMethod();
                    break;
            }

            #endregion
        }

        private bool Check()
        {
            if (_calculateMethod.WsgType != WsgTypeEnum.FewStations && listViewStationsMain.Items.Cast<ListViewItem>().Count(item => item.Checked) < 2)
            {
                MessageBox.Show("Выбрано малое количество станций, либо они же вообще не выбраны", "Внимание");
                return false;
            }

            if (_calculateMethod.EpicenterMethod is VadatiMethod &&
                listViewStationsVadati.Items.Cast<ListViewItem>().Count(item => item.Checked) != 4)
            {
                MessageBox.Show("При методе Вадати, количество станций должно быть равно 4м", "Внимание");
                return false;
            }

            return true;
        }

        private void Calculate()
        {
            if (_calculateMethod.EpicenterMethod is VadatiMethod)
            {
                // TODO:
                _calculateMethod.Earthquake.Stations =
                    GetStations()
                        .Union(_initialStations, new CompareByStationName())
                        .Where(s => listViewStationsVadati.Items
                            .Cast<ListViewItem>()
                            .Any(item => item.Text == s.Name && item.Checked))
                        .ToList();
            }
            else
            {
                _calculateMethod.Earthquake.Stations =
                    GetStations()
                        .Union(_initialStations, new CompareByStationName())
                        .Where(s => listViewStationsMain.Items
                            .Cast<ListViewItem>()
                            .Any(item => item.Text == s.Name && item.Checked))
                        .ToList();
            }

            // Рассчет землетрясения
            _calculateMethod.Calculate();
            labelMethodName.Text = _calculateMethod.EpicenterMethod is AutomaticMethod
                ? $"Автоматически выбран метод: {_calculateMethod.EpicenterMethod.Name}"
                : $"Выбран метод: {_calculateMethod.EpicenterMethod.Name}";

            //labelMethodName.Text = $@"{labelMethodName.Text}; Метод годографа: {_calculateMethod.DistanceMethod.Name}";
        }

        protected void FillData()
        {
            // Запись расчетного землетрясения в textBox в виде бюллетеня
            var eqFile = new FileS(null, new TextBoxWritter());
            eqFile.WriteBulletinTxt(null, _calculateMethod.Earthquake);
            textBoxEarthquake.Text = _result;

            // Запись расчетного землетрясения в таблицу
            dataGridViewEarthquake.DataSource = new List<EarthquakeView>
            {
                (EarthquakeView)_calculateMethod.Earthquake
            };

            // Редактирование
            FillDataGridView(_calculateMethod.Earthquake.Stations);
        }

        // Для записи бюллетеня в textBox
        private static string _result = string.Empty;
        class TextBoxWritter : IStorageWriter
        {
            public void Write(string filePath, string data) => _result = data;
        }

        protected void ClearAndDrawOnMapsAndChart()
        {
            // Очищаем, а потом рисуем на статичной карте
            _staticMap.ClearMap();
            DrawOnStaticMap();
            pictureBoxStaticMap.Refresh();
            // Очищаем, а потом рисуем на онлайн карте
            _overlay.Markers.Clear();
            _overlay.Polygons.Clear();
            DrawOnGMap();
            // Очищаем, а потом рисуем график Вадати
            foreach (var series in chartWadati.Series)
            {
                series.Points.Clear();
            }

            if (_calculateMethod.WsgType != WsgTypeEnum.FewStations) DrawWadatiChart();
        }

        protected void FillListViewStations(List<EarthquakeStation> stations)
        {
            for (var index = 0; index < stations.Count; index++)
            {
                var station = stations[index];

                listViewStationsMain.Items.Add(new ListViewItem
                {
                    Text = station.Name,
                    ForeColor = station.Station.Color,
                    Checked = true
                });

                listViewStationsVadati.Items.Add(new ListViewItem()
                {
                    Text = station.Name,
                    ForeColor = station.Station.Color,
                    Checked = index < 4
                });
            }
        }

        protected void FillListViewStations(List<DbEarthquakeStation> stations)
        {
            foreach (var station in stations)
            {
                var color = StationStorage.FindStationByName(station.Name).Color;

                listViewStationsMain.Items.Add(new ListViewItem
                {
                    Text = station.Name,
                    ForeColor = color,
                    Checked = station.IsIncluded
                });

                listViewStationsVadati.Items.Add(new ListViewItem()
                {
                    Text = station.Name,
                    ForeColor = color,
                    Checked = station.IsIncluded
                });
            }
        }

        #endregion

        #region DataGrid Operations

        private void FillDataGridView(IEnumerable<EarthquakeStation> stations)
        {
            dataGridViewEditStation.Rows.Clear();
            foreach (var station in stations)
            {
                dataGridViewEditStation.Rows.Add(
                    station.Name,
                    station.T0.ToString("HH:mm:ss.fff"),
                    station.K.ToString("F3"),
                    station.Tp.ToString("HH:mm:ss.fff"),
                    station.Sp,
                    station.EpicDistance.ToString("F3"),
                    station.Dt.ToString("F3"),
                    station.VpVs.ToString("F3"),
                    station.PeriodZ,
                    station.AmplitudeZ,
                    station.Period,
                    station.Amplitude,
                    station.Ep,
                    station.Es);
            }
        }

        private List<EarthquakeStation> GetStations()
        {
            List<EarthquakeStation> stations = new List<EarthquakeStation>();
            var date = _calculateMethod.Earthquake.Date;
            foreach (DataGridViewRow row in dataGridViewEditStation.Rows)
            {
                var t0 = DateTime.ParseExact(row.Cells[1].Value.ToString(), "HH:mm:ss.fff", CultureInfo.InvariantCulture);
                var tp = DateTime.ParseExact(row.Cells[3].Value.ToString(), "HH:mm:ss.fff", CultureInfo.InvariantCulture);
                var sp = double.Parse(row.Cells[4].Value.ToString().Replace('.', ','));
                if (tp > t0)
                {
                    tp = date
                        .AddHours(tp.Hour)
                        .AddMinutes(tp.Minute)
                        .AddSeconds(tp.Second)
                        .AddMilliseconds(tp.Millisecond);

                }
                else
                {
                    tp = date
                        .AddDays(1)
                        .AddHours(tp.Hour)
                        .AddMinutes(tp.Minute)
                        .AddSeconds(tp.Second)
                        .AddMilliseconds(tp.Millisecond);
                }
                stations.Add(new EarthquakeStation
                {
                    Name = row.Cells[0].Value.ToString(),
                    Tp = tp,
                    Sp = sp,
                    PeriodZ = double.Parse(row.Cells[8].Value.ToString()),
                    AmplitudeZ = double.Parse(row.Cells[9].Value.ToString()),
                    Period = double.Parse(row.Cells[10].Value.ToString()),
                    Amplitude = double.Parse(row.Cells[11].Value.ToString()),
                    Ep = row.Cells[12].Value.ToString(),
                    Es = row.Cells[13].Value.ToString()
                });
            }

            return stations;
        }

        #endregion

        #region StaticMap

        /// <summary>
        /// Рисует радиусы станцинных землетрясений на статичной карте
        /// </summary>
        private void DrawOnStaticMap()
        {
            try
            {
                foreach (var station in _calculateMethod.Earthquake.Stations)
                {
                    _staticMap.AddRadius(station.Station.Coordinate, station.EpicDistance, station.Station.Color, 3);
                    _staticMap.AddMarker(new Marker(station.Station.Coordinate, station.Station.Color));
                }
                _staticMap.AddEarhquake(_calculateMethod.Earthquake);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        /// <summary>
        /// Инициализация статичной карты
        /// </summary>
        private void InitStaticMap()
        {
            _staticMap = new StaticMapS(
                Resources.DagMap,
                30, 1447, 41, 45,
                55, 1091, 45, 49);
            pictureBoxStaticMap.Image = _staticMap.Image;
            pictureBoxStaticMap.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void SaveStaticMapImage(string path, ImageFormat format)
        {
            pictureBoxStaticMap.Image.Save(path, format);
        }

        #endregion

        #region GMap

        /// <summary>
        /// Рисует радиусы станцинных землетрясений на GMap карте
        /// </summary>
        private void DrawOnGMap()
        {
            var gMap = new GMapS();

            foreach (var station in _calculateMethod.Earthquake.Stations)
            {
                bool isInclude = _calculateMethod.DistanceMethod.IsInsideTime(station, _calculateMethod.Earthquake.T0, _calculateMethod.Earthquake.H);
                // Радиус (в виде полигонов) станционного землетрясения
                var gPolStationRadius = gMap.GetStationRadiusGPolygon(station, isInclude, 25);
                // Добавляем на GMap полигон радиуса
                _overlay.Polygons.Add(gPolStationRadius);
                // Маркер станции
                var gMarkerStation = gMap.GetStationGMarker(station.Station);
                // "Пустой" маркер станции (нужен для вывода информации)
                var gMarkerEmptyStation = gMap.GetEmptyStationGMarker(station.Station);
                // Добавляем на GMap маркеры станций
                _overlay.Markers.Add(gMarkerStation);
                _overlay.Markers.Add(gMarkerEmptyStation);
            }

            // Маркер самого землетрясения
            var eqMarker = gMap.GetEarthquakeGMarker(new PointLatLng(_calculateMethod.Earthquake.Lat, _calculateMethod.Earthquake.Lng));

            // Добавляем на GMap маркер землетрясения
            _overlay.Markers.Add(eqMarker);
        }

        protected void DrawOnGMapStations()
        {
            var gMap = new GMapS();

            foreach (var station in _initialStations)
            {
                // Маркер станции
                var gMarkerStation = gMap.GetStationGMarker(station.Station);
                // "Пустой" маркер станции (нужен для вывода информации)
                var gMarkerEmptyStation = gMap.GetEmptyStationGMarker(station.Station);
                _overlay.Markers.Add(gMarkerStation);
                _overlay.Markers.Add(gMarkerEmptyStation);
            }
        }

        /// <summary>
        /// Инициализация GMap
        /// </summary>
        private void InitGMap()
        {
            // Добавляем оверлей на наш Gmap
            gMapOnlineControl.Overlays.Add(_overlay);

            // Настройки для компонента GMap.
            gMapOnlineControl.Bearing = 0;

            // CanDragMap - Если параметр установлен в True,
            // пользователь может перетаскивать карту 
            // с помощью правой кнопки мыши. 
            gMapOnlineControl.CanDragMap = true;

            // Указываем, что перетаскивание карты осуществляется 
            // с использованием левой клавишей мыши.
            // По умолчанию - правая.
            gMapOnlineControl.DragButton = MouseButtons.Left;

            gMapOnlineControl.GrayScaleMode = true;

            // MarkersEnabled - Если параметр установлен в True,
            // любые маркеры, заданные вручную будет показаны.
            // Если нет, они не появятся.
            gMapOnlineControl.MarkersEnabled = true;

            // Указываем значение максимального приближения.
            gMapOnlineControl.MaxZoom = 18;

            // Указываем значение минимального приближения.
            gMapOnlineControl.MinZoom = 2;

            // Устанавливаем центр приближения/удаления
            // курсор мыши.
            /*
            gMapControl1.MouseWheelZoomType =
                GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            */

            // Отказываемся от негативного режима.
            gMapOnlineControl.NegativeMode = false;

            // Разрешаем полигоны.
            gMapOnlineControl.PolygonsEnabled = true;

            // Запрешаем маршруты
            gMapOnlineControl.RoutesEnabled = false;

            // Скрываем внешнюю сетку карты
            // с заголовками.
            gMapOnlineControl.ShowTileGridLines = false;

            // Указываем, что при загрузке карты будет использоваться 
            // 18ти кратное приближение.
            gMapOnlineControl.Zoom = 8.25;

            // Указываем что все края элемента управления
            // закрепляются у краев содержащего его элемента
            // управления(главной формы), а их размеры изменяются 
            // соответствующим образом.
            gMapOnlineControl.Dock = DockStyle.Fill;

            // Указываем что будем использовать карты Google.
            /*
            gMapControl1.MapProvider = 
                GMap.NET.MapProviders.GMapProviders.GoogleMap;
            */
            gMapOnlineControl.MapProvider =
                GMapProviders.GoogleMap;
            GMaps.Instance.Mode =
                AccessMode.ServerOnly;

            // Позиция
            gMapOnlineControl.Position = new PointLatLng(43.0088083, 46.8795529);
        }

        private void SaveGMapImage(string path, ImageFormat format)
        {
            gMapOnlineControl.ToImage().Save(path, format);
        }

        #endregion

        #region Chart

        private void InitChart()
        {
            chartWadati.Series[0].Color = Color.Black;
            chartWadati.Series[1].Color = Color.Black;
            chartWadati.Series[2].Color = Color.Blue;
            chartWadati.Series[2].BorderColor = Color.Black;
            chartWadati.Series[3].Color = Color.Red;
            chartWadati.Series[3].BorderColor = Color.Black;

            chartWadati.Series[0].SmartLabelStyle.MovingDirection = LabelAlignmentStyles.Left;
            chartWadati.Series[0].SmartLabelStyle.MinMovingDistance = 10;
            chartWadati.Series[0].SmartLabelStyle.MaxMovingDistance = 100;
            chartWadati.Series[0].SmartLabelStyle.CalloutLineColor = Color.Transparent;
            chartWadati.Series[1].SmartLabelStyle.MovingDirection = LabelAlignmentStyles.Left;
            chartWadati.Series[1].SmartLabelStyle.CalloutLineColor = Color.Transparent;
            chartWadati.Series[1].SmartLabelStyle.MinMovingDistance = 10;
            chartWadati.Series[1].SmartLabelStyle.MaxMovingDistance = 100;
        }

        void DrawWadatiChart()
        {
            var calculator = new CalculatorS();
            var wadatiData = _calculateMethod.WadatiData;
            var stations = _calculateMethod.Earthquake.Stations;
            var minTp = stations.Select(s => s.Tp).Min();
            var timeToCompare = new DateTime(
                minTp.Year,
                minTp.Month,
                minTp.Day,
                minTp.Hour,
                minTp.Minute,
                0,
                0);
            for (int index = 0; index < stations.Count; index++)
            {
                double tp = (stations[index].Tp - timeToCompare).TotalSeconds;
                double ts = (stations[index].Ts - timeToCompare).TotalSeconds;
                double sp = ts - tp;
                chartWadati.Series[0].Points.AddXY(tp, wadatiData[0][0] * tp + wadatiData[0][1]);
                chartWadati.Series[1].Points.AddXY(ts, wadatiData[1][0] * ts + wadatiData[1][1]);
                chartWadati.Series[2].Points.AddXY(tp, sp);
                chartWadati.Series[2].Points[index].Label = $"{stations[index].Name}";
                chartWadati.Series[3].Points.AddXY(ts, sp);
                chartWadati.Series[3].Points[index].Label = $"{stations[index].Name}";
                if (stations.Count == index + 1)
                {
                    chartWadati.Series[0].Points[index].Label =
                        calculator.LinearEquationFormat("sp", "tp", wadatiData[0][0], wadatiData[0][1]);
                    chartWadati.Series[1].Points[index].Label = calculator.LinearEquationFormat("sp", "ts", wadatiData[1][0], wadatiData[1][1]);
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Method Events
        private void ChangeMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem dropDownItem in methodToolStripMenuItem.DropDownItems)
            {
                dropDownItem.Checked = false;
            }

            if (sender is ToolStripMenuItem menuItem)
            {
                menuItem.Checked = true;
                hodographToolStripMenuItem.Enabled = menuItem != automaticToolStripMenuItem;
            }
        }

        #endregion

        #region Hodograph Events

        private void ChangeHodographMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem dropDownItem in hodographToolStripMenuItem.DropDownItems)
            {
                dropDownItem.Checked = false;
            }

            if (sender is ToolStripMenuItem menuItem)
            {
                menuItem.Checked = true;
            }
        }

        #endregion

        #region FileTool Events

        private void OnlineMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gMapOnlineControl.Refresh();
            saveFileDialogImage.FileName = $"Онлайн Изображение {GetDateTime()}";
            if (saveFileDialogImage.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogImage.FileName;
            ImageFormat imageFormat;
            switch (saveFileDialogImage.FilterIndex)
            {
                case 1:
                    imageFormat = ImageFormat.Png;
                    break;
                case 2:
                    imageFormat = ImageFormat.Jpeg;
                    break;
                case 3:
                    imageFormat = ImageFormat.Bmp;
                    break;
                case 4:
                    imageFormat = ImageFormat.Gif;
                    break;
                default:
                    imageFormat = ImageFormat.Png;
                    break;
            }
            SaveGMapImage(path, imageFormat);
        }

        private void StaticMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialogImage.FileName = $"Изображение {GetDateTime()}";
            if (saveFileDialogImage.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogImage.FileName;
            ImageFormat imageFormat;
            switch (saveFileDialogImage.FilterIndex)
            {
                case 1:
                    imageFormat = ImageFormat.Png;
                    break;
                case 2:
                    imageFormat = ImageFormat.Jpeg;
                    break;
                case 3:
                    imageFormat = ImageFormat.Bmp;
                    break;
                case 4:
                    imageFormat = ImageFormat.Gif;
                    break;
                default:
                    imageFormat = ImageFormat.Png;
                    break;
            }
            SaveStaticMapImage(path, imageFormat);
        }

        private void CatalogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialogDocument.FileName = $"Каталог {GetDateTime()}";
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var file = new FileS();
            file.WriteCatalogTxt(path, _calculateMethod.Earthquake);
        }

        private void BulletinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialogDocument.FileName = $"Бюллетень {GetDateTime()}";
            if (saveFileDialogDocument.ShowDialog() != DialogResult.OK) return;
            string path = saveFileDialogDocument.FileName;
            var file = new FileS();
            file.WriteBulletinTxt(path, _calculateMethod.Earthquake);
        }

        private string GetDateTime()
        {
            return $"{_calculateMethod.Earthquake.T0:HH-mm-ss}  {_calculateMethod.Earthquake.Date:dd.MM.yyyy}";
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region GMapTool Events

        readonly List<GMapProvider> _gMapProviders = new List<GMapProvider>
        {
            GMapProviders.GoogleMap,
            GMapProviders.GoogleHybridMap,
            GMapProviders.GoogleTerrainMap,
            GMapProviders.YandexMap,
            GMapProviders.OpenStreetMap,
            GMapProviders.OpenStreet4UMap,
            GMapProviders.BingMap
        };

        private void ChangeGMapProviderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem dropDownItem in onlineToolStripMenuItem.DropDownItems)
            {
                dropDownItem.Checked = false;
            }

            if (sender is ToolStripMenuItem menuItem)
            {
                menuItem.Checked = true;
            }

            int checkedIndex = onlineToolStripMenuItem
                .DropDownItems
                .Cast<ToolStripMenuItem>()
                .ToList()
                .FindIndex(tool => tool.Checked);
            gMapOnlineControl.MapProvider = _gMapProviders[checkedIndex];
        }

        #endregion

        #region Buttons Events

        protected virtual void OpenWsgToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        protected virtual void ButtonAction_Click(object sender, EventArgs e)
        {

        }

        private void ButtonCalculate_Click(object sender, EventArgs e)
        {
            buttonCalculate.Enabled = false;
            labelMethodName.Text = string.Empty;

            // Собираем данные
            Collect();

            // Если все данные в порядке
            if (Check())
            {
                try
                {
                    // Вычисляем
                    Calculate();

                    // Записываем расчетное землетрясение в таблицу и textBox
                    FillData();

                    // Рисуем расчетное землетрясение на картах
                    ClearAndDrawOnMapsAndChart();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Внимание");
                }
            }

            buttonCalculate.Enabled = true;
            buttonAction.Enabled = true;
        }

        #endregion

        #endregion
    }
}
