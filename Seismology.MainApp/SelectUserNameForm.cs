﻿using System;
using System.Windows.Forms;

namespace Seismology.MainApp
{
    public partial class SelectUserNameForm : Form
    {
        private string _userName;
        public SelectUserNameForm(string userName)
        {
            InitializeComponent();

            _userName = userName;
        }

        public string ReturnUserName()
        {
            return textBoxUserName.Text;
        }

        private void SelectUserNameForm_Load(object sender, EventArgs e)
        {
            textBoxUserName.Text = _userName;
        }
    }
}
