﻿using Seismology.Core;
using Seismology.Core.Data;
using Seismology.Core.Data.Access;
using Seismology.Core.IO;
using Seismology.Core.MathS;
using Seismology.MainApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

#if !DEBUG
using System.IO;
#endif

namespace Seismology.MainApp
{
    public partial class CalculateWsgForm : CalculateForm
    {
        public CalculateWsgForm()
        {
            InitializeComponent();
        }

        private void CalculateWsgForm_Load(object sender, EventArgs e)
        {
            tabControlMain.TabPages.Remove(tabPageDbData);
            buttonAction.Text = "Добавить";
            automaticToolStripMenuItem.Checked = true;
            hodographToolStripMenuItem.Enabled = false;
            tpToolStripMenuItem.Checked = true;
        }

        private void ReadWsg(string filePath)
        {
            try
            {
                // Чтение
                var eq = new FileS();
                var wsg = eq.ReadWsgEarthquake(filePath);
                _calculateMethod.Earthquake = ConvertS.ToEarthquake(wsg);

                if (_calculateMethod.Earthquake.Stations.Count < 3)
                {
                    string warningText =
                        "Так как количество станций меньше или равно 2м, то данные рассчета будут взяты из WSG файла";

                    MessageBox.Show(warningText, "Внимание!");

                    _calculateMethod.Earthquake.T0 = wsg.Time;
                    _calculateMethod.WsgType = WsgTypeEnum.FewStations;
                }
                else if (_calculateMethod.Earthquake.Stations.Count(s => s.Sp < 25) < 3)
                {
                    string warningText =
                        "Так как количество станций, в которых время SP < 25 секунд, меньше 3х, то данные рассчета будут взяты из WSG файла";

                    MessageBox.Show(warningText, "Внимание!");

                    _calculateMethod.WsgType = WsgTypeEnum.DistantEarthquakes;
                }
                else
                {
                    _calculateMethod.WsgType = null;
                }

                if (_calculateMethod.Earthquake.Stations.Count == 0)
                {
                    throw new Exception("Не обнаружено никаких данных по станциям");
                }

                if (_calculateMethod.Earthquake.Stations.Any(s => s.AmplitudeZ == 0 || s.Amplitude == 0))
                {
                    string warningText = "В файле для следующих станций:\r\n";
                    foreach (var station in _calculateMethod.Earthquake.Stations.Where(s => s.AmplitudeZ == 0 || s.Amplitude == 0))
                    {
                        warningText += $"{station.Name} ({station.Station.RusName})\r\n";
                    }

                    warningText +=
                        "Не были обнаружены амплитуды для P или S волный.\r\n" +
                        "Поэтому при рассчете класса землетрясения они не будут учитываться";

                    MessageBox.Show(warningText, "Внимание!");
                }

                // Добавление в таблицу wsg данных
                dataGridViewWsgData.DataSource =
                    wsg.WsgStations.SelectMany(s => s.Phases).ToList();

                // Инициализируем начальные станции
                _initialStations = new List<EarthquakeStation>(_calculateMethod.Earthquake.Stations);

                // Инициализируем чекбоксы для выборки
                listViewStationsMain.Items.Clear();
                listViewStationsVadati.Items.Clear();
                FillListViewStations(_initialStations);

                buttonCalculate.Enabled = true;
                labelMethodName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                //_calculateMethod.WsgEarthquake = null;
                _calculateMethod.Earthquake = null;
                dataGridViewWsgData.DataSource = null;
                buttonCalculate.Enabled = false;
                MessageBox.Show($"Не удалось прочитать данные!\r\nПричина: {ex.Message}\r\n", "Внимание");
            }
            finally
            {
                dataGridViewEarthquake.DataSource = null;
                _overlay.Markers.Clear();
                _overlay.Polygons.Clear();
                _staticMap.ClearMap();
                DrawOnGMapStations();
                dataGridViewEditStation.Rows.Clear();
                buttonAction.Enabled = false;
                textBoxEarthquake.Text = string.Empty;
            }
        }

        private void DeleteWsgFile()
        {
#if !DEBUG
            File.Delete(openFileDialogWsgFile.FileName);
#endif
        }

        private void ClearAndDisableAll()
        {
            //_calculateMethod.WsgEarthquake = null;
            _calculateMethod.Earthquake = null;
            dataGridViewWsgData.DataSource = null;
            dataGridViewEarthquake.DataSource = null;
            dataGridViewEditStation.Rows.Clear();
            _overlay.Markers.Clear();
            _overlay.Polygons.Clear();
            _staticMap.ClearMap();
            buttonCalculate.Enabled = false;
            buttonAction.Enabled = false;
            textBoxEarthquake.Text = string.Empty;
            listViewStationsMain.Clear();
            listViewStationsVadati.Clear();
            labelMethodName.Text = string.Empty;
        }

        protected override void OpenWsgToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialogWsgFile.FileName = "Wsg файл";
            if (openFileDialogWsgFile.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialogWsgFile.FileName;

                // Попытка чтения файла
                ReadWsg(filePath);
            }
        }

        protected override async void ButtonAction_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            if (_calculateMethod.Earthquake != null)
            {
                bool isSucces = false;
                try
                {
                    // Создаем объект для работы с БД
                    var db = new DbAccess(Settings.MySettings.CurrentDataSource);
                    // Копируем землетрясение со станционными данными, которые учавстовали в рассчете
                    var dbEarthquake = (DbEarthquake)_calculateMethod.Earthquake;
                    // Выбираем станционные данные, которые не учавстваовали в рассчете
                    var notIncludedStations = _initialStations
                        .Except(_calculateMethod.Earthquake.Stations, new CompareByStationName())
                        .ToList();
                    // Будем добавлять каждую из них при этому указывая, что она не включена в рассчет
                    foreach (var station in notIncludedStations)
                    {
                        var dbStation = (DbEarthquakeStation)station;
                        // Указываем, что эта станция не учтена
                        dbStation.IsIncluded = false;
                        dbEarthquake.Stations.Add(dbStation);
                    }
                    dbEarthquake.Method = _calculateMethod.EpicenterMethod.Name;
                    dbEarthquake.HodographMethod = _calculateMethod.DistanceMethod.Name;
                    dbEarthquake.Author = Settings.MySettings.UserName;
                    // Проверка, есть ли запись
                    if (await db.IsMatchAsync(dbEarthquake.Date, dbEarthquake.T0))
                    {
                        using (var warning = new AddWarningForm(dbEarthquake.Date, dbEarthquake.T0))
                        {
                            warning.ShowDialog();
                            var result = warning.GetOperationResult;
                            if (result == DataOperation.Add)
                            {
                                await db.AddAsync(dbEarthquake);
                                isSucces = true;
                                MessageBox.Show("Данные были занесены в БД!", " Успех");
                                DeleteWsgFile();
                            }
                            else if (result == DataOperation.Update)
                            {
                                int id = warning.GetId;
                                await db.UpdateAsync(id, dbEarthquake);
                                isSucces = true;
                                MessageBox.Show("Данные были заменены!", " Успех");
                                DeleteWsgFile();
                            }
                        }
                    }
                    else
                    {
                        await db.AddAsync(dbEarthquake);
                        isSucces = true;
                        MessageBox.Show("Данные были занесены в БД!", " Успех");
                        DeleteWsgFile();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не удалось добавить (изменить) запись в базу данных, причина: " +
                                    $"{ex.Message}; {ex.InnerException?.Message}");
                }
                if (isSucces)
                {
                    ClearAndDisableAll();
                }
            }

            this.Enabled = true;
        }
    }
}
