﻿using Seismology.Core.Data;
using Seismology.Core.Data.Access;
using System;
using System.Windows.Forms;

namespace Seismology.MainApp
{
    public partial class AddWarningForm : Form
    {
        private readonly DbAccess _db = new DbAccess(Settings.MySettings.CurrentDataSource);
        private DataOperation _dataOperations = DataOperation.None;
        private DateTime _date;
        private DateTime _t0;
        private int _id;


        public AddWarningForm(DateTime date, DateTime t0)
        {
            InitializeComponent();

            _date = date;
            _t0 = t0;
        }

        private async void AddWarningForm_Load(object sender, EventArgs e)
        {
            var earthquakes = await _db.MatchesAsync(_date, _t0);
            dataGridViewEarthquake.DataSource = earthquakes;
            dataGridViewEarthquake.Columns[2].DefaultCellStyle.Format = "HH:mm:ss.fff";
            Height = 200 + 22 * earthquakes.Count;
        }

        private async void OpenStationsData()
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                int id = Convert.ToInt32(dataGridViewEarthquake.CurrentRow.Cells[0].Value);
                var dbEarthquake = await _db.GetAsync(id);
                var stationForm = new DbStationForm(dbEarthquake);
                stationForm.Show();
            }
        }

        public DataOperation GetOperationResult => _dataOperations;

        public int GetId => _id;

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            _dataOperations = DataOperation.Add;
            Close();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                _dataOperations = DataOperation.Update;
                _id = Convert.ToInt32(dataGridViewEarthquake.CurrentRow.Cells[0].Value);
                Close();
            }
        }

        private void ButtonShow_Click(object sender, EventArgs e)
        {
            OpenStationsData();
        }

        private void DataGridViewEarthquake_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OpenStationsData();
        }

        private void DataGridViewEarthquake_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                buttonUpdate.Enabled = true;
                buttonShow.Enabled = true;
                buttonDelete.Enabled = true;
            }
        }

        private async void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewEarthquake.CurrentRow != null)
            {
                _id = Convert.ToInt32(dataGridViewEarthquake.CurrentRow.Cells[0].Value);
                string message = $"Вы точно хотите удалить следующую запись, под номером:\r\n{_id}";
                if (MessageBox.Show(
                        message,
                        "Внимание",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    await _db.DeleteAsync(_id);
                    dataGridViewEarthquake.DataSource = await _db.MatchesAsync(_date, _t0);
                }
            }
        }
    }
}
