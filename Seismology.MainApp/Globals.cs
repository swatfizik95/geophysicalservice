﻿using System;
using Seismology.Core;

namespace Seismology.MainApp
{
    public static class Globals
    {
        private static CompanyType _company;

        public static CompanyType Company => _company;

        public static void UseDefaultParams(string stationFilePath)
        {
            _company = CompanyType.Default;
            Configuration.UseDefaultParams(stationFilePath);
        }

        public static void UseRusHydroParams(string stationFilePath)
        {
            _company = CompanyType.RusHydro;
            Configuration.UseRusHydroParams(stationFilePath);
        }

        public static void UseConfiguration(string stationFilePath)
        {
            switch (_company)
            {
                case CompanyType.Default:
                    Configuration.UseDefaultParams(stationFilePath);
                    break;
                case CompanyType.RusHydro:
                    Configuration.UseRusHydroParams(stationFilePath);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
        }
    }
}