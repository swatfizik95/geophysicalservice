﻿using Seismology.Core.Data.Access;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Seismology.Core.Map;

namespace Seismology.MainApp
{
    public partial class MainForm : Form
    {
        private bool _isUserWantToUpdate;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            labelUserNameValue.Text = Settings.MySettings.UserName;
            labelDataSourceValue.Text = Settings.MySettings.CurrentDataSource;
            labelStationDataSourceValue.Text = Settings.MySettings.CurrentStationDataSource;
#if !DEBUG
            // if !DEBUG == if RELEASE
            // ВНИМАНИЕ!!!
            // При релизе увеличивать последнюю цифру, чтобь программа обновлялась!!!!!!!!
            // 0.8.0 -> 0.8.1
            AppVersion.CurrentVersion.Version = "1.1.9";
            //AppVersion.CurrentVersion.Date = DateTime.Now;
            AppVersion.CurrentVersion.Save();
#endif
            labelVersion.Text = $"Версия: {AppVersion.CurrentVersion.Version}";
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            var updater = new Updater();

            if (updater.IsNeedToUpdate())
            {
                string message = $"Текущая версия программы: {updater.CurrentVersion}{Environment.NewLine}" +
                                 $"Доступна новая версия: {updater.UpdateVersion}{Environment.NewLine}" +
                                 "Обновить прогамму?";

                _isUserWantToUpdate =
                    MessageBox.Show(message, "Внимание!", MessageBoxButtons.YesNo) == DialogResult.Yes;

                if (_isUserWantToUpdate)
                {
                    if (!File.Exists(Updater.UpdaterAppPath))
                    {
                        MessageBox.Show($"Не обнаружена программа для обновления!{Environment.NewLine}" +
                                        "Для обновление программы обратитесь к администратору!", "Внимание!");
                        _isUserWantToUpdate = false;
                    }
                    else
                    {
                        Close();
                    }
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CheckSettings();
            CheckUpdate();
        }


        #region File

        /// <summary>
        /// Начинает работу с WSG данными
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddWsgFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CheckStationIsNotNull() == false) return;

            var wsgForm = new CalculateWsgForm();
            wsgForm.ShowDialog();
        }

        /// <summary>
        /// Начинает работу с базой данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Settings.MySettings.CurrentDataSource) ||
                Settings.MySettings.CurrentDataSource == "Не задано")
            {
                MessageBox.Show("Пожалуйста, укажите корректный путь к Базе Данных!", "Путь к БД не указан");
                if (SetDataSource() == false) return;
            }

            if (CheckStationIsNotNull() == false) return;

            var dbForm = new DbForm();
            dbForm.ShowDialog();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckSettings();
            Close();
        }

        #endregion

        #region Settings

        /// <summary>
        /// Указывает новый источник данных (.mdb)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataSourceToolStripMenuItem_Click(object sender, EventArgs e) => SetDataSource();

        /// <summary>
        /// Задает новое имя пользователя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var userForm = new SelectUserNameForm(Settings.MySettings.UserName);
            userForm.ShowDialog();
            if (userForm.DialogResult != DialogResult.OK)
                return;
            Settings.MySettings.UserName = userForm.ReturnUserName();
            labelUserNameValue.Text = Settings.MySettings.UserName;
        }

        private void SaveSettingsToolStripMenuItem_Click(object sender, EventArgs e) =>
            Settings.MySettings.Save();

        private void CreateTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.FileName = "NewEmptyMainSeismologyDatabase";
                dialog.Filter = "База данных Microsoft Access 2000 (*.mdb) | *.mdb";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    DbAccess.CloneNewEmptyDb(dialog.FileName);
                    MessageBox.Show("Новая база данных была создана!", "Отчет");
                }
            }
        }

        #endregion

        /// <summary>
        /// Проверка на сохранение настроек
        /// </summary>
        private void CheckSettings()
        {
            var initSettings = Settings.LoadSettings();
            if (Settings.MySettings == initSettings) return;
            var dialogResult = MessageBox.Show(
                "Настройки программы были изменены.\r\n" +
                "Сохранить новые настройки?",
                "Внимание",
                MessageBoxButtons.YesNo);
            if (dialogResult != DialogResult.Yes) return;
            try
            {
                Settings.MySettings.Save();
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось сохранить настройки!");
            }
        }

        private void CheckUpdate()
        {
            if (_isUserWantToUpdate)
                if (Process.GetProcessesByName(Updater.UpdaterAppName).Length < 1)
                    Process.Start(Updater.UpdaterAppPath);
        }

        private bool SetDataSource()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.FileName = "База данных";
                dialog.Filter = @"База данных Access 2002-2003 (*.mdb) | *.mdb;";
                if (dialog.ShowDialog() != DialogResult.OK)
                    return false;
                Settings.MySettings.CurrentDataSource = dialog.FileName;
                labelDataSourceValue.Text = Settings.MySettings.CurrentDataSource;
            }

            return true;
        }

        private bool SetStationDataSource()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.FileName = "Данные о станции";
                dialog.Filter = @"Данные о станции (*.json) | *.json;";
                if (dialog.ShowDialog() != DialogResult.OK)
                    return false;
                Settings.MySettings.CurrentStationDataSource = dialog.FileName;
                labelStationDataSourceValue.Text = Settings.MySettings.CurrentStationDataSource;
                Globals.UseConfiguration(Settings.MySettings.CurrentStationDataSource);
            }

            return true;
        }

        private bool CheckStationIsNotNull()
        {
            if (string.IsNullOrWhiteSpace(Settings.MySettings.CurrentStationDataSource) ||
                Settings.MySettings.CurrentStationDataSource == "Не задано")
            {
                MessageBox.Show("Пожалуйста, укажите корректный путь к данным о станциях!",
                    "Путь к данным о станциях не указан");
                
                return SetStationDataSource();
            }

            return true;
        }

        private void ChangeCompanyTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem dropDownItem in companyTypeToolStripMenuItem.DropDownItems)
            {
                dropDownItem.Checked = false;
            }

            if (sender is ToolStripMenuItem menuItem)
            {
                menuItem.Checked = true;

                if (sender == defaultToolStripMenuItem)
                {
                    Globals.UseDefaultParams(Settings.MySettings.CurrentStationDataSource);
                    pictureBoxCompanyLogo.Image = null;
                }
                else if (sender == rusHydroToolStripMenuItem)
                {
                    Globals.UseRusHydroParams(Settings.MySettings.CurrentStationDataSource);
                    pictureBoxCompanyLogo.Image = Properties.Resources.RusHydroLogo;
                }

                labelDataSourceValue.Text = Settings.MySettings.CurrentDataSource;
                labelStationDataSourceValue.Text = Settings.MySettings.CurrentStationDataSource;
            }
        }

        private void StationDataToolStripMenuItem_Click(object sender, EventArgs e) =>
            SetStationDataSource();
    }
}