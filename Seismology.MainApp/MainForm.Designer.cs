﻿namespace Seismology.MainApp
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addWsgFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stationDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rusHydroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelUserNameValue = new System.Windows.Forms.Label();
            this.labelDataSourceValue = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelDataSource = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.pictureBoxCompanyLogo = new System.Windows.Forms.PictureBox();
            this.labelStationDataSource = new System.Windows.Forms.Label();
            this.labelStationDataSourceValue = new System.Windows.Forms.Label();
            this.menuStripMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompanyLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { this.fileToolStripMenuItem, this.settingsToolStripMenuItem, this.companyTypeToolStripMenuItem });
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(496, 29);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { this.addWsgFileToolStripMenuItem, this.openDatabaseToolStripMenuItem, this.toolStripSeparator1, this.exitToolStripMenuItem });
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(59, 25);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // addWsgFileToolStripMenuItem
            // 
            this.addWsgFileToolStripMenuItem.Name = "addWsgFileToolStripMenuItem";
            this.addWsgFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.addWsgFileToolStripMenuItem.Size = new System.Drawing.Size(307, 26);
            this.addWsgFileToolStripMenuItem.Text = "Добавить WSG данные";
            this.addWsgFileToolStripMenuItem.Click += new System.EventHandler(this.AddWsgFileToolStripMenuItem_Click);
            // 
            // openDatabaseToolStripMenuItem
            // 
            this.openDatabaseToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_database;
            this.openDatabaseToolStripMenuItem.Name = "openDatabaseToolStripMenuItem";
            this.openDatabaseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.openDatabaseToolStripMenuItem.Size = new System.Drawing.Size(307, 26);
            this.openDatabaseToolStripMenuItem.Text = "Открыть базу данных";
            this.openDatabaseToolStripMenuItem.Click += new System.EventHandler(this.OpenDatabaseToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(304, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(307, 26);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { this.dataBaseToolStripMenuItem, this.stationDataToolStripMenuItem, this.userNameToolStripMenuItem, this.saveSettingsToolStripMenuItem });
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(99, 25);
            this.settingsToolStripMenuItem.Text = "Настройки";
            // 
            // dataBaseToolStripMenuItem
            // 
            this.dataBaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { this.dataSourceToolStripMenuItem, this.createTablesToolStripMenuItem });
            this.dataBaseToolStripMenuItem.Name = "dataBaseToolStripMenuItem";
            this.dataBaseToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.dataBaseToolStripMenuItem.Text = "База данных";
            // 
            // dataSourceToolStripMenuItem
            // 
            this.dataSourceToolStripMenuItem.Name = "dataSourceToolStripMenuItem";
            this.dataSourceToolStripMenuItem.Size = new System.Drawing.Size(269, 26);
            this.dataSourceToolStripMenuItem.Text = "Источник данных";
            this.dataSourceToolStripMenuItem.Click += new System.EventHandler(this.DataSourceToolStripMenuItem_Click);
            // 
            // createTablesToolStripMenuItem
            // 
            this.createTablesToolStripMenuItem.Name = "createTablesToolStripMenuItem";
            this.createTablesToolStripMenuItem.Size = new System.Drawing.Size(269, 26);
            this.createTablesToolStripMenuItem.Text = "Создать новую пустую БД";
            this.createTablesToolStripMenuItem.Click += new System.EventHandler(this.CreateTablesToolStripMenuItem_Click);
            // 
            // stationDataToolStripMenuItem
            // 
            this.stationDataToolStripMenuItem.Name = "stationDataToolStripMenuItem";
            this.stationDataToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.stationDataToolStripMenuItem.Text = "Источник станций";
            this.stationDataToolStripMenuItem.Click += new System.EventHandler(this.StationDataToolStripMenuItem_Click);
            // 
            // userNameToolStripMenuItem
            // 
            this.userNameToolStripMenuItem.Name = "userNameToolStripMenuItem";
            this.userNameToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.userNameToolStripMenuItem.Text = "Имя пользователя";
            this.userNameToolStripMenuItem.Click += new System.EventHandler(this.UserNameToolStripMenuItem_Click);
            // 
            // saveSettingsToolStripMenuItem
            // 
            this.saveSettingsToolStripMenuItem.Name = "saveSettingsToolStripMenuItem";
            this.saveSettingsToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.saveSettingsToolStripMenuItem.Text = "Сохранить";
            this.saveSettingsToolStripMenuItem.Click += new System.EventHandler(this.SaveSettingsToolStripMenuItem_Click);
            // 
            // companyTypeToolStripMenuItem
            // 
            this.companyTypeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { this.defaultToolStripMenuItem, this.rusHydroToolStripMenuItem });
            this.companyTypeToolStripMenuItem.Name = "companyTypeToolStripMenuItem";
            this.companyTypeToolStripMenuItem.Size = new System.Drawing.Size(94, 25);
            this.companyTypeToolStripMenuItem.Text = "Компания";
            // 
            // defaultToolStripMenuItem
            // 
            this.defaultToolStripMenuItem.Checked = true;
            this.defaultToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
            this.defaultToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.defaultToolStripMenuItem.Text = "По умолчанию";
            this.defaultToolStripMenuItem.Click += new System.EventHandler(this.ChangeCompanyTypeToolStripMenuItem_Click);
            // 
            // rusHydroToolStripMenuItem
            // 
            this.rusHydroToolStripMenuItem.Name = "rusHydroToolStripMenuItem";
            this.rusHydroToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.rusHydroToolStripMenuItem.Text = "РусГидро";
            this.rusHydroToolStripMenuItem.Click += new System.EventHandler(this.ChangeCompanyTypeToolStripMenuItem_Click);
            // 
            // labelUserNameValue
            // 
            this.labelUserNameValue.AutoSize = true;
            this.labelUserNameValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelUserNameValue.Location = new System.Drawing.Point(170, 50);
            this.labelUserNameValue.Name = "labelUserNameValue";
            this.labelUserNameValue.Size = new System.Drawing.Size(153, 20);
            this.labelUserNameValue.TabIndex = 3;
            this.labelUserNameValue.Text = "Имя пользователя";
            // 
            // labelDataSourceValue
            // 
            this.labelDataSourceValue.ForeColor = System.Drawing.Color.DarkRed;
            this.labelDataSourceValue.Location = new System.Drawing.Point(170, 80);
            this.labelDataSourceValue.Name = "labelDataSourceValue";
            this.labelDataSourceValue.Size = new System.Drawing.Size(302, 79);
            this.labelDataSourceValue.TabIndex = 4;
            this.labelDataSourceValue.Text = "Путь к базе данных";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(12, 50);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(118, 20);
            this.labelUserName.TabIndex = 99;
            this.labelUserName.Text = "Вы вошли как:";
            // 
            // labelDataSource
            // 
            this.labelDataSource.AutoSize = true;
            this.labelDataSource.Location = new System.Drawing.Point(12, 80);
            this.labelDataSource.Name = "labelDataSource";
            this.labelDataSource.Size = new System.Drawing.Size(145, 20);
            this.labelDataSource.TabIndex = 100;
            this.labelDataSource.Text = "Источник данных:";
            // 
            // labelVersion
            // 
            this.labelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVersion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVersion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelVersion.Location = new System.Drawing.Point(16, 354);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(468, 31);
            this.labelVersion.TabIndex = 101;
            this.labelVersion.Text = "Версия:";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBoxCompanyLogo
            // 
            this.pictureBoxCompanyLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCompanyLogo.Location = new System.Drawing.Point(12, 267);
            this.pictureBoxCompanyLogo.MaximumSize = new System.Drawing.Size(400, 400);
            this.pictureBoxCompanyLogo.Name = "pictureBoxCompanyLogo";
            this.pictureBoxCompanyLogo.Size = new System.Drawing.Size(150, 100);
            this.pictureBoxCompanyLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCompanyLogo.TabIndex = 102;
            this.pictureBoxCompanyLogo.TabStop = false;
            // 
            // labelStationDataSource
            // 
            this.labelStationDataSource.AutoSize = true;
            this.labelStationDataSource.Location = new System.Drawing.Point(12, 158);
            this.labelStationDataSource.Name = "labelStationDataSource";
            this.labelStationDataSource.Size = new System.Drawing.Size(151, 20);
            this.labelStationDataSource.TabIndex = 104;
            this.labelStationDataSource.Text = "Источник станций:";
            // 
            // labelStationDataSourceValue
            // 
            this.labelStationDataSourceValue.ForeColor = System.Drawing.Color.DarkRed;
            this.labelStationDataSourceValue.Location = new System.Drawing.Point(170, 158);
            this.labelStationDataSourceValue.Name = "labelStationDataSourceValue";
            this.labelStationDataSourceValue.Size = new System.Drawing.Size(302, 79);
            this.labelStationDataSourceValue.TabIndex = 103;
            this.labelStationDataSourceValue.Text = "Путь к базе станций";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(496, 394);
            this.Controls.Add(this.labelStationDataSource);
            this.Controls.Add(this.labelStationDataSourceValue);
            this.Controls.Add(this.pictureBoxCompanyLogo);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.labelDataSource);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.labelDataSourceValue);
            this.Controls.Add(this.labelUserNameValue);
            this.Controls.Add(this.menuStripMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(500, 200);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сейсмология";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompanyLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.ToolStripMenuItem stationDataToolStripMenuItem;

        private System.Windows.Forms.Label labelStationDataSource;

        private System.Windows.Forms.Label labelStationDataSourceValue;

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userNameToolStripMenuItem;
        private System.Windows.Forms.Label labelUserNameValue;
        private System.Windows.Forms.ToolStripMenuItem addWsgFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createTablesToolStripMenuItem;
        private System.Windows.Forms.Label labelDataSourceValue;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelDataSource;
        private System.Windows.Forms.ToolStripMenuItem saveSettingsToolStripMenuItem;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.PictureBox pictureBoxCompanyLogo;
        private System.Windows.Forms.ToolStripMenuItem companyTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rusHydroToolStripMenuItem;
    }
}

