﻿using Seismology.Core;
using Seismology.Core.Data;
using Seismology.Core.Data.Access;
using Seismology.Core.MathS;
using Seismology.MainApp.Model;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.MainApp
{
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class CalculateDbForm : CalculateForm
    {
        private readonly DbEarthquake _dbEarthquake;
        public CalculateDbForm(DbEarthquake dbEarthquake)
        {
            InitializeComponent();

            _dbEarthquake = dbEarthquake;
        }

        private void CalculateDbForm_Load(object sender, EventArgs e)
        {
            var method = MethodType.GetEpicenterMethod(_dbEarthquake.Method);
            _calculateMethod.EpicenterMethod = method ?? new AutomaticMethod();
            if (method is WsgDataMethod)
            {
                if (_dbEarthquake.Stations.Count < 3) 
                    _calculateMethod.WsgType = WsgTypeEnum.FewStations;
                else if (_dbEarthquake.Stations.Count(s => s.Sp < 25) < 3)
                    _calculateMethod.WsgType = WsgTypeEnum.DistantEarthquakes;
                else
                    MessageBox.Show("Не удалось распознать WSG тип расчета!", "Внимание");
            }
            else if (method is null)
            {
                MessageBox.Show("Метод, по которому расситывалось землетрясениие не был найден.\r\n" +
                                "Будет выбран метод по умолчанию (Автоматический).");
                automaticToolStripMenuItem.Checked = true;
                hodographToolStripMenuItem.Enabled = false;
            }
            else switch (_calculateMethod.EpicenterMethod)
            {
                case MultiMethod _:
                    multiToolStripMenuItem.Checked = true;
                    break;
                case BinaryMethod _:
                    binaryToolStripMenuItem.Checked = true;
                    break;
                case IntersectionMethod _:
                    intersectionToolStripMenuItem.Checked = true;
                    break;
                case GridMethod _:
                    gridToolStripMenuItem.Checked = true;
                    break;
                case VadatiMethod _:
                    vadatiToolStripMenuItem.Checked = true;
                    break;
            }

            var distanceMethod = MethodType.GetDistanceMethod(_dbEarthquake.HodographMethod);
            _calculateMethod.DistanceMethod = distanceMethod ?? new TpMethod();
            if (distanceMethod == null)
            {
                MessageBox.Show("Метод, по которому расситывалось эпицентарльное расстояние не был найден.\r\n" +
                                "Будет выбран метод по умолчанию (Метод Tp).");
                tpToolStripMenuItem.Checked = true;
            }

            else switch (_calculateMethod.DistanceMethod)
            {
                case TpMethod _:
                    tpToolStripMenuItem.Checked = true;
                    break;
                case SpMethod _:
                    spToolStripMenuItem.Checked = true;
                    break;
                case TsMethod _:
                    tsToolStripMenuItem.Checked = true;
                    break;
            }

            tabControlMain.TabPages.Remove(tabPageWsgData);
            openWsgToolStripMenuItem.Visible = false;
            buttonAction.Text = "Изменить";

            FillListViewStations(_dbEarthquake.Stations);

            var earthquake = (Earthquake)_dbEarthquake;
            _initialStations = earthquake.Stations.ToList();
            dataGridViewDbData.DataSource = earthquake.Stations.ToList();

            _calculateMethod.Earthquake = earthquake;
            _calculateMethod.Earthquake.Stations = _dbEarthquake.Stations
                .Where(s => s.IsIncluded)
                .Select(s => (EarthquakeStation)s).ToList();

            FillData();

            var calc = new CalculatorS();
            var eq = _calculateMethod.Earthquake;
            if (_calculateMethod.WsgType != WsgTypeEnum.FewStations)
                _calculateMethod.WadatiData = calc.FindVpVsAndT0(ref eq);

            ClearAndDrawOnMapsAndChart();

            buttonCalculate.Enabled = true;
        }

        protected override async void ButtonAction_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            try
            {
                var db = new DbAccess(Settings.MySettings.CurrentDataSource);
                var dbEarthquake = (DbEarthquake)_calculateMethod.Earthquake;
                // Выбираем станционные данные, которые не учавстваовали в рассчете
                var notIncludedStations = _initialStations
                    .Except(_calculateMethod.Earthquake.Stations, new CompareByStationName())
                    .ToList();
                // Будем добавлять каждую из них при этому указывая, что она не включена в рассчет
                foreach (var station in notIncludedStations)
                {
                    var dbStation = (DbEarthquakeStation)station;
                    // Указываем, что эта станция не учтена
                    dbStation.IsIncluded = false;
                    dbEarthquake.Stations.Add(dbStation);
                }
                dbEarthquake.Method = _calculateMethod.EpicenterMethod.Name;
                dbEarthquake.HodographMethod = _calculateMethod.DistanceMethod.Name;
                dbEarthquake.Author = Settings.MySettings.UserName;
                await db.UpdateAsync(_dbEarthquake.Id, dbEarthquake);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось обновить запись в базе данных, причина: " +
                                $"{ex.Message}; " +
                                $"{ex.InnerException?.Message}");
            }
            this.Enabled = true;
        }
    }
}
