﻿using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace Seismology.MainApp
{
    /// <summary>
    /// Настройки
    /// </summary>
    public class Settings
    {
        private const string FileName = "settings.xml";

        private static readonly string FilePath =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\{FileName}";

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Источник данных (.mdb 2000-2003 Access)
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// Источник данных (.mdb 2000-2003 Access) для РусГидро
        /// </summary>
        public string DataSourceRusHydro { get; set; }

        /// <summary>
        /// Источник данных для станции
        /// </summary>
        public string StationDataSource { get; set; }

        /// <summary>
        /// Источник данных для станции для РусГидро
        /// </summary>
        public string StationDataSourceRusHydro { get; set; }

        [XmlIgnore]
        public string CurrentDataSource
        {
            get
            {
                switch (Globals.Company)
                {
                    case CompanyType.Default:
                        return DataSource;
                    case CompanyType.RusHydro:
                        return DataSourceRusHydro;
                    default:
                        throw new Exception("Неизвестная компания!");
                }
            }
            set
            {
                switch (Globals.Company)
                {
                    case CompanyType.Default:
                        DataSource = value;
                        break;
                    case CompanyType.RusHydro:
                        DataSourceRusHydro = value;
                        break;
                    default:
                        throw new Exception("Неизвестная компания!");
                }
            }
        }

        [XmlIgnore]
        public string CurrentStationDataSource
        {
            get
            {
                switch (Globals.Company)
                {
                    case CompanyType.Default:
                        return StationDataSource;
                    case CompanyType.RusHydro:
                        return StationDataSourceRusHydro;
                    default:
                        throw new Exception("Неизвестная компания!");
                }
            }
            set
            {
                switch (Globals.Company)
                {
                    case CompanyType.Default:
                        StationDataSource = value;
                        break;
                    case CompanyType.RusHydro:
                        StationDataSourceRusHydro = value;
                        break;
                    default:
                        throw new Exception("Неизвестная компания!");
                }
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            using (var fs = new FileStream(FilePath, FileMode.Create))
            {
                var xml = new XmlSerializer(typeof(Settings));
                xml.Serialize(fs, this);
            }
        }

        /// <summary>
        /// Загружает пользовательские настройки.
        /// Если таковых нет, то сохраняет и возвращает настройки по умолчанию.
        /// </summary>
        /// <returns>Настройки</returns>
        public static Settings LoadSettings()
        {
            if (File.Exists(FilePath))
            {
                using (var fs = new FileStream(FilePath, FileMode.Open))
                {
                    var xml = new XmlSerializer(typeof(Settings));
                    return (Settings)xml.Deserialize(fs);
                }
            }

            DefaultSettings.Save();
            return DefaultSettings;
        }

        private static Settings _mySettings;

        /// <summary>
        /// Мои настройки. Загружает пользовательские настройки.
        /// Если таковых нет, то сохраняет и возвращает настройки по умолчанию.
        /// </summary>
        public static Settings MySettings
        {
            get
            {
                if (_mySettings == null)
                {
                    _mySettings = LoadSettings();
                }

                return _mySettings;
            }
            set => _mySettings = value;
        }

        /// <summary>
        /// Настройки по умолчанию
        /// </summary>
        public static Settings DefaultSettings { get; } = new Settings
        {
            UserName = Environment.UserName,
            DataSource = "Не задано",
            DataSourceRusHydro = "Не задано",
            StationDataSource = "Не задано",
            StationDataSourceRusHydro = "Не задано",
        };

        #region Equals

        private bool Equals(Settings other) =>
            UserName == other.UserName && DataSource == other.DataSource &&
            DataSourceRusHydro == other.DataSourceRusHydro && StationDataSource == other.StationDataSource &&
            StationDataSourceRusHydro == other.StationDataSourceRusHydro;

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Settings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DataSource != null ? DataSource.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DataSourceRusHydro != null ? DataSourceRusHydro.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (StationDataSource != null ? StationDataSource.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^
                           (StationDataSourceRusHydro != null ? StationDataSourceRusHydro.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Settings left, Settings right) => 
            Equals(left, right);

        public static bool operator !=(Settings left, Settings right) => 
            !Equals(left, right);

        #endregion
    }
}