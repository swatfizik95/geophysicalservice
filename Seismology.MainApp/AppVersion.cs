﻿using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace Seismology.MainApp
{
    public class AppVersion
    {
        private const string FileName = "version.xml";
        private static readonly string FilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\{FileName}";

        public string Version { get; set; }
        public DateTime Date { get; set; }

        public void Save()
        {
            using (var fs = new FileStream(FilePath, FileMode.Create))
            {
                var xml = new XmlSerializer(typeof(AppVersion));
                xml.Serialize(fs, this);
            }
        }

        private static AppVersion LoadVersion()
        {
            if (File.Exists(FilePath))
            {
                using (var fs = new FileStream(FilePath, FileMode.Open))
                {
                    var xml = new XmlSerializer(typeof(AppVersion));
                    return (AppVersion)xml.Deserialize(fs);
                }
            }

            //DefaultVersion.Save();
            return DefaultVersion;
        }

        internal static string LoadVersion(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (var fs = new FileStream(filePath, FileMode.Open))
                {
                    var xml = new XmlSerializer(typeof(AppVersion));
                    return ((AppVersion)xml.Deserialize(fs)).Version;
                }
            }

            return null;
        }

        private static AppVersion _currentVersion;
        public static AppVersion CurrentVersion
        {
            get
            {
                if (_currentVersion == null)
                {
                    _currentVersion = LoadVersion();
                }
                return _currentVersion;
            }
            set => _currentVersion = value;
        }

        /// <summary>
        /// Настройки по умолчанию
        /// </summary>
        public static AppVersion DefaultVersion { get; } = new AppVersion
        {
            Version = "Неизвестно",
            Date = DateTime.MinValue
        };
    }
}
