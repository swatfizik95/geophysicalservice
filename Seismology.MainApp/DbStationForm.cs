﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using Seismology.Core;
using Seismology.Core.Data;
using Seismology.Core.Map;
using Seismology.MainApp.Model;
using Seismology.MainApp.Properties;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;

namespace Seismology.MainApp
{
    public partial class DbStationForm : Form
    {
        private readonly DbEarthquake _dbEarthquake;
        private readonly Earthquake _earthquake;
        // Оверлей для GMap
        protected readonly GMapOverlay _overlay = new GMapOverlay("markers");
        // Статичная карта Дагестана
        protected StaticMapS _staticMap;

        public DbStationForm(DbEarthquake dbEarthquake)
        {
            InitializeComponent();

            InitStaticMap();
            InitGMap();

            _dbEarthquake = dbEarthquake;
            _earthquake = (Earthquake)dbEarthquake;
            _earthquake.Stations = dbEarthquake.Stations
                .Where(s => s.IsIncluded)
                .Select(s => (EarthquakeStation)s)
                .ToList();


        }

        private void DbStationForm_Load(object sender, EventArgs e)
        {
            foreach (var station in _dbEarthquake.Stations)
            {
                listViewStations.Items.Add(new ListViewItem
                {
                    Text = station.Name,
                    ForeColor = StationStorage.FindStationByName(station.Name).Color,
                    Checked = station.IsIncluded
                });
            }

            dataGridViewDbData.DataSource = _earthquake.Stations.ToList();

            FillData();
            ClearAndDrawOnMaps();
        }


        #region StaticMap

        /// <summary>
        /// Рисует радиусы станцинных землетрясений на статичной карте
        /// </summary>
        private void DrawOnStaticMap()
        {
            try
            {
                foreach (var station in _earthquake.Stations)
                {
                    _staticMap.AddRadius(station.Station.Coordinate, station.EpicDistance, station.Station.Color, 3);
                    _staticMap.AddMarker(new Marker(station.Station.Coordinate, station.Station.Color));
                }
                _staticMap.AddEarhquake(_earthquake);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        /// <summary>
        /// Инициализация статичной карты
        /// </summary>
        private void InitStaticMap()
        {
            _staticMap = new StaticMapS(
                Resources.DagMap,
                30, 1447, 41, 45,
                55, 1091, 45, 49);
            pictureBoxStaticMap.Image = _staticMap.Image;
            pictureBoxStaticMap.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void SaveStaticMapImage(string path, ImageFormat format)
        {
            pictureBoxStaticMap.Image.Save(path, format);
        }

        #endregion

        #region GMap

        /// <summary>
        /// Рисует радиусы станцинных землетрясений на GMap карте
        /// </summary>
        private void DrawOnGMap()
        {
            var gMap = new GMapS();

            foreach (var station in _earthquake.Stations)
            {
                // Радиус (в виде полигонов) станционного землетрясения
                var gPolStationRadius = gMap.GetStationRadiusGPolygon(station);
                // Добавляем на GMap полигон радиуса
                _overlay.Polygons.Add(gPolStationRadius);
                // Маркер станции
                var gMarkerStation = gMap.GetStationGMarker(station.Station);
                // "Пустой" маркер станции (нужен для вывода информации)
                var gMarkerEmptyStation = gMap.GetEmptyStationGMarker(station.Station);
                // Добавляем на GMap маркеры станций
                _overlay.Markers.Add(gMarkerStation);
                _overlay.Markers.Add(gMarkerEmptyStation);
            }

            // Маркер самого землетрясения
            var eqMarker = gMap.GetEarthquakeGMarker(new PointLatLng(_earthquake.Lat, _earthquake.Lng));

            // Добавляем на GMap маркер землетрясения
            _overlay.Markers.Add(eqMarker);
        }

        /// <summary>
        /// Инициализация GMap
        /// </summary>
        private void InitGMap()
        {
            // Добавляем оверлей на наш Gmap
            gMapOnlineControl.Overlays.Add(_overlay);

            // Настройки для компонента GMap.
            gMapOnlineControl.Bearing = 0;

            // CanDragMap - Если параметр установлен в True,
            // пользователь может перетаскивать карту 
            // с помощью правой кнопки мыши. 
            gMapOnlineControl.CanDragMap = true;

            // Указываем, что перетаскивание карты осуществляется 
            // с использованием левой клавишей мыши.
            // По умолчанию - правая.
            gMapOnlineControl.DragButton = MouseButtons.Left;

            gMapOnlineControl.GrayScaleMode = true;

            // MarkersEnabled - Если параметр установлен в True,
            // любые маркеры, заданные вручную будет показаны.
            // Если нет, они не появятся.
            gMapOnlineControl.MarkersEnabled = true;

            // Указываем значение максимального приближения.
            gMapOnlineControl.MaxZoom = 18;

            // Указываем значение минимального приближения.
            gMapOnlineControl.MinZoom = 2;

            // Устанавливаем центр приближения/удаления
            // курсор мыши.
            /*
            gMapControl1.MouseWheelZoomType =
                GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            */

            // Отказываемся от негативного режима.
            gMapOnlineControl.NegativeMode = false;

            // Разрешаем полигоны.
            gMapOnlineControl.PolygonsEnabled = true;

            // Запрешаем маршруты
            gMapOnlineControl.RoutesEnabled = false;

            // Скрываем внешнюю сетку карты
            // с заголовками.
            gMapOnlineControl.ShowTileGridLines = false;

            // Указываем, что при загрузке карты будет использоваться 
            // 18ти кратное приближение.
            gMapOnlineControl.Zoom = 8.25;

            // Указываем что все края элемента управления
            // закрепляются у краев содержащего его элемента
            // управления(главной формы), а их размеры изменяются 
            // соответствующим образом.
            gMapOnlineControl.Dock = DockStyle.Fill;

            // Указываем что будем использовать карты Google.
            /*
            gMapControl1.MapProvider = 
                GMap.NET.MapProviders.GMapProviders.GoogleMap;
            */
            gMapOnlineControl.MapProvider =
                GMapProviders.OpenStreetMap;
            GMaps.Instance.Mode =
                AccessMode.ServerOnly;

            // Позиция
            gMapOnlineControl.Position = new PointLatLng(43.0088083, 46.8795529);
        }

        private void SaveGMapImage(string path, ImageFormat format)
        {
            gMapOnlineControl.ToImage().Save(path, format);
        }

        #endregion

        protected void ClearAndDrawOnMaps()
        {
            // Очищаем, а потом рисуем на статичной карте
            _staticMap.ClearMap();
            DrawOnStaticMap();
            pictureBoxStaticMap.Refresh();
            // Очищаем, а потом рисуем на онлайн карте
            _overlay.Markers.Clear();
            _overlay.Polygons.Clear();
            DrawOnGMap();
        }

        protected void FillData()
        {
            // Запись расчетного землетрясения в таблицу
            dataGridViewEarthquake.DataSource = new List<EarthquakeView>
            {
                (EarthquakeView)_earthquake
            };

            // Редактирование
            FillDataGridView(_earthquake.Stations);
        }

        private void FillDataGridView(IEnumerable<EarthquakeStation> stations)
        {
            dataGridViewEditStation.Rows.Clear();
            foreach (var station in stations)
            {
                dataGridViewEditStation.Rows.Add(
                    station.Name,
                    station.T0.ToString("HH:mm:ss.fff"),
                    station.K.ToString("F3"),
                    station.Tp.ToString("HH:mm:ss.fff"),
                    station.Sp,
                    station.EpicDistance.ToString("F3"),
                    station.Dt.ToString("F3"),
                    station.VpVs.ToString("F3"),
                    station.PeriodZ,
                    station.AmplitudeZ,
                    station.Period,
                    station.Amplitude,
                    station.Ep,
                    station.Es);
            }
        }
    }
}
