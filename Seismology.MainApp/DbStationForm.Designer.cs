﻿namespace Seismology.MainApp
{
    partial class DbStationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageDbData = new System.Windows.Forms.TabPage();
            this.tabPageResult = new System.Windows.Forms.TabPage();
            this.dataGridViewEarthquake = new System.Windows.Forms.DataGridView();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vpVsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.earthquakeViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewEditStation = new System.Windows.Forms.DataGridView();
            this.Station = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.K = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EpicDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VpVs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeriodZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmplitudeZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Period = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amplitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Es = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageMapAndSelection = new System.Windows.Forms.TabPage();
            this.listViewStations = new System.Windows.Forms.ListView();
            this.tabControlMap = new System.Windows.Forms.TabControl();
            this.tabPageOnline = new System.Windows.Forms.TabPage();
            this.gMapOnlineControl = new GMap.NET.WindowsForms.GMapControl();
            this.tabPageStatic = new System.Windows.Forms.TabPage();
            this.pictureBoxStaticMap = new System.Windows.Forms.PictureBox();
            this.earthquakeStationViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.earthquakeStationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewDbData = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epicDistanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodZDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amplitudeZDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amplitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControlMain.SuspendLayout();
            this.tabPageDbData.SuspendLayout();
            this.tabPageResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEarthquake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditStation)).BeginInit();
            this.tabPageMapAndSelection.SuspendLayout();
            this.tabControlMap.SuspendLayout();
            this.tabPageOnline.SuspendLayout();
            this.tabPageStatic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStaticMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDbData)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageDbData);
            this.tabControlMain.Controls.Add(this.tabPageResult);
            this.tabControlMain.Controls.Add(this.tabPageMapAndSelection);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1203, 601);
            this.tabControlMain.TabIndex = 1;
            // 
            // tabPageDbData
            // 
            this.tabPageDbData.Controls.Add(this.dataGridViewDbData);
            this.tabPageDbData.Location = new System.Drawing.Point(4, 29);
            this.tabPageDbData.Name = "tabPageDbData";
            this.tabPageDbData.Size = new System.Drawing.Size(1195, 568);
            this.tabPageDbData.TabIndex = 3;
            this.tabPageDbData.Text = "Данные из БД";
            this.tabPageDbData.UseVisualStyleBackColor = true;
            // 
            // tabPageResult
            // 
            this.tabPageResult.Controls.Add(this.dataGridViewEarthquake);
            this.tabPageResult.Controls.Add(this.dataGridViewEditStation);
            this.tabPageResult.Location = new System.Drawing.Point(4, 29);
            this.tabPageResult.Name = "tabPageResult";
            this.tabPageResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResult.Size = new System.Drawing.Size(1195, 568);
            this.tabPageResult.TabIndex = 1;
            this.tabPageResult.Text = "Результат";
            this.tabPageResult.UseVisualStyleBackColor = true;
            // 
            // dataGridViewEarthquake
            // 
            this.dataGridViewEarthquake.AllowUserToAddRows = false;
            this.dataGridViewEarthquake.AllowUserToDeleteRows = false;
            this.dataGridViewEarthquake.AutoGenerateColumns = false;
            this.dataGridViewEarthquake.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEarthquake.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEarthquake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEarthquake.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateDataGridViewTextBoxColumn,
            this.t0DataGridViewTextBoxColumn1,
            this.kDataGridViewTextBoxColumn1,
            this.hDataGridViewTextBoxColumn,
            this.fDataGridViewTextBoxColumn,
            this.lDataGridViewTextBoxColumn,
            this.dtDataGridViewTextBoxColumn1,
            this.vpVsDataGridViewTextBoxColumn1});
            this.dataGridViewEarthquake.DataSource = this.earthquakeViewBindingSource;
            this.dataGridViewEarthquake.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewEarthquake.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewEarthquake.Name = "dataGridViewEarthquake";
            this.dataGridViewEarthquake.ReadOnly = true;
            this.dataGridViewEarthquake.RowHeadersVisible = false;
            this.dataGridViewEarthquake.Size = new System.Drawing.Size(1189, 75);
            this.dataGridViewEarthquake.TabIndex = 3;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // t0DataGridViewTextBoxColumn1
            // 
            this.t0DataGridViewTextBoxColumn1.DataPropertyName = "T0";
            dataGridViewCellStyle6.Format = "HH:mm:ss.fff";
            this.t0DataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.t0DataGridViewTextBoxColumn1.HeaderText = "Время T0";
            this.t0DataGridViewTextBoxColumn1.Name = "t0DataGridViewTextBoxColumn1";
            this.t0DataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // kDataGridViewTextBoxColumn1
            // 
            this.kDataGridViewTextBoxColumn1.DataPropertyName = "K";
            this.kDataGridViewTextBoxColumn1.HeaderText = "K (Класс)";
            this.kDataGridViewTextBoxColumn1.Name = "kDataGridViewTextBoxColumn1";
            this.kDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // hDataGridViewTextBoxColumn
            // 
            this.hDataGridViewTextBoxColumn.DataPropertyName = "H";
            this.hDataGridViewTextBoxColumn.HeaderText = "H (Глубина)";
            this.hDataGridViewTextBoxColumn.Name = "hDataGridViewTextBoxColumn";
            this.hDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fDataGridViewTextBoxColumn
            // 
            this.fDataGridViewTextBoxColumn.DataPropertyName = "F";
            this.fDataGridViewTextBoxColumn.HeaderText = "F (Широта)";
            this.fDataGridViewTextBoxColumn.Name = "fDataGridViewTextBoxColumn";
            this.fDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lDataGridViewTextBoxColumn
            // 
            this.lDataGridViewTextBoxColumn.DataPropertyName = "L";
            this.lDataGridViewTextBoxColumn.HeaderText = "L (Долгота)";
            this.lDataGridViewTextBoxColumn.Name = "lDataGridViewTextBoxColumn";
            this.lDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtDataGridViewTextBoxColumn1
            // 
            this.dtDataGridViewTextBoxColumn1.DataPropertyName = "Dt";
            this.dtDataGridViewTextBoxColumn1.HeaderText = "Dt (Невязка)";
            this.dtDataGridViewTextBoxColumn1.Name = "dtDataGridViewTextBoxColumn1";
            this.dtDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // vpVsDataGridViewTextBoxColumn1
            // 
            this.vpVsDataGridViewTextBoxColumn1.DataPropertyName = "VpVs";
            this.vpVsDataGridViewTextBoxColumn1.HeaderText = "Vp/Vs";
            this.vpVsDataGridViewTextBoxColumn1.Name = "vpVsDataGridViewTextBoxColumn1";
            this.vpVsDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // earthquakeViewBindingSource
            // 
            this.earthquakeViewBindingSource.DataSource = typeof(Seismology.MainApp.Model.EarthquakeView);
            // 
            // dataGridViewEditStation
            // 
            this.dataGridViewEditStation.AllowUserToAddRows = false;
            this.dataGridViewEditStation.AllowUserToDeleteRows = false;
            this.dataGridViewEditStation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEditStation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEditStation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEditStation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEditStation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Station,
            this.T0,
            this.K,
            this.TP,
            this.SP,
            this.EpicDistance,
            this.Dt,
            this.VpVs,
            this.PeriodZ,
            this.AmplitudeZ,
            this.Period,
            this.Amplitude,
            this.Ep,
            this.Es});
            this.dataGridViewEditStation.Location = new System.Drawing.Point(3, 80);
            this.dataGridViewEditStation.Name = "dataGridViewEditStation";
            this.dataGridViewEditStation.ReadOnly = true;
            this.dataGridViewEditStation.RowHeadersVisible = false;
            this.dataGridViewEditStation.Size = new System.Drawing.Size(1192, 488);
            this.dataGridViewEditStation.TabIndex = 2;
            // 
            // Station
            // 
            this.Station.HeaderText = "Станция";
            this.Station.Name = "Station";
            this.Station.ReadOnly = true;
            // 
            // T0
            // 
            this.T0.HeaderText = "Время T0";
            this.T0.Name = "T0";
            this.T0.ReadOnly = true;
            // 
            // K
            // 
            this.K.HeaderText = "Класс";
            this.K.Name = "K";
            this.K.ReadOnly = true;
            // 
            // TP
            // 
            this.TP.HeaderText = "Время TP";
            this.TP.Name = "TP";
            this.TP.ReadOnly = true;
            // 
            // SP
            // 
            this.SP.HeaderText = "SP";
            this.SP.Name = "SP";
            this.SP.ReadOnly = true;
            // 
            // EpicDistance
            // 
            this.EpicDistance.HeaderText = "Радиус (Эпиц. дист.)";
            this.EpicDistance.Name = "EpicDistance";
            this.EpicDistance.ReadOnly = true;
            // 
            // Dt
            // 
            this.Dt.HeaderText = "Dt (Невязка)";
            this.Dt.Name = "Dt";
            this.Dt.ReadOnly = true;
            // 
            // VpVs
            // 
            this.VpVs.HeaderText = "Vp/Vs";
            this.VpVs.Name = "VpVs";
            this.VpVs.ReadOnly = true;
            // 
            // PeriodZ
            // 
            this.PeriodZ.HeaderText = "Период Z";
            this.PeriodZ.Name = "PeriodZ";
            this.PeriodZ.ReadOnly = true;
            this.PeriodZ.Visible = false;
            // 
            // AmplitudeZ
            // 
            this.AmplitudeZ.HeaderText = "Амплитуда Z";
            this.AmplitudeZ.Name = "AmplitudeZ";
            this.AmplitudeZ.ReadOnly = true;
            this.AmplitudeZ.Visible = false;
            // 
            // Period
            // 
            this.Period.HeaderText = "Период";
            this.Period.Name = "Period";
            this.Period.ReadOnly = true;
            this.Period.Visible = false;
            // 
            // Amplitude
            // 
            this.Amplitude.HeaderText = "Амплитуда";
            this.Amplitude.Name = "Amplitude";
            this.Amplitude.ReadOnly = true;
            this.Amplitude.Visible = false;
            // 
            // Ep
            // 
            this.Ep.HeaderText = "Ep";
            this.Ep.Name = "Ep";
            this.Ep.ReadOnly = true;
            this.Ep.Visible = false;
            // 
            // Es
            // 
            this.Es.HeaderText = "Es";
            this.Es.Name = "Es";
            this.Es.ReadOnly = true;
            this.Es.Visible = false;
            // 
            // tabPageMapAndSelection
            // 
            this.tabPageMapAndSelection.Controls.Add(this.listViewStations);
            this.tabPageMapAndSelection.Controls.Add(this.tabControlMap);
            this.tabPageMapAndSelection.Location = new System.Drawing.Point(4, 29);
            this.tabPageMapAndSelection.Name = "tabPageMapAndSelection";
            this.tabPageMapAndSelection.Size = new System.Drawing.Size(1195, 568);
            this.tabPageMapAndSelection.TabIndex = 2;
            this.tabPageMapAndSelection.Text = "Карта и выборка";
            this.tabPageMapAndSelection.UseVisualStyleBackColor = true;
            // 
            // listViewStations
            // 
            this.listViewStations.CheckBoxes = true;
            this.listViewStations.Dock = System.Windows.Forms.DockStyle.Right;
            this.listViewStations.LabelWrap = false;
            this.listViewStations.Location = new System.Drawing.Point(996, 0);
            this.listViewStations.Name = "listViewStations";
            this.listViewStations.Size = new System.Drawing.Size(199, 568);
            this.listViewStations.TabIndex = 3;
            this.listViewStations.UseCompatibleStateImageBehavior = false;
            // 
            // tabControlMap
            // 
            this.tabControlMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMap.Controls.Add(this.tabPageOnline);
            this.tabControlMap.Controls.Add(this.tabPageStatic);
            this.tabControlMap.Location = new System.Drawing.Point(0, 3);
            this.tabControlMap.Name = "tabControlMap";
            this.tabControlMap.SelectedIndex = 0;
            this.tabControlMap.Size = new System.Drawing.Size(997, 562);
            this.tabControlMap.TabIndex = 2;
            // 
            // tabPageOnline
            // 
            this.tabPageOnline.Controls.Add(this.gMapOnlineControl);
            this.tabPageOnline.Location = new System.Drawing.Point(4, 29);
            this.tabPageOnline.Name = "tabPageOnline";
            this.tabPageOnline.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOnline.Size = new System.Drawing.Size(989, 529);
            this.tabPageOnline.TabIndex = 0;
            this.tabPageOnline.Text = "Онлайн";
            this.tabPageOnline.UseVisualStyleBackColor = true;
            // 
            // gMapOnlineControl
            // 
            this.gMapOnlineControl.Bearing = 0F;
            this.gMapOnlineControl.CanDragMap = true;
            this.gMapOnlineControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gMapOnlineControl.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMapOnlineControl.GrayScaleMode = false;
            this.gMapOnlineControl.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMapOnlineControl.LevelsKeepInMemory = 5;
            this.gMapOnlineControl.Location = new System.Drawing.Point(3, 3);
            this.gMapOnlineControl.MarkersEnabled = true;
            this.gMapOnlineControl.MaxZoom = 2;
            this.gMapOnlineControl.MinZoom = 2;
            this.gMapOnlineControl.MouseWheelZoomEnabled = true;
            this.gMapOnlineControl.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMapOnlineControl.Name = "gMapOnlineControl";
            this.gMapOnlineControl.NegativeMode = false;
            this.gMapOnlineControl.PolygonsEnabled = true;
            this.gMapOnlineControl.RetryLoadTile = 0;
            this.gMapOnlineControl.RoutesEnabled = true;
            this.gMapOnlineControl.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMapOnlineControl.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMapOnlineControl.ShowTileGridLines = false;
            this.gMapOnlineControl.Size = new System.Drawing.Size(983, 523);
            this.gMapOnlineControl.TabIndex = 0;
            this.gMapOnlineControl.Zoom = 0D;
            // 
            // tabPageStatic
            // 
            this.tabPageStatic.Controls.Add(this.pictureBoxStaticMap);
            this.tabPageStatic.Location = new System.Drawing.Point(4, 29);
            this.tabPageStatic.Name = "tabPageStatic";
            this.tabPageStatic.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStatic.Size = new System.Drawing.Size(989, 529);
            this.tabPageStatic.TabIndex = 1;
            this.tabPageStatic.Text = "Статичная";
            this.tabPageStatic.UseVisualStyleBackColor = true;
            // 
            // pictureBoxStaticMap
            // 
            this.pictureBoxStaticMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStaticMap.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxStaticMap.Name = "pictureBoxStaticMap";
            this.pictureBoxStaticMap.Size = new System.Drawing.Size(983, 523);
            this.pictureBoxStaticMap.TabIndex = 0;
            this.pictureBoxStaticMap.TabStop = false;
            // 
            // earthquakeStationViewBindingSource
            // 
            this.earthquakeStationViewBindingSource.DataSource = typeof(Seismology.MainApp.Model.EarthquakeStationView);
            // 
            // earthquakeStationBindingSource
            // 
            this.earthquakeStationBindingSource.DataSource = typeof(Seismology.Core.EarthquakeStation);
            // 
            // dataGridViewDbData
            // 
            this.dataGridViewDbData.AllowUserToAddRows = false;
            this.dataGridViewDbData.AllowUserToDeleteRows = false;
            this.dataGridViewDbData.AutoGenerateColumns = false;
            this.dataGridViewDbData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDbData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewDbData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDbData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.epDataGridViewTextBoxColumn,
            this.tpDataGridViewTextBoxColumn,
            this.esDataGridViewTextBoxColumn,
            this.tsDataGridViewTextBoxColumn,
            this.spDataGridViewTextBoxColumn,
            this.epicDistanceDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.periodZDataGridViewTextBoxColumn,
            this.amplitudeZDataGridViewTextBoxColumn,
            this.periodDataGridViewTextBoxColumn,
            this.amplitudeDataGridViewTextBoxColumn});
            this.dataGridViewDbData.DataSource = this.earthquakeStationBindingSource;
            this.dataGridViewDbData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDbData.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewDbData.Name = "dataGridViewDbData";
            this.dataGridViewDbData.ReadOnly = true;
            this.dataGridViewDbData.RowHeadersVisible = false;
            this.dataGridViewDbData.Size = new System.Drawing.Size(1195, 568);
            this.dataGridViewDbData.TabIndex = 2;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 76;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "T0";
            dataGridViewCellStyle3.Format = "HH:mm:ss.fff";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn1.HeaderText = "T0";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 52;
            // 
            // epDataGridViewTextBoxColumn
            // 
            this.epDataGridViewTextBoxColumn.DataPropertyName = "Ep";
            this.epDataGridViewTextBoxColumn.HeaderText = "Ep";
            this.epDataGridViewTextBoxColumn.Name = "epDataGridViewTextBoxColumn";
            this.epDataGridViewTextBoxColumn.ReadOnly = true;
            this.epDataGridViewTextBoxColumn.Width = 54;
            // 
            // tpDataGridViewTextBoxColumn
            // 
            this.tpDataGridViewTextBoxColumn.DataPropertyName = "Tp";
            dataGridViewCellStyle4.Format = "HH:mm:ss.fff";
            this.tpDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.tpDataGridViewTextBoxColumn.HeaderText = "Tp";
            this.tpDataGridViewTextBoxColumn.Name = "tpDataGridViewTextBoxColumn";
            this.tpDataGridViewTextBoxColumn.ReadOnly = true;
            this.tpDataGridViewTextBoxColumn.Width = 52;
            // 
            // esDataGridViewTextBoxColumn
            // 
            this.esDataGridViewTextBoxColumn.DataPropertyName = "Es";
            this.esDataGridViewTextBoxColumn.HeaderText = "Es";
            this.esDataGridViewTextBoxColumn.Name = "esDataGridViewTextBoxColumn";
            this.esDataGridViewTextBoxColumn.ReadOnly = true;
            this.esDataGridViewTextBoxColumn.Width = 53;
            // 
            // tsDataGridViewTextBoxColumn
            // 
            this.tsDataGridViewTextBoxColumn.DataPropertyName = "Ts";
            dataGridViewCellStyle5.Format = "HH:mm:ss.fff";
            this.tsDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.tsDataGridViewTextBoxColumn.HeaderText = "Ts";
            this.tsDataGridViewTextBoxColumn.Name = "tsDataGridViewTextBoxColumn";
            this.tsDataGridViewTextBoxColumn.ReadOnly = true;
            this.tsDataGridViewTextBoxColumn.Width = 51;
            // 
            // spDataGridViewTextBoxColumn
            // 
            this.spDataGridViewTextBoxColumn.DataPropertyName = "Sp";
            this.spDataGridViewTextBoxColumn.HeaderText = "Sp";
            this.spDataGridViewTextBoxColumn.Name = "spDataGridViewTextBoxColumn";
            this.spDataGridViewTextBoxColumn.ReadOnly = true;
            this.spDataGridViewTextBoxColumn.Width = 54;
            // 
            // epicDistanceDataGridViewTextBoxColumn
            // 
            this.epicDistanceDataGridViewTextBoxColumn.DataPropertyName = "EpicDistance";
            this.epicDistanceDataGridViewTextBoxColumn.HeaderText = "EpicDistance";
            this.epicDistanceDataGridViewTextBoxColumn.Name = "epicDistanceDataGridViewTextBoxColumn";
            this.epicDistanceDataGridViewTextBoxColumn.ReadOnly = true;
            this.epicDistanceDataGridViewTextBoxColumn.Width = 128;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Dt";
            this.dataGridViewTextBoxColumn2.HeaderText = "Dt";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 51;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "K";
            this.dataGridViewTextBoxColumn3.HeaderText = "K";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 44;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "VpVs";
            this.dataGridViewTextBoxColumn4.HeaderText = "VpVs";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 73;
            // 
            // periodZDataGridViewTextBoxColumn
            // 
            this.periodZDataGridViewTextBoxColumn.DataPropertyName = "PeriodZ";
            this.periodZDataGridViewTextBoxColumn.HeaderText = "PeriodZ";
            this.periodZDataGridViewTextBoxColumn.Name = "periodZDataGridViewTextBoxColumn";
            this.periodZDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodZDataGridViewTextBoxColumn.Width = 89;
            // 
            // amplitudeZDataGridViewTextBoxColumn
            // 
            this.amplitudeZDataGridViewTextBoxColumn.DataPropertyName = "AmplitudeZ";
            this.amplitudeZDataGridViewTextBoxColumn.HeaderText = "AmplitudeZ";
            this.amplitudeZDataGridViewTextBoxColumn.Name = "amplitudeZDataGridViewTextBoxColumn";
            this.amplitudeZDataGridViewTextBoxColumn.ReadOnly = true;
            this.amplitudeZDataGridViewTextBoxColumn.Width = 115;
            // 
            // periodDataGridViewTextBoxColumn
            // 
            this.periodDataGridViewTextBoxColumn.DataPropertyName = "Period";
            this.periodDataGridViewTextBoxColumn.HeaderText = "Period";
            this.periodDataGridViewTextBoxColumn.Name = "periodDataGridViewTextBoxColumn";
            this.periodDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodDataGridViewTextBoxColumn.Width = 79;
            // 
            // amplitudeDataGridViewTextBoxColumn
            // 
            this.amplitudeDataGridViewTextBoxColumn.DataPropertyName = "Amplitude";
            this.amplitudeDataGridViewTextBoxColumn.HeaderText = "Amplitude";
            this.amplitudeDataGridViewTextBoxColumn.Name = "amplitudeDataGridViewTextBoxColumn";
            this.amplitudeDataGridViewTextBoxColumn.ReadOnly = true;
            this.amplitudeDataGridViewTextBoxColumn.Width = 105;
            // 
            // DbStationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 601);
            this.Controls.Add(this.tabControlMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "DbStationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Просмотр";
            this.Load += new System.EventHandler(this.DbStationForm_Load);
            this.tabControlMain.ResumeLayout(false);
            this.tabPageDbData.ResumeLayout(false);
            this.tabPageResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEarthquake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditStation)).EndInit();
            this.tabPageMapAndSelection.ResumeLayout(false);
            this.tabControlMap.ResumeLayout(false);
            this.tabPageOnline.ResumeLayout(false);
            this.tabPageStatic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStaticMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDbData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.TabControl tabControlMain;
        protected System.Windows.Forms.TabPage tabPageDbData;
        private System.Windows.Forms.TabPage tabPageResult;
        protected System.Windows.Forms.DataGridView dataGridViewEditStation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Station;
        private System.Windows.Forms.DataGridViewTextBoxColumn T0;
        private System.Windows.Forms.DataGridViewTextBoxColumn K;
        private System.Windows.Forms.DataGridViewTextBoxColumn TP;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP;
        private System.Windows.Forms.DataGridViewTextBoxColumn EpicDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dt;
        private System.Windows.Forms.DataGridViewTextBoxColumn VpVs;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmplitudeZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Period;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amplitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ep;
        private System.Windows.Forms.DataGridViewTextBoxColumn Es;
        private System.Windows.Forms.TabPage tabPageMapAndSelection;
        protected System.Windows.Forms.ListView listViewStations;
        private System.Windows.Forms.TabControl tabControlMap;
        private System.Windows.Forms.TabPage tabPageOnline;
        private GMap.NET.WindowsForms.GMapControl gMapOnlineControl;
        private System.Windows.Forms.TabPage tabPageStatic;
        private System.Windows.Forms.PictureBox pictureBoxStaticMap;
        private System.Windows.Forms.BindingSource earthquakeViewBindingSource;
        protected System.Windows.Forms.DataGridView dataGridViewEarthquake;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn vpVsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource earthquakeStationViewBindingSource;
        private System.Windows.Forms.BindingSource earthquakeStationBindingSource;
        protected System.Windows.Forms.DataGridView dataGridViewDbData;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn epDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tpDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn esDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn spDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn epicDistanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodZDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amplitudeZDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amplitudeDataGridViewTextBoxColumn;
    }
}