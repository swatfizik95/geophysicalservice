﻿using Seismology.Core;
using System.Collections.Generic;

namespace Seismology.MainApp.Model
{
    /// <summary>
    /// Класс дял сравнения по именам станций
    /// </summary>
    public class CompareByStationName : IEqualityComparer<EarthquakeStation>
    {
        public bool Equals(EarthquakeStation x, EarthquakeStation y)
        {
            return y != null && x != null && x.Name == y.Name;
        }

        public int GetHashCode(EarthquakeStation obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
