﻿using Seismology.Core;
using System;
using System.ComponentModel;

namespace Seismology.MainApp.Model
{
    class EarthquakeView
    {
        [DisplayName("Дата")]
        public DateTime Date { get; set; }
        [DisplayName("Время T0")]
        public DateTime T0 { get; set; }
        [DisplayName("K (Класс)")]
        public double K { get; set; }
        [DisplayName("H (Глубина)")]
        public double H { get; set; }
        [DisplayName("F (Широта)")]
        public double F { get; set; }
        [DisplayName("F (Погрешность)")]
        public double LatError { get; set; }
        [DisplayName("L (Долгота)")]
        public double L { get; set; }
        [DisplayName("L (Погрешность)")]
        public double LngError { get; set; }
        [DisplayName("Dt (Невязка)")]
        public double Dt { get; set; }
        [DisplayName("Среднее квадр. Dt")]
        public double DtRms { get; set; }
        [DisplayName("Vp/Vs")]
        public double VpVs { get; set; }

        public static explicit operator EarthquakeView(Earthquake earthquake)
        {
            return new EarthquakeView
            {
                Date = earthquake.Date,
                T0 = earthquake.T0,
                K = Math.Round(earthquake.K, 3),
                H = Math.Round(earthquake.H, 3),
                F = Math.Round(earthquake.Lat, 3),
                LatError = Math.Round(earthquake.LatError, 2),
                L = Math.Round(earthquake.Lng, 3),
                LngError = Math.Round(earthquake.LngError, 2),
                Dt = Math.Round(earthquake.Dt, 3),
                DtRms = Math.Round(earthquake.DtRms, 3),
                VpVs = Math.Round(earthquake.VpVs, 3)
            };
        }
    }
}
