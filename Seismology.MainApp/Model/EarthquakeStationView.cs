﻿using Seismology.Core;
using System;
using System.ComponentModel;

namespace Seismology.MainApp.Model
{
    public class EarthquakeStationView
    {
        [DisplayName("Станция")]
        public string Name { get; set; }
        [DisplayName("Время T0")]
        public DateTime T0 { get; set; }
        [DisplayName("K (Класс)")]
        public double K { get; set; }
        [DisplayName("Время Tp")]
        public DateTime Tp { get; set; }
        [DisplayName("Tp-Ts")]
        public double Sp { get; set; }
        [DisplayName("Радиус (Эпиц. дисс.)")]
        public double Radius { get; set; }
        [DisplayName("Dt (Невязка)")]
        public double Dt { get; set; }
        [DisplayName("Vp/Vs")]
        public double VpVs { get; set; }

        public static explicit operator EarthquakeStationView(EarthquakeStation station)
        {
            return new EarthquakeStationView
            {
                Name = station.Station.RusName,
                Tp = station.Tp,
                Sp = Math.Round((station.Ts - station.Tp).TotalSeconds, 3),
                T0 = station.T0,
                Radius = Math.Round(station.EpicDistance, 3),
                Dt = Math.Round(station.Dt, 3),
                K = Math.Round(station.K, 3),
                VpVs = Math.Round(station.VpVs, 3)
            };
        }
    }
}
