﻿namespace Seismology.MainApp
{
    partial class CalculateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageWsgData = new System.Windows.Forms.TabPage();
            this.dataGridViewWsgData = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evAzDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phazeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tResDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.azimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.azResDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slowDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sResDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.defDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sNRDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qualDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.magnitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.arrIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wsgPhaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPageDbData = new System.Windows.Forms.TabPage();
            this.dataGridViewDbData = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.epicDistanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodZDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amplitudeZDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amplitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.earthquakeStationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPageResult = new System.Windows.Forms.TabPage();
            this.tabControlResult = new System.Windows.Forms.TabControl();
            this.tabPageTable = new System.Windows.Forms.TabPage();
            this.dataGridViewEarthquake = new System.Windows.Forms.DataGridView();
            this.earthquakeViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewEditStation = new System.Windows.Forms.DataGridView();
            this.Station = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.K = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EpicDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VpVs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeriodZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmplitudeZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Period = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amplitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Es = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageBulletin = new System.Windows.Forms.TabPage();
            this.textBoxEarthquake = new System.Windows.Forms.TextBox();
            this.tabPageMapAndSelection = new System.Windows.Forms.TabPage();
            this.tabControlStations = new System.Windows.Forms.TabControl();
            this.tabPageStationsMain = new System.Windows.Forms.TabPage();
            this.listViewStationsMain = new System.Windows.Forms.ListView();
            this.tabPageStationsVadati = new System.Windows.Forms.TabPage();
            this.listViewStationsVadati = new System.Windows.Forms.ListView();
            this.tabControlMap = new System.Windows.Forms.TabControl();
            this.tabPageOnline = new System.Windows.Forms.TabPage();
            this.gMapOnlineControl = new GMap.NET.WindowsForms.GMapControl();
            this.tabPageStatic = new System.Windows.Forms.TabPage();
            this.pictureBoxStaticMap = new System.Windows.Forms.PictureBox();
            this.tabPageWadatiChart = new System.Windows.Forms.TabPage();
            this.chartWadati = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonAction = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.openFileDialogWsgFile = new System.Windows.Forms.OpenFileDialog();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openWsgToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.staticMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulletinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.methodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intersectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vadatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleHybridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleTerrainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yandexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openStreetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openStreet4UToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hodographToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialogImage = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialogDocument = new System.Windows.Forms.SaveFileDialog();
            this.labelMethodName = new System.Windows.Forms.Label();
            this.earthquakeStationViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t0DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LatError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LngError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DtRms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vpVsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControlMain.SuspendLayout();
            this.tabPageWsgData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWsgData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wsgPhaseBindingSource)).BeginInit();
            this.tabPageDbData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDbData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationBindingSource)).BeginInit();
            this.tabPageResult.SuspendLayout();
            this.tabControlResult.SuspendLayout();
            this.tabPageTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEarthquake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditStation)).BeginInit();
            this.tabPageBulletin.SuspendLayout();
            this.tabPageMapAndSelection.SuspendLayout();
            this.tabControlStations.SuspendLayout();
            this.tabPageStationsMain.SuspendLayout();
            this.tabPageStationsVadati.SuspendLayout();
            this.tabControlMap.SuspendLayout();
            this.tabPageOnline.SuspendLayout();
            this.tabPageStatic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStaticMap)).BeginInit();
            this.tabPageWadatiChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartWadati)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationViewBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageWsgData);
            this.tabControlMain.Controls.Add(this.tabPageDbData);
            this.tabControlMain.Controls.Add(this.tabPageResult);
            this.tabControlMain.Controls.Add(this.tabPageMapAndSelection);
            this.tabControlMain.Location = new System.Drawing.Point(12, 32);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1088, 469);
            this.tabControlMain.TabIndex = 0;
            // 
            // tabPageWsgData
            // 
            this.tabPageWsgData.Controls.Add(this.dataGridViewWsgData);
            this.tabPageWsgData.Location = new System.Drawing.Point(4, 29);
            this.tabPageWsgData.Name = "tabPageWsgData";
            this.tabPageWsgData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWsgData.Size = new System.Drawing.Size(1080, 436);
            this.tabPageWsgData.TabIndex = 0;
            this.tabPageWsgData.Text = "Данные WSG";
            this.tabPageWsgData.UseVisualStyleBackColor = true;
            // 
            // dataGridViewWsgData
            // 
            this.dataGridViewWsgData.AllowUserToAddRows = false;
            this.dataGridViewWsgData.AllowUserToDeleteRows = false;
            this.dataGridViewWsgData.AutoGenerateColumns = false;
            this.dataGridViewWsgData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewWsgData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewWsgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWsgData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn1,
            this.distDataGridViewTextBoxColumn,
            this.evAzDataGridViewTextBoxColumn,
            this.phazeDataGridViewTextBoxColumn,
            this.timeDataGridViewTextBoxColumn,
            this.tResDataGridViewTextBoxColumn,
            this.azimDataGridViewTextBoxColumn,
            this.azResDataGridViewTextBoxColumn,
            this.slowDataGridViewTextBoxColumn,
            this.sResDataGridViewTextBoxColumn,
            this.defDataGridViewTextBoxColumn,
            this.sNRDataGridViewTextBoxColumn,
            this.ampDataGridViewTextBoxColumn,
            this.perDataGridViewTextBoxColumn,
            this.qualDataGridViewTextBoxColumn,
            this.magnitudeDataGridViewTextBoxColumn,
            this.arrIDDataGridViewTextBoxColumn});
            this.dataGridViewWsgData.DataSource = this.wsgPhaseBindingSource;
            this.dataGridViewWsgData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewWsgData.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewWsgData.Name = "dataGridViewWsgData";
            this.dataGridViewWsgData.ReadOnly = true;
            this.dataGridViewWsgData.RowHeadersVisible = false;
            this.dataGridViewWsgData.Size = new System.Drawing.Size(1074, 430);
            this.dataGridViewWsgData.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn1.Width = 76;
            // 
            // distDataGridViewTextBoxColumn
            // 
            this.distDataGridViewTextBoxColumn.DataPropertyName = "Dist";
            this.distDataGridViewTextBoxColumn.HeaderText = "Dist";
            this.distDataGridViewTextBoxColumn.Name = "distDataGridViewTextBoxColumn";
            this.distDataGridViewTextBoxColumn.ReadOnly = true;
            this.distDataGridViewTextBoxColumn.Width = 62;
            // 
            // evAzDataGridViewTextBoxColumn
            // 
            this.evAzDataGridViewTextBoxColumn.DataPropertyName = "EvAz";
            this.evAzDataGridViewTextBoxColumn.HeaderText = "EvAz";
            this.evAzDataGridViewTextBoxColumn.Name = "evAzDataGridViewTextBoxColumn";
            this.evAzDataGridViewTextBoxColumn.ReadOnly = true;
            this.evAzDataGridViewTextBoxColumn.Width = 71;
            // 
            // phazeDataGridViewTextBoxColumn
            // 
            this.phazeDataGridViewTextBoxColumn.DataPropertyName = "Phaze";
            this.phazeDataGridViewTextBoxColumn.HeaderText = "Phaze";
            this.phazeDataGridViewTextBoxColumn.Name = "phazeDataGridViewTextBoxColumn";
            this.phazeDataGridViewTextBoxColumn.ReadOnly = true;
            this.phazeDataGridViewTextBoxColumn.Width = 79;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "Time";
            dataGridViewCellStyle6.Format = "dd.MM.yyyy  HH:mm:ss.fff";
            this.timeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.timeDataGridViewTextBoxColumn.HeaderText = "Time";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeDataGridViewTextBoxColumn.Width = 68;
            // 
            // tResDataGridViewTextBoxColumn
            // 
            this.tResDataGridViewTextBoxColumn.DataPropertyName = "TRes";
            this.tResDataGridViewTextBoxColumn.HeaderText = "TRes";
            this.tResDataGridViewTextBoxColumn.Name = "tResDataGridViewTextBoxColumn";
            this.tResDataGridViewTextBoxColumn.ReadOnly = true;
            this.tResDataGridViewTextBoxColumn.Width = 72;
            // 
            // azimDataGridViewTextBoxColumn
            // 
            this.azimDataGridViewTextBoxColumn.DataPropertyName = "Azim";
            this.azimDataGridViewTextBoxColumn.HeaderText = "Azim";
            this.azimDataGridViewTextBoxColumn.Name = "azimDataGridViewTextBoxColumn";
            this.azimDataGridViewTextBoxColumn.ReadOnly = true;
            this.azimDataGridViewTextBoxColumn.Width = 69;
            // 
            // azResDataGridViewTextBoxColumn
            // 
            this.azResDataGridViewTextBoxColumn.DataPropertyName = "AzRes";
            this.azResDataGridViewTextBoxColumn.HeaderText = "AzRes";
            this.azResDataGridViewTextBoxColumn.Name = "azResDataGridViewTextBoxColumn";
            this.azResDataGridViewTextBoxColumn.ReadOnly = true;
            this.azResDataGridViewTextBoxColumn.Width = 82;
            // 
            // slowDataGridViewTextBoxColumn
            // 
            this.slowDataGridViewTextBoxColumn.DataPropertyName = "Slow";
            this.slowDataGridViewTextBoxColumn.HeaderText = "Slow";
            this.slowDataGridViewTextBoxColumn.Name = "slowDataGridViewTextBoxColumn";
            this.slowDataGridViewTextBoxColumn.ReadOnly = true;
            this.slowDataGridViewTextBoxColumn.Width = 68;
            // 
            // sResDataGridViewTextBoxColumn
            // 
            this.sResDataGridViewTextBoxColumn.DataPropertyName = "SRes";
            this.sResDataGridViewTextBoxColumn.HeaderText = "SRes";
            this.sResDataGridViewTextBoxColumn.Name = "sResDataGridViewTextBoxColumn";
            this.sResDataGridViewTextBoxColumn.ReadOnly = true;
            this.sResDataGridViewTextBoxColumn.Width = 74;
            // 
            // defDataGridViewTextBoxColumn
            // 
            this.defDataGridViewTextBoxColumn.DataPropertyName = "Def";
            this.defDataGridViewTextBoxColumn.HeaderText = "Def";
            this.defDataGridViewTextBoxColumn.Name = "defDataGridViewTextBoxColumn";
            this.defDataGridViewTextBoxColumn.ReadOnly = true;
            this.defDataGridViewTextBoxColumn.Width = 60;
            // 
            // sNRDataGridViewTextBoxColumn
            // 
            this.sNRDataGridViewTextBoxColumn.DataPropertyName = "SNR";
            this.sNRDataGridViewTextBoxColumn.HeaderText = "SNR";
            this.sNRDataGridViewTextBoxColumn.Name = "sNRDataGridViewTextBoxColumn";
            this.sNRDataGridViewTextBoxColumn.ReadOnly = true;
            this.sNRDataGridViewTextBoxColumn.Width = 68;
            // 
            // ampDataGridViewTextBoxColumn
            // 
            this.ampDataGridViewTextBoxColumn.DataPropertyName = "Amp";
            this.ampDataGridViewTextBoxColumn.HeaderText = "Amp";
            this.ampDataGridViewTextBoxColumn.Name = "ampDataGridViewTextBoxColumn";
            this.ampDataGridViewTextBoxColumn.ReadOnly = true;
            this.ampDataGridViewTextBoxColumn.Width = 67;
            // 
            // perDataGridViewTextBoxColumn
            // 
            this.perDataGridViewTextBoxColumn.DataPropertyName = "Per";
            this.perDataGridViewTextBoxColumn.HeaderText = "Per";
            this.perDataGridViewTextBoxColumn.Name = "perDataGridViewTextBoxColumn";
            this.perDataGridViewTextBoxColumn.ReadOnly = true;
            this.perDataGridViewTextBoxColumn.Width = 58;
            // 
            // qualDataGridViewTextBoxColumn
            // 
            this.qualDataGridViewTextBoxColumn.DataPropertyName = "Qual";
            this.qualDataGridViewTextBoxColumn.HeaderText = "Qual";
            this.qualDataGridViewTextBoxColumn.Name = "qualDataGridViewTextBoxColumn";
            this.qualDataGridViewTextBoxColumn.ReadOnly = true;
            this.qualDataGridViewTextBoxColumn.Width = 67;
            // 
            // magnitudeDataGridViewTextBoxColumn
            // 
            this.magnitudeDataGridViewTextBoxColumn.DataPropertyName = "Magnitude";
            this.magnitudeDataGridViewTextBoxColumn.HeaderText = "Magnitude";
            this.magnitudeDataGridViewTextBoxColumn.Name = "magnitudeDataGridViewTextBoxColumn";
            this.magnitudeDataGridViewTextBoxColumn.ReadOnly = true;
            this.magnitudeDataGridViewTextBoxColumn.Width = 109;
            // 
            // arrIDDataGridViewTextBoxColumn
            // 
            this.arrIDDataGridViewTextBoxColumn.DataPropertyName = "ArrID";
            this.arrIDDataGridViewTextBoxColumn.HeaderText = "ArrID";
            this.arrIDDataGridViewTextBoxColumn.Name = "arrIDDataGridViewTextBoxColumn";
            this.arrIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.arrIDDataGridViewTextBoxColumn.Width = 72;
            // 
            // wsgPhaseBindingSource
            // 
            this.wsgPhaseBindingSource.DataSource = typeof(Seismology.Core.WsgPhase);
            // 
            // tabPageDbData
            // 
            this.tabPageDbData.Controls.Add(this.dataGridViewDbData);
            this.tabPageDbData.Location = new System.Drawing.Point(4, 29);
            this.tabPageDbData.Name = "tabPageDbData";
            this.tabPageDbData.Size = new System.Drawing.Size(1080, 436);
            this.tabPageDbData.TabIndex = 3;
            this.tabPageDbData.Text = "Данные из БД";
            this.tabPageDbData.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDbData
            // 
            this.dataGridViewDbData.AllowUserToAddRows = false;
            this.dataGridViewDbData.AllowUserToDeleteRows = false;
            this.dataGridViewDbData.AutoGenerateColumns = false;
            this.dataGridViewDbData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDbData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewDbData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDbData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.epDataGridViewTextBoxColumn,
            this.tpDataGridViewTextBoxColumn,
            this.esDataGridViewTextBoxColumn,
            this.tsDataGridViewTextBoxColumn,
            this.spDataGridViewTextBoxColumn,
            this.epicDistanceDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.periodZDataGridViewTextBoxColumn,
            this.amplitudeZDataGridViewTextBoxColumn,
            this.periodDataGridViewTextBoxColumn,
            this.amplitudeDataGridViewTextBoxColumn});
            this.dataGridViewDbData.DataSource = this.earthquakeStationBindingSource;
            this.dataGridViewDbData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDbData.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewDbData.Name = "dataGridViewDbData";
            this.dataGridViewDbData.ReadOnly = true;
            this.dataGridViewDbData.RowHeadersVisible = false;
            this.dataGridViewDbData.Size = new System.Drawing.Size(1080, 436);
            this.dataGridViewDbData.TabIndex = 1;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 76;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "T0";
            dataGridViewCellStyle7.Format = "HH:mm:ss.fff";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn1.HeaderText = "T0";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 52;
            // 
            // epDataGridViewTextBoxColumn
            // 
            this.epDataGridViewTextBoxColumn.DataPropertyName = "Ep";
            this.epDataGridViewTextBoxColumn.HeaderText = "Ep";
            this.epDataGridViewTextBoxColumn.Name = "epDataGridViewTextBoxColumn";
            this.epDataGridViewTextBoxColumn.ReadOnly = true;
            this.epDataGridViewTextBoxColumn.Width = 54;
            // 
            // tpDataGridViewTextBoxColumn
            // 
            this.tpDataGridViewTextBoxColumn.DataPropertyName = "Tp";
            dataGridViewCellStyle8.Format = "HH:mm:ss.fff";
            this.tpDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.tpDataGridViewTextBoxColumn.HeaderText = "Tp";
            this.tpDataGridViewTextBoxColumn.Name = "tpDataGridViewTextBoxColumn";
            this.tpDataGridViewTextBoxColumn.ReadOnly = true;
            this.tpDataGridViewTextBoxColumn.Width = 52;
            // 
            // esDataGridViewTextBoxColumn
            // 
            this.esDataGridViewTextBoxColumn.DataPropertyName = "Es";
            this.esDataGridViewTextBoxColumn.HeaderText = "Es";
            this.esDataGridViewTextBoxColumn.Name = "esDataGridViewTextBoxColumn";
            this.esDataGridViewTextBoxColumn.ReadOnly = true;
            this.esDataGridViewTextBoxColumn.Width = 53;
            // 
            // tsDataGridViewTextBoxColumn
            // 
            this.tsDataGridViewTextBoxColumn.DataPropertyName = "Ts";
            dataGridViewCellStyle9.Format = "HH:mm:ss.fff";
            this.tsDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.tsDataGridViewTextBoxColumn.HeaderText = "Ts";
            this.tsDataGridViewTextBoxColumn.Name = "tsDataGridViewTextBoxColumn";
            this.tsDataGridViewTextBoxColumn.ReadOnly = true;
            this.tsDataGridViewTextBoxColumn.Width = 51;
            // 
            // spDataGridViewTextBoxColumn
            // 
            this.spDataGridViewTextBoxColumn.DataPropertyName = "Sp";
            this.spDataGridViewTextBoxColumn.HeaderText = "Sp";
            this.spDataGridViewTextBoxColumn.Name = "spDataGridViewTextBoxColumn";
            this.spDataGridViewTextBoxColumn.ReadOnly = true;
            this.spDataGridViewTextBoxColumn.Width = 54;
            // 
            // epicDistanceDataGridViewTextBoxColumn
            // 
            this.epicDistanceDataGridViewTextBoxColumn.DataPropertyName = "EpicDistance";
            this.epicDistanceDataGridViewTextBoxColumn.HeaderText = "EpicDistance";
            this.epicDistanceDataGridViewTextBoxColumn.Name = "epicDistanceDataGridViewTextBoxColumn";
            this.epicDistanceDataGridViewTextBoxColumn.ReadOnly = true;
            this.epicDistanceDataGridViewTextBoxColumn.Width = 128;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Dt";
            this.dataGridViewTextBoxColumn2.HeaderText = "Dt";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 51;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "K";
            this.dataGridViewTextBoxColumn3.HeaderText = "K";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 44;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "VpVs";
            this.dataGridViewTextBoxColumn4.HeaderText = "VpVs";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 73;
            // 
            // periodZDataGridViewTextBoxColumn
            // 
            this.periodZDataGridViewTextBoxColumn.DataPropertyName = "PeriodZ";
            this.periodZDataGridViewTextBoxColumn.HeaderText = "PeriodZ";
            this.periodZDataGridViewTextBoxColumn.Name = "periodZDataGridViewTextBoxColumn";
            this.periodZDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodZDataGridViewTextBoxColumn.Width = 89;
            // 
            // amplitudeZDataGridViewTextBoxColumn
            // 
            this.amplitudeZDataGridViewTextBoxColumn.DataPropertyName = "AmplitudeZ";
            this.amplitudeZDataGridViewTextBoxColumn.HeaderText = "AmplitudeZ";
            this.amplitudeZDataGridViewTextBoxColumn.Name = "amplitudeZDataGridViewTextBoxColumn";
            this.amplitudeZDataGridViewTextBoxColumn.ReadOnly = true;
            this.amplitudeZDataGridViewTextBoxColumn.Width = 115;
            // 
            // periodDataGridViewTextBoxColumn
            // 
            this.periodDataGridViewTextBoxColumn.DataPropertyName = "Period";
            this.periodDataGridViewTextBoxColumn.HeaderText = "Period";
            this.periodDataGridViewTextBoxColumn.Name = "periodDataGridViewTextBoxColumn";
            this.periodDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodDataGridViewTextBoxColumn.Width = 79;
            // 
            // amplitudeDataGridViewTextBoxColumn
            // 
            this.amplitudeDataGridViewTextBoxColumn.DataPropertyName = "Amplitude";
            this.amplitudeDataGridViewTextBoxColumn.HeaderText = "Amplitude";
            this.amplitudeDataGridViewTextBoxColumn.Name = "amplitudeDataGridViewTextBoxColumn";
            this.amplitudeDataGridViewTextBoxColumn.ReadOnly = true;
            this.amplitudeDataGridViewTextBoxColumn.Width = 105;
            // 
            // earthquakeStationBindingSource
            // 
            this.earthquakeStationBindingSource.DataSource = typeof(Seismology.Core.EarthquakeStation);
            // 
            // tabPageResult
            // 
            this.tabPageResult.Controls.Add(this.tabControlResult);
            this.tabPageResult.Location = new System.Drawing.Point(4, 29);
            this.tabPageResult.Name = "tabPageResult";
            this.tabPageResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResult.Size = new System.Drawing.Size(1080, 436);
            this.tabPageResult.TabIndex = 1;
            this.tabPageResult.Text = "Результат";
            this.tabPageResult.UseVisualStyleBackColor = true;
            // 
            // tabControlResult
            // 
            this.tabControlResult.Controls.Add(this.tabPageTable);
            this.tabControlResult.Controls.Add(this.tabPageBulletin);
            this.tabControlResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlResult.Location = new System.Drawing.Point(3, 3);
            this.tabControlResult.Name = "tabControlResult";
            this.tabControlResult.SelectedIndex = 0;
            this.tabControlResult.Size = new System.Drawing.Size(1074, 430);
            this.tabControlResult.TabIndex = 1;
            // 
            // tabPageTable
            // 
            this.tabPageTable.Controls.Add(this.dataGridViewEarthquake);
            this.tabPageTable.Controls.Add(this.dataGridViewEditStation);
            this.tabPageTable.Location = new System.Drawing.Point(4, 29);
            this.tabPageTable.Name = "tabPageTable";
            this.tabPageTable.Size = new System.Drawing.Size(1066, 397);
            this.tabPageTable.TabIndex = 2;
            this.tabPageTable.Text = "Таблица";
            this.tabPageTable.UseVisualStyleBackColor = true;
            // 
            // dataGridViewEarthquake
            // 
            this.dataGridViewEarthquake.AllowUserToAddRows = false;
            this.dataGridViewEarthquake.AllowUserToDeleteRows = false;
            this.dataGridViewEarthquake.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEarthquake.AutoGenerateColumns = false;
            this.dataGridViewEarthquake.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEarthquake.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEarthquake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEarthquake.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateDataGridViewTextBoxColumn,
            this.t0DataGridViewTextBoxColumn1,
            this.kDataGridViewTextBoxColumn1,
            this.hDataGridViewTextBoxColumn,
            this.fDataGridViewTextBoxColumn,
            this.LatError,
            this.lDataGridViewTextBoxColumn,
            this.LngError,
            this.dtDataGridViewTextBoxColumn1,
            this.DtRms,
            this.vpVsDataGridViewTextBoxColumn1});
            this.dataGridViewEarthquake.DataSource = this.earthquakeViewBindingSource;
            this.dataGridViewEarthquake.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewEarthquake.Name = "dataGridViewEarthquake";
            this.dataGridViewEarthquake.ReadOnly = true;
            this.dataGridViewEarthquake.RowHeadersVisible = false;
            this.dataGridViewEarthquake.Size = new System.Drawing.Size(1066, 94);
            this.dataGridViewEarthquake.TabIndex = 1;
            // 
            // earthquakeViewBindingSource
            // 
            this.earthquakeViewBindingSource.DataSource = typeof(Seismology.MainApp.Model.EarthquakeView);
            // 
            // dataGridViewEditStation
            // 
            this.dataGridViewEditStation.AllowUserToAddRows = false;
            this.dataGridViewEditStation.AllowUserToDeleteRows = false;
            this.dataGridViewEditStation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEditStation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEditStation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewEditStation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEditStation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Station,
            this.T0,
            this.K,
            this.TP,
            this.SP,
            this.EpicDistance,
            this.Dt,
            this.VpVs,
            this.PeriodZ,
            this.AmplitudeZ,
            this.Period,
            this.Amplitude,
            this.Ep,
            this.Es});
            this.dataGridViewEditStation.Location = new System.Drawing.Point(0, 100);
            this.dataGridViewEditStation.Name = "dataGridViewEditStation";
            this.dataGridViewEditStation.RowHeadersVisible = false;
            this.dataGridViewEditStation.Size = new System.Drawing.Size(1066, 294);
            this.dataGridViewEditStation.TabIndex = 0;
            // 
            // Station
            // 
            this.Station.HeaderText = "Станция";
            this.Station.Name = "Station";
            this.Station.ReadOnly = true;
            // 
            // T0
            // 
            this.T0.HeaderText = "Время T0";
            this.T0.Name = "T0";
            this.T0.ReadOnly = true;
            // 
            // K
            // 
            this.K.HeaderText = "Класс";
            this.K.Name = "K";
            this.K.ReadOnly = true;
            // 
            // TP
            // 
            this.TP.HeaderText = "Время TP";
            this.TP.Name = "TP";
            // 
            // SP
            // 
            this.SP.HeaderText = "SP";
            this.SP.Name = "SP";
            // 
            // EpicDistance
            // 
            this.EpicDistance.HeaderText = "Радиус (Эпиц. дист.)";
            this.EpicDistance.Name = "EpicDistance";
            this.EpicDistance.ReadOnly = true;
            // 
            // Dt
            // 
            this.Dt.HeaderText = "Dt (Невязка)";
            this.Dt.Name = "Dt";
            this.Dt.ReadOnly = true;
            // 
            // VpVs
            // 
            this.VpVs.HeaderText = "Vp/Vs";
            this.VpVs.Name = "VpVs";
            this.VpVs.ReadOnly = true;
            // 
            // PeriodZ
            // 
            this.PeriodZ.HeaderText = "Период Z";
            this.PeriodZ.Name = "PeriodZ";
            this.PeriodZ.ReadOnly = true;
            this.PeriodZ.Visible = false;
            // 
            // AmplitudeZ
            // 
            this.AmplitudeZ.HeaderText = "Амплитуда Z";
            this.AmplitudeZ.Name = "AmplitudeZ";
            this.AmplitudeZ.ReadOnly = true;
            this.AmplitudeZ.Visible = false;
            // 
            // Period
            // 
            this.Period.HeaderText = "Период";
            this.Period.Name = "Period";
            this.Period.ReadOnly = true;
            this.Period.Visible = false;
            // 
            // Amplitude
            // 
            this.Amplitude.HeaderText = "Амплитуда";
            this.Amplitude.Name = "Amplitude";
            this.Amplitude.ReadOnly = true;
            this.Amplitude.Visible = false;
            // 
            // Ep
            // 
            this.Ep.HeaderText = "Ep";
            this.Ep.Name = "Ep";
            this.Ep.ReadOnly = true;
            this.Ep.Visible = false;
            // 
            // Es
            // 
            this.Es.HeaderText = "Es";
            this.Es.Name = "Es";
            this.Es.ReadOnly = true;
            this.Es.Visible = false;
            // 
            // tabPageBulletin
            // 
            this.tabPageBulletin.Controls.Add(this.textBoxEarthquake);
            this.tabPageBulletin.Location = new System.Drawing.Point(4, 29);
            this.tabPageBulletin.Name = "tabPageBulletin";
            this.tabPageBulletin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBulletin.Size = new System.Drawing.Size(1066, 397);
            this.tabPageBulletin.TabIndex = 1;
            this.tabPageBulletin.Text = "Бюллетень";
            this.tabPageBulletin.UseVisualStyleBackColor = true;
            // 
            // textBoxEarthquake
            // 
            this.textBoxEarthquake.AcceptsReturn = true;
            this.textBoxEarthquake.AcceptsTab = true;
            this.textBoxEarthquake.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxEarthquake.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxEarthquake.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEarthquake.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEarthquake.Location = new System.Drawing.Point(3, 3);
            this.textBoxEarthquake.Multiline = true;
            this.textBoxEarthquake.Name = "textBoxEarthquake";
            this.textBoxEarthquake.ReadOnly = true;
            this.textBoxEarthquake.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxEarthquake.Size = new System.Drawing.Size(1060, 391);
            this.textBoxEarthquake.TabIndex = 1;
            this.textBoxEarthquake.WordWrap = false;
            // 
            // tabPageMapAndSelection
            // 
            this.tabPageMapAndSelection.Controls.Add(this.tabControlStations);
            this.tabPageMapAndSelection.Controls.Add(this.tabControlMap);
            this.tabPageMapAndSelection.Location = new System.Drawing.Point(4, 29);
            this.tabPageMapAndSelection.Name = "tabPageMapAndSelection";
            this.tabPageMapAndSelection.Size = new System.Drawing.Size(1080, 436);
            this.tabPageMapAndSelection.TabIndex = 2;
            this.tabPageMapAndSelection.Text = "Карта и выборка";
            this.tabPageMapAndSelection.UseVisualStyleBackColor = true;
            // 
            // tabControlStations
            // 
            this.tabControlStations.Controls.Add(this.tabPageStationsMain);
            this.tabControlStations.Controls.Add(this.tabPageStationsVadati);
            this.tabControlStations.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabControlStations.Location = new System.Drawing.Point(881, 0);
            this.tabControlStations.Name = "tabControlStations";
            this.tabControlStations.SelectedIndex = 0;
            this.tabControlStations.Size = new System.Drawing.Size(199, 436);
            this.tabControlStations.TabIndex = 3;
            // 
            // tabPageStationsMain
            // 
            this.tabPageStationsMain.Controls.Add(this.listViewStationsMain);
            this.tabPageStationsMain.Location = new System.Drawing.Point(4, 29);
            this.tabPageStationsMain.Name = "tabPageStationsMain";
            this.tabPageStationsMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStationsMain.Size = new System.Drawing.Size(191, 403);
            this.tabPageStationsMain.TabIndex = 0;
            this.tabPageStationsMain.Text = "Основные";
            this.tabPageStationsMain.UseVisualStyleBackColor = true;
            // 
            // listViewStationsMain
            // 
            this.listViewStationsMain.CheckBoxes = true;
            this.listViewStationsMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewStationsMain.HideSelection = false;
            this.listViewStationsMain.LabelWrap = false;
            this.listViewStationsMain.Location = new System.Drawing.Point(3, 3);
            this.listViewStationsMain.Name = "listViewStationsMain";
            this.listViewStationsMain.Size = new System.Drawing.Size(185, 397);
            this.listViewStationsMain.TabIndex = 3;
            this.listViewStationsMain.UseCompatibleStateImageBehavior = false;
            // 
            // tabPageStationsVadati
            // 
            this.tabPageStationsVadati.Controls.Add(this.listViewStationsVadati);
            this.tabPageStationsVadati.Location = new System.Drawing.Point(4, 29);
            this.tabPageStationsVadati.Name = "tabPageStationsVadati";
            this.tabPageStationsVadati.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStationsVadati.Size = new System.Drawing.Size(191, 403);
            this.tabPageStationsVadati.TabIndex = 1;
            this.tabPageStationsVadati.Text = "Вадати";
            this.tabPageStationsVadati.UseVisualStyleBackColor = true;
            // 
            // listViewStationsVadati
            // 
            this.listViewStationsVadati.CheckBoxes = true;
            this.listViewStationsVadati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewStationsVadati.HideSelection = false;
            this.listViewStationsVadati.LabelWrap = false;
            this.listViewStationsVadati.Location = new System.Drawing.Point(3, 3);
            this.listViewStationsVadati.Name = "listViewStationsVadati";
            this.listViewStationsVadati.Size = new System.Drawing.Size(185, 397);
            this.listViewStationsVadati.TabIndex = 4;
            this.listViewStationsVadati.UseCompatibleStateImageBehavior = false;
            // 
            // tabControlMap
            // 
            this.tabControlMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMap.Controls.Add(this.tabPageOnline);
            this.tabControlMap.Controls.Add(this.tabPageStatic);
            this.tabControlMap.Controls.Add(this.tabPageWadatiChart);
            this.tabControlMap.Location = new System.Drawing.Point(0, 0);
            this.tabControlMap.Name = "tabControlMap";
            this.tabControlMap.SelectedIndex = 0;
            this.tabControlMap.Size = new System.Drawing.Size(882, 436);
            this.tabControlMap.TabIndex = 2;
            // 
            // tabPageOnline
            // 
            this.tabPageOnline.Controls.Add(this.gMapOnlineControl);
            this.tabPageOnline.Location = new System.Drawing.Point(4, 29);
            this.tabPageOnline.Name = "tabPageOnline";
            this.tabPageOnline.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOnline.Size = new System.Drawing.Size(874, 403);
            this.tabPageOnline.TabIndex = 0;
            this.tabPageOnline.Text = "Онлайн";
            this.tabPageOnline.UseVisualStyleBackColor = true;
            // 
            // gMapOnlineControl
            // 
            this.gMapOnlineControl.Bearing = 0F;
            this.gMapOnlineControl.CanDragMap = true;
            this.gMapOnlineControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gMapOnlineControl.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMapOnlineControl.GrayScaleMode = false;
            this.gMapOnlineControl.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMapOnlineControl.LevelsKeepInMemory = 5;
            this.gMapOnlineControl.Location = new System.Drawing.Point(3, 3);
            this.gMapOnlineControl.MarkersEnabled = true;
            this.gMapOnlineControl.MaxZoom = 2;
            this.gMapOnlineControl.MinZoom = 2;
            this.gMapOnlineControl.MouseWheelZoomEnabled = true;
            this.gMapOnlineControl.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMapOnlineControl.Name = "gMapOnlineControl";
            this.gMapOnlineControl.NegativeMode = false;
            this.gMapOnlineControl.PolygonsEnabled = true;
            this.gMapOnlineControl.RetryLoadTile = 0;
            this.gMapOnlineControl.RoutesEnabled = true;
            this.gMapOnlineControl.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMapOnlineControl.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMapOnlineControl.ShowTileGridLines = false;
            this.gMapOnlineControl.Size = new System.Drawing.Size(868, 397);
            this.gMapOnlineControl.TabIndex = 0;
            this.gMapOnlineControl.Zoom = 0D;
            // 
            // tabPageStatic
            // 
            this.tabPageStatic.Controls.Add(this.pictureBoxStaticMap);
            this.tabPageStatic.Location = new System.Drawing.Point(4, 29);
            this.tabPageStatic.Name = "tabPageStatic";
            this.tabPageStatic.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStatic.Size = new System.Drawing.Size(874, 403);
            this.tabPageStatic.TabIndex = 1;
            this.tabPageStatic.Text = "Статичная";
            this.tabPageStatic.UseVisualStyleBackColor = true;
            // 
            // pictureBoxStaticMap
            // 
            this.pictureBoxStaticMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStaticMap.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxStaticMap.Name = "pictureBoxStaticMap";
            this.pictureBoxStaticMap.Size = new System.Drawing.Size(868, 397);
            this.pictureBoxStaticMap.TabIndex = 0;
            this.pictureBoxStaticMap.TabStop = false;
            // 
            // tabPageWadatiChart
            // 
            this.tabPageWadatiChart.BackColor = System.Drawing.Color.Transparent;
            this.tabPageWadatiChart.Controls.Add(this.chartWadati);
            this.tabPageWadatiChart.Location = new System.Drawing.Point(4, 29);
            this.tabPageWadatiChart.Name = "tabPageWadatiChart";
            this.tabPageWadatiChart.Size = new System.Drawing.Size(874, 403);
            this.tabPageWadatiChart.TabIndex = 2;
            this.tabPageWadatiChart.Text = "График Вадати";
            // 
            // chartWadati
            // 
            chartArea2.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.Name = "ChartArea1";
            this.chartWadati.ChartAreas.Add(chartArea2);
            this.chartWadati.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.chartWadati.Legends.Add(legend2);
            this.chartWadati.Location = new System.Drawing.Point(0, 0);
            this.chartWadati.Name = "chartWadati";
            series5.BorderWidth = 2;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            series5.IsVisibleInLegend = false;
            series5.LabelToolTip = "Уравнение";
            series5.Legend = "Legend1";
            series5.Name = "SeriesTp";
            series5.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series5.SmartLabelStyle.CalloutLineAnchorCapStyle = System.Windows.Forms.DataVisualization.Charting.LineAnchorCapStyle.None;
            series5.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series5.SmartLabelStyle.MinMovingDistance = 5D;
            series5.ToolTip = "Tp";
            series6.BorderWidth = 2;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            series6.IsVisibleInLegend = false;
            series6.LabelToolTip = "Уравнение";
            series6.Legend = "Legend1";
            series6.Name = "SeriesTs";
            series6.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series6.SmartLabelStyle.CalloutLineAnchorCapStyle = System.Windows.Forms.DataVisualization.Charting.LineAnchorCapStyle.None;
            series6.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series6.SmartLabelStyle.MinMovingDistance = 5D;
            series6.ToolTip = "Ts";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series7.Legend = "Legend1";
            series7.LegendText = "Tsp-Tp";
            series7.MarkerSize = 15;
            series7.Name = "SeriesTspTp";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series8.Legend = "Legend1";
            series8.LegendText = "Tsp-Ts";
            series8.MarkerSize = 15;
            series8.Name = "SeriesTspTs";
            this.chartWadati.Series.Add(series5);
            this.chartWadati.Series.Add(series6);
            this.chartWadati.Series.Add(series7);
            this.chartWadati.Series.Add(series8);
            this.chartWadati.Size = new System.Drawing.Size(874, 403);
            this.chartWadati.TabIndex = 0;
            this.chartWadati.Text = "chart1";
            // 
            // buttonAction
            // 
            this.buttonAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAction.Enabled = false;
            this.buttonAction.Location = new System.Drawing.Point(897, 507);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(97, 30);
            this.buttonAction.TabIndex = 1;
            this.buttonAction.Text = "Действие";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.ButtonAction_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(1000, 507);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 30);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // openFileDialogWsgFile
            // 
            this.openFileDialogWsgFile.FileName = "Wsg файл";
            this.openFileDialogWsgFile.Filter = "Текстовые документы (*.txt) | *.txt;";
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCalculate.Enabled = false;
            this.buttonCalculate.Location = new System.Drawing.Point(12, 507);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(120, 30);
            this.buttonCalculate.TabIndex = 4;
            this.buttonCalculate.Text = "Рассчитать";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.ButtonCalculate_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.methodToolStripMenuItem,
            this.mapToolStripMenuItem,
            this.hodographToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1112, 29);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openWsgToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(59, 25);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // openWsgToolStripMenuItem
            // 
            this.openWsgToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_open;
            this.openWsgToolStripMenuItem.Name = "openWsgToolStripMenuItem";
            this.openWsgToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openWsgToolStripMenuItem.Size = new System.Drawing.Size(200, 26);
            this.openWsgToolStripMenuItem.Text = "Открыть";
            this.openWsgToolStripMenuItem.ToolTipText = "Открыть WSG файл";
            this.openWsgToolStripMenuItem.Click += new System.EventHandler(this.OpenWsgToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageToolStripMenuItem,
            this.documentToolStripMenuItem});
            this.saveAsToolStripMenuItem.Image = global::Seismology.MainApp.Properties.Resources.icons_save;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(200, 26);
            this.saveAsToolStripMenuItem.Text = "Сохранить как";
            this.saveAsToolStripMenuItem.ToolTipText = "Сохранить как изображение или документ";
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlineMapToolStripMenuItem,
            this.staticMapToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.imageToolStripMenuItem.Text = "Изображение";
            // 
            // onlineMapToolStripMenuItem
            // 
            this.onlineMapToolStripMenuItem.Name = "onlineMapToolStripMenuItem";
            this.onlineMapToolStripMenuItem.Size = new System.Drawing.Size(204, 26);
            this.onlineMapToolStripMenuItem.Text = "Онлайн карты";
            this.onlineMapToolStripMenuItem.Click += new System.EventHandler(this.OnlineMapToolStripMenuItem_Click);
            // 
            // staticMapToolStripMenuItem
            // 
            this.staticMapToolStripMenuItem.Name = "staticMapToolStripMenuItem";
            this.staticMapToolStripMenuItem.Size = new System.Drawing.Size(204, 26);
            this.staticMapToolStripMenuItem.Text = "Статичной карты";
            this.staticMapToolStripMenuItem.Click += new System.EventHandler(this.StaticMapToolStripMenuItem_Click);
            // 
            // documentToolStripMenuItem
            // 
            this.documentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogToolStripMenuItem,
            this.bulletinToolStripMenuItem});
            this.documentToolStripMenuItem.Name = "documentToolStripMenuItem";
            this.documentToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.documentToolStripMenuItem.Text = "Документ";
            // 
            // catalogToolStripMenuItem
            // 
            this.catalogToolStripMenuItem.Name = "catalogToolStripMenuItem";
            this.catalogToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.catalogToolStripMenuItem.Text = "Каталог";
            this.catalogToolStripMenuItem.Click += new System.EventHandler(this.CatalogToolStripMenuItem_Click);
            // 
            // bulletinToolStripMenuItem
            // 
            this.bulletinToolStripMenuItem.Name = "bulletinToolStripMenuItem";
            this.bulletinToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.bulletinToolStripMenuItem.Text = "Бюллетень";
            this.bulletinToolStripMenuItem.Click += new System.EventHandler(this.BulletinToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(197, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(200, 26);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.ToolTipText = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // methodToolStripMenuItem
            // 
            this.methodToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.automaticToolStripMenuItem,
            this.multiToolStripMenuItem,
            this.binaryToolStripMenuItem,
            this.gridToolStripMenuItem,
            this.intersectionToolStripMenuItem,
            this.vadatiToolStripMenuItem});
            this.methodToolStripMenuItem.Name = "methodToolStripMenuItem";
            this.methodToolStripMenuItem.Size = new System.Drawing.Size(69, 25);
            this.methodToolStripMenuItem.Text = "Метод";
            // 
            // automaticToolStripMenuItem
            // 
            this.automaticToolStripMenuItem.Name = "automaticToolStripMenuItem";
            this.automaticToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.automaticToolStripMenuItem.Text = "Автоматически";
            this.automaticToolStripMenuItem.Click += new System.EventHandler(this.ChangeMethodToolStripMenuItem_Click);
            // 
            // multiToolStripMenuItem
            // 
            this.multiToolStripMenuItem.Name = "multiToolStripMenuItem";
            this.multiToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.multiToolStripMenuItem.Text = "Мульти";
            this.multiToolStripMenuItem.ToolTipText = "Мульти метод";
            this.multiToolStripMenuItem.Click += new System.EventHandler(this.ChangeMethodToolStripMenuItem_Click);
            // 
            // binaryToolStripMenuItem
            // 
            this.binaryToolStripMenuItem.Name = "binaryToolStripMenuItem";
            this.binaryToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.binaryToolStripMenuItem.Text = "Двоичный";
            this.binaryToolStripMenuItem.ToolTipText = "Двоичный метод";
            this.binaryToolStripMenuItem.Click += new System.EventHandler(this.ChangeMethodToolStripMenuItem_Click);
            // 
            // gridToolStripMenuItem
            // 
            this.gridToolStripMenuItem.Name = "gridToolStripMenuItem";
            this.gridToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.gridToolStripMenuItem.Text = "Сеток";
            this.gridToolStripMenuItem.Click += new System.EventHandler(this.ChangeMethodToolStripMenuItem_Click);
            // 
            // intersectionToolStripMenuItem
            // 
            this.intersectionToolStripMenuItem.Name = "intersectionToolStripMenuItem";
            this.intersectionToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.intersectionToolStripMenuItem.Text = "Пересечений";
            this.intersectionToolStripMenuItem.Click += new System.EventHandler(this.ChangeMethodToolStripMenuItem_Click);
            // 
            // vadatiToolStripMenuItem
            // 
            this.vadatiToolStripMenuItem.Name = "vadatiToolStripMenuItem";
            this.vadatiToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.vadatiToolStripMenuItem.Text = "Вадати";
            this.vadatiToolStripMenuItem.Click += new System.EventHandler(this.ChangeMethodToolStripMenuItem_Click);
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlineToolStripMenuItem});
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(63, 25);
            this.mapToolStripMenuItem.Text = "Карта";
            // 
            // onlineToolStripMenuItem
            // 
            this.onlineToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.googleToolStripMenuItem,
            this.googleHybridToolStripMenuItem,
            this.googleTerrainToolStripMenuItem,
            this.yandexToolStripMenuItem,
            this.openStreetToolStripMenuItem,
            this.openStreet4UToolStripMenuItem,
            this.bingToolStripMenuItem});
            this.onlineToolStripMenuItem.Name = "onlineToolStripMenuItem";
            this.onlineToolStripMenuItem.Size = new System.Drawing.Size(135, 26);
            this.onlineToolStripMenuItem.Text = "Онлайн";
            // 
            // googleToolStripMenuItem
            // 
            this.googleToolStripMenuItem.Checked = true;
            this.googleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.googleToolStripMenuItem.Name = "googleToolStripMenuItem";
            this.googleToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.googleToolStripMenuItem.Text = "Google";
            this.googleToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // googleHybridToolStripMenuItem
            // 
            this.googleHybridToolStripMenuItem.Name = "googleHybridToolStripMenuItem";
            this.googleHybridToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.googleHybridToolStripMenuItem.Text = "GoogleHybrid";
            this.googleHybridToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // googleTerrainToolStripMenuItem
            // 
            this.googleTerrainToolStripMenuItem.Name = "googleTerrainToolStripMenuItem";
            this.googleTerrainToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.googleTerrainToolStripMenuItem.Text = "GoogleTerrain";
            this.googleTerrainToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // yandexToolStripMenuItem
            // 
            this.yandexToolStripMenuItem.Name = "yandexToolStripMenuItem";
            this.yandexToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.yandexToolStripMenuItem.Text = "Yandex";
            this.yandexToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // openStreetToolStripMenuItem
            // 
            this.openStreetToolStripMenuItem.Name = "openStreetToolStripMenuItem";
            this.openStreetToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.openStreetToolStripMenuItem.Text = "OpenStreet";
            this.openStreetToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // openStreet4UToolStripMenuItem
            // 
            this.openStreet4UToolStripMenuItem.Name = "openStreet4UToolStripMenuItem";
            this.openStreet4UToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.openStreet4UToolStripMenuItem.Text = "OpenStreet4U";
            this.openStreet4UToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // bingToolStripMenuItem
            // 
            this.bingToolStripMenuItem.Name = "bingToolStripMenuItem";
            this.bingToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.bingToolStripMenuItem.Text = "Bing";
            this.bingToolStripMenuItem.Click += new System.EventHandler(this.ChangeGMapProviderToolStripMenuItem_Click);
            // 
            // hodographToolStripMenuItem
            // 
            this.hodographToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tpToolStripMenuItem,
            this.spToolStripMenuItem,
            this.tsToolStripMenuItem});
            this.hodographToolStripMenuItem.Name = "hodographToolStripMenuItem";
            this.hodographToolStripMenuItem.Size = new System.Drawing.Size(91, 25);
            this.hodographToolStripMenuItem.Text = "Годограф";
            // 
            // tpToolStripMenuItem
            // 
            this.tpToolStripMenuItem.Name = "tpToolStripMenuItem";
            this.tpToolStripMenuItem.Size = new System.Drawing.Size(98, 26);
            this.tpToolStripMenuItem.Text = "Tp";
            this.tpToolStripMenuItem.Click += new System.EventHandler(this.ChangeHodographMethodToolStripMenuItem_Click);
            // 
            // spToolStripMenuItem
            // 
            this.spToolStripMenuItem.Name = "spToolStripMenuItem";
            this.spToolStripMenuItem.Size = new System.Drawing.Size(98, 26);
            this.spToolStripMenuItem.Text = "Sp";
            this.spToolStripMenuItem.Click += new System.EventHandler(this.ChangeHodographMethodToolStripMenuItem_Click);
            // 
            // tsToolStripMenuItem
            // 
            this.tsToolStripMenuItem.Name = "tsToolStripMenuItem";
            this.tsToolStripMenuItem.Size = new System.Drawing.Size(98, 26);
            this.tsToolStripMenuItem.Text = "Ts";
            this.tsToolStripMenuItem.Click += new System.EventHandler(this.ChangeHodographMethodToolStripMenuItem_Click);
            // 
            // saveFileDialogImage
            // 
            this.saveFileDialogImage.FileName = "Изображение";
            this.saveFileDialogImage.Filter = "PNG (*.png) | *.png|JPEG (*.jpg) | *.jpg|BMP (*.bmp) | *.bmp|GIF (*.gif) | *.gif";
            // 
            // saveFileDialogDocument
            // 
            this.saveFileDialogDocument.FileName = "Документ";
            this.saveFileDialogDocument.Filter = "Текстовые документы (*.txt) | *.txt;";
            // 
            // labelMethodName
            // 
            this.labelMethodName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMethodName.AutoSize = true;
            this.labelMethodName.Location = new System.Drawing.Point(138, 512);
            this.labelMethodName.Name = "labelMethodName";
            this.labelMethodName.Size = new System.Drawing.Size(0, 20);
            this.labelMethodName.TabIndex = 6;
            // 
            // earthquakeStationViewBindingSource
            // 
            this.earthquakeStationViewBindingSource.DataSource = typeof(Seismology.MainApp.Model.EarthquakeStationView);
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // t0DataGridViewTextBoxColumn1
            // 
            this.t0DataGridViewTextBoxColumn1.DataPropertyName = "T0";
            dataGridViewCellStyle10.Format = "HH:mm:ss.fff";
            this.t0DataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.t0DataGridViewTextBoxColumn1.HeaderText = "Время T0";
            this.t0DataGridViewTextBoxColumn1.Name = "t0DataGridViewTextBoxColumn1";
            this.t0DataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // kDataGridViewTextBoxColumn1
            // 
            this.kDataGridViewTextBoxColumn1.DataPropertyName = "K";
            this.kDataGridViewTextBoxColumn1.HeaderText = "K (Класс)";
            this.kDataGridViewTextBoxColumn1.Name = "kDataGridViewTextBoxColumn1";
            this.kDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // hDataGridViewTextBoxColumn
            // 
            this.hDataGridViewTextBoxColumn.DataPropertyName = "H";
            this.hDataGridViewTextBoxColumn.HeaderText = "H (Глубина)";
            this.hDataGridViewTextBoxColumn.Name = "hDataGridViewTextBoxColumn";
            this.hDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fDataGridViewTextBoxColumn
            // 
            this.fDataGridViewTextBoxColumn.DataPropertyName = "F";
            this.fDataGridViewTextBoxColumn.HeaderText = "F (Широта)";
            this.fDataGridViewTextBoxColumn.Name = "fDataGridViewTextBoxColumn";
            this.fDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // LatError
            // 
            this.LatError.DataPropertyName = "LatError";
            this.LatError.HeaderText = "F (Погрешность)";
            this.LatError.Name = "LatError";
            this.LatError.ReadOnly = true;
            // 
            // lDataGridViewTextBoxColumn
            // 
            this.lDataGridViewTextBoxColumn.DataPropertyName = "L";
            this.lDataGridViewTextBoxColumn.HeaderText = "L (Долгота)";
            this.lDataGridViewTextBoxColumn.Name = "lDataGridViewTextBoxColumn";
            this.lDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // LngError
            // 
            this.LngError.DataPropertyName = "LngError";
            this.LngError.HeaderText = "L (Погрешность)";
            this.LngError.Name = "LngError";
            this.LngError.ReadOnly = true;
            // 
            // dtDataGridViewTextBoxColumn1
            // 
            this.dtDataGridViewTextBoxColumn1.DataPropertyName = "Dt";
            this.dtDataGridViewTextBoxColumn1.HeaderText = "Dt (Невязка)";
            this.dtDataGridViewTextBoxColumn1.Name = "dtDataGridViewTextBoxColumn1";
            this.dtDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // DtRms
            // 
            this.DtRms.DataPropertyName = "DtRms";
            this.DtRms.HeaderText = "DtRms (Ср. кв.)";
            this.DtRms.Name = "DtRms";
            this.DtRms.ReadOnly = true;
            // 
            // vpVsDataGridViewTextBoxColumn1
            // 
            this.vpVsDataGridViewTextBoxColumn1.DataPropertyName = "VpVs";
            this.vpVsDataGridViewTextBoxColumn1.HeaderText = "Vp/Vs";
            this.vpVsDataGridViewTextBoxColumn1.Name = "vpVsDataGridViewTextBoxColumn1";
            this.vpVsDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // CalculateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 549);
            this.Controls.Add(this.labelMethodName);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAction);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "CalculateForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Работа с ";
            this.tabControlMain.ResumeLayout(false);
            this.tabPageWsgData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWsgData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wsgPhaseBindingSource)).EndInit();
            this.tabPageDbData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDbData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationBindingSource)).EndInit();
            this.tabPageResult.ResumeLayout(false);
            this.tabControlResult.ResumeLayout(false);
            this.tabPageTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEarthquake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditStation)).EndInit();
            this.tabPageBulletin.ResumeLayout(false);
            this.tabPageBulletin.PerformLayout();
            this.tabPageMapAndSelection.ResumeLayout(false);
            this.tabControlStations.ResumeLayout(false);
            this.tabPageStationsMain.ResumeLayout(false);
            this.tabPageStationsVadati.ResumeLayout(false);
            this.tabControlMap.ResumeLayout(false);
            this.tabPageOnline.ResumeLayout(false);
            this.tabPageStatic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStaticMap)).EndInit();
            this.tabPageWadatiChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartWadati)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.earthquakeStationViewBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.TabControl tabControlMain;
        protected System.Windows.Forms.TabPage tabPageWsgData;
        private System.Windows.Forms.TabPage tabPageResult;
        protected System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Button buttonCancel;
        protected System.Windows.Forms.DataGridView dataGridViewWsgData;
        private System.Windows.Forms.TabPage tabPageMapAndSelection;
        protected System.Windows.Forms.OpenFileDialog openFileDialogWsgFile;
        protected System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.TabControl tabControlResult;
        private System.Windows.Forms.TabPage tabPageBulletin;
        protected System.Windows.Forms.TextBox textBoxEarthquake;
        private System.Windows.Forms.BindingSource wsgPhaseBindingSource;
        private System.Windows.Forms.BindingSource earthquakeStationViewBindingSource;
        private System.Windows.Forms.BindingSource earthquakeViewBindingSource;
        private System.Windows.Forms.TabControl tabControlMap;
        private System.Windows.Forms.TabPage tabPageOnline;
        private System.Windows.Forms.TabPage tabPageStatic;
        private System.Windows.Forms.PictureBox pictureBoxStaticMap;
        protected System.Windows.Forms.ListView listViewStationsMain;
        private GMap.NET.WindowsForms.GMapControl gMapOnlineControl;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn distDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn evAzDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phazeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tResDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn azimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn azResDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn slowDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sResDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn defDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sNRDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ampDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qualDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn magnitudeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn arrIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem methodToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem binaryToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem multiToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem openWsgToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem staticMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bulletinToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialogImage;
        private System.Windows.Forms.SaveFileDialog saveFileDialogDocument;
        private System.Windows.Forms.TabPage tabPageTable;
        protected System.Windows.Forms.DataGridView dataGridViewEditStation;
        protected System.Windows.Forms.DataGridView dataGridViewEarthquake;
        protected System.Windows.Forms.ToolStripMenuItem gridToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem automaticToolStripMenuItem;
        protected System.Windows.Forms.Label labelMethodName;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yandexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openStreetToolStripMenuItem;
        protected System.Windows.Forms.TabPage tabPageDbData;
        private System.Windows.Forms.BindingSource earthquakeStationBindingSource;
        protected System.Windows.Forms.DataGridView dataGridViewDbData;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn epDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tpDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn esDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn spDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn epicDistanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodZDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amplitudeZDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amplitudeDataGridViewTextBoxColumn;
        protected System.Windows.Forms.ToolStripMenuItem hodographToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem tpToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem spToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem tsToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Station;
        private System.Windows.Forms.DataGridViewTextBoxColumn T0;
        private System.Windows.Forms.DataGridViewTextBoxColumn K;
        private System.Windows.Forms.DataGridViewTextBoxColumn TP;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP;
        private System.Windows.Forms.DataGridViewTextBoxColumn EpicDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dt;
        private System.Windows.Forms.DataGridViewTextBoxColumn VpVs;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmplitudeZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Period;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amplitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ep;
        private System.Windows.Forms.DataGridViewTextBoxColumn Es;
        protected System.Windows.Forms.ToolStripMenuItem intersectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openStreet4UToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleHybridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleTerrainToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControlStations;
        private System.Windows.Forms.TabPage tabPageStationsMain;
        private System.Windows.Forms.TabPage tabPageStationsVadati;
        protected System.Windows.Forms.ListView listViewStationsVadati;
        protected System.Windows.Forms.ToolStripMenuItem vadatiToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPageWadatiChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartWadati;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn t0DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LatError;
        private System.Windows.Forms.DataGridViewTextBoxColumn lDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LngError;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DtRms;
        private System.Windows.Forms.DataGridViewTextBoxColumn vpVsDataGridViewTextBoxColumn1;
    }
}