﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Seismology.Updater
{
    public class AppVersion
    {
        public string Version { get; set; }
        public DateTime Date { get; set; }

        /// <summary>
        /// Возвращает версию программы. Если ее нет, то вернет null
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string LoadVersion(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (var fs = new FileStream(filePath, FileMode.Open))
                {
                    var xml = new XmlSerializer(typeof(AppVersion));
                    return ((AppVersion) xml.Deserialize(fs)).Version;
                }
            }

            return null;
        }
    }
}
