﻿using System.IO;
using System.Linq;
using System.Reflection;

namespace Seismology.Updater
{
    public sealed class Updater
    {
        private static readonly string CurrentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private int[] _curVersion;
        private int[] _updVersion;

        public static string UpdaterAppName => "Seismology.Updater";
        public static string MainAppName => "Seismology.MainApp";
        public static string MainAppPath => $"{CurrentPath}\\{MainAppName}.exe";
        
        public string CurrentDirPath => CurrentPath;
        public string UpdateDirPath => @"\\fdev\BaseN\\Seismolog";
        
        public Updater()
        {
            string curVersion = AppVersion.LoadVersion($"{CurrentPath}\\version.xml")?.Trim();
            string updVersion = AppVersion.LoadVersion($"{UpdateDirPath}\\version.xml")?.Trim();

            if (!string.IsNullOrWhiteSpace(curVersion) &&
                !string.IsNullOrWhiteSpace(updVersion))
            {
                _curVersion = curVersion.Split('.').Select(ParseInt).ToArray();
                _updVersion = updVersion.Split('.').Select(ParseInt).ToArray();
            }
        }

        public bool IsNeedToUpdate()
        {
            bool isNeedToUpdate = false;

            if (_curVersion != null && _updVersion != null)
            {
                for (int i = 0; i < _curVersion.Length; i++)
                {
                    if (_curVersion[i] < _updVersion[i])
                    {
                        isNeedToUpdate = true;
                        break;
                    }
                }
            }

            return isNeedToUpdate;
        }

        private int ParseInt(string line)
        {
            return int.TryParse(line, out var result) ? result : 0;
        }
    }
}