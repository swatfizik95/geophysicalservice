﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Seismology.Updater
{
    public partial class FormMain : Form
    {
        private int _count;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            var updater = new Updater();

            if (updater.IsNeedToUpdate())
            {
                int tryCount = 0;
                while (Process.GetProcessesByName(Updater.MainAppName).Length > 0)
                {
                    Thread.Sleep(250);
                    tryCount++;
                    if (tryCount == 15)
                    {
                        string message =
                            $"Программа обновления обнаружила запущенный эксземпляр Сейсмолога{Environment.NewLine}" +
                            "Пожалуйста, завершите работу, так как программа автоматически закроект его!";
                        MessageBox.Show(message, "Внимание!");
                        foreach (var process in Process.GetProcessesByName(Updater.MainAppName)) process?.Kill();
                        break;
                    }
                }

                Count(updater.UpdateDirPath, updater.CurrentDirPath);
                progressBarLoad.Maximum = _count;
                Copy(updater.UpdateDirPath, updater.CurrentDirPath);
            }

            string mainAppPath = Updater.MainAppPath;
            
            if (File.Exists(mainAppPath))
            {
                Process.Start(mainAppPath);
            }

            Close();
        }

        private void Copy(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);

            foreach (var file in GetFiles(sourceDir))
            {
                if (Path.GetFileName(file) != "settings.xml")
                {
                    File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)), true);
                    Thread.Sleep(50);
                    progressBarLoad.Value++;
                }
            }

            foreach (var directory in Directory.GetDirectories(sourceDir))
                Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
        }
        
        private void Count(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);

            foreach (var file in GetFiles(sourceDir))
            {
                if (Path.GetFileName(file) != "setting.xml")
                {
                    _count++;
                }
            }

            foreach (var directory in Directory.GetDirectories(sourceDir))
                Count(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
        }

        private string[] GetFiles(string sourceDir) => Directory.GetFiles(sourceDir)
            .Where(f => !Path.GetFileName(f).StartsWith(Updater.UpdaterAppName)).ToArray();
    }
}
